-- Revert sqitch/sqitch.plan:user_game_settings_on_last_creation from pg

BEGIN;

alter table users drop column game_settings_on_last_creation;

COMMIT;
