(ns thm.game-ui.stub-ui-module
  (:require [thm.game-ui.game-ui-module :as game-ui-module]
            [taoensso.timbre :refer-macros (debug warn)]
            [thm.utils :refer [stub]]))

(defrecord StubUiModule [code]
  game-ui-module/GameUiModule

  (get-game-code [_]
    code)

  (your-request-code-for-humans [_ text-descr primary-role]
    (stub "your-request-code-for-humans" text-descr primary-role))

  (mode-for-humans [_ mode text-descr]
    (stub "mode-for-humans" mode text-descr))

  (answer-for-humans [_ answer req]
    (stub "answer-for-humans" answer req))

  (other-request-code-for-humans [_ player-name text-descr]
    (stub "other-request-code-for-humans" player-name text-descr))

  (game-container [_ primary-role]
    (stub "game-container" primary-role))

  (log-item-for-humans [_ log-item log-helper]
    (stub "log-item-for-humans" log-item))

  (small-role-identifier [_ role]
    (stub "small-role-identifier" role))

  (player-plaque-info-line [_ primary-role role]
    (stub "player-plaque-info-line" primary-role role))

  (sidebar-global-info [_ primary-role]
    (stub "sidebar-global-info" primary-role))

  (compact-player-plaque [_ primary-role role player-name usernames]
    (stub "compact-player-plaque" primary-role role player-name usernames))

  (compact-game-container [_ primary-role]
    (stub "compact-game-container" primary-role))

  (your-hand [_ primary-role]
    (stub "your-hand" primary-role))

  (has-left-hand-of-choose-mode-modal? [_ req]
    false)

  (left-hand-of-choose-mode-modal [_ req]
    (stub "left-hand-of-choose-mode-modal" req))

  (extended-player-plaque-info [_ modal-data]
    (stub "extended-player-plaque-info" modal-data))

  (modal-content [_ modal-data primary-role]
    (stub "modal-content" modal-data primary-role))

  (get-text [_ text-descr]
    (stub "text" text-descr))

  (get-pane-associated-with-request [_ your-request your-role]
    nil)

  (get-pane-associated-with-delta [_ delta your-role]
    nil)

  (get-animation-kind-for-zone-name [_ zone-name your-role]
    nil)

  (get-delay-of-var [_ var-name]
    nil)

  (get-delay-of-vars [_ var-names]
    nil)

  (state-timing-for-humans [_ state]
    (stub "state-timing-for-humans"))

  )
