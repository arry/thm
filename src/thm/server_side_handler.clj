(ns thm.server-side-handler
  (:require
   [clojure.java.io :as io]
   [compojure.core :refer [defroutes GET POST]]
   [compojure.route :as route]
   [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
   [ring.middleware.anti-forgery]
   [ring.util.response :refer [response status content-type resource-response not-found
                               file-response redirect]]
   [taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug)]
   [taoensso.encore  :as enc]
   [thm.backend-utils.cookies :refer (read-session-cookie)]
   [thm.config :refer [config cookie-name cookie-key-prefix]]
   [thm.backend-utils.users :refer (validate-user)]
   [thm.utils :refer [my-pid now-timestamp pprint-str]]
   [taoensso.carmine :as car]
   [system.repl :refer [system]]
   ))

(defmacro wcar* [system & body]
  `(car/wcar (:conn-opts (:carmine ~system)) ~@body))

(defn publish [message]
  (let [sub-prefix (-> system :worker :sub-prefix)]
    (wcar* system (car/publish (str sub-prefix ":all->worker") message))))

(defn username-from-request [req]
  (let [cookie-key (-> req :cookies (get cookie-name) :value)
        session-cookie (read-session-cookie cookie-key)]
    (:username session-cookie)))

(defn handle-upload-avatar [req]
  (let [username (username-from-request req)
        avatars-directory (:avatars-directory config)]
    (if (some? username)
      (let [tempfile (-> req :params :avatar-file :tempfile)]
        (if (= (type tempfile) java.io.File)
          (try
            (let [out-file (java.io.File. avatars-directory (str username ".png"))
                  result (-> (ProcessBuilder. ["convert"
                                               "-geometry" "64x64!"
                                               (.getPath tempfile)
                                               (.getPath out-file)])
                             .start
                             .waitFor)]
              (if (zero? result)
                (do
                  (publish [:avatar-image-uploaded username])
                  :ok)
                :error-processing-file))
            (catch Exception e
              (errorf "Error processing avatar of '%s': %s" username e)
              :error-processing-file))
          :no-file))
      :invalid-username)))

(defn download-avatar-file [uri file]
  (with-open [in (io/input-stream uri)
              out (io/output-stream file)]
    (io/copy in out)))


(defn upload-proxy [req]
  (let [tempfile (-> req :params :image_name :tempfile)
        short-name (-> req :params :short-name)]
    (debug "trying to copy blob" tempfile short-name)
    (io/copy tempfile (io/file (str "/tmp/proxies/" short-name ".png")))))

(defroutes routes
  (GET  "/chsk"  req ((:ring-ajax-get-or-ws-handshake (:sente system)) req))
  (POST "/chsk"  req ((:ring-ajax-post (:sente system)) req))
  (GET "/avatar/*" req (let [file-name (-> req :params :*)
                             avatars-directory (:avatars-directory config)
                             file (io/file avatars-directory file-name)]
                         (if (.exists file)
                           (file-response (.getPath file))
                           (let [uri (str "https://robohash.org/" file-name "?size=64x64&bgset=bg2")]
                             (future (download-avatar-file uri file))
                             (redirect uri)))))
  (POST "/upload-avatar" req (let [upload-tag (handle-upload-avatar req)]
                               (if (= upload-tag :ok)
                                 (-> (response "ok")
                                     (content-type "text/plain"))
                                 (-> (response (str upload-tag))
                                     (status 401)
                                     (content-type "text/plain")))))
  (POST "/upload-proxy" req (do (upload-proxy req)
                                (-> (response "ok")
                                    (content-type "text/plain"))))
  (GET "/*" [] (-> (resource-response "/index.html")
                   (content-type "text/html")))
  (route/not-found "Not Found"))

(def middleware (-> site-defaults
                    (assoc-in [:static :resources] "/")
                    (assoc-in [:security :anti-forgery]
                              {:read-token (fn [req]
                                             (-> req :params :csrf-token))})
                    (assoc-in [:session :cookie-attrs :max-age] (:secs config))
                    (assoc-in [:session :store] (:store config))
                    ))

(def app
  (wrap-defaults #'routes middleware))

(defn sente-send [{:as ev-msg :keys [uid send-fn]} data]
  (send-fn uid data))

(defn get-cookie-key-of-ev-msg [ev-msg]
  (-> (:ring-req ev-msg) :cookies (get cookie-name) :value))

(defn get-or-create-cookie-key-of-ev-msg [ev-msg]
  (let [existing (get-cookie-key-of-ev-msg ev-msg)]
    (if (or (nil? existing)
            (= existing "null"))
      (str cookie-key-prefix ":" (enc/uuid-str))
      existing)))


(defmulti event-msg-handler :id) ; Dispatch on event-id
;; Wrap for logging, catching, etc.:
(defn     event-msg-handler* [{:as ev-msg :keys [id ?data event]}]
  ;; (debugf "Event: %s" event)
  (event-msg-handler ev-msg))

(defmethod event-msg-handler :default ; Fallback
  [{:as ev-msg :keys [event id ?data ring-req ?reply-fn send-fn]}]
  (let [session (:session ring-req)
        uid     (:uid     session)]
    (debugf "Unhandled event: %s" event)
    (when ?reply-fn
      (?reply-fn {:umatched-event-as-echoed-from-from-server event}))))

(defmethod event-msg-handler :chsk/uidport-open
  [{:as ev-msg :keys [uid]}]
  (let [cookie-key (get-or-create-cookie-key-of-ev-msg ev-msg)
        _ (debugf "uid %s has cookie-key %s" uid (pprint-str cookie-key))
        session-cookie (read-session-cookie cookie-key)
        username (:username session-cookie)
        anti-forgery-token (:ring.middleware.anti-forgery/anti-forgery-token session-cookie)]
    (publish [:client-connected {:pid (my-pid)
                                 :uid uid
                                 :username username
                                 :cookie-key cookie-key
                                 :anti-forgery-token anti-forgery-token}])))

(defmethod event-msg-handler :chsk/uidport-close
  [{:keys [uid]}]
  (publish [:client-disconnected {:uid uid}]))

(defmethod event-msg-handler :chsk/ws-ping
  [ev-msg]
  ;; Ignore
  )

(defmethod event-msg-handler :thmc.game-ui/my-answer
  [{:as ev-msg :keys [uid ?data]}]
  (debug "client reports my-answer " uid ?data)
  (publish [:client-sent-answer {:uid uid, :data ?data}]))

(defmethod event-msg-handler :thmc.test-ui/switch-scenario
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:test-switch-scenario {:uid uid, :index ?data}]))

(defmethod event-msg-handler :thmc.test-ui/new-game
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:thm.test-ui/new-game {:uid uid}]))

(defmethod event-msg-handler :thmc.game-ui/abandon-game
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:thm.game-ui/abandon-game {:uid uid}]))

(defmethod event-msg-handler :thmc.game-ui/ui-layout-chosen
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:thmc.game-ui/ui-layout-chosen {:uid uid, :data ?data}]))

(defmethod event-msg-handler :thmc.play-game/save-personal-notes
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:thmc.play-game/save-personal-notes {:uid uid, :data ?data}]))

(defmethod event-msg-handler :thmc.login/submit-login-form
  [{:as ev-msg :keys [uid ?data]}]
  (let [{:keys [username-or-email password]} ?data
        username (validate-user (:ds system) username-or-email password)]
    (debugf "Attempting to validate user %s" username-or-email)
    (if (some? username)
      (do
        (publish [:client-logged-in {:uid uid, :username username}])
        (sente-send ev-msg [:thms.login/login-successful {:username username}]))
      (sente-send ev-msg [:thms.login/login-failure nil]))))

(defmethod event-msg-handler :thmc/navigate
  [{:as ev-msg :keys [uid ?data]}]
  (let [page-desc ?data]
    (publish [:navigate {:uid uid, :page-desc page-desc}])))

(defmethod event-msg-handler :thmc.account/submit-logout-form
  [{:as ev-msg :keys [uid]}]
  (publish [:client-logged-out {:uid uid}])
  (sente-send ev-msg [:thms.account/logout-successful {}]))

(defmethod event-msg-handler :thmc.account/submit-account-settings-form
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:thmc.account/submit-account-settings-form {:uid uid, :form-data ?data}]))

(defmethod event-msg-handler :thmc/chat-line
  [{:as ev-msg :keys [?data uid]}]
  (publish [:chat-line {:uid uid, :text ?data}]))

(defmethod event-msg-handler :thmc.lobby/submit-new-game-form
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:submit-new-game-form {:uid uid, :form-data ?data}]))

(defmethod event-msg-handler :thmc.view-game/game-control
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:submit-game-control {:uid uid, :control ?data}]))

(defmethod event-msg-handler :thmc.signup/submit-signup-form
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:thm.signup/submit-signup-form {:uid uid, :form-data ?data}]))

(defmethod event-msg-handler :thmc.signup/submit-confirm-registration-form
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:thm.signup/submit-confirm-registration-form {:uid uid, :form-data ?data}]))

(defmethod event-msg-handler :thmc.reset-password/submit-forgot-form
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:thm.reset-password/submit-forgot-form {:uid uid, :form-data ?data}]))

(defmethod event-msg-handler :thmc.reset-password/submit-reset-form
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:thm.reset-password/submit-reset-form {:uid uid, :form-data ?data}]))

(defmethod event-msg-handler :thmc.term/new-tm-game
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:thmc.term/new-tm-game {:uid uid, :data ?data}]))

(defmethod event-msg-handler :thmc.term/get-engine-str-rep
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:thmc.term/get-engine-str-rep {:uid uid, :data ?data}]))

(defmethod event-msg-handler :thmc.term/put-answer
  [{:as ev-msg :keys [uid ?data]}]
  (publish [:thmc.term/put-answer {:uid uid, :data ?data}]))
