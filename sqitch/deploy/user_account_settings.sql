-- Deploy sqitch/sqitch.plan:user_account_settings to pg
-- requires: users

BEGIN;

alter table users add column account_settings jsonb null;

COMMIT;
