(ns thm.td.game-ui.td-ui
  (:require [reagent.core :as r]
            [re-frame.core :refer [dispatch subscribe]]
            [taoensso.timbre :refer-macros [debug warn]]
            [clojure.string :as str]


            [thm.views.common :refer [modal-content-wrapper
                                      avatar-img
                                      action-link
                                      outcome-for-humans
                                      connection-indicator
                                      dispatch-resize-when-mounted
                                      flip-move
                                      ]]
            [thm.views.game-ui-common :refer [zone-of-cards
                                              with-card-preview
                                              card-title-link
                                              zone-link
                                              ]]
            [thm.utils :refer [forbid-widget-activation?
                               state-version-is-at-least?
                               get-your-request
                               half-round-up
                               dispatch-e
                               svg-arrow
                               denumber-id
                               multipart-name-from-descr
                               map-cross
                               bull
                               seq-contains?
                               n-cards-in-zone
                               sequence-for-humans
                               pluralize
                               parse-text-descr
                               parenthetical-list
                               sort-explode-map
                               change-for-humans
                               ]]
            [thm.td.td-data :as td-data]
            [thm.td.td-texts :refer [zone-name-for-humans
                                     us-symbol su-symbol symbol-of-role
                                     role-for-humans
                                     defcon-track-for-humans
                                     battleground-for-humans
                                     card-alignment-for-humans
                                     card-play-mode-for-humans
                                     card-title-by-id
                                     short-card-title
                                     big-card-img-src
                                     ]]
            [thm.td.td-utils :as td-utils
             :refer [defcon-level-and-area
                     get-your-agenda
                     get-track-board-widget-value
                     defcon-level
                     other-role
                     good-for-war?
                     n-cards-in-hand
                     good-answer-for-choose-place-influence?
                     good-answer-for-choose-change-defcon?
                     ]]
            ))

(def GAME-CODE "td")

(defn observer? [primary-role]
  (= primary-role "*observer*"))

(defn percent-measure [value scale-key]
  (str (* 100.0 (/ value (scale-key (:map td-data/SIZES)))) "%"))

(defn bg-tooltip-text [us-value su-value bg]
  (str (battleground-for-humans bg) " - "
       us-value us-symbol "/"
       su-value su-symbol))

(def war-symbol "☢")

(defn simple-zone-modal-content [primary-role zone-name]
  (modal-content-wrapper (zone-name-for-humans zone-name)
                         [zone-of-cards {:code GAME-CODE
                                         :class "horizontal-zone"}
                          primary-role
                          zone-name]))

(defn strategy-discard-modal-content [primary-role]
  (simple-zone-modal-content primary-role "strategy-discard"))

(defn agenda-discard-modal-content [primary-role]
  (simple-zone-modal-content primary-role "agenda-discard"))

(defn aftermath-reveal-modal-content [primary-role]
  (simple-zone-modal-content primary-role "aftermath-reveal"))

(defn your-aftermath-modal-content [primary-role]
  (let [data @(subscribe [:thm.game-ui.td/your-aftermath-modal-data primary-role])]
    (modal-content-wrapper (zone-name-for-humans "aftermath")
                           (if (observer? primary-role)
                             [:div
                              [:div (role-for-humans "s") " saved " (pluralize (:s-n data) "card") "."]
                              [:div (role-for-humans "u") " saved " (pluralize (:u-n data) "card") "."]]
                             [:div
                              [:div "You have saved " (pluralize (:your-n data) "card")
                               (if (zero? (:your-n data)) "." ":")]

                              [zone-of-cards {:code GAME-CODE :class "horizontal-zone"}
                               primary-role
                               [primary-role "aftermath"]]

                              [:div "Your opponent has saved " (pluralize (:other-n data) "card") "."]]))))

(defn modal-content [modal-data primary-role]
  (case (:kind modal-data)
    :strategy-discard [strategy-discard-modal-content primary-role]
    :agenda-discard   [agenda-discard-modal-content   primary-role]
    :aftermath-reveal [aftermath-reveal-modal-content primary-role]
    :your-aftermath   [your-aftermath-modal-content   primary-role]
    (do (warn "Unknown kind of modal data:" modal-data)
        [:div])))

(defn map-element-style [start finish]
  (let [x1 (:x start)  y1 (:y start)
        x2 (:x finish) y2 (:y finish)
        x (min x1 x2) y (min y1 y2)
        w (Math/abs (- x2 x1)) h (Math/abs (- y2 y1))]
    {:left (percent-measure x :w) :top (percent-measure y :h)
     :width (percent-measure w :w) :height (percent-measure h :h)}))

(defn low-level-map-marker [image-name more-classes marker-name marker-size title displacement]
  (let [base-coords (td-data/marker-coords marker-name marker-size)
        coords {:x (+ (:x base-coords) (* (:w marker-size) (and displacement (:x displacement))))
                :y (+ (:y base-coords) (* (:h marker-size) (and displacement (:y displacement))))}]
    [:img {:src (str "/images/markers/" image-name)
           :title title
           :class ["map-marker"
                   more-classes]
           :style {:width (percent-measure (:w marker-size) :w)
                   :height (percent-measure (:h marker-size) :h)
                   :left (percent-measure (:x coords) :w)
                   :top (percent-measure (:y coords) :h)}}]))

(defn map-marker [primary-role type & args]
  (case type
    :prestige
    (let [val @(subscribe [:thm.game-ui/track-var primary-role "prestige"])
          marker-name (str "pr" val)
          marker-size (get td-data/SIZES :disc)
          title (str "Prestige: " val) ;; XXX for-humans, localizable
          displacement nil]
      [low-level-map-marker "prestige-marker.png" "prestige" marker-name marker-size title displacement])

    :round
    (let [val @(subscribe [:thm.game-ui/track-var primary-role "round"])
          marker-name (str "r" val)
          marker-size (get td-data/SIZES :disc)
          title (str "Round: " val) ;; XXX for-humans, localizable
          displacement nil]
      [low-level-map-marker "round-marker.png" "round" marker-name marker-size title displacement])

    :defcon
    (let [[r t] args
          val @(subscribe [:thm.game-ui/track-var primary-role ["d" r t]])
          marker-name (str t val)
          image-name (str r "-defcon.png")
          title (str/join " " [(role-for-humans r)
                               (td-data/defcon-tracks t)
                               "DEFCON:"
                               (defcon-level-and-area val)])
          displacement {:x 0 :y (if (= r "u") -0.2 0.2)}
          marker-size (get td-data/SIZES :disc)]
      [low-level-map-marker image-name "defcon-disc" marker-name marker-size title displacement])

    :flag
    (let [[r i] args
          val @(subscribe [:thm.game-ui/track-var primary-role ["f" r i]])
          your-agenda @(subscribe [:thm.game-ui.td/your-agenda primary-role])

          image-name (str r "-flag.png")
          marker-name val

          additional-class (cond (not= r primary-role) "potential"
                                 (nil? your-agenda) "potential"
                                 (= your-agenda val) "actual"
                                 :else "decoy")

          ;; XXX mixing class and for humans; and should be localizable
          title (str (role-for-humans r) "'s " additional-class " agenda")

          marker-size (get td-data/SIZES :flag)
          displacement (if (contains? td-data/defcon-tracks val)
                         {:x (* 0.2 (dec i))
                          :y (if (= r "u") -0.7 0.7)}
                         {:x (if (= r "u") -1.5 1.5)
                          :y -1.3})]
      (when (some? val)
        [low-level-map-marker image-name (str "flag " additional-class) marker-name marker-size title displacement]))

    [:div type]))

(defn board-widgets [primary-role]
  [:div.board-widgets
   (doall (for [{:keys [start finish name bg? track] :as widget} td-data/visible-markers
                :let [active? @(subscribe [:thm.game-ui.td/board-widget-active? primary-role widget])

                      title (when bg? (bg-tooltip-text
                                       @(subscribe [:thm.game-ui/track-var primary-role ["i" "u" name]])
                                       @(subscribe [:thm.game-ui/track-var primary-role ["i" "s" name]])
                                       name))]]
            [:div {:class ["board-widget"
                           name
                           (when track "defcon")
                           (when active? "is-active")]
                   :on-click (when (or active? (= name "af"))
                               #(dispatch-e [:thm.game-ui.td/board-widget-clicked primary-role widget] %))
                   :key name
                   :title title
                   :style (map-element-style start finish)}
             (when bg?
               (doall (for [role ["u" "s"]]
                        [flip-move {:stagger-delay-by 50
                                    :class (str "cubes " role) :key role}
                         (let [now    @(subscribe [:thm.game-ui/track-var    primary-role ["i" role name]])
                               recent @(subscribe [:thm.game-ui/track-recent primary-role ["i" role name]])
                               max (if (neg? recent) (- now recent) now)]
                           (for [i (range max)]
                             (let [[class suffix] (cond
                                                    (>= i now) ["minus" "-minus"]
                                                    (and (pos? recent) (>= i (- now recent))) ["plus" "-plus"]
                                                    :else [nil ""])
                                   src (str "/images/markers/" role "-cube" suffix ".png")]
                               [:img {:key i
                                      :class class
                                      :src src}])))])))
             [:div.cover]
             (let [checks @(subscribe [:thm.game-ui.td/board-widget-checks primary-role widget])]
               (when (not (zero? checks))
                 (let [[n style] (if (pos? checks)
                                   [checks "\u2713"]
                                   [(- checks) "\u2717"])]
                   [:div.checks
                    (str/join "" (repeat n style))])))]))

   [map-marker primary-role :prestige]
   [map-marker primary-role :round]

   (doall (map-cross (fn [r t]
                       ^{:key (str "d" r t)}
                       [map-marker primary-role :defcon r t])
                     td-data/both-roles (keys td-data/defcon-tracks)))

   (doall (map-cross (fn [r i]
                       ^{:key (str "f" r i)}
                       [map-marker primary-role :flag r i])
                     td-data/both-roles (range 3)))])

(defn svg-prestige-change-arrow [primary-role]
  (let [change @(subscribe [:thm.game-ui/track-recent primary-role "prestige"])
        now @(subscribe [:thm.game-ui/track-var primary-role "prestige"])]
    (when (and (some? change)
               (not (zero? change)))
      "prestige" (let [orientation (if (pos? change) :right :left)
                       m1 (td-data/marker-coords (str "pr" (- now change)))
                       m2 (td-data/marker-coords (str "pr" now))]
                   [svg-arrow m1 m2 "#770" {:orientation orientation}]))))

(defn svg-defcon-change-arrow [primary-role r t]
  (let [change @(subscribe [:thm.game-ui/track-recent primary-role ["d" r t]])
        now @(subscribe [:thm.game-ui/track-var primary-role ["d" r t]])]
    (when (and (some? change)
               (not (zero? change)))
      (let [orientation (if (pos? change) :up :down)
            [ox oy color] (if (= r "u")
                            [15 0 "#007"]
                            [-15 -20 "#700"])
            rect {:w (* 2 ox), :h (* 2 oy)}
            m1 (td-data/marker-coords (str t (- now change)) rect)
            m2 (td-data/marker-coords (str t now) rect)]
        [svg-arrow m1 m2 color {:orientation orientation}]))))

(defn svg-container [primary-role]
  [:div.svg-container
   [:svg {:width (:w (:map td-data/SIZES))
          :height (:h (:map td-data/SIZES))}
    [svg-prestige-change-arrow primary-role]
    (doall (map-cross (fn [r t]
                        ^{:key (str "d" r t)}
                        [svg-defcon-change-arrow primary-role r t])
                      td-data/both-roles (keys td-data/defcon-tracks)))]])

(defn map-div [primary-role]
  [:div.map
   [:img.board-img {:src "/images/board.jpg"}]
   [board-widgets primary-role]
   [svg-container primary-role]])

(defn effects [primary-role role additional-class]
  (let [effects @(subscribe [:thm.game-ui/track-var primary-role ["ef" role]])
        ui-layout @(subscribe [:thm.game-ui/layout])]

        [:div {:class ["effects"
                       role
                       additional-class
                       (when (empty? effects) "is-hidden")]}
         [:ul
          [flip-move {:duration 200}
           (doall (for [effect effects]
                    [:li (if (= ui-layout "compact")
                           {:key effect
                            :on-click #(dispatch-e [:thm.game-ui/show-modal {:kind :look-at-card
                                                                             :cid effect}] %)}
                           (with-card-preview effect {:key effect}))
                     (card-title-by-id effect)]))]]]))

(defn your-hand [primary-role]
  [zone-of-cards {:code GAME-CODE
                  :class ["horizontal-zone"
                          (when (= @(subscribe [:thm.game-ui/layout]) "classic") "your-hand-container")]}
   primary-role
   [primary-role "hand"]])

(defn map-container [primary-role]
  [:div.map-container
   [map-div primary-role]])

(defn holder-zones [primary-role]
  (concat
   (if (observer? primary-role)
     (list
      ^{:key "s-hand"}
      [zone-of-cards {:code GAME-CODE
                      :class "s-hand non-holding-place"
                      :mode :i-non-holding-place}
       primary-role
       "s:hand"]

      ^{:key "u-hand"}
      [zone-of-cards {:code GAME-CODE
                      :class "u-hand non-holding-place"
                      :mode :i-non-holding-place}
       primary-role
       "u:hand"])

     (list
      ^{:key "other-hand"}
      [zone-of-cards {:code GAME-CODE
                      :class "other-hand non-holding-place"
                      :mode :i-non-holding-place}
       primary-role
       [(other-role primary-role) "hand"]]))

   (list
    ^{:key "s-agenda"}
    [zone-of-cards {:code GAME-CODE
                    :class "s agenda-zone"
                    :mode (if (= primary-role "s")
                            :v-row
                            :i-facedown-row)}
     primary-role
     "s:agenda"]

    ^{:key "u-agenda"}
    [zone-of-cards {:code GAME-CODE
                    :class "u agenda-zone"
                    :mode (if (= primary-role "u")
                            :v-row
                            :i-facedown-row)}
     primary-role
     "u:agenda"]

    ^{:key "s-aftermath"}
    [zone-of-cards {:code GAME-CODE
                    :mode :v-non-holding-place
                    :class "non-holding-place s aftermath-zone"}
     primary-role
     "s:aftermath"]

    ^{:key "u-aftermath"}
    [zone-of-cards {:code GAME-CODE
                    :mode :i-non-holding-place
                    :class "non-holding-place u aftermath-zone"}
     primary-role
     "u:aftermath"]

    ^{:key "agenda-deck"}
    [zone-of-cards {:code GAME-CODE :mode :i-non-holding-place :class "non-holding-place"}
     primary-role
     "agenda-deck"]

    ^{:key "strategy-deck"}
    [zone-of-cards {:code GAME-CODE :mode :i-non-holding-place :class "non-holding-place"}
     primary-role
     "strategy-deck"]

    ^{:key "agenda-discard"}
    [zone-of-cards {:code GAME-CODE :mode :v-non-holding-place :class "non-holding-place"}
     primary-role
     "agenda-discard"])))

(defn game-container [primary-role]
  (let [opposing-role (other-role primary-role)]
    [:div.game-container
     [map-container primary-role]

     (holder-zones primary-role)

     (when-not (observer? primary-role)
       [your-hand primary-role])

     (if (observer? primary-role)
       [:div.auxiliary-container
        ;; XXX the class names are still {your|other}-{effects|letter}, even
        ;; though you're an observer.  It's here strictly for positioning via
        ;; css.
        [zone-of-cards {:code GAME-CODE
                        :class "other-letter"}
         primary-role
         "s:letter"]
        [zone-of-cards {:code GAME-CODE
                        :class "strategy-discard v-stack"
                        :mode :v-stack}
         primary-role
         "strategy-discard"]
        [zone-of-cards {:code GAME-CODE
                        :class "in-play"}
         primary-role
         "in-play"]
        [zone-of-cards {:code GAME-CODE
                        :class "your-letter"}
         primary-role
         "u:letter"]
        [effects primary-role "s" "other-effects"]
        [effects primary-role "u" "your-effects"]]

       [:div.auxiliary-container
        [zone-of-cards {:code GAME-CODE
                        :class "other-letter"}
         primary-role
         [opposing-role "letter"]]
        [zone-of-cards {:code GAME-CODE
                        :class "strategy-discard v-stack"
                        :mode :v-stack}
         primary-role
         "strategy-discard"]
        [zone-of-cards {:code GAME-CODE
                        :class "in-play"}
         primary-role
         "in-play"]
        [zone-of-cards {:code GAME-CODE
                        :class "your-letter"}
         primary-role
         [primary-role "letter"]]
        [effects primary-role opposing-role "other-effects"]
        [effects primary-role primary-role "your-effects"]])]))

(defn compact-game-container [primary-role]
  [:div.compact-game-container
   [map-container primary-role]
   (holder-zones primary-role)
   [:div.auxiliary-container
    [zone-of-cards {:code GAME-CODE
                    :class "strategy-discard v-stack"
                    :mode :v-stack
                    :modal-kind :strategy-discard}
     primary-role
     "strategy-discard"]
    [zone-of-cards {:code GAME-CODE
                    :class "in-play"}
     primary-role
     "in-play"]
    [effects primary-role (other-role primary-role) "other-effects"]
    [effects primary-role primary-role "your-effects"]]])

(defn update-map-size-class []
  (doseq [index (range (.-length (js/document.getElementsByClassName "map")))
          :let [map (aget (js/document.getElementsByClassName "map") index)
                mw (-> td-data/SIZES :map :w)
                mh (-> td-data/SIZES :map :h)
                ratio (/ mw mh)
                sw (.-clientWidth map)
                sh (.-clientHeight map)
                visible-ratio (/ sw sh)
                size-to-set (if (< ratio visible-ratio)
                              ;; Container is wider than map: set height to container
                              ;; height, and scale width accordingly.
                              {:h sh
                               :w (* mw (/ sh mh))}
                              ;; Container is higher than map: set width to container
                              ;; width, and scale height accordingly.
                              {:w sw
                               :h (* mh (/ sw mw))})
                img (aget (js/document.getElementsByClassName "board-img") index)]]
    (when (not= (:w size-to-set) 0)
      (set! (.-width img) (:w size-to-set))
      (set! (.-height img) (:h size-to-set)))))

(defn place-zone-by-widget [zone-class-names index marker-name percent hor-or-ver]
  (let [{:keys [start finish]} (td-data/marker-by-name marker-name)
        zone-element (aget (js/document.getElementsByClassName zone-class-names) index)

        ver? (= hor-or-ver :ver)

        w (- (:x finish) (:x start))
        h (- (:y finish) (:y start))

        x (if ver? (- (:x start) (* w 0.6)) (+ (:x start) (* w 0.25)))
        y (if ver? (+ (:y start) (* h 0.2)) (:y start))

        w (if ver? (* w 2) (* w 0.5))]
    (when zone-element
      (set! (.-style zone-element)
            (str "left: " (* x percent) "px; top: " (* y percent) "px; width: " (* w percent) "px;")))))

(defn update-board-widget-sizes []
  (doseq [index (range (.-length (js/document.getElementsByClassName "board-img")))
          :let [img (aget (js/document.getElementsByClassName "board-img") index)
                board-widgets (aget (js/document.getElementsByClassName "board-widgets") index)
                svg-container (aget (js/document.getElementsByClassName "svg-container") index)]]
    (when (and map img board-widgets svg-container)
      (let [width (.-clientWidth img)
            percent (/ width (-> td-data/SIZES :map :w))]
        (when (not= width 0)
          (set! (.-width (.-style board-widgets)) (str width "px"))
          (set! (.-height (.-style board-widgets)) (str (.-clientHeight img) "px"))
          (set! (.-transform (.-style svg-container)) (str "scale(" percent ")"))

          (place-zone-by-widget "s agenda-zone" index "s:agenda" percent :ver)
          (place-zone-by-widget "u agenda-zone" index "u:agenda" percent :ver)

          (place-zone-by-widget "s aftermath-zone" index "af" percent :hor)
          (place-zone-by-widget "u aftermath-zone" index "af" percent :hor))))))

(defn do-resize []
  (update-map-size-class)
  (update-board-widget-sizes))

(defn small-role-identifier [role]
  [:span (symbol-of-role role)])

(defn player-plaque-info-line [primary-role role]
  (let [war-danger? @(subscribe [:thm.game-ui.td/good-for-war? primary-role role])
        n-cubes @(subscribe [:thm.game-ui/track-var primary-role ["c" role]])
        n-cards @(subscribe [:thm.game-ui/n-cards-in-zone primary-role [role "hand"]])]
    [:div.line
     [:span {:title "Influence cubes"} (str n-cubes "▨")] (bull)
     [:span {:title "Cards"} n-cards "🂠"] (bull)
     [:span.defcon-levels {:title (str "DEFCON: Military, Political, World Opinion"
                                       (when war-danger? " - Nuclear War danger!"))}
      (doall (for [t (keys td-data/defcon-tracks)]
               ^{:key t}
               [:span [:span {:class [t "defcon-value"]}
                       (defcon-level-and-area @(subscribe [:thm.game-ui/track-var primary-role ["d" role t]]))]
                " "]))
      (when war-danger? [:span.war war-symbol])]]))

(defn compact-player-plaque [primary-role role player-name usernames]
  (let [connected? (seq-contains? usernames player-name)
        war-danger? @(subscribe [:thm.game-ui.td/good-for-war? primary-role role])
        influence-cubes @(subscribe [:thm.game-ui/track-var primary-role ["c" role]])
        cards @(subscribe [:thm.game-ui/n-cards-in-zone primary-role [role "hand"]])
        has-letter? (pos? @(subscribe [:thm.game-ui/n-cards-in-zone primary-role [role "letter"]]))
        has-initiative? (= role @(subscribe [:thm.game-ui/track-var primary-role "initiative-player"]))
        active? (contains? @(subscribe [:thm.game-ui/request-map-for primary-role]) role)]
    [:div.compact-avatar-container {:class [role
                                            (when active?
                                              "is-active")]
                                    :on-click #(dispatch-e [:thm.game-ui.compact/show-extended-player-plaque
                                                            {:role role
                                                             :player-name player-name
                                                             :connected? connected?
                                                             :influence-cubes influence-cubes
                                                             :cards cards
                                                             :has-letter? has-letter?
                                                             :has-initiative? has-initiative?
                                                             :war-danger? war-danger?}] %)}
     [:div.line
      (connection-indicator connected?)
      " "
      (symbol-of-role role)
      " "

      ]
     [:div.line
      [:span {:title "Influence cubes"} influence-cubes]
      (bull)
      [:span {:title "Cards"} cards]]
     [:div.line
      (when has-letter?
        [:span {:key "letter"} "PL"])
      " "
      (when has-initiative?
        "I")
      " "
      (when war-danger? [:span.war war-symbol])]
     [avatar-img player-name]]))

(defn extended-player-plaque-info [data]
  (let [{:keys [role influence-cubes cards has-letter? has-initiative? war-danger?]} data]
    [:div
     [:div (symbol-of-role role) " Plays as " (role-for-humans role)]
     [:div (pluralize influence-cubes "influence cube")]
     [:div (pluralize cards "card") " in hand"]
     (when has-letter?
       [:div "PL: Has Personal Letter"]) ;; todo its image
     (when has-initiative?
       [:div "I: Has initiative"])
     (when war-danger?
       [:div war-symbol " In danger of starting the Nuclear War"])]))

(defn initiative-text [primary-role]
  (if-let [r @(subscribe [:thm.game-ui/track-var primary-role "initiative-player"])]
    (str (role-for-humans r) " has initiative.")
    ""))

(defn prestige-text [primary-role]
  (let [p @(subscribe [:thm.game-ui/track-var primary-role "prestige"])]
    (str "Prestige: " p)))

(defn sidebar-global-info [primary-role]
  [:div
   [:div.global.text
    [initiative-text primary-role] [:br]
    [prestige-text primary-role]]
   [:div.text
    "Discards: "
    [zone-link primary-role :strategy-discard "Strategy" "strategy-discard"]
    (bull)
    [zone-link primary-role :agenda-discard "Agenda" "agenda-discard"]]])

(defn r-el [element-name role]
  (if role
    (keyword (str (name element-name) "." role))
    element-name))

;; XXX duplication with td-texts
(defn phase-for-humans [tag]
  (case tag
    :escalate-defcon "Escalate DEFCON tracks"
    :draw-agendas "Draw Agendas"
    :strategy-cards "Strategy cards"
    :save-for-aftermath "Save cards for Aftermath"
    :wo-bonus "World Opinion bonus"
    :resolve-agendas "Resolve Agendas"
    :check-war "Check for nuclear war"
    :advance-round "Advance round marker"))

(defn ctl
  ([cid]
   [card-title-link cid [:b (card-title-by-id cid)]])

  ([cid text]
   [card-title-link cid text]))

(defn war-reason-for-humans [reason]
  (case reason
    :any-marker-in-1-area "marker in DEFCON 1 area"
    :all-markers-in-2-area "all markers in DEFCON 2 area"))

(defn log-item-for-humans [[kind & delta-args :as log-item]]
  (let [[text-code args] (case kind
                           :text (let [[text-code & text-args] delta-args]
                                   [text-code text-args])
                           (:deck-shuffled, :game-finished) [kind delta-args])]
    (case text-code
      :*commit-timestamp* nil
      :game-starts (let [[u-player s-player] args]
                     [:div [:h3 "Game starts"]
                      [:span [:b u-player] " plays as " (role-for-humans "u")] ", "
                      [:span [:b s-player] " plays as " (role-for-humans "")] "."])
      :round-starts (let [[n] args]
                      [:h4 "Round " n])
      :phase-starts (let [[tag] args]
                      [:h5 (phase-for-humans tag)])
      :turn-starts (let [[role n] args]
                     [(r-el :h5 role) (role-for-humans role) " turn #" n])
      :aftermath-starts [:h4 "Aftermath"]
      :choosing-initiative-player (let [[role] args]
                                    [(r-el :span role) [:b (role-for-humans role)] " chooses a player to get initiative."])
      :initiative-player (let [[role] args]
                           [(r-el :span role) [:b (role-for-humans role)] " gets initiative."])
      :play-card (let [[role denumbered-cid mode] args
                       design (td-data/card-design-by-denumbered-id denumbered-cid)]
                   [(r-el :span role) [:b (role-for-humans role)]
                    " player plays "
                    [ctl denumbered-cid
                     [:span [:b (:title design)]
                      " (" (card-alignment-for-humans (:alignment design))
                      ", " (pluralize (:cubes design) "cube") ")"]]
                    " for "
                    [:b (card-play-mode-for-humans mode)]
                    "."])
      :resolving-event (let [[role denumbered-cid] args]
                         [(r-el :span role) "Resolving event of "
                          [ctl denumbered-cid]
                          ":"])
      :skip-event (let [[r] args]
                    [(r-el :span r) [:b (role-for-humans r)] " chooses not to resolve the event."])
      :tv-wo-bonus (let [[r] args]
                     [(r-el :span r) [:b (role-for-humans r)] " gets Television bonus: escalate or degrade one DEFCON track."])
      :un-wo-bonus (let [[r] args]
                     [(r-el :span r) [:b (role-for-humans r)] " gets United Nations bonus: take Personal Letter."])
      :no-wo-bonus (let [[bg] args]
                     [:span "Nobody gets the World Opinion bonus for dominating "
                      (battleground-for-humans bg) "."])
      :alliances-wo-bonus (let [[r] args]
                            [(r-el :span r) [:b (role-for-humans r)] " gets Alliances bonus: additional Aftermath card."])
      :resolve-agenda (let [[{:keys [cid winner base-award total-bonus total]}] args]
                        [:span "Resolving " [ctl cid] ": "
                         (if winner
                           (str (role-for-humans winner) " dominates for "
                                base-award "+" total-bonus "="
                                (pluralize (js/Math.abs total) "prestige point") ".")
                           "no one dominates.")])
      :no-war "Nuclear war is averted... so far."
      :count-aftermath (let [[{:keys [u-cubes s-cubes dominator]}] args]
                         [:span "Counting Aftermath cubes: "
                          [(r-el :span "u") (role-for-humans "u") " has " (pluralize u-cubes "cube")] ", "
                          [(r-el :span "s") (role-for-humans "s") " has " (pluralize s-cubes "cube")] ": "
                          (if dominator
                            [(r-el :span dominator) [:b (role-for-humans dominator)] " gets the award."]
                            "no one gets the award.")])
      :no-influence-change (let [[role] args]
                             [(r-el :span role) [:b (role-for-humans role)] " doesn't add or remove any cubes."])
      :no-cubes-placed (let [[role] args]
                         [(r-el :span role) [:b (role-for-humans role)] " doesn't place any cubes."])
      :no-cubes-removed (let [[role] args]
                          [(r-el :span role) [:b (role-for-humans role)] " doesn't remove any cubes."])
      :influence-changed (let [[r bg change now-u now-s] args
                               [verb prep] (if (pos? change)
                                             ["adds" "to"]
                                             ["removes" "from"])
                               ch (js/Math.abs change)]
                           [(r-el :span r) [:b (role-for-humans r)]
                            " " verb
                            " " [:b (pluralize ch "cube")]
                            " " prep
                            " " [:b (battleground-for-humans bg)]
                            ", now at " now-u us-symbol "/" now-s su-symbol "."])
      :adds-to-aftermath (let [[r] args]
                           [(r-el :span r) [:b (role-for-humans r)] " adds the card to Aftermath."])
      :discards (let [[r cid] args]
                  [(r-el :span r) [:b (role-for-humans r)] " discards "
                   [ctl cid] "."])
      :defcon-changed (let [[r changes] args]
                        (conj
                         (into [(r-el :span r)
                                [:b (role-for-humans r)] " "]
                               (sequence-for-humans (map (fn [[t change now]]
                                                           (let [ch (js/Math.abs change)]
                                                             [:span
                                                              [:b (defcon-track-for-humans t)] " track "
                                                              (if (pos? change) "is escalated" "is deflated") " "
                                                              "by " [:b ch] ", now at " (defcon-level-and-area now)]))
                                                         changes)
                                                    :always-comma))
                         "."))
      :no-defcon-changed (let [[r] args]
                           [(r-el :span r) [:b (role-for-humans r)] " doesn't change DEFCON."])
      :prestige-changed (let [[change now] args
                              [r ch] (if (pos? change)
                                       ["s" change]
                                       ["u" (- change)])]
                          [(r-el :span r) [:b (role-for-humans r)]
                           " gains " [:b ch] " Prestige, now at " now "."])
      :intrep-got (let [[cid] args]
                    [:span "Intelligence Reports gets " [ctl cid] "."])
      :summee-no-cards-discarded (let [[r] args]
                                   [(r-el :span r) [:b (role-for-humans r)] " doesn't discard any cards."])
      :discards-many (let [[r cids] args]
                       (conj
                        (into
                         [(r-el :span r) [:b (role-for-humans r)] " discards "]
                         (sequence-for-humans
                          (map (fn [cid]
                                 [ctl cid]) cids))) "."))
      :fiffif-no-dominator [:span "Nobody dominates Television."]
      :inofcu-max-defcon [:span "USA Military DEFCON is maximally escalated already."]
      :deck-shuffled [:span [:b (zone-name-for-humans (first args))] " is shuffled."]
      :game-finished (outcome-for-humans GAME-CODE args)
      :effect-started (let [[r effect] args]
                        [(r-el :span r) "Effect of " [ctl effect]
                         " has started on " [:b (role-for-humans r)] "."])
      ;; XXX these three are probably unneeded since :precondition-not-met handles them.
      :offmis-defcon-not-3 [:span "USA Political DEFCON is more escalated than 3."]
      :eytoey-defcon-not-dominated [:span "USA isn't more escalated than USSR on the Military DEFCON track."]
      :maskir-defcon-not-3 [:span "USSR Military DEFCON is more escalated than 3."]
      :precondition-not-met (let [[cid] args]
                              [:span "The precondition of " [ctl cid] " is not met."])
      :player-reveals-agenda (let [[r cid] args]
                               [(r-el :span r) [:b (role-for-humans r)] " reveals " [ctl cid] "."])
      :has-agenda-options (let [[r cids] args]
                            (conj
                             (into
                              [(r-el :span r) [:b (role-for-humans r)] " has agenda options: "]
                              (sequence-for-humans
                               (map (fn [cid]
                                      [ctl cid]) cids))) "."))
      :war-danger-warning (let [[r reason] args]
                            [(r-el :span r) [:b (role-for-humans r)] " risks triggering the nuclear war: " (war-reason-for-humans reason) "."])

      (do (warn "Unrecognized text code: " log-item)
          (str/join (bull) log-item)))))


(defn your-request-code-for-humans [text-descr primary-role]
  (let [{:keys [text-code text-args]} (parse-text-descr text-descr)]
    (case text-code
      :choose-agenda-card "Choose your agenda"
      :choose-initiative-player "Choose who gets initiative:"
      :choose-card-to-play "Choose a card to play"
      :choose-mode-of-play (let [[cid] text-args]
                             [:span "Play " [:b.tmi (card-title-by-id cid)] " for..."])
      :choose-command-t (let [[cubes] text-args]
                          [:span "Command " (pluralize cubes "cube")])
      :choose-command-onto-t (let [[cubes] text-args]
                               [:span "Command " (pluralize cubes "cube") " (add only)"])
      :choose-place-influence-t (let [[max max-per defcon? exact-amount?] text-args
                                      how-many (if exact-amount? "" "up to ")]
                                  [:span "Place "
                                   (when-not exact-amount? "up to ")
                                   (pluralize max "cube")
                                   (parenthetical-list
                                    (when max-per
                                       (str "max " max-per "/bg"))
                                     (when defcon? war-symbol))])
      :choose-remove-influence-t
      ,(let [[whose max policy defcon?] text-args
             cubes-text (case policy
                          (:single-bg, :many-bgs) (str "up to "
                                                       (pluralize max (str (role-for-humans whose) " cube")))
                          :half (str "half the " (role-for-humans whose) " cubes"))]
         [:span "Remove " cubes-text (parenthetical-list
                                      (when defcon? war-symbol))])
      :want-event? (let [[cid] text-args]
                     [:span "Do you want to resolve Event of " [:b (card-title-by-id cid)] "?"])
      :choose-change-defcon-t
      ,(let [[whose max-tracks allowed-tracks] text-args
             intermission (if (= whose primary-role)
                            ""
                            (str (role-for-humans whose) "'s "))
             which (case (count allowed-tracks)
                     3 "any"
                     2 (str/join " or "
                                 (map defcon-track-for-humans (sort allowed-tracks)))
                     1 (str "the " (defcon-track-for-humans (first allowed-tracks))))]
         (if (= max-tracks 2)
           (str "Change any 2 " intermission "DEFCON tracks")
           (str "Change " which " " intermission "DEFCON track")))
      :aftermath-or-discard (let [[cid] text-args]
                              [:span "Where to send " [:b (card-title-by-id cid)] "?"])
      :intrep-play-or-discard (let [[cid] text-args]
                                [:span "Got " [:b (card-title-by-id cid)] ": "])
      :summee-choose-cards-to-discard [:span "Choose cards to discard"]
      :airstr-remove-or-place [:span "Remove or place influence?"]
      :baofpi-remove-or-effect [:span "Remove cubes from Alliances or suffer the effect?"]
      (str "Unknown text code of text description " text-descr))))

(defn big-card [cid]
  [:img.big-card {:src (big-card-img-src cid)}])

(defn has-left-hand-of-choose-mode-modal? [req]
  (let [[kind text-descr & args] req
        {:keys [text-code text-args]} (parse-text-descr text-descr)]
    (case text-code
      :choose-initiative-player false
      true)))

(defn left-hand-of-choose-mode-modal [req]
  (let [[kind text-descr & args] req
        {:keys [text-code text-args]} (parse-text-descr text-descr)]
    (case text-code
      :choose-initiative-player [:div]
      :choose-mode-of-play (let [[cid] text-args]
                             (big-card cid))
      :want-event? (let [[cid] text-args]
                     (big-card cid))
      :aftermath-or-discard (let [[cid] text-args]
                              (big-card cid))
      :intrep-play-or-discard (let [[cid] text-args]
                                (big-card cid))
      :airstr-remove-or-place (big-card "airstr")
      :baofpi-remove-or-effect (big-card "baofpi")
      (str "Unknown text code of choose-mode-like request: " text-code req))))

;; XXX find out why span with bolded player name displays at different height
;; than the span with button.
(defn other-request-code-for-humans [player-name text-code]
  [:span [:b player-name] " "
   (case text-code
     :choose-agenda-card "chooses an agenda"
     :choose-initiative-player "chooses who gets initiative"
     :choose-card-to-play "chooses a card to play"
     :choose-mode-of-play "chooses how to play the card"
     :choose-command-t "chooses how to Command cubes"
     :choose-command-onto-t "chooses how to Command cubes"
     :choose-place-influence-t "chooses how to place cubes"
     :choose-remove-influence-t "chooses how to remove cubes"
     :want-event? "chooses whether to resolve the Event"
     :choose-change-defcon-t "chooses how to change DEFCON"
     :aftermath-or-discard "chooses whether to send the card to Aftermath or Discard"
     :intrep-play-or-discard "chooses whether to play or discard the card"
     :summee-choose-cards-to-discard "chooses cards to discard"
     :airstr-remove-or-place "chooses whether to remove or place influence"
     :baofpi-remove-or-effect "chooses whether to remove influence or suffer the effect"
     (do
       (warn "Unrecognized text-code of other request: " text-code)
       (str "does something unknown: " text-code)))
   "."])

(defn mode-for-humans [v text-descr]
  (let [{:keys [text-code text-args]} (parse-text-descr text-descr)]
    (case text-code
      :choose-initiative-player (role-for-humans v)
      :choose-mode-of-play (card-play-mode-for-humans v :short)
      :aftermath-or-discard (if (= v "aftermath") "Aftermath" "Discard")
      :intrep-play-or-discard (if (= v "play") "Play" "Discard")
      :airstr-remove-or-place (if (= v "remove") "Remove" "Place")
      :baofpi-remove-or-effect (if (= v "remove") "Remove" "Effect")
      (do (warn "Unrecognized text-code in translating mode: " text-descr)
          v))))

(defn answer-for-humans [v req]
  (let [[kind text-descr & args] req]
    (case kind
      :choose-card (card-title-by-id v)
      :choose-card-and-mode (card-title-by-id (first v))
      :choose-cards (str/join ", " (sort (map short-card-title v)))
      :choose-mode (mode-for-humans v text-descr)
      (:choose-command :choose-command-onto)
      ,(let [[bg change] v]
         (cond
           (neg? change) (str "-" (- change) " from " (battleground-for-humans bg))
           (zero? change) (str "no change in " (battleground-for-humans bg))
           (pos? change) (str "+" change " to " (battleground-for-humans bg))))

      :choose-place-influence (str/join " " (sort-explode-map v))
      :choose-remove-influence (str/join " " (sort-explode-map v))
      :choose-change-defcon (let [track->change v]
                              (str/join ", " (map #(str (defcon-track-for-humans (key %))
                                                        " "
                                                        (change-for-humans (val %)))
                                                  track->change)))
      (do (warn "Unrecognized request kind in translating answer: " kind text-descr)
          v))))
