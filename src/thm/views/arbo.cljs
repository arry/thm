(ns thm.views.arbo
  (:require [reagent.core :as r]
            [clojure.string :as str]
            [taoensso.timbre :refer-macros (debug warn)]
            [thm.views.common :refer [link]]
            [thm.utils :refer [bull
                               to-integer
                               dispatch-e
                               svg-arrow
                               map-cross
                               range-inclusive]]))

(def all-colors "bgdn" #_"bgdnopruwy")

(def all-cids
  (vec (map-cross #(str %1 %2) all-colors "12345678")))

(def arbo-state (r/atom {}))

(defn small-card [cid opts]
  [:div.small-card {:class [(when (:active? opts) "is-active")
                            (when (:selected? opts) "is-checked")]
                    :on-click #(when-let [handler (:on-click opts)]
                                 (handler cid))}
   [:img {:src (str "/arbo-images/" cid ".png")}]
   [:div.cover]])

(defn slot-id-from-coords [x y]
  (str "c:" x ":" y))

(defn slot-id-from-point [[x y]]
  (slot-id-from-coords x y))

(defn remove-cid [cids cid-to-remove]
  (vec (remove #(= % cid-to-remove) cids)))

(defn do-remove-selected-card-from-hand [cs]
  (update cs :hand remove-cid (:selected-cid cs)))

(defn at-edge? [cs x y]
  (or (= (:bx cs) x)
      (= (:ex cs) x)
      (= (:by cs) y)
      (= (:ey cs) y)))

(defn extend-edge [cs x y]
  (cond (= (:bx cs) x)
        (update cs :bx dec)

        (= (:ex cs) x)
        (update cs :ex inc)

        (= (:by cs) y)
        (update cs :by dec)

        (= (:ey cs) y)
        (update cs :ey inc)

        :else
        (assert false (str "Not at edge! " cs x y))))

(defn has-card? [grid x y]
  (some? (get grid (slot-id-from-coords x y))))

(defn just-starting? [cs]
  (not (has-card? (:grid cs) 0 0)))

(defn do-place-selected-card [cs x y]
  (cond-> cs
    true (update :grid assoc (slot-id-from-coords x y) (:selected-cid cs))
    (at-edge? cs x y) (extend-edge x y)))

(defn do-switch-to-choose-card-state [cs]
  (assoc cs
         :state :choose-card-to-place
         :selected-cid nil))

(defn do-draw-new-card [cs]
  (let [[taken-cards remaining-cards] (split-at 1 (:deck cs))]
    (assoc cs
           :deck (vec remaining-cards)
           :hand (vec (concat (:hand cs) taken-cards)))))

(defn card-color [cid]
  (str (first cid)))

(defn card-number [cid]
  (to-integer (str (last cid))))

(defn all-neighbors-of-coords [x y]
  [[     x (dec y)]
   [(inc x)     y]
   [     x (inc y)]
   [(dec x)     y]])

(defn cs-get-card-at [cs x y]
  (get (:grid cs) (slot-id-from-coords x y)))

(defn calculate-best-path-from-card [cs start-x start-y card]
  (let [start-color (card-color card)
        start-number (card-number card)
        start-mark {:length 1, :score (if (= start-number 1) 2 1), :can-finish? false}

        marks (loop [marks {[start-x start-y :single] start-mark}
                     queue [[start-x start-y start-color start-number :single]]]
                (if (empty? queue)
                  marks
                  (let [[cur-x cur-y cur-color cur-number cur-path-type :as cur-point] (peek queue)
                        popped-queue (pop queue)
                        [new-marks new-queue] (reduce (fn [[cur-marks cur-queue] [that-x that-y]]
                                                        (let [that-card (cs-get-card-at cs that-x that-y)]
                                                          (if (nil? that-card)
                                                            [cur-marks cur-queue]
                                                            (let [that-color (card-color that-card)
                                                                  that-number (card-number that-card)]
                                                              (if (and (number? that-number) (number? cur-number) (<= that-number cur-number))
                                                                [cur-marks cur-queue]

                                                                (let [cur-mark (get marks [cur-x cur-y cur-path-type])
                                                                      _ (assert (some? cur-mark)
                                                                                (str "Must have mark for current point" cur-x cur-y))
                                                                      color-matches? (= that-color start-color)

                                                                      path-type (if (and (= cur-path-type :single)
                                                                                           color-matches?)
                                                                                    :single
                                                                                    :multi)

                                                                      new-mark {:length (inc (:length cur-mark))
                                                                                :score (+ (:score cur-mark)
                                                                                          (if (= path-type :single)
                                                                                            (cond
                                                                                                (< (:length cur-mark) 3) 1
                                                                                                (= (:length cur-mark) 3) 5
                                                                                                :else 2)
                                                                                            1)
                                                                                          (if (= that-number 8) 2 0))
                                                                                :can-finish? color-matches?
                                                                                :pred [cur-x cur-y cur-path-type]}

                                                                      existing-that-mark (get marks [that-x that-y path-type])]
                                                                  (if (and existing-that-mark
                                                                           (>= (:score existing-that-mark) (:score new-mark)))
                                                                    [cur-marks cur-queue]
                                                                    [(assoc cur-marks [that-x that-y path-type] new-mark)
                                                                     (conj cur-queue [that-x that-y that-color that-number path-type])])))))))
                                                      [marks popped-queue] (all-neighbors-of-coords cur-x cur-y))]
                    (recur new-marks new-queue))))

        find-best (fn [marks]
                    (reduce (fn [best-so-far [[x y path-type] candidate-mark]]
                              (if (and (:can-finish? candidate-mark)
                                       (or (nil? best-so-far)
                                           (> (:score candidate-mark) (get best-so-far 3))))
                                [x y path-type (:score candidate-mark)]
                                best-so-far))
                            nil marks))

        calc-path-from (fn [best marks]
                         (let [pointer [(get best 0) (get best 1) (get best 2)]
                               mark (get marks pointer)
                               first-predecessor (:pred mark)

                               path-points (loop [result [pointer]
                                                  cur-point first-predecessor]
                                             (let [mark (get marks cur-point)
                                                   pred (:pred mark)]
                                               (if (some? pred)
                                                 (recur (conj result cur-point)
                                                        pred)
                                                 (conj result cur-point))))]
                           {:color start-color
                            :score (get best 3)
                            :points (vec (reverse (map (fn [[x y path-type]] [x y]) path-points)))}))

        best (find-best marks)]
    (when best
      (calc-path-from best marks))))

(defn calculate-paths [cs]
  (let [path-from-each-card (for [x (range-inclusive (:bx cs) (:ex cs))
                                  y (range-inclusive (:by cs) (:ey cs))
                                  :let [card (cs-get-card-at cs x y)]
                                  :when (some? card)
                                  :let [path (calculate-best-path-from-card cs x y card)]
                                  :when path]
                              path)

        paths (reduce (fn [result candidate-path]
                        (let [color (:color candidate-path)
                              existing-path (get result color)]
                          (if (or (nil? existing-path)
                                  (> (:score candidate-path) (:score existing-path)))
                            (assoc result color candidate-path)
                            result)))
                      {} path-from-each-card)]
    paths))

(defn do-calculate-paths [cs]
  (assoc cs :paths (calculate-paths cs)))

(defn on-slot-chosen [x y]
  (let [cs @arbo-state]
    (case (:state cs)
      :choose-slot (swap! arbo-state
                          #(-> cs
                               (do-remove-selected-card-from-hand)
                               (do-place-selected-card x y)
                               (do-draw-new-card)
                               (do-switch-to-choose-card-state)
                               (do-calculate-paths)))
      nil)))

(defn slot [x y]
  (let [cs @arbo-state
        slot-id (slot-id-from-coords x y)
        active? (and (= (:state cs) :choose-slot)
                     (contains? (:active-slots cs) slot-id))]
    [:div.slot {:class (when active? "is-active")
                :on-click #(when active?
                             (on-slot-chosen x y))}
     (when-let [cid (get (:grid cs) slot-id)]
       [small-card cid {}])
     [:div.cover]]))

(defn find-valid-placements [cs]
  (if (just-starting? cs)
    #{"c:0:0"}
    (into #{}
          (for [y (range-inclusive (:by cs) (:ey cs))
                x (range-inclusive (:bx cs) (:ex cs))
                :when (has-card? (:grid cs) x y)
                [nx ny] (all-neighbors-of-coords x y)
                :when (not (has-card? (:grid cs) nx ny))]
            (slot-id-from-coords nx ny)))))

(defn on-card-clicked [cid]
  (let [cs @arbo-state]
    (case (:state cs)
      :choose-card-to-place (swap! arbo-state
                                   assoc
                                   :selected-cid cid
                                   :state :choose-slot
                                   :active-slots (find-valid-placements cs))
      nil)))

(defn undo []
  (swap! arbo-state identity))

;; XXX measurements are based on slot's css (slot.w + slot.m * 2), which in
;; turn is from card's css.
(def cw 82)
(def ch 124)

(def svg-path-properties
  (let [dx (* cw 0.1), dy (* ch 0.09)]
    {"b" {:ox (* dx -4.5) :oy (* dy  4.5) :s "#007"}
     "g" {:ox (* dx -3.5) :oy (* dy  3.5) :s "#070"}
     "d" {:ox (* dx -2.5) :oy (* dy  2.5) :s "#077"}
     "n" {:ox (* dx -1.5) :oy (* dy  1.5) :s "#770"}
     "o" {:ox (* dx -0.5) :oy (* dy  0.5) :s "#707"}
     "p" {:ox (* dx  0.5) :oy (* dy -0.5) :s "#c3c"}
     "r" {:ox (* dx  1.5) :oy (* dy -1.5) :s "#700"}
     "u" {:ox (* dx  2.5) :oy (* dy -2.5) :s "#cc3"}
     "w" {:ox (* dx  3.5) :oy (* dy -3.5) :s "#ccc"}
     "y" {:ox (* dx  4.5) :oy (* dy -4.5) :s "#3cc"}}))

(defn svg-line [from to stroke]
  [:path {:d (str "M" (:x from) " " (:y from)
                  "L" (:x to)   " " (:y to))
          :stroke-linecap "round"
          :style {:stroke-width 8
                  :stroke stroke}}])

(defn svg-path-among-trees [tree-color points]
  (let [cs @arbo-state
        {:keys [bx by]} cs

        {:keys [ox oy s]} (get svg-path-properties tree-color)

        f (fn [x y]
            {:x (+ ox (* cw (+ 0.5 (- x bx))))
             :y (+ oy (* ch (+ 0.5 (- y by))))})]
    [:g
     (doall (for [i (range 1 (count points))
                  :let [[x1 y1] (get points (dec i))
                        [x2 y2] (get points      i)]]
              (if (= i (dec (count points)))
                ^{:key i}
                [svg-arrow (f x1 y1) (f x2 y2) s :auto i]
                ^{:key i}
                [svg-line (f x1 y1) (f x2 y2) s])))]))

(defn svg-container []
  (let [cs @arbo-state]
    [:div.svg-container
     [:svg {:width "100%"
            :height "100%"}
      [:g
       (doall (for [tree-color all-colors
                    :let [path (get (:paths cs) tree-color)]
                    :when (some? path)]
                ^{:key tree-color}
                [svg-path-among-trees tree-color (:points path)]))]]]))

(def n-cards-in-hand 25)

(defn create-new-game-state []
  (let [initial-deck (identity all-cids)]
    {:hand (vec (take n-cards-in-hand initial-deck))
     :deck (vec (drop n-cards-in-hand initial-deck))

     :bx -1, :ex 1
     :by -1, :ey 1

     :grid {}


     :state :choose-card-to-place
     }))

(defn new-game! []
  (reset! arbo-state (-> (create-new-game-state)
                         (do-calculate-paths))))

(new-game!)

(defn arbo []
  (let [cs @arbo-state]
    [:div.container.arbo.ui
     [:div.row.my-4
      [link "/home" "Done"]]
     [:div.row.my-4
      [:button.btn.btn-light {:on-click #(new-game!)}
       "New game"]
      [:button.btn.btn-danger {:on-click undo}
       "Undo"]
      ]
     [:div.row.my-4
      (doall (for [cid (:hand cs)]
               ^{:key cid}
               [small-card cid {:active? (= (:state cs) :choose-card-to-place)
                                :selected? (= cid (:selected-cid cs))
                                :on-click on-card-clicked}]))]
     [:div.row.my-4
      [:div.grid

       (doall (for [y (range-inclusive (:by cs) (:ey cs))]
                ^{:key (str "row" y)}
                [:div.arbo-row
                 (doall (for [x (range-inclusive (:bx cs) (:ex cs))]
                          ^{:key (str "slot" x)}
                          [slot x y]))]))
       [svg-container]]]]))
