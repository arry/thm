(ns thm.game-ui.all-game-ui-modules
  (:require [thm.td.td-ui-module :as td-ui-module]
            [thm.viti.viti-ui-module :as viti-ui-module]
            [thm.game-ui.stub-ui-module :refer [StubUiModule]]
            [thm.game-ui.ui-module-registry :refer [get-module]]))

(def stubs (atom {}))

(defn get-game-ui-module [code]
  (if-let [module (get-module code)]
    module
    (if-let [module (get @stubs code)]
      module
      (let [module (StubUiModule. code)]
        (swap! stubs assoc code module)
        module))))

(defn module-from-cs [cs]
  (get-game-ui-module (:code cs)))
