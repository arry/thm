-- Verify sqitch/sqitch.plan:room_messages on pg

BEGIN;

select room_id, created_at, message_code, args from room_messages where false;

ROLLBACK;
