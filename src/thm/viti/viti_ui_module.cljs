(ns thm.viti.viti-ui-module
  (:require [thm.game-ui.game-ui-module :as game-ui-module]
            [taoensso.timbre :refer-macros (debug warn)]
            [thm.viti.game-ui.viti-ui :as viti-ui]
            [thm.viti.viti-utils :refer [get-figure-id figure-image state-timing]]
            [thm.viti.viti-texts :as viti-texts :refer [season-for-humans]]
            [thm.viti.viti-data :refer [cid->design all-field-names]]
            [thm.utils :refer [stub html-entity bull
                               unrecognized
                               parse-text-descr
                               pluralize
                               sequence-for-humans
                               split-multipart
                               seq-contains?
                               denumber-id]]
            [clojure.string :as str]
            [thm.views.common :refer [modal-content-wrapper]]
            [thm.views.game-ui-common :refer [zone-of-cards]]
            [thm.game-ui.ui-module-registry :refer [register-module]]
            [thm.views.game-ui-common :refer [card-title-link]]

            [re-frame.core :refer [dispatch subscribe]]

            ))

(def GAME-CODE "viti")

(defmethod game-ui-module/do-resize GAME-CODE [_]
  (viti-ui/do-resize))

(defn zone-name->alias [zone-name your-role]
  zone-name)


(defn get-pane-associated-with-request [req _your-role]
  nil)

(defn get-pane-of-var [var-name]
  nil)

(defn get-pane-of-cards-moved [from to your-role]
  nil)

(defn get-pane-associated-with-delta [delta your-role]
  nil)

(defn space []
  (html-entity "nbsp"))

(defn span-n-cards [n & [bright?]]
  [:span.n-cards (when bright? {:class "bright"}) n])

(defn wakeup-row-for-humans [number]
  (case number
    1 "#1 - no bonus"
    2 "#2 - draw vine"
    3 "#3 - draw wine order"
    4 "#4 - gain 1 lira"
    5 "#5 - draw visitor"
    6 "#6 - gain 1 vp"
    7 "#7 - get gray worker"
    (unrecognized "wakeup row number" number)))

(defn field-name-for-humans [field-name]
  (case field-name
    "f5" "5-value field"
    "f6" "6-value field"
    "f7" "7-value field"
    (unrecognized "field name" field-name)))

(defn sequence-to-str-for-humans [coll & [opt]]
  (str/join "" (sequence-for-humans coll opt)))

(defn space-for-humans [space-name]
  (let [[board space] (split-multipart space-name)]
    (if (= board "m")
      (case space
        "r1f" (wakeup-row-for-humans 1)
        "r2f" (wakeup-row-for-humans 2)
        "r3f" (wakeup-row-for-humans 3)
        "r4f" (wakeup-row-for-humans 4)
        "r5f" (wakeup-row-for-humans 5)
        "r6f" (wakeup-row-for-humans 6)
        "r7f" (wakeup-row-for-humans 7)

        "green-deck" "vine"
        "yellow-deck" "summer visitor"
        "purple-deck" "wine order"
        "blue-deck" "winter visitor"

        "cart" "gain 1 lira"

        ("dv1" "dv@") "draw vine"
        ("gt1" "gt@") "give tour"
        ("bs1" "bs@") "build structure"
        ("sv1" "sv@") "play summer visitor"
        ("sgf1" "sgf@") "sell grapes or buy/sell field"
        ("pv1" "pv@") "plant vine"

        ("do1" "do@") "draw wine order"
        ("hf1" "hf@") "harvest field"
        ("tw1" "tw@") "train worker"
        ("wv1" "wv@") "play winter visitor"
        ("mw1" "mw@") "make wine tokens"
        ("fo1" "fo@") "fill wine order"

        (unrecognized "main board space name" space-name))

      (case space
        "t" "trellis"
        "n" "windmill"
        "i" "irrigation"
        "c" "cottage"
        "s" "tasting room"
        "m" "medium cellar"
        "l" "large cellar"
        "y-s" "yoke"

        "y" "yoke"

        ("f5" "f6" "f7") (field-name-for-humans space)

        "pr1" "red-1"
        "pr2" "red-2"
        "pr3" "red-3"
        "pr4" "red-4"
        "pr5" "red-5"
        "pr6" "red-6"
        "pr7" "red-7"
        "pr8" "red-8"
        "pr9" "red-9"

        "pw1" "white-1"
        "pw2" "white-2"
        "pw3" "white-3"
        "pw4" "white-4"
        "pw5" "white-5"
        "pw6" "white-6"
        "pw7" "white-7"
        "pw8" "white-8"
        "pw9" "white-9"

        "cr1" "red-1"
        "cr2" "red-2"
        "cr3" "red-3"
        "cr4" "red-4"
        "cr5" "red-5"
        "cr6" "red-6"
        "cr7" "red-7"
        "cr8" "red-8"
        "cr9" "red-9"

        "cw1" "white-1"
        "cw2" "white-2"
        "cw3" "white-3"
        "cw4" "white-4"
        "cw5" "white-5"
        "cw6" "white-6"
        "cw7" "white-7"
        "cw8" "white-8"
        "cw9" "white-9"

        "cb1" "blush-1"
        "cb2" "blush-2"
        "cb3" "blush-3"
        "cb4" "blush-4"
        "cb5" "blush-5"
        "cb6" "blush-6"
        "cb7" "blush-7"
        "cb8" "blush-8"
        "cb9" "blush-9"

        "cs1" "sparkling-1"
        "cs2" "sparkling-2"
        "cs3" "sparkling-3"
        "cs4" "sparkling-4"
        "cs5" "sparkling-5"
        "cs6" "sparkling-6"
        "cs7" "sparkling-7"
        "cs8" "sparkling-8"
        "cs9" "sparkling-9"



        (unrecognized "vineyard space name" space-name)))))

(defn card-from-deck-for-humans [deck-name]
  (case deck-name
    "green-deck" "vine"
    "yellow-deck" "summer visitor"
    "purple-deck" "wine order"
    "blue-deck" "winter visitor"
    (unrecognized "deck name" deck-name)))

(defn structure-name-for-humans [structure-name]
  (case structure-name
    :trellis "trellis"
    :windmill "windmill"
    :irrigation "irrigation"
    :cottage "cottage"
    :tasting-room "tasting room"
    :medium-cellar "medium cellar"
    :large-cellar "large cellar"
    :yoke "yoke"
    (unrecognized "structure-name" structure-name)))

(def icon-name-from-type
  {:vp "i-vp-1"
   :lira "i-lira-1"
   :card-green "i-c-green"
   :card-purple "i-c-purple"
   :card-yellow "i-c-yellow"
   :card-blue "i-c-blue"
   :grapes "i-grapes"})

(def icon-title
  {:vp "Victory points"
   :lira "Lira"
   :card-green "Vine cards"
   :card-purple "Wine order cards"
   :card-yellow "Summer visitor cards"
   :card-blue "Winter visitor cards"
   :grapes "First player"})

(defn worker-type-for-humans [worker-type]
  (case worker-type
    :regular "Regular"
    :grande "Grande"
    (unrecognized "worker type" worker-type)))

(defn worker-of-type-for-humans [worker-type]
  (case worker-type
    :regular "a regular worker"
    :grande "the grande worker"
    (unrecognized "worker type" worker-type)))

(defn icon-img [icon-type & [opts]]
  [:img {:src (str "/viti-images/" (icon-name-from-type icon-type) ".png")
         :title (icon-title icon-type)
         :class [(when (:invisible? opts) "invisible")]}])

(defn papa-effect-for-humans [value]
  (if (vector? value)
    (do (assert (= (first value) :lira)
                (str "A vector papa effect should be :lira but is " value))
        (str "Gain " (second value) " lira"))
    (case value
      :one-vp "Gain 1 vp"
      :worker "Gain a worker"
      :cottage "Build cottage"
      :irrigation "Build irrigation"
      :medium-cellar "Build medium cellar"
      :tasting-room "Build tasting room"
      :trellis "Build trellis"
      :windmill "Build windmill"
      :yoke "Build yoke"
      (unrecognized "papa effect" value))))

(defn pluralize-lira [n]
  (str n " lira"))

(defn lira-cost-for-humans [n]
  (pluralize-lira n))

(defn card-title-by-id [cid]
  (if-let [design (cid->design (denumber-id cid))]
    (:title design)
    (unrecognized "card id" cid)))

(defn ctl
  ([cid]
   [card-title-link cid [:b (card-title-by-id cid)]])

  ([cid text]
   [card-title-link cid text]))

(defn vineyard-play-area-of-role [role]
  (str role ":vineyard"))

(defn viti-get-play-area-of-space [space-name]
  (let [[x space] (split-multipart space-name)]
    (if (= x "m")
      "board"
      (vineyard-play-area-of-role x))))

(defn viti-get-play-area-of-zone [zone-name primary-role]
  (let [parts (split-multipart zone-name)]
    (case (count parts)
      1
      "board"

      2
      (let [[role subname] parts]
        (cond (and (= role primary-role)
                   (= subname "hand"))
              "your-hand"

              (seq-contains? all-field-names subname)
              (vineyard-play-area-of-role role)

              :else nil))

      nil)))

(defn short-grape-for-humans [color value]
  (str (case color
         :red "red"
         :white "white"
         (unrecognized "grape color" color))
       "-" value))

(defn short-grape-obj-for-humans [grape]
  ;; TODO use accessors that are currently in viti-engine, move them up.
  (short-grape-for-humans (first grape) (second grape)))

(defn grape-for-humans [color value]
  (str "a " (short-grape-for-humans color value)
       " grape"))

(defn short-wine-for-humans [wine]
  (str (case (:kind wine)
         :red "red"
         :white "white"
         :blush "blush"
         :sparkling "sparkling"
         (unrecognized "wine kind" wine))
       "-"
       (:value wine)))

(defn wine-for-humans [wine]
  (str "a " (short-wine-for-humans wine) " wine"))

(defn wine-kind-or-skip-for-humans [wine-kind]
  (case wine-kind
    :red "Red"
    :white "White"
    :blush "Blush"
    :sparkling "Sparkling"
    :skip "Don't make"
    (unrecognized "wine kind" wine-kind)))

(defn yoke-mode-for-humans [mode]
  (case mode
    :uproot "Uproot vine"
    :harvest "Harvest field"
    (unrecognized "yoke mode" mode)))

(defn sgf-mode-for-humans [mode]
  (case mode
    :sell-grapes "Sell grapes"
    :buy-field "Buy field"
    :sell-field "Sell field"
    (unrecognized "sell grapes/buy fields/sell fields mode" mode)))

(defn zone-name-for-humans [zone-name]
  (case zone-name
    "green-discard" "Vine discard pile"
    "yellow-discard" "Summer visitor discard pile"
    "purple-discard" "Wine order discard pile"
    "blue-discard" "Winter visitor discard pile"
    (unrecognized "zone name for translating for humans: " zone-name)))

(def discard-zones #{"green-discard" "yellow-discard" "purple-discard" "blue-discard"})

(defn discard-modal-content [primary-role zone-name]
  (modal-content-wrapper (zone-name-for-humans zone-name)
                         [zone-of-cards {:code GAME-CODE
                                         :class "horizontal-zone"}
                          primary-role
                          zone-name]))

(def ANIMATED-VAR-REGEXP #"^(vp:|rp:).*")

(defn get-delay-of-var [var-name]
  (when (re-matches ANIMATED-VAR-REGEXP var-name)
    :short))

(deftype VitiUiModule []
  game-ui-module/GameUiModule

  (get-game-code [_]
    GAME-CODE)

  (your-request-code-for-humans [_ text-descr primary-role]
    (let [{:keys [text-code text-args]} (parse-text-descr text-descr)]
      (case text-code
        :choose-rooster-space "Choose a wake-up row"
        :choose-papa-effect "Choose a papa effect"
        :choose-deck-to-draw "Choose a deck to draw a card from"
        :fake-request "(choose anything)"
        :choose-summer-action-or-pass "Choose a summer action"
        :choose-worker-to-send "Which worker to send?"
        :choose-structure-to-build "Choose a structure to build"
        :choose-vine-to-plant "Choose a vine card to plant"
        :choose-field-to-plant-vine (let [[cid] text-args]
                                      (str "Choose a field to plant " (card-title-by-id cid)))
        :choose-winter-action "Choose a winter action"
        :choose-field-to-harvest "Choose a field to harvest"
        :choose-wine-recipe-to-apply "Which kind?"
        :choose-grape-tokens "Choose grape tokens to make wine"
        :choose-wine-to-make "Choose a wine to make"
        :choose-grapes-to-make-wine (let [[wine] text-args]
                                      (str "Choose grape tokens to make " (wine-for-humans wine)))
        :choose-yoke-mode "Choose yoke action"
        :choose-vine-to-uproot "Choose a vine card to uproot"
        :choose-order-to-fill "Choose wine order to fill"
        :choose-sgf-mode "Choose action"
        :choose-grapes-to-sell "Choose grapes to sell"
        :choose-cards-to-discard (let [[n] text-args]
                                   (str "Choose " (pluralize n "card") " to discard"))
        (do (warn "Unrecognized text-code of your request: " text-descr)
            (stub "your-request-code-for-humans" text-descr primary-role)))))

  (other-request-code-for-humans [_ player-name text-descr]
    (let [{:keys [text-code text-args]} (parse-text-descr text-descr)]
      [:span [:b player-name] " "
       (case text-code
         :choose-rooster-space "chooses a wake-up row"
         :choose-papa-effect "chooses a papa effect"
         :choose-deck-to-draw "chooses a deck to draw a card from"
         :fake-request "(does something)"
         :choose-summer-action-or-pass "chooses a summer action"
         :choose-worker-to-send "chooses which worker to send"
         :choose-structure-to-build "chooses a structure to build"
         :choose-vine-to-plant "chooses a vine card to plant"
         :choose-field-to-plant-vine "chooses a field to plant the vine"
         :choose-winter-action "chooses a winter action"
         :choose-field-to-harvest "chooses a field to harvest"
         :choose-wine-recipe-to-apply "chooses which kind of wine to make"
         :choose-grape-tokens "chooses grape tokens to make wine"
         :choose-wine-to-make "chooses a wine to make"
         :choose-grapes-to-make-wine "chooses grape tokens to make wine"
         :choose-yoke-mode "chooses yoke action"
         :choose-vine-to-uproot "chooses a vine card to uproot"
         :choose-order-to-fill "chooses wine order to fill"
         :choose-sgf-mode "chooses an action"
         :choose-grapes-to-sell "chooses grapes to sell"
         :choose-cards-to-discard "chooses cards to discard"
         (do
           (warn "Unrecognized text-code of other request: " text-descr)
           (str "does something unknown: " text-code)))
       "."]))

  (answer-for-humans [_ answer req]
    (let [[kind text-descr & args] req]
      (case kind
        :choose-space (space-for-humans answer)
        :choose-spaces (sequence-to-str-for-humans (map space-for-humans answer)
                                            :omit-and)
        :choose-spaces-from-bags (sequence-to-str-for-humans (map space-for-humans (apply concat answer)) :omit-and)
        :choose-card (card-title-by-id answer)
        :choose-card-and-mode (card-title-by-id (first answer))
        :choose-cards-flexible (str (count answer) " chosen")
        (do (warn "Unrecognized request kind in translating answer: " req)
            answer))))

  (mode-for-humans [_ mode request-text-descr]
    (let [{:keys [text-code text-args]} (parse-text-descr request-text-descr)]
      (case text-code
        :choose-papa-effect (papa-effect-for-humans mode)
        :choose-worker-to-send (worker-type-for-humans mode)
        :choose-wine-recipe-to-apply (wine-kind-or-skip-for-humans mode)
        :choose-yoke-mode (yoke-mode-for-humans mode)
        :choose-sgf-mode (sgf-mode-for-humans mode)
        (do (warn "Unrecognized text-code in translating mode: " request-text-descr)
            mode))))

  (game-container [_ primary-role]
    (viti-ui/game-container primary-role))

  (log-item-for-humans [_ [kind text-code & args :as log-item] log-helper]
    (let [player (fn [role & rest]
                   [:span {:class role}
                    [:b (get (:role->player-name log-helper) role)]
                    " "
                    (into [:span] rest)
                    "."])]
      (case text-code
        :game-starts [:h4 "Game starts"]
        :mama-is-applied (let [[role cid] args]
                           (player role "applies a mama effect of " (ctl cid)))
        :papa-is-applied (let [[role cid] args]
                           (player role "applies a papa effect of " (ctl cid)))
        :draws-cards (let [[role deck-name amount] args]
                       (player role "draws "
                               (pluralize amount
                                          (card-from-deck-for-humans deck-name))))
        :builds-structure (let [[role structure-name] args]
                            (player role "builds " (structure-name-for-humans structure-name)))
        :gains-available-worker (let [[role] args]
                                  (player role "gains a worker"))
        :year-starts (let [[n] args]
                       [:h4 "Year " n])
        :builds-structure-for-cost (let [[role structure-name cost] args]
                                     (player role
                                             "builds " (structure-name-for-humans structure-name)
                                             " for " (lira-cost-for-humans cost)))
        :gains-vp (let [[role amount new-value] args]
                    (player role "gains " (pluralize amount "victory point")
                            ", now at " new-value))
        :gains-rp (let [[role amount new-value] args]
                    (player role "increases residual payments by " amount
                            ", now at " new-value))
        :gains-lira (let [[role amount new-value] args]
                      (player role "gains " (pluralize-lira amount)
                              ", now at " new-value))
        :season-starts (let [[season] args]
                         [:h5 (season-for-humans season)])
        :chooses-wakeup-row (let [[role number] args]
                              (player role "chooses the wakeup row " (wakeup-row-for-humans number)))
        :player-passes (let [[role] args]
                         (player role "passes"))
        :player-passes-for-lack-of-workers (let [[role] args]
                                             (player role "passes (no available workers)"))
        :sends-worker-of-type-to (let [[role worker-type space-name] args]
                                   (player role "sends "
                                           (worker-of-type-for-humans worker-type)
                                           " to the "
                                           "'" (space-for-humans space-name) "'"
                                           " space"))
        :plants-vine (let [[role vine-cid field-name] args]
                       (player role "plants " (ctl vine-cid)
                               " on the " (field-name-for-humans field-name)))
        :trains-worker-for-cost (let [[role cost] args]
                                  (player role "trains a worker "
                                          "for " (lira-cost-for-humans cost)))
        :wastes-grape (let [[role color value] args]
                        (player role "cannot take "
                                (grape-for-humans color value)
                                " because there no free spaces in the crushpad"))
        :harvests-field (let [[role field-name] args]
                          (player role "harvests " (field-name-for-humans field-name)))
        :adds-grape-token (let [[role color value] args]
                            (player role "adds " (grape-for-humans color value)
                                    " to the crushpad"))
        :adds-devalued-grape-token (let [[role color actual-value wanted-value] args]
                                     (player role "adds " (grape-for-humans color actual-value)
                                             "(devalued from " wanted-value ")"
                                             " to the crushpad"))
        :makes-wine (let [[role wine grapes] args]
                      (player role "makes " (wine-for-humans wine) " from "
                              (pluralize (count grapes) "grape") ": "
                              (sequence-to-str-for-humans (map short-grape-obj-for-humans grapes))))
        :makes-wine-devalued (let [[role wine total grapes] args]
                               (player role "makes " (wine-for-humans wine)
                                       " (devalued from " total ")"
                                       " from "
                                       (pluralize (count grapes) "grape") ": "
                                       (sequence-to-str-for-humans (map short-grape-obj-for-humans grapes))))
        :cannot-make-wine-token (let [[role n] args]
                                  (player role "cannot make " (case n
                                                                2 "the second"
                                                                3 "the third"
                                                                (unrecognized "wine token number" n))
                                          " wine token"))
        :fills-order (let [[role cid wines] args]
                       (player role "fills wine order " (ctl cid)
                               " with wines " (sequence-to-str-for-humans (map short-wine-for-humans wines))))

        :uproots-vine (let [[role cid field-name] args]
                        (player role "uproots " (ctl cid) " from the " (field-name-for-humans field-name)))
        :sells-grapes (let [[role grapes total] args]
                        (player role "sells grapes "
                                (sequence-to-str-for-humans (map short-grape-obj-for-humans grapes))
                                " for " (lira-cost-for-humans total)))
        :gets-tasting-room-bonus (let [[role] args]
                                   (player role "gets the benefit of the tasting room"))
        :gets-windmill-bonus (let [[role] args]
                               (player role "gets the benefit of the windmill"))
        :players-age-grapes-and-wines "Players age grape and wine tokens."
        :players-retrieve-workers "Players retrieve workers."
        :players-collect-rp "Players collect residual payments."
        :discards-cards (let [[role cids] args]
                          (player role "discards " (map-indexed (fn [index x]
                                                                  ^{:key index}
                                                                  [:span x])
                                                                (sequence-for-humans (map ctl cids)))))
        :first-player-marker-passed-to (let [[role] args]
                                         (player role "is now the first player"))
        :players-remove-roosters "Players remove roosters."
        (stub "log-item-for-humans" log-item))))

  (small-role-identifier [_ role]
     ;; TODO prettier icon
    (figure-image (get-figure-id role :vp)))

  (player-plaque-info-line [_ primary-role role]
    [:div.line
     ;; The icon is always present to make the other values line up.
     (let [invisible? (not= @(subscribe [:thm.game-ui/track-var primary-role "first-player"]) role)]
       [icon-img :grapes {:invisible? invisible?}])
     (space)
     [icon-img :vp] @(subscribe [:thm.game-ui/track-var primary-role ["vp" role]])
     (space)
     [icon-img :lira] @(subscribe [:thm.game-ui/track-var primary-role ["lira" role]])
     (space) (space)
     [icon-img :card-green] (span-n-cards @(subscribe [:thm.game-ui/track-var primary-role ["nc" role "green"]]))
     [icon-img :card-yellow] (span-n-cards @(subscribe [:thm.game-ui/track-var primary-role ["nc" role "yellow"]]) :bright)
     [icon-img :card-purple] (span-n-cards @(subscribe [:thm.game-ui/track-var primary-role ["nc" role "purple"]]))
     [icon-img :card-blue] (span-n-cards @(subscribe [:thm.game-ui/track-var primary-role ["nc" role "blue"]]))
     (space) (space)
     (for [figure-id @(subscribe [:thm.game-ui/get-all-figures-in-space primary-role [role "workers"]])]
       ^{:key figure-id}
       [:span.worker-container
        (figure-image figure-id)])

     ])

  (sidebar-global-info [_ primary-role]
    (html-entity "nbsp"))

  (compact-player-plaque [_ primary-role role player-name usernames]
    (stub "compact-player-plaque" primary-role role player-name usernames))

  (compact-game-container [_ primary-role]
    (stub "compact-game-container" primary-role))

  (your-hand [_ primary-role]
    (stub "your-hand" primary-role))

  (has-left-hand-of-choose-mode-modal? [_ req]
    false)

  (left-hand-of-choose-mode-modal [_ req]
    (stub "left-hand-of-choose-mode-modal" req))

  (extended-player-plaque-info [_ modal-data]
    (stub "extended-player-plaque-info" modal-data))

  (modal-content [_ modal-data primary-role]
    (if (contains? discard-zones (:kind modal-data))
      (discard-modal-content primary-role (:kind modal-data))
      [:div "Unrecognized kind of modal-data: " modal-data]))

  (get-text [_ text-descr]
    (let [{:keys [text-code text-args]} (parse-text-descr text-descr)
          fn (get viti-texts/fns text-code)]
      (apply fn text-args)))

  (get-pane-associated-with-request [_ your-request your-role]
    nil)

  (get-pane-associated-with-delta [_ delta your-role]
    nil)

  (get-animation-kind-for-zone-name [_ zone-name your-role]
    (cond (= zone-name (str your-role ":hand")) :v-row
          (contains? #{"green-deck" "yellow-deck" "purple-deck" "blue-deck"} zone-name) :i-non-holding-place ;; XXX something else
          (contains? discard-zones zone-name) :v-stack
          (re-matches #".*:f[567]" zone-name) :v-row ;; or vertical row
          :else nil))

  (get-delay-of-var [_ var-name]
    (get-delay-of-var var-name))

  (get-delay-of-vars [_ var-names]
    (when (some #(get-delay-of-var %) var-names)
      :short))

  (state-timing-for-humans [_ state]
    ((:timing viti-texts/fns)
     (state-timing state)))

  (get-play-areas-associated-with-request [_ request primary-role]
    (when-let [[kind text-descr & args] request]
      (case kind
        :choose-space
        (let [[spaces options] args]
          (->> spaces
               (map viti-get-play-area-of-space)
               (into #{})))

        :choose-spaces
        (let [[spaces options] args]
          ;; Cheating: the only place where this request happens is to choose
          ;; grapes.
          (vineyard-play-area-of-role primary-role))

        :choose-spaces-from-bags
        (let [[bags] args]
          (into #{}
                (for [bag bags
                      :let [spaces (:spaces bag)]
                      space spaces]
                  (viti-get-play-area-of-space space))))

        :choose-several-spaces-from-variants
        (let [[variants] args]
          ;; This is cheating a bit because we know that this request always
          ;; applies on your vineyard, so we don't analyze the args.
          (vineyard-play-area-of-role primary-role))

        :choose-mode
        (let [[modes] args
              {:keys [text-code text-args]} (parse-text-descr text-descr)]
          (case text-code
            :choose-papa-effect
            "your-hand"

            (:choose-wine-recipe-to-apply :choose-sgf-mode)
            (vineyard-play-area-of-role primary-role)

            nil))

        :choose-card
        (let [[zone->cids] args]
          (->> (keys zone->cids)
               (map #(viti-get-play-area-of-zone % primary-role))
               (into #{})))

        :choose-cards-flexible
        (let [[zone-name cids options] args]
          (viti-get-play-area-of-zone zone-name primary-role))

        nil)))

  (get-play-area-associated-with-delta [_ delta your-role]
    (let [[kind & args] delta]
      (case kind
        :figure-moved (let [[from to figure-id from-index to-index options] args]
                        (viti-get-play-area-of-space to))
        :var-changed (let [[var-name amount new-value] args]
                       (if (re-matches ANIMATED-VAR-REGEXP var-name)
                         "board"
                         nil))
        (:cards-moved-iv :cards-moved-vv) (let [[from to cids] args]
                                            (viti-get-play-area-of-zone to your-role))
        nil)))

  (on-var-set [_ primary-role var-name value]
    (when (and (= var-name "setting-up-mamapapas")
               (= value false))
      (dispatch [:thm.game-ui/on-play-area-tab-hidden primary-role "mamapapa" "board"])))

  game-ui-module/GameUiWithSpaces

  (get-space-visibility [_ space-name primary-role]
    (if (re-matches #".*(:supply|:source|f[567])" space-name)
      :invisible
      :visible))

  (get-play-area-of-space [_ space-name]
    (viti-get-play-area-of-space space-name))

  (get-replaced-from-space-name [_ to-space-name]
    (let [[owner _name] (split-multipart to-space-name)]
      (if (= owner "m")
        "m:source"
        (str owner ":supply"))))
  )

(register-module GAME-CODE (VitiUiModule.))
