(ns thm.core
  (:gen-class)
  (:require [thm.systems :refer [prod-system]]
            [system.repl :refer [set-init! start]]
            [taoensso.timbre :refer [debug]]
            [thm.game-engine.all-game-modules]))

(defn -main
  "Start a production system, unless a system is passed as argument (as in the dev-run task)."
  [& args]
  (let [candidate (first args)
        system (condp #(%1 %2) candidate
                    fn? candidate
                    var? candidate
                    #'prod-system)]
    (set-init! system)
    (start)
    (debug "Server started")))
