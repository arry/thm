-- Deploy sqitch/sqitch.plan:room_messages to pg

BEGIN;

create table room_messages (
   room_id text not null,
   created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
   message_code text not null,
   args jsonb null
);

COMMIT;
