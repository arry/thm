-- Revert sqitch/sqitch.plan:users from pg

BEGIN;

drop table users cascade;
drop table pending_registrations;
drop table password_recoveries;

COMMIT;
