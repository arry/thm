* Thaumaturgy

A discipline that powers Chantry: a site for playing board games online.

Adapted from the Holy Grail example (https://github.com/danielsz/holygrail).

This workflow is built on top of [[http://boot-clj.com/][Boot]], a build tool, and [[https://github.com/danielsz/system/tree/master/examples/boot][system]], a component
library.

** Dependencies

- Postgres
- Redis
- Clojure (http://clojure.org/) and Boot (http://boot-clj.com/)
- sqitch

Optionally, you might want to create a Mailgun account.

** What to do after cloning
*** config
Copy `config/config.edn.example` to `config/config.edn`, and modify to
taste (the latter file is ignored in the source control).  In particular, enter
your Mailgun credentials in case you want email sending to work.

*** user.clj
Create a file at `src/clj/thm/user.clj' with the following contents:

#+BEGIN_SRC clojure

(ns thm.user
  (:use boot.user thm.td-engine))

#+END_SRC

You can use that file to add any code that helps you in development.  It's not
tracked by source control either, so put whatever you wish in there.

*** Database
Create a database and the user:

#+BEGIN_SRC shell

$ sudo createdb thm

$ sudo -u postgres createuser thm -P

#+END_SRC

(Input `thm' in the password prompt.)

Install sqitch migration manager.  (On Ubuntu, it's packaged for apt.)

Run migrations to create necessary tables:

#+BEGIN_SRC shell

$ cd sqitch
$ sqitch deploy

#+END_SRC

** Principle of operation
*** Manual
You want to operate with a REPL, start the system, and reset it when you make
changes.

Start the build pipeline:

#+BEGIN_SRC shell
$ boot dev
#+END_SRC

 Pay attention to the line that says:
#+BEGIN_SRC shell
nREPL server started on port 49722 on host 127.0.0.1 - nrepl://127.0.0.1:49722
#+END_SRC

Launch your favorite editor, and connect to the headless REPL (~M-x
cider-connect~ in Emacs works great). Then type:
#+BEGIN_SRC clojure
(go)
#+END_SRC

The "old" chantry-games website is started and listening at
[[http://localhost:3001]]. When you make changes that require a system restart,
type in the REPL:

#+BEGIN_SRC clojure
(reset)
#+END_SRC

** Deployment

#+BEGIN_SRC shell
$ boot build
$ java -jar -Dhttp.port=8000 -Drepl.port=8001 target/project.jar
#+END_SRC
