(ns thm.client-ws-handler
  (:require [re-frame.core :as re-frame :refer (dispatch)]
            [taoensso.timbre :as timbre :refer-macros (tracef debugf infof warnf errorf debug)]))

(defmulti handler :id)

(defn handle-ws-message [{:as ev-msg :keys [id ?data event]}]
  (handler ev-msg))

(defmethod handler :default
  [{:as ev-msg :keys [event]}]
  (debugf "Unhandled event: %s" event))

(defmethod handler :chsk/state
  [{:as ev-msg :keys [?data]}]
  (let [[_old current] ?data]
    (dispatch [:ws-state-changed (:open? current)])))

(defmethod handler :chsk/handshake
  [{:as ev-msg :keys [?data]}]
  ;; Ignore
  )

(defmulti recv-handler first)

(defmethod handler :chsk/recv
  [{:as ev-msg :keys [?data]}]
  (debug "recv" ?data)
  (recv-handler ?data))

(defmethod recv-handler :default
  [[name data]]
  (dispatch [name data]))

(defmethod recv-handler :chsk/ws-ping
  [{:as ev-msg :keys [?data]}]
  ;; Ignore
  )
