-- Deploy sqitch/sqitch.plan:user_game_settings_on_last_creation to pg
-- requires: users

BEGIN;

alter table users add column game_settings_on_last_creation jsonb not null  default jsonb('{}');

COMMIT;
