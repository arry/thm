-- Verify sqitch/sqitch.plan:games on pg

BEGIN;

select gid, code, kind, status, created_at,
  owner_username, comment, game_settings, players, last_modified_at,
  runtime_info, outcome
from games
where false;

select gid, engine_checkpoint
from game_engine_checkpoints
where false;

ROLLBACK;
