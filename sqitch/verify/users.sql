-- Verify sqitch/sqitch.plan:users on pg

BEGIN;

select username, password_hash, email, created_at, has_avatar, last_seen_at
from users
where false;

select email, confirmation_code, is_confirmed
from pending_registrations
where false;

select email, recovery_code, created_at
from password_recoveries
where false;

ROLLBACK;
