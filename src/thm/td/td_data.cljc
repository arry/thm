(ns thm.td.td-data
  "Data for 13 Days."
  (:require [thm.utils :refer [map-cross denumber-id shorten-title]]))

(def battlegrounds {"l" {:title "Alliances" :track "w"}
                    "a" {:title "Atlantic" :track "m"}
                    "b" {:title "Berlin" :track "m"}
                    "c" {:title "Cuba (Military)" :track "m"}
                    "k" {:title "Cuba (Political)" :track "p"}
                    "i" {:title "Italy" :track "p"}
                    "v" {:title "Television" :track "w"}
                    "t" {:title "Turkey" :track "p"}
                    "u" {:title "United Nations" :track "w"}})

(def defcon-tracks {"m" "Military"
                    "p" "Political"
                    "w" "World Opinion"})

(def initial-defcon-values {"d:u:m" 0
                            "d:u:p" 1
                            "d:u:w" 0
                            "d:s:m" 1
                            "d:s:p" 0
                            "d:s:w" 0})

(def starting-cubes 15)

(def MAX-CUBES-IN-BG 5)

(def MAX-DEFCON 7)

(def initial-influence-values {"i:u:i" 1
                               "i:u:t" 1
                               "i:s:b" 1
                               "i:s:c" 1})

(def zones-data [{:name "letter",           :v :all,  :facing :up,   :per-player? true}
                 {:name "hand",             :v :you,                 :per-player? true}
                 {:name "agenda",           :v :you,  :facing :down, :per-player? true}
                 {:name "aftermath",        :v :you, :facing :down, :per-player? true}
                 {:name "in-play",          :v :all,  :facing :up                  }
                 {:name "strategy-deck",    :v :none, :facing :down, :ordered? true}
                 {:name "strategy-discard", :v :all,  :facing :up,   :ordered? true}
                 {:name "agenda-deck",      :v :none, :facing :down, :ordered? true}
                 {:name "agenda-discard",   :v :all,  :facing :up,   :ordered? true}
                 {:name "aftermath-reveal", :v :all,  :facing :up}])

(def common-zones ["play" "agenda" "discard" "deck"])

(def per-player-zones ["hand" "letter"])

(def both-roles ["u" "s"])

(def strategy-cards
  [{:number 1 :title "Speech to the Nation" :cubes 3 :alignment "n" :defcon false
    :text "Place up to 3 Influence cubes in total on one or more world opinion battlegrounds. Max 2 per battleground."}
   {:number 2 :title "The Guns of August" :cubes 3 :alignment "n" :defcon true
    :text "Escalate/deflate one of your DEFCON tracks by up to 2 steps. Then Command 1 Influence cube."}
   {:number 3 :title "Fifty-Fifty" :cubes 3 :alignment "n" :defcon false
    :text "The player with the most Influence cubes on the Television battleground may escalate/deflate two of their DEFCON tracks by 1 step (any mix)."}
   {:number 4 :title "SOPs" :cubes 1 :alignment "n" :defcon false
    :text "All your Command actions have +1 Influence cube for this round."}
   {:number 5 :title "Close Allies" :cubes 2 :alignment "n" :defcon false
    :text "Place up to 2 Influence cubes in total on one or more political battlegrounds."}
   {:number 6
    :title "Intelligence Reports"
    :cubes 2 :alignment "n" :defcon true
    :text "Draw one random Strategy card from your opponent’s hand. Play it as normal or discard it. Opponent draws a replacement card."}

   {:number 7
    :title "Summit Meeting"
    :cubes 2 :alignment "n" :defcon false
    :text "Discard any number of Strategy cards from your hand. Draw one Strategy card per card so discarded."}

   {:number 8
    :title "To the Brink"
    :cubes 2 :alignment "n" :defcon false
    :text "Play on opponent. All their Command actions have -1 Influence cube for this round (to a minimum of 1 Influence cube)."}

   {:number 9
    :title "Nuclear Submarines"
    :cubes 1 :alignment "n" :defcon false
    :text "Place up to 2 Influence cubes in total on one or more military battlegrounds."}

   {:number 10
    :title "U Thant"
    :cubes 1 :alignment "n" :defcon false
    :text "Deflate all your DEFCON tracks by 1 step."}

   {:number 11
    :title "Containment"
    :cubes 2 :alignment "n" :defcon false
    :text "Play on opponent. They can’t use Events from cards they played themselves to deflate their DEFCON tracks for this round."}

   {:number 12
    :title "A Face-Saver"
    :cubes 1 :alignment "n" :defcon true
    :text "Command 3 Influence cubes. Then opponent may Command 1 Influence cube."}

   {:number 13
    :title "Scramble"
    :cubes 3 :alignment "n" :defcon false
    :text "Place 1 Influence cube on each of up to three different battlegrounds."}

   {:number 14
    :title "Mathematical Precision"
    :cubes 3 :alignment "u" :defcon true
    :text "Escalate/deflate the US political DEFCON track by up to 2 steps. Then Command 1 Influence cube."}

   {:number 15
    :title "EXCOMM"
    :cubes 3 :alignment "u" :defcon true
    :text "Place up to 4 Influence cubes in total on battlegrounds where the US player currently has no Influence cubes. Max 2 per battleground."}

   {:number 16
    :title "Public Protests"
    :cubes 3 :alignment "u" :defcon false
    :text "Remove any number of US Influence cubes from any one battleground."}

   {:number 17
    :title "Lessons of Munich"
    :cubes 3 :alignment "u" :defcon false
    :text "Place up to 4 Influence cubes in total on Berlin, Italy, and Turkey battlegrounds. Max 2 per battleground."}

   {:number 18
    :title "Operation Mongoose"
    :cubes 2 :alignment "u" :defcon false
    :text "US gains 1 Prestige. Then USSR may escalate/deflate a US DEFCON track by 1 step."}

   {:number 19
    :title "Air Strike"
    :cubes 2 :alignment "u" :defcon false
    :text "EITHER remove half the USSR Influence cubes from one Cuba battleground (rounded up) OR place up to 2 Influence cubes on the Alliances battleground."}

   {:number 20
    :title "Non-Invasion Pledge"
    :cubes 2 :alignment "u" :defcon false
    :text "Remove up to 2 USSR Influence cubes from the Turkey battleground. Then escalate/deflate the US political DEFCON track by up to 2 steps."}

   {:number 21
    :title "Offensive Missiles"
    :cubes 2 :alignment "u" :defcon false
    :text "If US political DEFCON track is in the DEFCON 3 area, place up to 1 Influence cube on all political battlegrounds."}

   {:number 22
    :title "Invasion of Cuba"
    :cubes 1 :alignment "u" :defcon false
    :text "Escalate the US military DEFCON track by up to 2 steps. You may then deflate another US DEFCON track by the same number of steps."}

   {:number 23
    :title "Quarantine"
    :cubes 1 :alignment "u" :defcon false
    :text "Place up to 2 Influence cubes on the Atlantic battleground."}

   {:number 24
    :title "U-2 Photographs"
    :cubes 1 :alignment "u" :defcon true
    :text "Command 3 Influence cubes on to one military battleground."}

   {:number 25
    :title "Wave and Smile"
    :cubes 2 :alignment "u" :defcon false
    :text "Remove up to 2 US Influence cubes in total from one or more battlegrounds. Place them on other battlegrounds."}

   {:number 26
    :title "Eyeball to Eyeball"
    :cubes 1 :alignment "u" :defcon true
    :text "If US is more escalated than USSR on the military DEFCON track, place up to 3 Influence cubes in total on one or both Cuba battlegrounds."}

   {:number 27
    :title "MRBMs & IRBMs"
    :cubes 3 :alignment "s" :defcon true
    :text "Escalate/deflate the USSR military DEFCON track by up to 2 steps. Then Command 1 Influence cube."}

   {:number 28
    :title "Moscow is our Brain"
    :cubes 3 :alignment "s" :defcon true
    :text "Place up to 4 Influence cubes in total on battlegrounds where the USSR player currently has Influence cubes. Max 2 per battleground."}

   {:number 29
    :title "Missile Trade"
    :cubes 3 :alignment "s" :defcon true
    :text "Remove up to 3 USSR Influence cubes in total from one or more battlegrounds."}

   {:number 30
    :title "Fidel Castro"
    :cubes 3 :alignment "s" :defcon false
    :text "Place up to 3 Influence cubes in total on one or both Cuba battlegrounds."}

   {:number 31
    :title "Berlin Blockade"
    :cubes 2 :alignment "s" :defcon false
    :text "USSR gains 2 Prestige. Then US player may escalate/deflate a USSR DEFCON track by up to 2 steps."}

   {:number 32
    :title "Suez-Hungary"
    :cubes 2 :alignment "s" :defcon false
    :text "Keep placing 1 USSR Influence cube on the Italy battleground until the USSR runs out, reaches 5, or has one more Influence cube there than the US player."}

   {:number 33
    :title "Maskirovka"
    :cubes 2 :alignment "s" :defcon false
    :text "If USSR military DEFCON track is in the DEFCON 3 area, place up to 1 Influence cube on all military battlegrounds."}

   {:number 34
    :title "Bay of Pigs"
    :cubes 2 :alignment "s" :defcon false
    :text "Play on opponent. They EITHER remove 2 Influence cubes from the Alliances battleground OR they can’t play Events to deflate their DEFCON tracks for this round."}

   {:number 35
    :title "Turn Back the Ships"
    :cubes 1 :alignment "s" :defcon false
    :text "Deflate the most escalated USSR DEFCON track by up to 2 steps (if tied, pick one)."}

   {:number 36
    :title "Strategic Balance"
    :cubes 1 :alignment "s" :defcon true
    :text "Place up to 3 Influence cubes on the Atlantic battleground."}

   {:number 37
    :title "National Liberation"
    :cubes 1 :alignment "s" :defcon true
    :text "Command 3 Influence cubes on to one political battleground."}

   {:number 38
    :title "U-2 Downed"
    :cubes 2 :alignment "s" :defcon false
    :text "Place up to 2 Influence cubes on the Turkey battleground. Remove half the US Influence cubes from one Cuba battleground (rounded up)."}

   {:number 39
    :title "Defensive Missiles"
    :cubes 1 :alignment "s" :defcon true
    :text "Place up to 2 Influence cubes in total on the Television and United Nations battlegrounds."}
   ])

(def personal-letter-card
  {:title "Personal Letter"
   :text "(You may play it alongside a Strategy card played for Command to increase its Influence by 1. Then pass the Personal Personal Letter to your opponent.)"})

(def agenda-cards (flatten
                   (concat
                    (map (fn [index]
                           [{:title "Military"
                             :type "track"
                             :letter "m"
                             :defcon true
                             :bonus 1
                             :duplicate-number (inc index)
                             }
                            {:title "Political"
                             :type "track"
                             :letter "p"
                             :defcon true
                             :bonus 1
                             :duplicate-number (inc index)
                             }
                            {:title "World Opinion"
                             :type "track"
                             :letter "w"
                             :defcon true
                             :bonus 1
                             :duplicate-number (inc index)
                             }]) (range 2))
                    [{:title "Turkey"
                      :type "bg"
                      :letter "t"
                      :bonus 0}
                     {:title "Berlin"
                      :type "bg"
                      :letter "b"
                      :bonus 1}
                     {:title "Italy"
                      :type "bg"
                      :letter "i"
                      :bonus 1}
                     {:title "Cuba (Political)"
                      :type "bg"
                      :letter "k"
                      :bonus :x
                      :neighbors ["c" "a"]}
                     {:title "Cuba (Military)"
                      :type "bg"
                      :letter "c"
                      :bonus :x
                      :neighbors ["k" "a"]}
                     {:title "Atlantic"
                      :type "bg"
                      :letter "a"
                      :bonus :x
                      :neighbors ["c" "k"]}
                     {:title "Personal Letter"
                      :type "card"
                      :letter "u"
                      :bonus 2}])))

(defn cid-from-design [design]
  (str
   (shorten-title (:title design))
   (when-let [n (:duplicate-number design)]
     (str ":" n))))

(def all-agenda-cids
  (map cid-from-design agenda-cards))

(def all-strategy-cids
  (map cid-from-design strategy-cards))

(def personal-letter-cid "pl")

(defn zone-props-by-name [zone-name]
  ;; TODO cache the result
  (let [zone-props (fn [sublist]
                     (zipmap (map :name sublist) sublist))
        per-player (filter :per-player? zones-data)
        per-player-map (into {} (map-cross (fn [r zp] [(str r ":" (:name zp)) (assoc zp :role r)])
                                           both-roles per-player))
        prop-by-name (merge
                      (zone-props (remove :per-player? zones-data))
                      per-player-map)]
    (prop-by-name zone-name)))

(def card-map (into {} (map (fn [design] [(cid-from-design design) design])
                            (concat strategy-cards agenda-cards))))

(defn card-design-by-id [id]
  (get card-map id))

(def denumbered-card-map (into {} (map (fn [design] [(denumber-id (cid-from-design design)) design])
                                       (concat strategy-cards agenda-cards))))

(defn card-design-by-denumbered-id [id]
  (get denumbered-card-map id))

(do
  #?@(:cljs
      [(def SIZES {:map {:w 1658 :h 1158}
                   :disc {:w 69 :h 48}
                   :flag {:w 68 :h 68}})

       (def markers
         [{:start {:x 724 :y 74} :finish {:x 764 :y 114} :name "pr-5" :hide? true}
          {:start {:x 776 :y 75} :finish {:x 816 :y 115} :name "pr-4" :hide? true}
          {:start {:x 829 :y 115} :finish {:x 869 :y 75} :name "pr-3" :hide? true}
          {:start {:x 881 :y 116} :finish {:x 921 :y 76} :name "pr-2" :hide? true}
          {:start {:x 933 :y 76} :finish {:x 973 :y 116} :name "pr-1" :hide? true}
          {:start {:x 986 :y 77} :finish {:x 1026 :y 117} :name "pr0" :hide? true}
          {:start {:x 1037 :y 77} :finish {:x 1078 :y 118} :name "pr1" :hide? true}
          {:start {:x 1090 :y 78} :finish {:x 1130 :y 118} :name "pr2" :hide? true}
          {:start {:x 1142 :y 119} :finish {:x 1182 :y 79} :name "pr3" :hide? true}
          {:start {:x 1194 :y 79} :finish {:x 1235 :y 120} :name "pr4" :hide? true}
          {:start {:x 1246 :y 80} :finish {:x 1287 :y 120} :name "pr5" :hide? true}

          {:start {:x 1019 :y 773} :finish {:x 1300 :y 961} :name "l" :bg? true}
          {:start {:x 427 :y 593} :finish {:x 707 :y 400}   :name "a" :bg? true}
          {:start {:x 732 :y 372} :finish {:x 1013 :y 174}  :name "b" :bg? true}
          {:start {:x 198 :y 733} :finish {:x 475 :y 926}   :name "c" :bg? true}
          {:start {:x 95 :y 385} :finish {:x 377 :y 580}    :name "k" :bg? true}
          {:start {:x 1005 :y 419} :finish {:x 1283 :y 608} :name "i" :bg? true}
          {:start {:x 583 :y 856} :finish {:x 861 :y 1045}  :name "v" :bg? true}
          {:start {:x 1048 :y 371} :finish {:x 1327 :y 180} :name "t" :bg? true}
          {:start {:x 690 :y 608} :finish {:x 971 :y 796}   :name "u" :bg? true}

          {:start {:x 123 :y 1018} :finish {:x 174 :y 1070} :name "r1" :hide? true}
          {:start {:x 193 :y 1013} :finish {:x 237 :y 1056} :name "r2" :hide? true}
          {:start {:x 259 :y 999} :finish {:x 309 :y 1048} :name "r3" :hide? true}
          {:start {:x 320 :y 960} :finish {:x 421 :y 1061} :name "ra" :hide? true}

          {:start {:x 18 :y 742} :finish {:x 95 :y 1000} :name "u:agenda"}
          {:start {:x 1031 :y 1049} :finish {:x 1283 :y 1141} :name "af"}
          {:start {:x 1554 :y 63} :finish {:x 1641 :y 308} :name "s:agenda"}

          {:start {:x 1379 :y 469} :finish {:x 1440 :y 525} :name "m" :hide? true}
          {:start {:x 1440 :y 525} :finish {:x 1502 :y 463} :name "p" :hide? true}
          {:start {:x 1502 :y 463} :finish {:x 1568 :y 523} :name "w" :hide? true}

          {:start {:x 1464 :y 1019} :finish {:x 1408 :y 1084} :name "m0" :track "m"}
          {:start {:x 1402 :y 959} :finish {:x 1464 :y 1019}  :name "m1" :track "m"}
          {:start {:x 1459 :y 893} :finish {:x 1402 :y 959}   :name "m2" :track "m"}
          {:start {:x 1395 :y 834} :finish {:x 1458 :y 893}   :name "m3" :track "m"}
          {:start {:x 1452 :y 768} :finish {:x 1395 :y 834}   :name "m4" :track "m"}
          {:start {:x 1389 :y 709} :finish {:x 1452 :y 768}   :name "m5" :track "m"}
          {:start {:x 1447 :y 644} :finish {:x 1389 :y 709}   :name "m6" :track "m"}
          {:start {:x 1444 :y 580} :finish {:x 1385 :y 646}   :name "m7" :track "m"}

          {:start {:x 1467 :y 1081} :finish {:x 1529 :y 1016} :name "p0" :track "p"}
          {:start {:x 1529 :y 1016} :finish {:x 1461 :y 956}  :name "p1" :track "p"}
          {:start {:x 1461 :y 956} :finish {:x 1523 :y 891}   :name "p2" :track "p"}
          {:start {:x 1523 :y 891} :finish {:x 1456 :y 831}   :name "p3" :track "p"}
          {:start {:x 1456 :y 831} :finish {:x 1517 :y 766}   :name "p4" :track "p"}
          {:start {:x 1517 :y 766} :finish {:x 1450 :y 706}   :name "p5" :track "p"}
          {:start {:x 1450 :y 706} :finish {:x 1510 :y 641}   :name "p6" :track "p"}
          {:start {:x 1510 :y 641} :finish {:x 1444 :y 580}   :name "p7" :track "p"}

          {:start {:x 1529 :y 1016} :finish {:x 1596 :y 1076} :name "w0" :track "w"}
          {:start {:x 1589 :y 951} :finish {:x 1529 :y 1016}  :name "w1" :track "w"}
          {:start {:x 1523 :y 891} :finish {:x 1589 :y 951}   :name "w2" :track "w"}
          {:start {:x 1582 :y 826} :finish {:x 1523 :y 891}   :name "w3" :track "w"}
          {:start {:x 1517 :y 766} :finish {:x 1582 :y 826}   :name "w4" :track "w"}
          {:start {:x 1576 :y 701} :finish {:x 1517 :y 766}   :name "w5" :track "w"}
          {:start {:x 1510 :y 641} :finish {:x 1576 :y 701}   :name "w6" :track "w"}
          {:start {:x 1571 :y 575} :finish {:x 1510 :y 641}   :name "w7" :track "w"}
          ])

       (def marker-by-name
         (into {} (map (fn [p]
                         [(:name p) p])
                       markers)))

       (def visible-markers (remove :hide? markers))

       (defn marker-coords
         "Find coordinates of a point to which a top-left corner of a rect should be
  placed so that the rect is centered at marker's center."
         [marker-name & [rect]]
         (let [rect (or rect {:w 0, :y 0})
               {:keys [start finish]} (get marker-by-name marker-name)
               trans (fn [coord dim]
                       (let [coord1 (min (coord start) (coord finish))
                             coord2 (max (coord start) (coord finish))]
                         (+ coord1
                            (/ (- coord2 coord1 (dim rect))
                               2))))]
           {:x (trans :x :w)
            :y (trans :y :h)}))

       ]))

(def td-options
  [{:type :selection,
    :name :side-selection,
    :display-name "Your side:",
    :default "random",
    :values
    [{:display-name "Random side",
      :value "random",
      :choice-display-name "Random"}
     {:display-name "Owner is US",
      :value "owner-u",
      :choice-display-name "United States"}
     {:display-name "Owner is USSR",
      :value "owner-s",
      :choice-display-name "Soviet Union"}]}])

(def SEPARATE-AFTERMATH-VERSION
  "Before this version, aftermath was a single zone.  From this version,
  aftermath is two separate zones so that the user can review which cards he or
  she has saved for aftermath."
  2)

(def CHOOSE-CARD-AND-MODE-VERSION
  "From this version, for playing the card, :choose-card-and-mode request is issued,
  for which you answer a pair [cid mode]; mode is optional (cljs client will
  have minimal changes and send nil; while the js client will send the mode);
  if not provided the :choose-mode will be sent as before.
  "
  3)
