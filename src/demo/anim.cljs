(ns demo.anim
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [reagent.core :as r]
            [taoensso.timbre :refer-macros (debug warn warnf)]
            [thm.views.common :refer [indexed-component flip-move]]

            ))

(def initial-value {:normal [:foo :bar :baz]
                    :promoted [:frob]})

(def *items (r/atom initial-value))

(defn vec-remove [v i]
  (vec (concat (subvec v 0 i) (subvec v (inc i)))))

(defn vec-remove-value [v value]
  (let [index (.indexOf v value)]
    (vec-remove v index)))

(defn vec-add-at [v value new-index]
  (if (= new-index (count v))
    (conj v value)
    (vec (concat (subvec v 0 new-index) [value] (subvec v new-index)))))

(defn jsdebug [& args]
  (apply js/console.debug args))

(def tracker (r/atom {}))

(def out-tracker (r/atom {}))

(defn get-bounding-client-rect [this]
  (-> this r/dom-node .getBoundingClientRect))

(defn animate-it [node old-rect new-rect]
  (let [dx (- (.-x old-rect) (.-x new-rect))
        dy (- (.-y old-rect) (.-y new-rect))]
    (when (not (and (zero? dx) (zero? dy)))
      (aset (.-style node)
            "transform"
            (str "translate(" dx "px," dy "px)"))
      (aset (.-style node)
            "transition" "")

      (r/next-tick (fn []
                     (aset (.-style node)
                           "transform" "")
                     (aset (.-style node)
                           "transition" "transform 0.5s")
                     )))))

(defn volant-item [my-props cid]
  (let [on-click (get my-props :on-click)]
    (r/create-class
     {:display-name "volant-item"

      :component-did-mount (fn [this & args]
                             (jsdebug ":component-did-mount" cid this (get-bounding-client-rect this) (.-style (r/dom-node this)))
                             (when-let [old-rect (get @out-tracker cid)]
                               (swap! out-tracker dissoc cid)
                               (let [node (r/dom-node this)
                                     new-rect (.getBoundingClientRect node)]
                                 (animate-it node old-rect new-rect)))

                             #_(apply jsdebug ":component-did-mount" cid args))

      :component-will-update (fn [this & args]
                               (jsdebug ":component-will-update"  args)
                               ;;(apply jsdebug ":get-snapshot-before-update" cid args)
                               (let [rect (get-bounding-client-rect this)]
                                   (swap! tracker assoc cid rect))

                               )

      :component-did-update (fn [this & args]
                              (let [node (r/dom-node this)
                                    new-rect (.getBoundingClientRect node)
                                    old-rect (get @tracker cid)
                                    ]
                                (when (and new-rect old-rect)
                                  (animate-it node old-rect new-rect)


                                  )
                                ))

      :component-will-unmount (fn [this & args]
                                (apply jsdebug ":component-will-unmount" cid args)
                                (let [rect (get-bounding-client-rect this)]
                                  (swap! out-tracker assoc cid rect))
                                )

      :reagent-render (fn [my-props cid]
                        [:div.item {:on-click #(when on-click
                                                 (on-click cid))
                                    :style {:width "100px"
                                            :height "100px"
                                            :outline "1px solid black"
                                            :background-color "magenta"
                                            :float "left"
                                            :margin-right "2em"}}
                         (str cid)
                         ])

      })))

(defn volant-container [my-props & contents]
  (let []
    (r/create-class
     {:component-will-update (fn [this & args]
                               (debug "container :component-will-update"  args)

                               )

      :component-did-update (fn [this & args]
                              (debug "container :component-did-update"  args)
                              )


      :reagent-render (fn [my-props & contents]
                        [:div.volant.clearfix contents])})))

(defn main []
  (fn []
    [:div.container
     [:div.row
      [:div.col
       [:div.item-container.my-4
        [volant-container {}
         (indexed-component (:normal @*items)
                            (fn [index iter]
                              [volant-item {:key iter
                                           :on-click #(swap! *items update :normal vec-remove-value %)} iter]))]
        ]

       [:hr.my-4]
       [:div.item-container.my-4
        [volant-container {}
         (indexed-component (:promoted @*items)
                            (fn [index iter]
                              [volant-item {:key iter
                                           :on-click #(swap! *items update :promoted vec-remove-value %)} iter]))]]
       ]

      [:div.col
       [:div
        [:button.btn.btn-light {:on-click #(swap! *items update :normal conj (keyword (gensym "qux")))}
         "Add a qux"]
        [:button.btn.btn-light {:on-click #(swap! *items update :promoted conj (keyword (gensym "promoqux")))}
         "Add a promoted qux"]
        [:button.btn.btn-light {:on-click #(reset! *items initial-value)}
         "Reset"]
        [:button.btn.btn-light {:on-click (fn []
                                            (let [size (count (:normal @*items))
                                                  that-size (count (:promoted @*items))]
                                              (when (pos? size)
                                                (let [index (rand-int size)
                                                      value (get (:normal @*items) index)
                                                      new-index (rand-int (inc that-size))]
                                                  (swap! *items #(-> %
                                                                     (update :normal vec-remove index)
                                                                     (update :promoted vec-add-at value new-index))))))

                                            )}
         "Promote random"]
        ]
       ]]]))
