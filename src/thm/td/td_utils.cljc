(ns thm.td.td-utils
  "Helper function to deal with 13 Days game state."
  (:require [thm.utils :refer [denumber-id first-key n-cards-in-zone get-var
                               in-spread? all-cards-in-zone]]
            [thm.td.td-data :as data :refer [both-roles defcon-tracks]]
            [clojure.string :as str]
            #?@(:clj [[taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug warn)]
                      ]

                :cljs [[taoensso.timbre :refer-macros (debug warn)]

                       ])))

(defn other-role [role]
  (if (= role (first both-roles))
    (second both-roles)
    (first both-roles)))

(defn defcon-area-from-level [val]
  (condp >= val
    2 3
    5 2
    1))

(defn defcon-level [cs role t]
  (get-var cs ["d" role t]))

(defn get-allowed-command-spread [state role bg cubes mode]
  (let [reserve (get-var state ["c" role])
        inf (get-var state ["i" role bg])
        lower (if (= mode :onto)
                0
                (- (min cubes inf)))
        higher (min cubes reserve (- data/MAX-CUBES-IN-BG inf))]
    [lower higher]))

(defn within-allowed-command-spread?
  "Return true if change is in the spread allowed by cubes and existing
  influence cubes of the player by role in the battleground bg."
  [state role bg cubes change mode]
  (let [[lower higher] (get-allowed-command-spread state role bg cubes mode)]
    (in-spread? change lower higher)))

(defn get-defcon-area [state role track]
  (defcon-area-from-level
    (get-var state ["d" role track])))

(defn get-war-reason [state role]
  (let [areas (map #(get-defcon-area state role %) (keys defcon-tracks))]
    (cond
      (some #{1} areas) :any-marker-in-1-area
      (every? #{2} areas) :all-markers-in-2-area
      :else nil)))

(defn good-for-war? [state role]
  (not (nil? (get-war-reason state role))))

(defn good-answer-for-choose-place-influence? [state role req-args bg->amount]
  (and bg->amount
       (let [[max max-per _defcon? allowed-bgs exact-amount?] req-args
             bgs (keys bg->amount)
             amounts (vals bg->amount)
             sum (reduce + amounts)]
         (and
          (distinct? bgs)
          (every? #(contains? allowed-bgs %) bgs)
          (every? (fn [[bg amount]]
                    (<= (+ (get-var state ["i" role bg]) amount) data/MAX-CUBES-IN-BG))
                  bg->amount)
          (every? pos? amounts)
          (if max-per
            (every? #(<= % max-per) amounts)
            true)
          (if exact-amount?
            (= sum max)
            (<= sum max))
          (<= sum (get-var state ["c" role]))))))

(defn good-answer-for-choose-remove-influence? [state role req-args bg->amount]
  (and bg->amount
       (let [[whose max policy _defcon? allowed-bgs] req-args
             bgs (keys bg->amount)
             amounts (vals bg->amount)
             sum (reduce + amounts)]
         (and
          (distinct? bgs)
          (every? #(contains? allowed-bgs %) bgs)
          (every? pos? amounts)
          (every? (fn [[bg amount]]
                    (<= amount (get-var state ["i" whose bg])))
                  bg->amount)
          (case policy
            (:single-bg, :half) (<= (count bgs) 1)
            true)
          (<= sum max)))))

(defn good-answer-for-choose-change-defcon? [state role req-args track->amount]
  (and track->amount
       (let [[whose max-tracks allowed-tracks lower higher exact-amount?] req-args]
         (and
          (<= (count track->amount) max-tracks)
          (distinct? (keys track->amount))
          (every? #(contains? allowed-tracks %) (keys track->amount))
          (if exact-amount?
            (every? (fn [[t amount]]
                      (or (zero? amount)
                          (let [cur (get-var state ["d" whose t])
                                actual-lower (max lower (- cur))
                                actual-higher (min higher (- data/MAX-DEFCON cur))]
                            (or
                             (= amount actual-lower)
                             (= amount actual-higher)))))
                    track->amount)
            (every? #(in-spread? % lower higher) (vals track->amount)))))))

(defn state-timing [state]
  {:round (get-var state "round")
   :phase (get-var state "phase")
   :turn (get-var state "turn")
   :active-player (get-var state "active-player")})

(do
  #?@(:clj [
            (defn has-effect? [state role effect]
              (some #{effect} (get-var state ["ef" role])))

            (defn contained?
              "Return whether the player by role r is affected by Containment
  or the like effect of Bay of Pigs; optionally for an event that requires to
  change `whose` defcon."
              [state r & [whose]]
              (and (if whose (= whose r) true)
                   (or (and (has-effect? state r "contai")
                            (= r (get-var state "active-player")))
                       (has-effect? state r "baofpi"))))
            ])

  #?@(:cljs
      [
       (defn n-cards-in-hand [cs role]
         (n-cards-in-zone cs [role "hand"]))

       (defn defcon-level-and-area [level]
         (let [area (defcon-area-from-level level)]
           (str level "(" area ")")))

       (defn get-track-board-widget-value [name]
         (js/parseInt (subs name 1)))

       (defn get-your-agenda [cs]
         (let [cards (all-cards-in-zone cs [(:your-role cs) "agenda"])]
           (when (seq cards)
             (-> (first cards)
                 :cid
                 denumber-id
                 data/card-design-by-denumbered-id
                 :letter))))
       ]))
