(ns thm.game-ui.ui-module-registry)

(def modules (atom {}))

(defn register-module [code instance]
  (swap! modules assoc code instance))

(defn get-module [code]
  (get @modules code))
