(ns thm.tm-tests
  (:use
        thm.tm.tm-engine
        thm.tm.tm-data
        thm.game-engine.game-base)
  (:require
   [thm.utils :refer [all-cards-in-zone get-var get-request-for-role seq-contains?
                      n-cards-in-zone
                      ]]
   [taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug spy)]
   [thm.game-engine.game-engine :refer [player-role]]))

(defn new-game* []
  (-> (new-game ["John"] {:seed 1})
      (assoc :pending-actions [])))


;; helper for repl testing:
;; (use 'thm.tm.tm-engine)
;; (-> (state-after-pending-actions (new-game ["J"] {:seed 1})) (state-after-answer "credic") (state-after-answer ["asmico" "arcalg" "invcon" "mine" "carpro" "farmin"]))

;; (fact "Discounts"

;;       (discount-applies? (:b-discount (card-design-by-id "spasta"))
;;                          (:tags (card-design-by-id "phspha"))) => true

;;       )


;; (fact "Insulation"
;;       (let [st (-> (new-game*)
;;                    (state-after-action-sequence
;;                     [:set-var "s:p:h" 5]
;;                     [:set-var "s:a:m" 44]
;;                     [:move-card "deck" "s:hand" "insula"]
;;                     [:do-actions])

;;                    (state-after-answer "play-card1"))]
;;         (get-var st "s:a:m") => 42

;;         (get-request-for-role st "s") => (just
;;                                           [:choose-number [:choose-amount-of-production-to-convert "h" "m"] 0 5])

;;         (let [st (-> st
;;                      (state-after-answer 3))]
;;           (get-var st "s:p:h") => 2
;;           (get-var st "s:p:m") => 3)))

;; (fact "Business Contacts - look-take"
;;       (let [st (-> (new-game*)
;;                    (state-after-action-sequence
;;                     [:set-var "s:a:m" 44]
;;                     [:move-cards "deck" "s:hand" ["buscon"]]
;;                     [:move-cards "deck" "s:hand" ["insula"]]
;;                     [:do-actions])
;;                    (state-after-answer "play-card1"))]
;;         (get-var st "s:a:m") => 37

;;         (all-cards-in-zone st "s:hand") => (just ["insula" "powinf" "ioag" "asmico" "worms"])

;;         (get-request-for-role st "s") => (just
;;                                           [:choose-cards-flexible :choose-cards-to-take
;;                                            "s:hand" (just ["powinf" "ioag" "asmico" "worms"])
;;                                            {:higher 2, :lower 2}])

;;         (let [st (-> st (state-after-answer ["ioag" "worms"]))]
;;           (all-cards-in-zone st "s:hand") => (just ["insula" "ioag" "worms"]))

;;         ))

;; (fact "Nitrogen-Rich Asteroid: conditional formula"
;;       (let [st (-> (new-game*)
;;                    (state-after-action-sequence
;;                     [:set-var "s:a:m" 44]
;;                     [:move-cards "deck" "s:hand" ["nirias"]]
;;                     [:do-actions])
;;                    (state-after-answer "play-card1"))]
;;         (get-var st "s:p:p") => 1
;;         )

;;       (let [st (-> (new-game*)
;;                    (state-after-action-sequence
;;                     [:set-var "s:a:m" 44]
;;                     [:move-cards "deck" "s:hand" ["nirias"]]
;;                     [:move-cards "deck" "s:table-blue" ["ecozon"]]
;;                     [:move-cards "deck" "s:table-green" ["ecnp" "adalic"]]
;;                     [:do-actions])
;;                    (state-after-answer "play-card1"))]
;;         (get-var st "s:p:p") => 4))

;; (fact "Robotic Workforce"
;;       (let [st (-> (new-game*)
;;                    (state-after-action-sequence
;;                     [:set-var "s:a:m" 44]
;;                     [:move-cards "deck" "s:hand" ["robwor"]]
;;                     [:move-cards "deck" "s:table-green" ["cotrca" ;; imm=nil - cannot
;;                                                          "medlab" ;; inc-prod-formula - can
;;                                                          "aicen" ;; dec-prod "e" - cannot
;;                                                          "biocom" ;; inc-prod "m" - can
;;                                                          ]]

;;                     [:move-cards "deck" "s:table-blue" ["marrai" ;; imm doesn't have production box - cannot
;;                                                         "spaele"]] ;; inc-prod "t" - can

;;                     [:do-actions])
;;                    (state-after-answer "play-card1"))]

;;         (get-request-for-role st "s") => [:choose-card :choose-card-to-duplicate-with-robwor
;;                                           {"s:table-blue" ["spaele"]
;;                                            "s:table-green" ["medlab" "biocom"]}]

;;         (let [st (-> st
;;                      (state-after-answer "spaele"))]
;;           (get-var st "s:p:t") => 1)

;;         ))

;; (fact "Ecological zone and add-another"
;;       (let [st (-> (new-game*)
;;                    (state-after-action-sequence
;;                     [:set-var "s:a:m" 44]
;;                     [:set-var "s:a:h" 5]
;;                     [:move-cards "deck" "s:hand" ["lohetr"]]
;;                     [:move-cards "deck" "s:table-blue" ["ecozon"]]

;;                     [:do-actions])
;;                    (state-after-answer "play-card1"))]

;;         (get-request-for-role st "s") => [:choose-mode :choose-effect-to-resolve ["gain-4-p" "add-another-animals-2"]]

;;         (let [st (-> st
;;                      (state-after-answer "add-another-animals-2"))]
;;           (get-request-for-role st "s")
;;           => [:choose-card [:choose-card-to-add-resources "animals" 2] {"s:table-blue" ["ecozon"]}]

;;         )
;;       ))

;; (fact "Olympic Conference"
;;       (let [st (-> (thm.tm.tm-engine/new-game ["J"] {:seed 2})
;;                    (state-after-pending-actions)
;;                    (state-after-answer "thorga")
;;                    (state-after-answer ["olycon" "resear"])
;;                    (state-after-answer "play-card1")
;;                    (state-after-answer "add-this-science-1")
;;                    (state-after-answer "play-card1") )]
;;         (get-request-for-role st "s")
;;         => [:choose-mode :choose-effect-to-resolve ["add-this-science-1" "remove-1-science->draw-card"]]

;;         (let [st (state-after-answer st
;;                                      "remove-1-science->draw-card")]
;;           (get-request-for-role st "s")
;;           => [:choose-mode :choose-effect-to-resolve ["add-this-science-1"]]


;;         )))

;; (fact "Viral Enhancer shouldn't allow to add microbes to non-collecting microbe cards"
;;       (let [st (-> (thm.tm.tm-engine/new-game ["J"] {:seed 31})
;;                    (state-after-pending-actions)
;;                    (state-after-answer "teract")
;;                    (state-after-answer ["virenh" "archae"])
;;                    (state-after-answer "play-card1")
;;                    (state-after-answer "gain-1-p")
;;                    (state-after-answer "play-card1") )]
;;         (get-request-for-role st "s")
;;         => [:choose-mode :choose-effect-to-resolve ["gain-1-p"]]))

;; (fact "Robotic Workforce with Mining Area"
;;       (let [st (-> (thm.tm.tm-engine/new-game ["J"] {:seed 198})
;;                    (state-after-pending-actions)
;;                    (state-after-answer "intcin")
;;                    (state-after-answer ["minare" "robwor"])
;;                    (state-after-answer "greenery")
;;                    (state-after-answer 1)
;;                    (state-after-answer "play-card1")
;;                    (state-after-answer [0 2])
;;                    (state-after-answer 7)
;;                    (state-after-answer "pass")
;;                    (state-after-answer [])
;;                    )]
;;         (let [st (-> st
;;                      (state-after-answer "play-card1")
;;                      (state-after-answer "minare"))]

;;           (get-var st "s:p:s") => 2
;;           )

;;       ))
