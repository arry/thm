(ns thm.backend-components.carmine
  (:require [com.stuartsierra.component :as component]
            [taoensso.carmine :as car :refer (wcar)]))

(defrecord Carmine [conn-opts options]
  component/Lifecycle
  (start [component]
    (assoc component
           :conn-opts conn-opts))
  (stop [component]
    component))

(defmacro comp-wcar [component & body]
  `(car/wcar (:conn-opts ~component) ~@body))

(defn new-carmine
  ([conn-opts]
   (new-carmine conn-opts {}))
  ([conn-opts options]
   (map->Carmine {:conn-opts conn-opts :options options})))

(defn subscribe
  "Subscribe to channel with a handler function.  Returns a listener that could
  be passed to unsubscribe."
  [component channel handler]
  (car/with-new-pubsub-listener (:spec (:conn-opts component))
    {channel handler}
    (car/subscribe channel)))

(defn unsubscribe [listener]
  (car/close-listener listener))
