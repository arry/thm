(ns thm.subs
    (:require-macros [reagent.ratom :refer [reaction]])
    (:require [re-frame.core :refer [reg-sub subscribe]]
              [taoensso.timbre :as timbre :refer-macros (debug warn spy)]
              [thm.utils :refer [all-cards-in-zone
                                 seq-contains?
                                 finished?
                                 get-role->player-name
                                 get-ordered-roles
                                 get-var
                                 get-all-figures-in-space
                                 n-cards-in-zone
                                 multipart-name-from-descr
                                 get-your-request
                                 parse-text-descr
                                 first-index
                                 shift-so-it-starts-with
                                 good-answer-for-choose-spaces-from-bags?
                                 as-set
                                 ]]

              [clojure.set :refer [map-invert]]

              [re-frame.subs :refer (clear-all-handlers!)]

              [thm.game-ui.game-ui-module :as game-ui-module]
              [thm.game-ui.all-game-ui-modules :refer [module-from-cs]]

              ;; td-specific
              [thm.td.td-utils :refer [other-role
                                       good-answer-for-choose-place-influence?
                                       good-answer-for-choose-change-defcon?
                                       get-track-board-widget-value
                                       get-your-agenda
                                       state-timing
                                       good-for-war?]]
              [thm.utils :refer [forbid-widget-activation? half-round-up]]
              [thm.texts :as t]

              ;; viti-specific
              [thm.viti.viti-utils :as viti-utils]
              ))

(clear-all-handlers!)

(defn simple-toplevel-data
  "Register subscriber named `k` that just gets the top-level value out of db
  which is named `k` as well."
  [k]
  (reg-sub k
           (fn [db _]
             (get db k))))

(doseq [name [:page-loading? :page-desc :ws-open?
              :username
              :usernames
              :chat-messages
              :anti-forgery-token

              :loading?

              :thm.global/navbar-open?

              :thm.lobby/open-game-by-id, :thm.lobby/running-game-by-id
              :thm.lobby/create-game-error, :thm.lobby/active-tab
              :thm.lobby/game-settings-on-last-creation

              :thm.login/login-failure?
              :thm.view-game/game, :thm.view-game/controls

              :thm.game-ui/layout
              :thm.game-ui/client-states, :thm.game-ui/your-role
              :thm.game-ui/preview
              :thm.game-ui/play-menu-open?
              :thm.game-ui/ui-sizes
              :thm.game-ui.compact/active-pane, :thm.game-ui.compact/new-chat-message-alert?
              :thm.game-ui/chat-n-log-active-tab
              :thm.game-ui/play-area-active-tab

              :thm.home/active-game-by-id, :thm.home/past-game-by-id

              :thm.test-ui/scenario-names, :thm.test-ui/display-all-client-states-on-one-page

              :thm.play-game/game, :thm.play-game/active-gids,
              :thm.play-game/personal-notes-content

              :thm.signup/signup-error-message, :thm.signup/confirmation-code-valid?
              :thm.signup/confirm-error-messages, :thm.signup/confirmation-email-sent?

              :thm.reset-password/forgot-error-message, :thm.reset-password/reset-email-sent?
              :thm.reset-password/code-valid?, :thm.reset-password/error-messages
              :thm.reset-password/reset-successful?

              :thm.term/state

              :thm.account/settings-applied?, :thm.account/avatar-upload-result


              :thm.proxier/state

              ]]
  (simple-toplevel-data name))

(defn get-client-state [db]
  (get (:thm.game-ui/client-states db)
       (:thm.game-ui/your-role db)))

;; == game-ui subscriptions

(reg-sub :thm.game-ui/client-state-for
         (fn [_]
           (subscribe [:thm.game-ui/client-states]))
         (fn [client-states [_ role]]
           (get client-states role)))

(reg-sub :thm.game-ui/any-client-state
         (fn [_]
           (subscribe [:thm.game-ui/client-states]))
         (fn [client-states [_]]
           (val (first client-states))))

(def client-state-for-role-signal
  (fn [[_ role & _args]]
    (subscribe [:thm.game-ui/client-state-for role])))

(def any-client-state-signal
  (fn [_]
    (subscribe [:thm.game-ui/any-client-state])))

(reg-sub :thm.game-ui/roles-of-client-states
         (fn [_]
           (subscribe [:thm.game-ui/client-states]))
         (fn [client-states _]
           (sort (keys client-states))))

(reg-sub :thm.game-ui/modal-data
         (fn [db _]
           (peek (get db :thm.game-ui/modal-stack))))

(reg-sub :thm.game-ui/n-cards-in-zone
         client-state-for-role-signal
         (fn [cs [_ primary-role zone-descr]]
           (n-cards-in-zone cs zone-descr)))

(reg-sub :thm.game-ui/all-cards-in-zone
         client-state-for-role-signal
         (fn [cs [_ primary-role zone-descr]]
           (all-cards-in-zone cs zone-descr)))

(reg-sub :thm.game-ui/log-of-client-state-for
         client-state-for-role-signal
         (fn [cs [_ primary-role]]
           (:log cs)))

(reg-sub :thm.game-ui/log-helper
         client-state-for-role-signal
         (fn [cs [_ primary-role]]
           {:role->player-name (get-role->player-name cs)}))

(reg-sub :thm.game-ui/request-map-for
         client-state-for-role-signal
         (fn [cs [_ primary-role]]
           (:request-map cs)))

(reg-sub :thm.game-ui/your-request-for
         client-state-for-role-signal
         (fn [cs [_ _primary-role]]
           (get-your-request cs)))

(reg-sub :thm.game-ui/play-areas-associated-with-your-request
         client-state-for-role-signal
         (fn [cs [_ _primary-role]]
           (as-set (game-ui-module/get-play-areas-associated-with-request (module-from-cs cs)
                                                                          (get-your-request cs)
                                                                          (:your-role cs)))))

(reg-sub :thm.game-ui/applying-deltas?
         client-state-for-role-signal
         (fn [cs [_]]
           (:applying-deltas? cs)))

(reg-sub :thm.game-ui/answer-for
         client-state-for-role-signal
         (fn [cs [_ _primary-role]]
           (:answer cs)))

(reg-sub :thm.game-ui/answer-sent-for?
         client-state-for-role-signal
         (fn [cs [_ _primary-role]]
           (:answer-sent? cs)))

(reg-sub :thm.game-ui/paused-for?
         client-state-for-role-signal
         (fn [cs [_ _primary-role]]
           (get cs :paused?)))

(reg-sub :thm.game-ui/request-helper-for
         client-state-for-role-signal
         (fn [cs [_ _primary-role]]
           (:request-helper cs)))

(reg-sub :thm.game-ui/track-var
         client-state-for-role-signal
         (fn [cs [_ _primary-role var-descr]]
           (get-var cs var-descr)))

(reg-sub :thm.game-ui/get-all-figures-in-space
         client-state-for-role-signal
         (fn [cs [_ _primary-role space-descr]]
           (get-all-figures-in-space cs space-descr)))

(reg-sub :thm.game-ui/track-recent
         client-state-for-role-signal
         (fn [cs [_ _primary-role var-descr]]
           (get-in cs [:recent (multipart-name-from-descr var-descr)] 0)))

(reg-sub :thm.game-ui/can-cancel?
         client-state-for-role-signal
         (fn [cs _]
           (:can-cancel? cs)))

(reg-sub :thm.game-ui/should-disable-ready-button?
         client-state-for-role-signal
         (fn [cs _]
           (when-let [req (get-your-request cs)]
             (let [[kind text-descr & args] req
                   answer (:answer cs)]

               (case kind
                 :choose-things
                 (let [[things options] args
                       lower (:lower options)]
                   (if lower
                     (< (count answer) lower)
                     false))

                 :choose-cards-flexible
                 (let [helper (:request-helper cs)]
                   (not (<= (:lower helper) (count answer) (:higher helper))))

                 :choose-spaces
                 (let [helper (:request-helper cs)]
                   (not (<= (:lower helper) (count answer) (:higher helper))))

                 :choose-spaces-from-bags
                 (let [[bags] args]
                   (not (good-answer-for-choose-spaces-from-bags? bags answer)))

                 ;; XXX :choose-place-influence and :choose-change-defcon are td-specific
                 :choose-place-influence
                 (if (nil? answer)
                   true
                   (not (good-answer-for-choose-place-influence? cs (:your-role cs) args answer)))

                 :choose-change-defcon
                 (if (nil? answer)
                   true
                   (not (good-answer-for-choose-change-defcon? cs (:your-role cs) args answer)))

                 (nil? answer))))))

(reg-sub :thm.game-ui/get-ordered-map-of-player-name->role
         client-state-for-role-signal
         (fn [cs [_ primary-role]]
           (let [pn->r (:player-name->role cs)
                 ordered-roles (get-ordered-roles cs)
                 reordered-roles (shift-so-it-starts-with ordered-roles primary-role)
                 r->i (zipmap reordered-roles (range 0 (count reordered-roles)))]
             (into (sorted-map-by #(compare (r->i (pn->r %1)) (r->i (pn->r %2))))
                   pn->r))))

(reg-sub :thm.game-ui/get-all-playing-roles
         client-state-for-role-signal
         (fn [cs [_ _primary-role]]
           (get-ordered-roles cs)))

(reg-sub :thm.game-ui/get-map-of-role->player-name
         client-state-for-role-signal
         (fn [cs [_ _primary-role]]
           (get-role->player-name cs)))

(reg-sub :thm.game-ui/finished?
         any-client-state-signal
         (fn [cs [_]]
           (finished? cs)))

(reg-sub :thm.game-ui/card-active?
         client-state-for-role-signal
         (fn [cs [_ _primary-role cid zone-descr]]
           (when-let [[kind text-descr & args] (get-your-request cs)]
             (case kind
               :choose-card (let [[zone->cids] args]
                              (seq-contains? (get zone->cids (multipart-name-from-descr zone-descr))
                                             cid))
               :choose-card-and-mode (let [[zone->cids cid->modes] args]
                                        (seq-contains? (get zone->cids (multipart-name-from-descr zone-descr))
                                                       cid))
               :choose-cards (let [[request-zone-name cids] args]
                               (and (= request-zone-name (multipart-name-from-descr zone-descr))
                                    (seq-contains? cids cid)))
               :choose-cards-flexible (contains? (:set-of-cids (:request-helper cs)) cid)
               :choose-thing (contains? (:bucket->things (:request-helper cs)) cid)
               :choose-things (contains? (:bucket->things (:request-helper cs)) cid)

               false))))

(reg-sub :thm.game-ui/widget-active?
         client-state-for-role-signal
         (fn [cs [_ _primary-role widget-name]]
           (cond
             (forbid-widget-activation? cs) false
             (nil? (get-your-request cs)) false
             :else
             ,(let [[kind text-descr & args :as req] (get-your-request cs)]
                (case kind
                  (:choose-space,
                   :choose-spaces,
                   :choose-spaces-from-bags)
                  (contains? (:set-of-space-names (:request-helper cs))
                             widget-name)

                  false)))))

(reg-sub :thm.game-ui/widget-checked?
         client-state-for-role-signal
         (fn [cs [_ _primary-role widget-name]]
           (let [[kind text-descr & args :as req] (get-your-request cs)
                 answer (:answer cs)]
             (case kind
               :choose-space (= answer widget-name)
               :choose-spaces (seq-contains? answer widget-name)
               :choose-spaces-from-bags (some #(seq-contains? % widget-name)
                                              answer)
               false))))


(reg-sub :thm.game-ui/card-checked?
         client-state-for-role-signal
         (fn [cs [_ _primary-role cid zone-descr]]
           (when-let [[kind text-descr & args] (get-your-request cs)]
             (let [answer (:answer cs)]
               (case kind
                 :choose-card (= cid answer)
                 :choose-card-and-mode (and answer (= cid (first answer)))
                 :choose-cards (seq-contains? answer cid)
                 :choose-thing (let [[things options] args]
                                 (if (:multilevel? options)
                                   false
                                   (= cid
                                      (get-in (:request-helper cs) [:tid->thing answer :cid]))))
                 :choose-things (let [[things options] args
                                      set-of-cids (into #{}
                                                        (map #(get-in (:request-helper cs) [:tid->thing % :cid])
                                                             answer))]
                                  (contains? set-of-cids cid))
                 :choose-cards-flexible (seq-contains? answer cid)

                 false)))))

(reg-sub :thm.game-ui/outcome
         any-client-state-signal
         (fn [cs [_]]
           (:outcome cs)))

(reg-sub :thm.game-ui/game-code-in-cs
         any-client-state-signal
         (fn [cs [_]]
           (:code cs)))

(reg-sub :thm.game-ui/zone-animation-info
         client-state-for-role-signal
         (fn [cs [_ primary-role zone-name]]
           (get-in cs [:animation-info zone-name])))

(reg-sub :thm.game-ui/space-animation-info
         client-state-for-role-signal
         (fn [cs [_ primary-role space-name]]
           (get-in cs [:animation-info space-name])))

 ;; XXX it's td-specific, so should be named accordingly, but better to move it to request-helper
(reg-sub :thm.game-ui/spread-for
         client-state-for-role-signal
         (fn [cs [_ _primary-role]]
           (:spread cs)))

(reg-sub :thm.game-ui/state-timing-for-humans
         client-state-for-role-signal
         (fn [cs [_ _primary-role module]]
           (game-ui-module/state-timing-for-humans module cs)))

;; == td-specific

(reg-sub :thm.game-ui.td/your-agenda
         client-state-for-role-signal
         (fn [cs [_ _primary-role]]
           (get-your-agenda cs)))

(reg-sub :thm.game-ui.td/board-widget-active?
         client-state-for-role-signal
         (fn [cs [_ _primary-role {:keys [name bg? track] :as widget}]]
           (cond
             (= name "af") (= (get-var cs "phase") "aftermath")
             (forbid-widget-activation? cs) false
             (nil? (get-your-request cs)) false
             :else
             ,(let [[kind text-descr & args :as req] (get-your-request cs)]
                (case kind
                  :choose-command bg?
                  :choose-command-onto (and bg?
                                            (let [[_cubes bgs] args]
                                              (contains? bgs name)))
                  :choose-change-defcon (and track
                                             (let [[whose max-tracks allowed-tracks lower higher exact-amount?] args
                                                   bw-value (get-track-board-widget-value name)
                                                   cur-value (get-var cs ["d" whose track])]
                                               (and
                                                (contains? allowed-tracks track)
                                                (<= (+ cur-value lower) bw-value (+ cur-value higher)))))
                  :choose-place-influence (and bg?
                                               (let [[max max-per defcon? bgs exact-amount?] args]
                                                 (contains? bgs name)))
                  :choose-remove-influence (and bg?
                                                (let [[whose max policy defcon? bgs] args]
                                                  (contains? bgs name)))
                  false)))))

(reg-sub :thm.game-ui.td/board-widget-checks
         client-state-for-role-signal
         (fn [cs [_ _primary-role {:keys [name bg? track] :as widget}]]
           (when-let [[kind text-descr & args :as req] (get-your-request cs)]
             (case kind
               (:choose-command :choose-command-onto)
               ,(if (and (:answer cs) (= name (first (:answer cs))))
                  (second (:answer cs))
                  0)
               :choose-place-influence (or (get (:answer cs) name) 0)
               :choose-remove-influence (if-let [val (get (:answer cs) name)]
                                          (let [[whose max policy defcon? bgs] args]
                                            (if (= policy :half)
                                              (- (half-round-up (get-var cs ["i" whose name])))
                                              (- val)))
                                          0)
               :choose-change-defcon (if (and track
                                              (contains? (:answer cs) track)
                                              (let [[whose max-tracks allowed-tracks lower higher exact-amount?] args]
                                                (= (get-track-board-widget-value name)
                                                   (+ (get-var cs ["d" whose track])
                                                      (get (:answer cs) track)))))
                                       1
                                       0)
               0))))

(reg-sub :thm.game-ui.td/good-for-war?
         client-state-for-role-signal
         (fn [cs [_ _primary-role role]]
           (good-for-war? cs role)))

(reg-sub :thm.game-ui.td/your-aftermath-modal-data
         client-state-for-role-signal
         (fn [cs [_ primary-role]]
           (if (= primary-role "*observer*")
             {:s-n (n-cards-in-zone cs "s:aftermath")
              :u-n (n-cards-in-zone cs "u:aftermath")}
             {:your-n (n-cards-in-zone cs [primary-role "aftermath"])
              :other-n (n-cards-in-zone cs [(other-role primary-role) "aftermath"])})))

(reg-sub :thm.game-ui.td/request-is-choose-card-to-play?
         client-state-for-role-signal
         (fn [cs _]
           (when-let [[kind text-descr & args] (get-your-request cs)]
             (let [{:keys [text-code text-args]} (parse-text-descr text-descr)]
               (= text-code :choose-card-to-play)))))

;; XXX viti-specific
(reg-sub :thm.game-ui.viti/n-available-figures-of-type
         client-state-for-role-signal
         (fn [cs [_ _primary-role role figure-type]]
           (viti-utils/get-n-available-figures-of-type cs role figure-type)))

;; == account page subscriptions

(reg-sub :thm.account/avatar-update-timestamp-for
         (fn [db [_ username]]
           (get-in db [:thm.account/avatar-update-timestamps
                       username])))

(reg-sub :thm.account/your-avatar-update-timestamp
         (fn [db _]
           (get-in db [:thm.account/avatar-update-timestamps
                       (:username db)])))
