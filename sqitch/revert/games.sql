-- Revert sqitch/sqitch.plan:games from pg

BEGIN;

drop table game_engine_checkpoints;
drop trigger update_games_modtime on games;
drop table games;
drop type game_time_kind;

COMMIT;
