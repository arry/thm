(ns thm.views.marker
    (:require [reagent.core :as r]
            ))

(def marker-state (r/atom {:coords {:x 0 :y 0}
                           :state "init"
                           :rects []}))

(defn print-results []
  (println (pr-str (:rects @marker-state))))

(defn mark-point []
  (let [{:keys [state coords]} @marker-state]
    (if (= state "init")
      (swap! marker-state
             assoc
             :saved-coords coords
             :state "saved")
      (swap! marker-state
             #(-> %
                  (update :rects conj {:start (:saved-coords @marker-state)
                                       :finish coords
                                       :name (js/prompt "name?")})
                  (assoc :state "init"
                         :saved-coords nil))))))

(defn handle-key-press [e-wrapper]
  (let [e (.-event_ e-wrapper)]
    (when-not (or (.-altKey e) (.-ctrlKey e) (.-metaKey e))
      (case (.-key e)
        "j" (swap! marker-state update-in [:coords :y] inc)
        "k" (swap! marker-state update-in [:coords :y] dec)
        "h" (swap! marker-state update-in [:coords :x] dec)
        "l" (swap! marker-state update-in [:coords :x] inc)
        "Enter" (mark-point)
        "p" (print-results)
        nil
        ))))
