(ns thm.game-engine.game-base
  (:require
   [taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug spy)]
   [thm.utils :refer [denumber-id multipart-name-from-descr finished? has-active-request? get-request-for-role
                      multipart-name-from-descr get-var shuffle-with-random player-name-by-role
                      find-first remove-first seq-contains?
                      index-of
                      index-of-matching
                      all-cards-in-zone backwards-apply
                      get-zone
                      remove-from-vector-at-index
                      ]]
   [thm.utils :refer [parse-text-descr
                      good-answer-for-choose-spaces-from-bags?]]
   ))

(def DEBUG (atom false))

(defn start-debug! []
  (reset! DEBUG true))

(defn stop-debug! []
  (reset! DEBUG false))

(defmacro with-debug [& forms]
  `(do (start-debug!)
       ~@forms
       (stop-debug!)))

(defonce ^:dynamic *actions* (atom {}))

(defonce ^:dynamic *game-definitions* (atom {}))

(defn register-game-definition [code game-definition]
  (swap! *game-definitions* assoc code game-definition))

(defn game-definition-by-code [code]
  (or (get @*game-definitions* code)
      (assert false (format "Must have registered game definition for code %s" code))))

(defmacro defaction-coded
  "An upgraded defn, defines a function named n and remembers this function in
  the *actions* map under the code and the keyword matching the name.  An
  action should be a function that takes a state as the first argument, and
  returns the hash containing the new state under :state, and either :delta
  or :deltas, which is an outcome of the action."
  [code n & forms]
  (let [n (if (keyword? n) (symbol (name n))
              n)]
    `(swap! *actions* update ~code assoc (keyword '~n) (defn ~n ~@forms))))

(defmacro defaction-base
  [n & forms]
  `(defaction-coded "base" ~n ~@forms))

(defonce ^:dynamic *answer-validators* (atom {}))

(defn get-answer-validator [code kind]
  (-> @*answer-validators*
      (get code)
      (get kind)))

(defmacro def-answer-validator [code kind args & forms]
  `(swap! *answer-validators* update ~code assoc ~kind (fn ~args ~@forms)))

(defn has-action? [state action-name]
  (let [h @*actions*]
    (or (contains? (h (:code state))
                   (keyword action-name))
        (contains? (h "base")
                   (keyword action-name)))))

(defn get-action [state action-name]
  (let [h @*actions*]
    (or (get (h (:code state))
             (keyword action-name))
        (get (h "base")
             (keyword action-name)))))

(defn visibility-visible-to-role? [vis zone role]
  (case vis
    (nil :all) true
    :none false
    :you (= (:role zone) role)
    (= role vis)))

(defn zone-visible-to-role? [zone role]
  (visibility-visible-to-role? (:v zone) zone role))

(defn zone-facedown-visible-to-role? [zone role]
  (visibility-visible-to-role? (:fv zone) zone role))

(defn zone-active? [zone]
  (:a zone))

(defn private-var?
  "Private vars are visible only to the server side, never clients.  Their
  names begin with a minus sign."
  [var-name]
  (= (first var-name) \-))

(defn private-prop-name?
  [prop-name]
  (private-var? (name prop-name)))

;; === Lens
;; Bridge between server-side and client-side state representation, including
;; artifacts like deltas and requests.

(defn new-lens
  "Lens is an entity that translates things from the server-side view to the
  client side and back.  The things translated are state, deltas, requests, and
  answers.  During translation, only the things relevant and visible to the
  role in question are preserved, and others are hidden.  A lens is stateful,
  because the duplicate cards are numbered differently on the server and
  client (due to the fact that the client doesn't see all the cards), so the
  mappings between these views is stored here.
  "
  [role code & [options]]
  (assert code "Must have code on lens creation")
  (let [basic-lens {:code code, :role role}]
    (if (:disable-card-numbers? options)
      (assoc basic-lens :disable-card-numbers? true)
      (assoc basic-lens :s->c {}, :chop->n {}))))

(defn lens-map-keys
  "Return a hash containing :lens and :result keys.  f is a function that takes
  a lens, key, and value (latter two are from map m) and returns a hash
  containing :lens and :result; this new result is supposed to be a mapped
  key.  The lens is 'modified', i.e. each new call to f is with a lens
  returned by a previous call.  The returned :result is a map that contains the
  mapped keys associating the original values."
  [lens f m]
  (reduce (fn [{l :lens, res :result} [k v]]
            (let [{new-lens :lens, partial-result :result} (f l k v)]
              {:result (assoc res partial-result v)
               :lens new-lens}))
          {:lens lens, :result {}}
          m))

(defn lens-map-values
  "Return a hash containing :lens and :result keys.  f is a function that takes
  a lens, key, and value (latter two are from map m) and returns a hash
  containing :lens and :result; this new result is supposed to be a mapped
  value.  The lens is 'modified', i.e. each new call to f is with a lens
  returned by a previous call.  The returned :result is a map that contains the
  original keys, but mapped values."
  [lens f m]
  (reduce (fn [{l :lens, res :result} [k v]]
            (let [{new-lens :lens, partial-result :result} (f l k v)]
              {:result (assoc res k partial-result)
               :lens new-lens}))
          {:lens lens, :result {}}
          m))

(defn lens-map [lens f coll & [opts]]
  (reduce (fn [{l :lens, res :result} v]
            (let [{new-lens :lens, partial-result :result} (f l v opts)]
              {:result (if (nil? partial-result)
                         res
                         (conj res partial-result))
               :lens new-lens}))
          {:lens lens, :result []}
          coll))

(defn get-cid-base-numbered [lens cid cid-base-for-client]
  (let [next-number (inc (or (get-in lens [:chop->n cid-base-for-client]) 0))
        client-id (str cid-base-for-client ":" next-number)]
    {:lens (-> lens
               (update :s->c assoc cid client-id)
               (update :chop->n assoc cid-base-for-client next-number))
     :result client-id}))

(defn translate-visible-cid [lens cid & [opts]]
  (if (:disable-card-numbers? lens)
    {:lens lens, :result cid}
    (if-let [client-id (get-in lens [:s->c cid])]
      {:lens lens, :result client-id}
      (get-cid-base-numbered lens cid (denumber-id cid)))))

(defn translate-cid-first-time [lens cid & [opts]]
  (let [hide-cid-because-facedown? (:hide-cid-because-facedown? opts)]
    (if (and (:disable-card-numbers? lens)
             (not hide-cid-because-facedown?))
      {:lens lens, :result cid}
      (let [cid-base-for-client (if hide-cid-because-facedown?
                                  "*fd*"
                                  (denumber-id cid))]
        (assert (not (contains? (:s->c lens) cid))
                (str "When translating cid for the first time, shouldn't have its s->c mapping already: " (:s->c lens) cid))
        (get-cid-base-numbered lens cid cid-base-for-client)))))

(defn translate-visible-cids [lens cids & [opts]]
  (lens-map lens translate-visible-cid cids opts))

(defn translate-appearing-cids [lens cids & [opts]]
  (lens-map lens translate-cid-first-time cids opts))

(defn translate-visible-cids-in-zone [lens zone cids & [opts]]
  (translate-visible-cids lens cids))

(defn translate-active-card [lens card & [opts]]
  (let [{:keys [zone role]} opts
        {:keys [cid facedown?]} card

        hide-cid-because-facedown? (and facedown?
                                     (not (zone-facedown-visible-to-role? zone role)))]
    (let [{:keys [lens result]} (translate-cid-first-time lens cid
                                                          {:hide-cid-because-facedown? hide-cid-because-facedown?})
          translated-card (-> {}
                              (into (remove #(private-prop-name? (key %)) card))
                              (assoc :cid result))]
      {:lens lens, :result translated-card})))

(defn translate-active-cards [lens cards & [opts]]
  (lens-map lens translate-active-card cards opts))

(defn translate-disappearing-cids [lens cids]
  (if (:disable-card-numbers? lens)
    {:lens lens, :result cids}
    (let [s->c (:s->c lens)
          translated (mapv s->c cids)]
      {:lens (assoc lens :s->c (apply dissoc s->c cids))
       :result translated})))

(defn translate-choosable-thing [lens thing & [opts]]
  (let [public-only (into {} (remove #(private-prop-name? (key %)) thing))]
    (if-let [cid (:cid thing)]
      (let [{:keys [lens result]} (translate-visible-cid lens cid)]
        {:lens lens, :result (assoc public-only :cid result)})
      {:lens lens, :result public-only})))

(defn translate-choosable-things [lens things]
  (lens-map lens translate-choosable-thing things))


(defn translate-zone [lens zone-descr zone & [opts]]
  (let [{:keys [role]} lens
        cards (:cards zone)]
    (if (zone-visible-to-role? zone role)
      (let [trans (if (zone-active? zone)
                    (translate-active-cards lens cards {:zone zone, :role role})
                    (translate-visible-cids lens cards))]
        {:lens (:lens trans)
         ;; TODO make it with properties that are nicer to this role, e.g. visible-to-you?
         :result (assoc zone :cards
                        (:result trans))})
      {:lens lens, :result (assoc zone :cards (count cards))})))

(defn translate-zones [lens zones]
  (lens-map-values lens translate-zone zones))


(defmulti translate-your-request-args (fn [lens kind & args]
                                        kind))

(defmethod translate-your-request-args :default
  [lens kind & args]
  {:lens lens, :result args})

(defmethod translate-your-request-args :choose-card
  [lens kind zone->cards]
  ;; FIXME we should assert that all zones mentioned in zone->cards are
  ;; visible.  It makes no sense to ask to choose a card from among the
  ;; invisible ones.
  (let [{:keys [lens result]}
        (lens-map-values lens translate-visible-cids-in-zone zone->cards)]
    {:lens lens, :result [result]}))

(defmethod translate-your-request-args :choose-card-and-mode
  [lens kind zone->cards cid->modes]
  (let [{:keys [lens result]}
        (lens-map-values lens translate-visible-cids-in-zone zone->cards)
        another (lens-map-keys lens translate-visible-cid cid->modes)
        ]
    {:lens (:lens another), :result [result (:result another)]}))

(defmethod translate-your-request-args :choose-cards
  [lens kind zone cids]
  ;; FIXME same as in :choose-card above.
  (let [{:keys [lens result]}
        (translate-visible-cids lens cids)]
    {:lens lens, :result [zone result]}))

(defmethod translate-your-request-args :choose-cards-flexible
  [lens kind zone cids options]
  ;; FIXME same as in :choose-card above.
  (let [{:keys [lens result]}
        (if (nil? cids)
          {:lens lens, :result nil}
          (translate-visible-cids lens cids))]
    {:lens lens, :result [zone result options]}))

(defmethod translate-your-request-args :choose-thing
  [lens kind things options]
  (let [{:keys [lens result]} (translate-choosable-things lens things)]
    {:lens lens, :result [result options]}))

(defmethod translate-your-request-args :choose-things
  [lens kind things options]
  (let [{:keys [lens result]} (translate-choosable-things lens things)]
    {:lens lens, :result [result options]}))

(defn translate-request [lens req-role [kind text-descr & args :as req] & [opts]]
  (let [{:keys [role]} lens
        {:keys [text-code text-args]} (parse-text-descr text-descr)]
    (if (= req-role role)
      (let [that (apply translate-your-request-args lens kind args)]
        {:lens (:lens that),
         :result (into [kind text-descr] (:result that))})
      {:lens lens, :result [kind text-code]})))

(defn translate-request-map [lens request-map]
  (lens-map-values lens translate-request request-map))

(defn translate-vars [lens vars]
  {:lens lens,
   :result (into {} (remove #(private-var? (first %)) vars))})


(defn delta-has-text? [delta]
  (#{:deck-shuffled :text :game-finished} (first delta)))


(defn translate-state [lens state & [past-deltas]]
  (let [game-definition (game-definition-by-code (:code state))
        {lens :lens zones :result} (translate-zones lens (:zones state))
        {lens :lens request-map :result} (translate-request-map lens (:request-map state))
        {lens :lens vars :result} (translate-vars lens (:vars state))]
    {:result (cond-> {:your-role (:role lens)
                      :code (:code state)
                      :version (get state :version 0)
                      :ordered-roles (:ordered-roles state)
                      :player-name->role (:player-name->role state)
                      :role->player-name (:role->player-name state)
                      :vars vars
                      :zones zones
                      :request-map request-map
                      :can-cancel? (:can-cancel? state)
                      :outcome (:outcome state)
                      :log (filterv delta-has-text? past-deltas)}
               (:has-spaces? game-definition)
               (assoc :spaces (:spaces state)))
     :lens lens}))


(defmulti translate-delta-m (fn [lens [kind & args :as delta] & [opts]]
                            kind))

(defmethod translate-delta-m :default
  [lens [kind & args :as delta] & [opts]]
  (assert false (format "Unrecognized kind of delta: %s" delta)))

(defmethod translate-delta-m :cards-created
  [lens [kind & args :as delta] & [opts]]
  (let [[zn card-descrs _cids] args
        {:keys [role]} lens
        zone (get-zone (:state opts) zn)
        active? (:a zone)
        visible? (zone-visible-to-role? zone role)]
    (cond (and active? visible?)
          (let [{:keys [lens result]} (translate-active-cards lens card-descrs {:zone zone, :role role})]
            {:lens lens, :result [:cards-created-a zn result]})

          visible?
          (let [{:keys [lens result]} (translate-visible-cids lens card-descrs)]
            {:lens lens, :result [:cards-created-v zn result]})

          :else
          {:lens lens, :result [:cards-created-i zn (count card-descrs)]})))

(defmethod translate-delta-m :cards-destroyed
  [lens [kind & args :as delta] & [opts]]
  (let [[zn cids _cards] args
        {:keys [role]} lens
        zone (get-zone (:state opts) zn)
        active? (zone-active? zone)
        visible? (zone-visible-to-role? zone role)]
    (cond (and active? visible?)
          (let [{:keys [lens result]} (translate-disappearing-cids lens cids)]
            {:lens lens, :result [:cards-destroyed-a zn result]})

          visible?
          (let [{:keys [lens result]} (translate-disappearing-cids lens cids)]
            {:lens lens, :result [:cards-destroyed-v zn result]})

          :else
          {:lens lens, :result [:cards-destroyed-i zn (count cids)]})))

(defmethod translate-delta-m :cards-moved
  [lens [kind & args :as delta] & [opts]]
  (let [[from to cids] args
        {:keys [role]} lens

        from-zone (get-zone (:state opts) from)
        to-zone (get-zone (:state opts) to)

        from-visible? (zone-visible-to-role? from-zone role)
        from-active? (zone-active? from-zone)
        to-visible? (zone-visible-to-role? to-zone role)
        to-active? (zone-active? to-zone)]
    ;; XXX it's better to unify the deltas to be :cards-moved, and make the
    ;; active/visible designation an argument, but for compatibility with td,
    ;; where the -vv, -iv, -vi, -ii originated, the proliferation of
    ;; duplication is retained so far.
    (cond
      (and from-visible? to-visible?)
      (let [{:keys [lens result]} (translate-visible-cids lens cids)
            tag (cond (and from-active?           to-active?)  :cards-moved-aa
                      (and from-active?      (not to-active?)) :cards-moved-av
                      (and (not from-active?)     to-active?)  :cards-moved-va
                      :else                                    :cards-moved-vv)]
          {:lens lens, :result [tag from to result]})
      ,
      (and (not from-visible?) to-visible?)
      (let [{:keys [lens result]} (translate-visible-cids lens cids)
            tag (if to-active? :cards-moved-ia :cards-moved-iv)]
          {:lens lens, :result [tag from to result]})
      ,
      (and from-visible? (not to-visible?))
      (let [{:keys [lens result]} (translate-disappearing-cids lens cids)
            tag (if from-active? :cards-moved-ai :cards-moved-vi)]
          {:lens lens, :result [tag from to result]})
      ,
      :else
      {:lens lens, :result [:cards-moved-ii from to (count cids)]})))

(defmethod translate-delta-m :ns-cards-moved
  [lens [kind & args :as delta] & [opts]]
  (let [[from to cids delta-opts] args
        {:keys [role]} lens

        from-zone (get-zone (:state opts) from)
        to-zone (get-zone (:state opts) to)

        from-visible? (zone-visible-to-role? from-zone role)
        from-active? (zone-active? from-zone)
        to-visible? (zone-visible-to-role? to-zone role)
        to-active? (zone-active? to-zone)

        calculate-status (fn [visible? active?]
                           (if visible?
                             (if active?
                               :a
                               :v)
                             :i))

        from-status (calculate-status from-visible? from-active?)

        to-status (calculate-status to-visible? to-active?)

        generate-result (fn [lens result]
                          {:lens lens, :result [:ns-cards-moved from to from-status to-status
                                                result delta-opts]})]

    (cond
      (and from-visible? to-visible?)
      (let [{:keys [lens result]} (translate-visible-cids lens cids delta-opts)]
        (generate-result lens result))
      ,
      (and (not from-visible?) to-visible?)
      (let [hide-cid-because-facedown? (and (:change-to-facedown? delta-opts)
                                            (not (zone-facedown-visible-to-role? to-zone role)))
            {:keys [lens result]} (translate-appearing-cids lens cids
                                                            {:hide-cid-because-facedown? hide-cid-because-facedown?})]
        (generate-result lens result))
      ,
      (and from-visible? (not to-visible?))
      (let [{:keys [lens result]} (translate-disappearing-cids lens cids)]
        (generate-result lens result))
      ,
      :else
      (generate-result lens (count cids)))))

(defmethod translate-delta-m :card-props-set
  [lens [kind & args :as delta] & [opts]]
  (let [[zn cid prop-new-old-triplets] args
        translated-pairs (->> prop-new-old-triplets
                              (remove #(private-prop-name? (first %)))
                              (map (fn [[prop new old]]
                                     [prop new]))
                              vec)]
    (if (empty? translated-pairs)
      {:lens lens, :result nil}
      (let [role (:role lens)
            zone (get-zone (:state opts) zn)
            visible? (zone-visible-to-role? zone role)
            _ (assert visible? ("Must have visible zone to change props: " delta role))
            {:keys [lens result]} (translate-visible-cid lens cid)]
        {:lens lens, :result [:card-props-set zn result translated-pairs]}))))

(defmethod translate-delta-m :card-props-changed
  [lens [kind & args :as delta] & [opts]]
  (let [[zn cid prop-change-new-triplets] args
        public-triplets (remove #(private-prop-name? (first %))
                                prop-change-new-triplets)]

    (if (empty? public-triplets)
      {:lens lens, :result nil}
      (let [role (:role lens)
            zone (get-zone (:state opts) zn)
            visible? (zone-visible-to-role? zone role)
            _ (assert visible? ("Must have visible zone to change props: " delta role))
            {:keys [lens result]} (translate-visible-cid lens cid)]
        {:lens lens, :result [:card-props-changed zn result (vec public-triplets)]}))))

(defmethod translate-delta-m :card-props-unset
  [lens [kind & args :as delta] & [opts]]
  (let [[zn cid prop-old-pairs] args
        translated-props (->> prop-old-pairs
                              (remove #(private-prop-name? (first %)))
                              (map (fn [[prop old]]
                                     prop))
                              vec)]
    (if (empty? translated-props)
      {:lens lens, :result nil}
      (let [role (:role lens)
            zone (get-zone (:state opts) zn)
            visible? (zone-visible-to-role? zone role)
            _ (assert visible? ("Must have visible zone to change props: " delta role))
            {:keys [lens result]} (translate-visible-cid lens cid)]
        {:lens lens, :result [:card-props-unset zn result translated-props]}))))

(defmethod translate-delta-m :request-put
  [lens [kind & args :as delta] & [opts]]
  (let [[request-map] args
        res (translate-request-map lens request-map)]
    {:lens (:lens res), :result [kind (:result res)]}))

(defmethod translate-delta-m :var-set
  [lens [kind var-name value _old-value :as delta] & [opts]]
  (if (private-var? var-name)
    {:lens lens, :result nil}
    ;; XXX don't show the old value in the translation
    {:lens lens, :result delta}))

(defmethod translate-delta-m :var-unset
  [lens [kind var-name _old-value :as delta] & [opts]]
  (if (private-var? var-name)
    {:lens lens, :result nil}
    ;; XXX don't show the old value in the translation
    {:lens lens, :result delta}))

(defmethod translate-delta-m :vars-set
  [lens [kind & mappings :as delta] & [opts]]
  (let [new-mappings (remove #(private-var? (first %)) mappings)]
    (if (empty? new-mappings)
      {:lens lens, :result nil}
      {:lens lens, :result [kind (mapv (fn [[k new old]]
                                         [k new])
                                       new-mappings)]})))

(defmethod translate-delta-m :var-changed
  [lens [kind var-name amount value :as delta] & [opts]]
  (if (private-var? var-name)
    {:lens lens, :result nil}
    {:lens lens, :result delta}))

(defn translate-delta [lens [kind & args :as delta] & [opts]]
  (let [{:keys [role]} lens]
    (case kind
      (:deck-shuffled
       :request-answered
       :text
       :remove-last-text
       :game-finished
       :pause
       :reveal-zone
       :figure-created
       :figure-destroyed
       :figure-moved
       :-can-cancel-request?) {:lens lens, :result delta}
      (translate-delta-m lens delta opts))))

(defn translate-deltas [lens state deltas]
  ;; (debug "translate-deltas:" state deltas)
  (lens-map lens translate-delta deltas {:state state}))

(defn server-cids-from-client-cids [lens client-cids]
  ;; FIXME for facedown cards will not map the *fd* ids back to server ids
  ;; if :disable-card-numbers? is true.
  (if (:disable-card-numbers? lens)
    (vec client-cids)
    (let [c->s (clojure.set/map-invert (:s->c lens))]
      (mapv c->s client-cids))))

(defmulti translate-answer (fn [lens client-answer [kind text-descr & args :as req]]
                             kind))

(defmethod translate-answer :default
  [lens client-answer req]
  client-answer)

(defmethod translate-answer :choose-card
  [lens client-answer req]
  (first (server-cids-from-client-cids lens [client-answer])))

(defmethod translate-answer :choose-card-and-mode
  [lens client-answer req]
  [(first (server-cids-from-client-cids lens [(first client-answer)]))
   (second client-answer)])

(defmethod translate-answer :choose-cards
  [lens client-answer req]
  (server-cids-from-client-cids lens client-answer))

(defmethod translate-answer :choose-cards-flexible
  [lens client-answer req]
  (server-cids-from-client-cids lens client-answer))

;; === Performing actions: meat of the engine

(def cycle-detector (atom {}))
(def ^:dynamic *with-detecting-cycles* false)

(defn valid-answer? [state role [kind text-descr & args :as req] answer]
  (case kind
    :choose-card (let [[zone->cards] args]
                   (some #(not= -1 (.indexOf % answer)) (vals zone->cards)))
    :choose-card-and-mode (let [[zone->cards cid->modes] args]
                            (and (sequential? answer)
                                 ;; XXX skipped (second answer) should be in cid->modes
                                 (some #(not= -1 (.indexOf % (first answer))) (vals zone->cards))))
    :choose-cards (let [[zone-name cids] args]
                    (and (sequential? answer)
                         (distinct? answer)
                         (every? (set cids) answer)))
    :choose-cards-flexible (let [[zone-name cids options] args]
                             (and (sequential? answer)
                                  (distinct? answer)
                                  (if-let [lower (:lower options)]
                                    (<= lower (count answer))
                                    true)
                                  (if-let [higher (:higher options)]
                                    (<= (count answer) higher)
                                    true)
                                  (every? (if (:all-in-zone? options)
                                            (set (all-cards-in-zone state zone-name))
                                            (set cids))
                                          answer)))
    :choose-mode (let [[modes] args]
                   (contains? (set modes) answer))

    :choose-number (let [[lower higher] args]
                     (and (integer? answer)
                          (<= lower answer higher)))
    :choose-number-from-list (let [[numbers] args]
                               (contains? (set numbers) answer))
    :yn (boolean? answer)

    :choose-thing
    (let [[things options] args]
      (seq-contains? (map :tid things) answer))

    :choose-things
    (let [[things options] args
          set-of-tids (into #{} (map :tid things))]
      (and (sequential? answer)
           (distinct? answer)
           (if-let [lower (:lower options)]
             (<= lower (count answer))
             true)
           (if-let [higher (:higher options)]
             (<= (count answer) higher)
             true)
           (every? #(contains? set-of-tids %) answer)))

    :choose-space
    (let [[spaces options] args]
      (or (contains? (set spaces) answer)
          (and (:can-pass? options) (= answer "*pass*"))))

    :choose-spaces
    (let [[spaces options] args
          lower (or (:lower options) 0)
          higher (or (:higher options) (count spaces))
          set-of-spaces (set spaces)]
      (and (coll? answer)
           (<= lower (count answer) higher)
           (distinct? answer)
           (every? #(contains? set-of-spaces %) answer)))

    :choose-spaces-from-bags
    (let [[bags] args]
      (good-answer-for-choose-spaces-from-bags? bags answer))

    (let [validator (get-answer-validator (:code state) kind)]
      (if validator
        (validator state role req answer)
        (do
          (warnf "No validator for request kind: %s" kind)
          true)))))

(defn perform-action [state action-descriptor]
  (let [[action-name args] (if (sequential? action-descriptor)
                             [(first action-descriptor) (rest action-descriptor)]
                             [action-descriptor []])
        action-name (or action-name :do-nothing)]
    (let [fn (get-action state action-name)]
      (cond (or (nil? action-name) (= action-name :do-nothing))
            {:state state}

            (some? fn)
            (do
              (when @DEBUG (debug "perform-action" #_state action-descriptor))
              (let [res (apply fn state args)]
                (if-let [delta (:delta res)]
                  (-> res (assoc :deltas [delta])
                      (dissoc :delta))
                  res)))

            :else
            (do (warnf "Unrecognized action name: %s" action-name)
                {:state state
                 :deltas []})))))

(def MAX-ACTIONS-BEFORE-REPORTING-CYCLE 10)

(defn perform-sequence [state & action-descriptors]
  (when @DEBUG (debug "perform-sequence " #_state action-descriptors))
  (letfn [(helper [s cur-deltas [action & next-actions :as cur-actions]]
            (if (and *with-detecting-cycles*
                     (some? action)
                     (>= (get @cycle-detector cur-actions 0) MAX-ACTIONS-BEFORE-REPORTING-CYCLE))
              (do
                (errorf "Detected cycle: action sequence '%s' is repeated too much" cur-actions)
                {:state (assoc s :pending-actions (vec cur-actions)), :deltas cur-deltas, :pan 0})
              (let [res (perform-action s action)
                    {ns :state, deltas :deltas, pan :pan
                     ;; pan is the position to which the next actions would be
                     ;; inserted: the old pending actions (which were before this
                     ;; sequence) are to the right, and the new ones (from the
                     ;; nested perform-sequence calls) are to the left.
                     :or {pan 0}} res
                    new-deltas (into cur-deltas deltas)]
                ;; (debug "after:" res)
                (when *with-detecting-cycles*
                  (swap! cycle-detector assoc cur-actions (inc (get @cycle-detector cur-actions 0))))
                (cond
                  (finished? ns) {:state ns, :deltas new-deltas}
                  (has-active-request? ns) {:state (update ns :pending-actions
                                                           #(vec (concat (take pan %) next-actions (drop pan %))))
                                            :deltas new-deltas
                                            :pan (+ pan (count next-actions))}
                  (seq next-actions) (recur ns new-deltas next-actions)
                  :else {:state ns, :deltas new-deltas, :pan pan}))))]
    (helper state [] action-descriptors)))

(defn expand-answer
  "Sometimes the answer is identified by a short-hand, and the answer-handling
  routines are interested in full representation which is kept in the request
  map so far.  This function converts the short-hand answer to its full
  representation."
  [[kind text-descr & args :as req] answer]
  (case kind
    :choose-thing (let [[things options] args]
                     (first (filter #(= (:tid %) answer) things)))
    :choose-things (let [[things options] args]
                     (map (fn [tid]
                            (first (filter #(= (:tid %) tid) things)))
                          answer))
    answer))

(defn supply-answer [state role answer]
  (let [request (get-request-for-role state role)]
    (assert request (format "Must have a request for player %s" role))
    (assert (valid-answer? state role request answer)
            (format "Invalid answer for request: %s, %s" answer request))
    {:state (-> state
                 (assoc-in [:answers role] (expand-answer request answer))
                 (update :request-map dissoc role))
      :delta [:request-answered role]}))

(defn perform-pending-actions [state]
  (if (or (finished? state) (has-active-request? state))
    {:state state, :deltas []}
    (apply perform-sequence (dissoc state :pending-actions) (:pending-actions state))))

(defn state-after-action-sequence
  "Return just the state after performing given actions.  Useful in repl to get
  from one state to the next quickly."
  [state & action-descriptors]
  (swap! cycle-detector empty)
  (binding [*with-detecting-cycles* true]
    (let [res (apply perform-sequence state action-descriptors)]
      (:state res))))

(defn state-after-answer
  "Return just the state after an answer is supplied, and the pending actions
  run.  If role is not given, assumes that the current request is for a single
  player only."
  [state answer & [role]]
  (swap! cycle-detector empty)
  (binding [*with-detecting-cycles* true]
    (-> state
        (supply-answer (or role (key (first (:request-map state)))) answer)
        :state
        (perform-pending-actions)
        :state)))

(defn state-after-pending-actions [state]
  (swap! cycle-detector empty)
  (binding [*with-detecting-cycles* true]
    (-> state
        (perform-pending-actions)
        :state)))

;; === Primitive actions
;; They create a delta, and they're used as building blocks by the
;; more involved actions.

(defaction-base do-nothing [state]
  {:state state})

(defaction-base action-sequence [state & action-descriptors]
  (apply perform-sequence state action-descriptors))

(defaction-base text [state text-code & args]
  {:state state
   :delta (into [:text text-code] args)})

(defaction-base pause [state]
  {:state state, :delta [:pause]})

(defaction-base set-var [state var-descr value]
  ;; (debug "set-var" state var-descr value)
  (let [var-name (multipart-name-from-descr var-descr)
        old-value (get-var state var-name)]
    (if (= value old-value)
      (do-nothing state)
      {:state (assoc-in state [:vars var-name] value)
       :delta [:var-set var-name value old-value]})))

(defaction-base set-vars [state & mappings]
  (let [var-name->value (map (fn [[k v]]
                               [(multipart-name-from-descr k) v])
                             mappings)
        var-new-old-triplets (filterv some?
                                      (map (fn [[k new-v]]
                                             (let [old-v (get-var state k)]
                                               (when-not (= new-v old-v)
                                                 [k new-v old-v])))
                                           var-name->value))]
    (if (empty? var-new-old-triplets)
      (do-nothing state)
      ;; (debug "set-vars" mappings var-new-old-triplets)
      {:state (update state :vars into var-name->value)
       :delta (into [:vars-set] var-new-old-triplets)})))

(defaction-base unset-var [state var-descr]
  (let [var-name (multipart-name-from-descr var-descr)]
    {:state (update state :vars dissoc var-name)
     :delta [:var-unset var-name (get-var state var-name)]}))

(defaction-base change-var [state var-descr amount]
  (if (zero? amount)
    (do-nothing state)
    (let [var-name (multipart-name-from-descr var-descr)
          cur-value (get-in state [:vars var-name] 0)
          new-value (+ cur-value amount)
          new-state (assoc-in state [:vars var-name] new-value)]
      {:state new-state
       :delta [:var-changed var-name amount new-value]})))

(defaction-base change-var-with-limits [state var-descr amount min-limit max-limit & [options]]
  (let [value (get-var state var-descr)
        amount (if min-limit (max amount (- min-limit value))
                   amount)
        amount (if max-limit (min amount (- max-limit value))
                   amount)]
    (perform-sequence state
                      [:change-var var-descr amount]
                      (when-let [var-name (:remember-change options)]
                        [:set-var var-name amount]))))

(defaction-base shuffle-deck [state deck-descr]
  {:state (update-in state [:zones (multipart-name-from-descr deck-descr) :cards]
                     shuffle-with-random (:rnd state))
   :delta [:deck-shuffled (multipart-name-from-descr deck-descr)]})

;; TODO viti can have several winners.  viti can have interesting outcome data
;; per player (vp, lira, etc).
(defaction-base finish-game [state mode submode & [winner-role]]
  (let [winner (player-name-by-role state winner-role)]
    {:state (assoc state :outcome [mode submode winner])
     :delta [:game-finished mode submode winner]}))

(defaction-base draw-cards [state deck-descr zone-descr amount & [options]]
  ;; Assumes that the target zone is inactive.
  (let [deck-name (multipart-name-from-descr deck-descr)
        zn (multipart-name-from-descr zone-descr)

        from-zone (get-zone state deck-name)
        to-zone (get-zone state zn)

        from-active? (zone-active? from-zone)
        to-active? (zone-active? to-zone)
        from-cards (:cards from-zone)
        [cids new-from-cards] (split-at amount from-cards)
        cids (vec cids)]
    (assert (and (not from-active?) (not to-active?))
            (format "For draw-cards, both zones should be inactive: %s %s" deck-name zn))
    (if (zero? amount)
      (do-nothing state)
      {:state (cond-> state
                true (assoc-in [:zones deck-name :cards] (vec new-from-cards))
                true (update-in [:zones zn :cards] into cids)

                (some? (:storing-in options))
                (backwards-apply (fn [the-state]
                                   (let [var-name (multipart-name-from-descr (:storing-in options))
                                         _ (assert (private-var? var-name)
                                                   (format "Var to store the drawn cids must be private: %s" var-name))
                                         val (get-var the-state var-name [])
                                         new-val (into val cids)]
                                     (update the-state :vars assoc var-name new-val)))))

       :delta [:cards-moved deck-name zn cids]})))

(defaction-base move-cards [state from to cids & [opts]]
  (if (empty? cids)
    (do-nothing state)
    (let [from (multipart-name-from-descr from)
          to (multipart-name-from-descr to)
          from-zone (get-zone state from)
          to-zone (get-zone state to)

          change-to-facedown? (:change-to-facedown? opts)

          from-cards (:cards from-zone)
          to-cards (:cards to-zone)

          from-active? (zone-active? from-zone)
          to-active? (zone-active? to-zone)

          pred (if from-active?
                 #(contains? (set cids) (:cid %))
                 #(contains? (set cids)       %))

          groups (group-by pred from-cards) ;; TODO reorder by cids, because group-by returns the order in from-cards

          new-from-cards (vec (get groups false))

          transform-active-card (fn [card]
                                  (cond-> card
                                    change-to-facedown? (assoc :facedown? true)))

          promote-cid->card (fn [cid]
                              (cond-> {:cid cid}
                                change-to-facedown? (assoc :facedown? true)))

          demote-card->cid (fn [card]
                             (:cid card))

          taken-cards (get groups true)
          added-cards (cond (and      from-active?       to-active?)  (map transform-active-card taken-cards)
                            (and (not from-active?)      to-active?)  (map promote-cid->card     taken-cards)
                            (and      from-active?  (not to-active?)) (map demote-card->cid      taken-cards)
                            :else                                     taken-cards)
          new-to-cards (into to-cards added-cards)]
      (assert (= (count new-from-cards) (- (count from-cards)
                                           (count cids)))
              (format "Must have all cids in the source zone: %s, %s" from-cards cids))
      {:state (-> state
                  (assoc-in [:zones from :cards] new-from-cards)
                  (assoc-in [:zones to :cards] new-to-cards))
       :delta (if (:new-style-delta? opts)
                [:ns-cards-moved from to (vec cids)
                 (cond-> nil
                   change-to-facedown? (assoc :change-to-facedown? true))]
                [:cards-moved from to (vec cids)])})))

(defaction-base create-cards [state zone-descr card-descrs]
  (if (empty? card-descrs)
    (do-nothing state)
    (let [zn (multipart-name-from-descr zone-descr)
          zone (get-zone state zn)
          active? (zone-active? zone)]
      {:state (update-in state [:zones zn :cards] into card-descrs)
       :delta [:cards-created zn card-descrs (if active?
                                               (map :cid card-descrs)
                                               card-descrs)]})))

(defaction-base destroy-cards [state zone-descr cids]
  (if (empty? cids)
    (do-nothing state)
    (let [zn (multipart-name-from-descr zone-descr)
          zone (get-zone state zn)
          zone-cards (:cards zone)
          active? (zone-active? zone)
          pred (if active?
                 (fn [card] (contains? (set cids) (:cid card)))
                 (fn [cid]  (contains? (set cids) cid)))
          new-cards (vec (remove pred zone-cards))
          cards (filterv pred zone-cards)]
      {:state (update state :zones
                      assoc-in [zn :cards] new-cards)
       :delta [:cards-destroyed zn (vec cids) cards]})))

(defaction-base move-card [state from to cid & [opts]]
  (move-cards state from to [cid] opts))

(defn find-active-card [state cid]
  (reduce (fn [_ zn]
            (let [cards (get-in state [:zones zn :cards])]
              (when-let [index (index-of-matching #(= (:cid %) cid)
                                                  cards)]
                (reduced [zn (get cards index) index]))))
          nil
          (keys (filter #(:a (val %))
                        (:zones state)))))

(defaction-base move-card-from-wherever [state to cid]
  ;; XXX only active cards are searched
  (let [[zn card index] (find-active-card state cid)]
    (move-cards state zn to [cid])))

(defaction-base move-all-cards-in-zone [state from to]
  (let [from (multipart-name-from-descr from)
        to (multipart-name-from-descr to)

        from-zone (get-zone state from)
        from-cards (:cards from-zone)

        from-active? (zone-active? from-zone)
        cids (if from-active?
                   (map (fn [card] (:cid card)) from-cards)
                   from-cards)]
    (if (empty? from-cards)
      (do-nothing state)
      (move-cards state from to cids))))

(defaction-base put-request [state request-map]
  {:state (assoc state
                 :request-map request-map
                 :answers {})
   :delta [:request-put request-map]})

(defaction-base easy-put-request [state role request on-answered]
  (perform-sequence state
                    [:put-request {role request}]
                    on-answered))

(defaction-base reveal-zone [state zone-name]
  {:state state
   :delta [:reveal-zone zone-name]})

(defaction-base set-card-props [state cid k v & kvs]
  (if-let [[zn card index] (find-active-card state cid)]
    (let [new-card (apply assoc card k v kvs)
          k->v (partition 2 (into [k v] kvs))
          prop-new-old-triplets (filterv some?
                                         (map (fn [[k v]]
                                                 (let [old-v (get card k)]
                                                   (when-not (= v old-v)
                                                     [k v old-v])))
                                               k->v))]
      {:state (update state :zones
                      update-in [zn :cards] assoc index new-card)
       :delta [:card-props-set zn cid prop-new-old-triplets]})
    (assert false (format "Must have active card by id %s" cid))))

(defaction-base unset-card-props [state cid k & ks]
  (if-let [[zn card index] (find-active-card state cid)]
    (let [new-card (apply dissoc card k ks)
          prop-old-pairs (filterv some?
                                  (map (fn [k]
                                         (when (contains? card k)
                                           [k (get card k)]))
                                       (into [k] ks)))]
      {:state (update state :zones
                      update-in [zn :cards] assoc index new-card)
       :delta [:card-props-unset zn cid prop-old-pairs]})
    (assert false (format "Must have active card by id %s" cid))))

(defn get-card-prop [state cid prop-name & [default]]
  (let [[zn card index] (find-active-card state cid)]
    (get card prop-name default)))

(defn get-card-props [state cid prop-name & more]
  (let [[zn card index] (find-active-card state cid)]
    (map #(get card %) (into [prop-name] more))))


(defaction-base change-card-props [state cid k n & kns]
  (let [pairs (->> (into [[k n]] (partition 2 kns))
                       (remove #(zero? (second %))))]
    (if (empty? pairs)
      (do-nothing state)
      (if-let [[zn card index] (find-active-card state cid)]
        (let [prop-change-new-triplets (mapv (fn [[k n]]
                                               [k n (+ (get card k 0) n)])
                                             pairs)
              prop->new-value (map (fn [[k n v]]
                                     [k v])
                                   prop-change-new-triplets)
              new-card (apply assoc card (apply concat prop->new-value))]
          {:state (update state :zones
                          update-in [zn :cards] assoc index new-card)
           :delta [:card-props-changed zn cid prop-change-new-triplets]})
        (assert false (format "Must have active card by id %s" cid))))))

(defaction-base change-card-prop [state cid prop-name amount]
  (change-card-props state cid prop-name amount))

(defaction-base create-figure [state space-descr figure-id]
  (let [space-name (multipart-name-from-descr space-descr)
        figures (get-in state [:spaces space-name] [])]
    {:state (update state :spaces
                    assoc space-name (conj figures figure-id))
     :delta [:figure-created space-name figure-id (count figures)]}))

(defaction-base destroy-figure [state space-descr figure-id]
  (let [space-name (multipart-name-from-descr space-descr)
        figures (get-in state [:spaces space-name] [])
        index (index-of figure-id figures)]
    (assert (some? index)
            (format "Must have figure '%s' in space to destroy '%s'" figure-id space-name))
    {:state (update state :spaces
                    assoc space-name (remove-from-vector-at-index figures index))
     :delta [:figure-destroyed space-name figure-id index]}))

(defaction-base move-figure [state from-space-descr to-space-descr figure-id & [options]]
  (let [from-name (multipart-name-from-descr from-space-descr)
        to-name (multipart-name-from-descr to-space-descr)

        from-figures (get-in state [:spaces from-name])
        to-figures (get-in state [:spaces to-name] [])
        index (index-of figure-id from-figures)]
    (assert (some? index)
            (format "Must have figure '%s' in source space to move from '%s' to '%s'" figure-id from-name to-name))
    {:state (update state :spaces
                    (fn [spaces]
                      (-> spaces
                          (assoc from-name (remove-from-vector-at-index from-figures index))
                          (assoc to-name (conj to-figures figure-id)))))
     :delta [:figure-moved from-name to-name figure-id index nil options]}))

(defmulti all-playing-roles++ (fn [state] (:code state)))

(defmulti lens-creation-options (fn [state] (:code state)))

(defmethod lens-creation-options :default
  [state]
  nil)
