(ns thm.backend-utils.cookies
  (:require [taoensso.carmine.ring :refer [carmine-store]]
            [taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug)]
            [ring.middleware.session.store :as store]
            [thm.config :refer [config]]
  ))

(def st (:store config))

(defn create-session-cookie [cookie-key data]
  (debug "create-session-cookie" cookie-key data)
  (store/write-session st cookie-key data))

(defn read-session-cookie [cookie-key]
  (let [r (store/read-session st cookie-key)]
    (debug "read-session-cookie" cookie-key r)
    r))

(defn update-session-cookie [cookie-key f & args]
  (let [value (or (read-session-cookie cookie-key) {})
        new-value (apply f value args)]
    (debug "update-session-cookie" cookie-key new-value)
    (store/write-session st cookie-key new-value)))
