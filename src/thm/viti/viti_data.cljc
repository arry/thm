(ns thm.viti.viti-data
  (:require [thm.utils :refer [get-and-dissoc shorten-title
                               denumber-id]]))

(defn prepare-designs [common designs]
  (vec (mapcat (fn [design]
                 (let [[copies custom-cid d] (get-and-dissoc design :copies :custom-cid)
                       copies (or copies 1)
                       base-cid (or custom-cid
                                    (shorten-title (:title design)))]
                   (map (fn [number]
                          (merge (assoc d
                                        :cid (if (> copies 1)
                                               (str base-cid ":" number)
                                               base-cid))
                                 common))
                        (range 1 (inc copies)))))
               designs)))

(def green-designs
  (prepare-designs
   {:type "green"}
   [{:title "Sangiovese",         :red 1, :white 0, :copies 4}
    {:title "Malvasia",           :red 0, :white 1, :copies 4}
    {:title "Pinot",              :red 1, :white 1, :copies 6, :req :trel}
    {:title "Syrah",              :red 2, :white 0, :copies 5, :req :trel}
    {:title "Trebbiano",          :red 0, :white 2, :copies 5, :req :trel}
    {:title "Merlot",             :red 3, :white 0, :copies 5, :req :irr}
    {:title "Sauvignon Blanc",    :red 0, :white 3, :copies 5, :req :irr}
    {:title "Cabernet Sauvignon", :red 4, :white 0, :copies 4, :req :trel+irr}
    {:title "Chardonnay",         :red 0, :white 4, :copies 4, :req :trel+irr}]))

(def purple-designs
  (prepare-designs
   {:type "purple"}
   [{:title "Red-1, white-3"            :custom-cid "r1w3", :vp 2, :rp 1, :wines {:red [1], :white [3]}}
    {:title "Red-2, white-2, blush-5"   :custom-cid "r2w2b5", :vp 5, :rp 2, :wines {:red [2], :white [2], :blush [5]}}
    {:title "Red-2, white-2"            :custom-cid "r2w2", :vp 2, :rp 1, :wines {:red [2], :white [2]}}
    {:title "Red-2, white-4"            :custom-cid "r2w4", :vp 3, :rp 1, :wines {:red [2], :white [4]}}
    {:title "White-2, sparkling-8"      :custom-cid "w2s8", :vp 6, :rp 2, :wines {:white [2], :sparkling [8]}}
    {:title "Red-3, white-1"            :custom-cid "r3w1", :vp 2, :rp 1, :wines {:red [3], :white [1]}}
    {:title "Red-3, white-3"            :custom-cid "r3w3", :vp 3, :rp 1, :wines {:red [3], :white [3]}}
    {:title "Red-3, white-5"            :custom-cid "r3w5", :vp 4, :rp 1, :wines {:red [3], :white [5]}}
    {:title "Red-3, blush-7"            :custom-cid "r3b7", :vp 5, :rp 2, :wines {:red [3], :blush [7]}}
    {:title "White-4, white-3, white-2" :custom-cid "w4w3w2", :vp 4, :rp 1, :wines {:white [4 3 2]}}
    {:title "White-3, blush-7"          :custom-cid "w3b7", :vp 5, :rp 2, :wines {:white [3], :blush [7]}}
    {:title "White-3, sparkling-7"      :custom-cid "w3s7", :vp 6, :rp 2, :wines {:white [3], :sparkling [7]}}
    {:title "White-3, sparkling-8"      :custom-cid "w3s8", :vp 6, :rp 2, :wines {:white [3], :sparkling [8]}}
    {:title "Blush-4"                   :custom-cid "b4", :vp 2, :rp 1, :wines {:blush [4]}}
    {:title "Red-4, white-2"            :custom-cid "r4w2", :vp 3, :rp 1, :wines {:red [4], :white [2]}}
    {:title "Red-4, red-3, red-2"       :custom-cid "r4r3r2", :vp 4, :rp 1, :wines {:red [4 3 2]}}
    {:title "Red-4, red-3"              :custom-cid "r4r3", :vp 3, :rp 1, :wines {:red [4 3]}}
    {:title "Red-4, blush-4"            :custom-cid "r4b4", :vp 4, :rp 1, :wines {:red [4], :blush [4]}}
    {:title "White-4, white-3"          :custom-cid "w4w3", :vp 3, :rp 1, :wines {:white [4 3]}}
    {:title "White-4, blush-4"          :custom-cid "w4b4", :vp 4, :rp 1, :wines {:white [4], :blush [4]}}
    {:title "White-4, sparkling-7"      :custom-cid "w4s7", :vp 6, :rp 2, :wines {:white [4], :sparkling [7]}}
    {:title "Red-5, white-3"            :custom-cid "r5w3", :vp 4, :rp 1, :wines {:red [5], :white [3]}}
    {:title "Red-5"                     :custom-cid "r5", :vp 2, :rp 1, :wines {:red [5]}}
    {:title "White-5"                   :custom-cid "w5", :vp 2, :rp 1, :wines {:white [5]}}
    {:title "Blush-6, blush-5"          :custom-cid "b6b5", :vp 6, :rp 2, :wines {:blush [6 5]}}
    {:title "Blush-6"                   :custom-cid "b6", :vp 3, :rp 1, :wines {:blush [6]}}
    {:title "Red-6, white-6"            :custom-cid "r6w6", :vp 5, :rp 2, :wines {:red [6], :white [6]}}
    {:title "Red-6"                     :custom-cid "r6", :vp 3, :rp 1, :wines {:red [6]}}
    {:title "White-6"                   :custom-cid "w6", :vp 3, :rp 1, :wines {:white [6]}}
    {:title "Red-7, red-6"              :custom-cid "r7r6", :vp 5, :rp 2, :wines {:red [7 6]}}
    {:title "Sparkling-7"               :custom-cid "s7", :vp 4, :rp 1, :wines {:sparkling [7]}}
    {:title "White-7, white-6"          :custom-cid "w7w6", :vp 5, :rp 2, :wines {:white [7 6]}}
    {:title "Blush-8"                   :custom-cid "b8", :vp 4, :rp 1, :wines {:blush [8]}}
    {:title "Red-8"                     :custom-cid "r8", :vp 4, :rp 1, :wines {:red [8]}}
    {:title "White-8"                   :custom-cid "w8", :vp 4, :rp 1, :wines {:white [8]}}
    {:title "Sparkling-9"               :custom-cid "s9", :vp 5, :rp 2, :wines {:sparkling [9]}}]))

(def yellow-designs
  (prepare-designs
   {:type "yellow"}
   [{:title "Surveyor"
     :text "Gain 2 lira for each empty field you own OR gain 1 vp for each planted field you own."}
    {:title "Broker"
     :text "Pay 9 lira to gain 3 vp OR lose 2 vp to gain 6 lira."}
    {:title "Wine Critic"
     :text "Draw 2 [b] OR discard 1 [w] of value 7 or more to gain 4 vp."}
    {:title "Novice Guide"
     :text "Gain 3 lira OR make up to 2 [w]."}
    {:title "Uncertified Broker"
     :text "Lose 3 vp to gain 9 lira OR pay 6 lira to gain 2 vp."}
    {:title "Planter"
     :text "Plant up to 2 [g] and gain 1 lira OR uproot and discard 1 [g] to gain 2 vp."}
    {:title "Uncertified Architect"
     :text "Lose 1 vp to build a 2 lira or 3 lira structure or lose 2 vp to build any structure."}
    {:title "Patron"
     :text "Gain 4 lira OR draw 1 [p] and 1 [b]."}
    {:title "Auctioneer"
     :text "Discard 2 [c] to gain 4 lira OR discard 4 [c] to gain 3 vp."}
    {:title "Horticulturist"
     :text "Plant any 1 [g] even if you don't have the required structure(s) OR uproot and discard 2 [g] to gain 3 vp."}
    {:title "Peddler"
     :text "Discard 2 [c] to draw 1 of each type of card."}
    {:title "Banker"
     :text "Gain 5 lira. Each opponent may lose 1 vp to gain 3 lira."}
    {:title "Grower"
     :text "Plant 1 [g]. Then, if you have planted a total of at least 6 [g], gain 2 vp."}
    {:title "Negotiator"
     :text "Discard 1 [r/wg] to gain 1 [residual] OR discard 1 [w] to gain 2 [residual]."}
    {:title "Cultivator"
     :text "Plant 1 [g]. You may plant it on a field even if the total value of that field exceeds the max vine value."}
    {:title "Swindler"
     :text "Each opponent may give you 2 lira. For each oponent who does not, gain 1 vp."}
    {:title "Producer"
     :text "Pay 2 lira to retrieve up to 2 [worker] from other actions. They may be used again this year."}
    {:title "Organizer"
     :text "Move your [rooster] to an empty row on wake-up chart, take the bonus, then pass to the next season."}
    {:title "Volunteer Crew"
     :text "All players may plant 1 [g]. Gain 2 lira for each opponent who does this."}
    {:title "Wedding Party"
     :text "Pay up to 3 opponents 2 lira each. Gain 1 vp for each of those opponents."}
    {:title "Blacksmith"
     :text "Build a structure at a 2 lira discount. If it is a [5 lira] or [6 lira] structure, also gain 1 vp."}
    {:title "Contractor"
     :text "Choose 2: Gain 1 vp, build 1 structure, or plant 1 [g]."}
    {:title "Tour Guide"
     :text "Gain 4 lira OR harvest 1 field."}
    {:title "Buyer"
     :text "Pay 2 lira to place a [1-value r/wg] on your crush pad OR discard 1 [r/wg] to gain 2 lira and 1 vp."}
    {:title "Landscaper"
     :text "Draw 1 [g] and plant 1 [g] OR switch 2 [g] on your fields."}
    {:title "Architect"
     :text "Build a structure ata 3 lira discount OR gain 1 vp for each [4-cost] structure you have built."}
    {:title "Entertainer"
     :text "Pay 4 lira to draw 3 [b] OR discard 1 [w] and 3 visitor cards to gain 3 vp."}
    {:title "Vendor"
     :text "Draw 1 [g], 1 [p] and 1 [b]. Each opponent may draw 1 [y]."}
    {:title "Handyman"
     :text "All players may build 1 structure at a 2 lira discount. You gain 1 vp for each opponent who does this."}
    {:title "Overseer"
     :text "Build 1 structure at its regular cost and plant 1 [g]. If it is a 4-value [g], gain 1 vp."}
    {:title "Importer"
     :text "Draw 3 [b] unless all opponents combine to give you 3 visitor cards (total)."}
    {:title "Sharecropper"
     :text "Plant 1 [g] even without the required structure(s) OR uproot and discard 1 [g] to gain 2 vp."}
    {:title "Homesteader"
     :text "Build 1 structure at a 3 lira discount OR plant up to 2 [g]. You may lose 1 vp to do both."}
    {:title "Planner"
     :text "Place a worker on an action in a future season. Take the action at the beginning of that season."}
    {:title "Agriculturist"
     :text "Plant 1 [g]. Then, if you have at least 3 different types of [g] planted on that field, gain 2 vp."}
    {:title "Sponsor"
     :text "Draw 2 [g] OR gain 3 lira. You may lose 1 vp to do both."}
    {:title "Artisan"
     :text "Choose 1: Gain 3 lira, build a structure at a 1 lira discount, or plant up to to 2 [g]."}
    {:title "Stonemason"
     :text "Pay 8 lira to build any 2 structures (ignore their regular costs)."}]))

;; Note: Governess and Governor shorten to the same cids; as do Christian and
;; Christine.  We use 'mal'/'fem' prefix for those.

(def blue-designs
  (prepare-designs
   {:type "blue"}
   [{:title "Merchant"
     :text "Pay 3 lira to place [1-value rg] and [1-value wg] on your crush pad OR fill 1 [p] and gain 1 vp extra."}
    {:title "Marketer"
     :text "Draw 2 [y] and gain 1 lira OR fill 1 [p] and gain 1 vp extra."}
    {:title "Crush Expert"
     :text "Gain 3 lira and draw 1 [p] OR make up to 3 [w]."}
    {:title "Uncertified Teacher"
     :text "Lose 1 vp to train 1 [worker] OR gain 1 vp for each opponent who has a total of 6 [worker]."}
    {:title "Queen"
     :text "The player on your right must choose 1: lose 1 vp, give you 2 [c], or pay you 3 lira."}
    {:title "Harvester"
     :text "Harvest up to 2 fields and choose 1: Gain 2 lira or gain 1 vp."}
    {:title "Professor"
     :text "Pay 2 lira to train 1 [worker] OR gain 2 vp if you have a total of 6 [worker]."}
    {:title "Mentor"
     :text "All players may make up to 2 [wine]. Draw 1 [g] or 1 [y] for each opponent who does this."}
    {:title "Harvest Expert"
     :text "Harvest 1 field and either draw 1 [g] or pay 1 lira to build a Yoke."}
    {:title "Innkeeper"
     :text "As you play this card, put the top card of 2 different discard piles into your hand."}
    {:title "Scholar"
     :text "Draw 2 [p] OR pay 3 lira to train [w]. You may lose 1 vp to do both."}
    {:title "Reaper"
     :text "Harvest up to 3 fields. If you harvest 3 fields, gain 2 vp."}
    {:title "Motivator"
     :text "Each player may retrieve their grande worker. Gain 1 vp for each opponent who does this."}
    {:title "Laborer"
     :text "Harvest up to 2 fields OR make up to 3 [w]. You may lose 1 vp to do both."}
    {:title "Designer"
     :text "Build 1 structure at its regular cost. Then, if you have at least 6 structures, gain 2 vp."}
    {:title "Governess"
     :text "Pay 3 lira to train 1 [w] that you may use this year OR discard 1 [w] to gain 2 vp.", :custom-cid "femgov"}
    {:title "Governor"
     :text "Choose up to 3 opponents to each give you 1 [y]. Gain 1 vp for each of them who cannot." :custom-cid "malgov"}
    {:title "Taster"
     :text "Discard 1 [w] to gain 4 lira. If it is the most valuable [w] in any player's cellar, gain 2 vp."}
    {:title "Caravan"
     :text "Turn the top card of each deck face up. Draw 2 of those cards and discard the others."}
    {:title "Crusher"
     :text "Gain 3 lira and draw 1 [y] OR draw 1 [p] and make up to 2 [w]."}
    {:title "Judge"
     :text "Draw 2 [y] OR discard 1 [w] of value 4 or more to gain 3 vp."}
    {:title "Oenologist"
     :text "Age all [w] in your cellar twice OR pay 3 lira to upgrade your cellar to the next level."}
    {:title "Teacher"
     :text "Make up to 2 [w] OR pay 2 lira to train 1 [w]."}
    {:title "Benefactor"
     :text "Draw 1 [v] and gain 1 [y] OR discard 2 visitor cards to gain 2 vp."}
    {:title "Assessor"
     :text "Gain 1 lira for each card in your hand OR discard your hand (min of 1 card) to gain 2 vp."}
    {:title "Master vintner"
     :text "Upgrade your cellar to the next level at a 2 lira discount OR age 1 [w] and fill 1 [p]."}
    {:title "Uncertified Oenologist"
     :text "Age all [w] in your cellar twice OR lose 1 vp to upgrade your cellar to the next level."}
    {:title "Promoter"
     :text "Discard [r/wg] or [w] to gain 1 vp and 1 [residual]."}
    {:title "Jack-of-all-trades"
     :text "Choose 2: Harvest 1 field, make up to 2 [w], or fill 1 [p]."}
    {:title "Politician"
     :text "If you have less than 0 vp, gain 6 lira. Otherwise, draw 1 [g], 1 [y], and 1 [p]."}
    {:title "Supervisor"
     :text "Make up to 2 [w]. Gain 1 vp for each sparkling wine token you make."}
    {:title "Bottler"
     :text "Make up to 3 [w]. Gain 1 vp for each type of wine you make."}
    {:title "Craftsman"
     :text "Choose 2: Draw 1 [p], upgrade your cellar at the regular cost, or gain 1 vp."}
    {:title "Exporter"
     :text "Choose 1: Make up to 2 [w], fill 1 [p], or discard 1 [r/wg] to gain 2 vp."}
    {:title "Manager"
     :text "Take any action (no bonus) from a previous season without placing a worker."}
    {:title "Zymologist"
     :text "Make up to 2 [w] of value 4 or greater, even if you haven't upgraded your cellar."}
    {:title "Noble"
     :text "Pay 1 lira to gain 1 [residual] or lose 2 [residual] to gain 2 vp."}
    {:title "Guest Speaker"
     :text "All players may pay 1 lira to train 1 [worker]. Gain 1 vp for each opponent who does this."}]))

(def mama-designs
  (prepare-designs
   {:type "mama"}
   [{:title "Naja" :text "y b b", :effects {:y 1 :b 2}}

    {:title "Laura" :text "g p p", :effects {:g 1 :p 2}}

    {:title "Falon" :text "p b b", :effects {:p 1 :b 2}}

    {:title "Jess" :text "y p p", :effects {:y 1 :p 2}}

    {:title "Nicole" :text "2 lira, g b", :effects {:two-lira 1 :g 1 :b 1}}

    {:title "Casey" :text "p p b", :effects {:p 2 :b 1}}

    {:title "Ariel" :text "2 lira, y p", :effects {:two-lira 1 :y 1 :p 1}}

    {:title "Christine" :text "g b b", :effects {:g 1 :b 2}, :custom-cid "femchr"}

    {:title "Danyel" :text "y y b", :effects {:y 2 :b 1}}

    {:title "Rebecca" :text "y y p", :effects {:y 2 :p 1}}

    {:title "Emily" :text "g y y", :effects {:g 1 :y 2}}

    {:title "Teruyo" :text "g g b", :effects {:g 2 :b 1}}

    {:title "Deann" :text "g p b", :effects {:g 1 :p 1 :b 1}}

    {:title "Margot" :text "y p b", :effects {:y 1 :p 1 :b 1}}

    {:title "Margaret" :text "g g y", :effects {:g 2 :y 1}}

    {:title "Nici" :text "g g p", :effects {:g 2 :p 1}}

    {:title "Alaena" :text "g y p", :effects {:g 1 :y 1 :p 1}}

    {:title "Alyssa" :text "g y b", :effects {:g 1 :y 1 :b 1}}]))

(def papa-designs
  (prepare-designs
   {:type "papa"}
   [{:title "Rafael" :base-lira 2, :or-stuff :worker, :or-lira 4}

    {:title "Joel" :base-lira 4, :or-stuff :medium-cellar, :or-lira 3}

    {:title "Steven" :base-lira 6, :or-stuff :yoke, :or-lira 1}

    {:title "Gary" :base-lira 3, :or-stuff :worker, :or-lira 3}

    {:title "Raymond" :base-lira 3, :or-stuff :cottage, :or-lira 3}

    {:title "Stephan" :base-lira 4, :or-stuff :irrigation, :or-lira 2}

    {:title "Morten" :base-lira 4, :or-stuff :one-vp, :or-lira 3}

    {:title "Alan" :base-lira 5, :or-stuff :one-vp, :or-lira 2}

    {:title "Jerry" :base-lira 2, :or-stuff :windmill, :or-lira 4}

    {:title "Trevor" :base-lira 1, :or-stuff :tasting-room, :or-lira 5}

    {:title "Paul" :base-lira 5, :or-stuff :trellis, :or-lira 1}

    {:title "Matt" :base-lira 0, :or-stuff :tasting-room, :or-lira 6}

    {:title "Matthew" :base-lira 1, :or-stuff :windmill, :or-lira 5}

    {:title "Kozi" :base-lira 2, :or-stuff :cottage, :or-lira 4}

    {:title "Josh" :base-lira 3, :or-stuff :medium-cellar, :or-lira 4}

    {:title "Jay" :base-lira 5, :or-stuff :yoke, :or-lira 2}

    {:title "Andrew" :base-lira 4, :or-stuff :trellis, :or-lira 2}

    {:title "Christian" :base-lira 3, :or-stuff :irrigation, :or-lira 3, :custom-cid "malchr"}]))

(let [prepare (fn [v designs]
                (into v (map (fn [design]
                               [(denumber-id (:cid design)) design])
                             designs)))]
  (def cid->design
    (-> {}
        (prepare green-designs)
        (prepare yellow-designs)
        (prepare purple-designs)
        (prepare blue-designs)
        (prepare mama-designs)
        (prepare papa-designs))))

(def zones-data [{:name "hand", :v :you, :per-player? true}
                 {:name "f5", :v :all, :per-player? true}
                 {:name "f6", :v :all, :per-player? true}
                 {:name "f7", :v :all, :per-player? true}
                 {:name "starting", :v :all, :per-player? true}

                 {:name "in-play", :v :all }

                 {:name "green-deck", :v :none}
                 {:name "green-discard", :v :all}

                 {:name "yellow-deck", :v :none}
                 {:name "yellow-discard", :v :all}

                 {:name "purple-deck", :v :none}
                 {:name "purple-discard", :v :all}

                 {:name "blue-deck", :v :none}
                 {:name "blue-discard", :v :all}

                 {:name "mama-deck", :v :none}

                 {:name "papa-deck", :v :none}])

(def all-field-names ["f5" "f6" "f7"])


(do
  #?@(:cljs
      [
       (def board-sizes
         {:board {:width 1202 :height 961}
          :vineyard {:width 1224 :height 829}})

       (def widget-data
         {:board
          [{:start {:x 53, :y 58}, :finish {:x 168, :y 227}, :name "green-deck"}
           {:start {:x 194, :y 58}, :finish {:x 309, :y 227}, :name "green-discard", :discard-zone? true}
           {:start {:x 334, :y 58}, :finish {:x 450, :y 228}, :name "yellow-deck"}
           {:start {:x 476, :y 58}, :finish {:x 591, :y 227}, :name "yellow-discard", :discard-zone? true}
           {:start {:x 615, :y 55}, :finish {:x 730, :y 224}, :name "purple-deck"}
           {:start {:x 756, :y 54}, :finish {:x 871, :y 223}, :name "purple-discard", :discard-zone? true}
           {:start {:x 898, :y 55}, :finish {:x 1010, :y 222}, :name "blue-deck"}
           {:start {:x 1039, :y 54}, :finish {:x 1151, :y 222}, :name "blue-discard", :discard-zone? true}
           {:start {:x 345, :y 345}, :finish {:x 387, :y 377}, :name "sv1", :rounded? true}
           {:start {:x 386, :y 361}, :finish {:x 428, :y 392}, :name "sv2", :rounded? true}
           {:start {:x 428, :y 376}, :finish {:x 469, :y 407}, :name "sv3", :rounded? true}
           {:start {:x 271, :y 493}, :finish {:x 313, :y 524}, :name "dv1", :rounded? true}
           {:start {:x 306, :y 470}, :finish {:x 348, :y 501}, :name "dv2", :rounded? true}
           {:start {:x 337, :y 445}, :finish {:x 379, :y 477}, :name "dv3", :rounded? true}
           {:start {:x 408, :y 515}, :finish {:x 450, :y 547}, :name "sgf1", :rounded? true}
           {:start {:x 454, :y 521}, :finish {:x 496, :y 553}, :name "sgf2", :rounded? true}
           {:start {:x 499, :y 525}, :finish {:x 541, :y 557}, :name "sgf3", :rounded? true}
           {:start {:x 174, :y 665}, :finish {:x 216, :y 696}, :name "gt1", :rounded? true}
           {:start {:x 212, :y 643}, :finish {:x 254, :y 674}, :name "gt2", :rounded? true}
           {:start {:x 248, :y 618}, :finish {:x 290, :y 650}, :name "gt3", :rounded? true}
           {:start {:x 296, :y 813}, :finish {:x 338, :y 845}, :name "bs1", :rounded? true}
           {:start {:x 324, :y 787}, :finish {:x 366, :y 818}, :name "bs2", :rounded? true}
           {:start {:x 350, :y 758}, :finish {:x 392, :y 789}, :name "bs3", :rounded? true}
           {:start {:x 474, :y 621}, :finish {:x 516, :y 652}, :name "pv1", :rounded? true}
           {:start {:x 515, :y 637}, :finish {:x 557, :y 667}, :name "pv2", :rounded? true}
           {:start {:x 557, :y 652}, :finish {:x 598, :y 683}, :name "pv3", :rounded? true}
           {:start {:x 591, :y 450}, :finish {:x 626, :y 478}, :name "do1", :rounded? true}
           {:start {:x 632, :y 436}, :finish {:x 671, :y 463}, :name "do2", :rounded? true}
           {:start {:x 678, :y 422}, :finish {:x 718, :y 450}, :name "do3", :rounded? true}
           {:start {:x 851, :y 320}, :finish {:x 892, :y 351}, :name "wv1", :rounded? true}
           {:start {:x 895, :y 329}, :finish {:x 937, :y 361}, :name "wv2", :rounded? true}
           {:start {:x 941, :y 336}, :finish {:x 983, :y 368}, :name "wv3", :rounded? true}
           {:start {:x 677, :y 520}, :finish {:x 717, :y 551}, :name "hf1", :rounded? true}
           {:start {:x 715, :y 497}, :finish {:x 755, :y 527}, :name "hf2", :rounded? true}
           {:start {:x 753, :y 473}, :finish {:x 788, :y 501}, :name "hf3", :rounded? true}
           {:start {:x 868, :y 521}, :finish {:x 909, :y 551}, :name "mw1", :rounded? true}
           {:start {:x 909, :y 540}, :finish {:x 949, :y 569}, :name "mw2", :rounded? true}
           {:start {:x 951, :y 557}, :finish {:x 990, :y 587}, :name "mw3", :rounded? true}
           {:start {:x 712, :y 734}, :finish {:x 752, :y 766}, :name "tw1", :rounded? true}
           {:start {:x 753, :y 719}, :finish {:x 794, :y 751}, :name "tw2", :rounded? true}
           {:start {:x 796, :y 706}, :finish {:x 838, :y 738}, :name "tw3", :rounded? true}
           {:start {:x 605, :y 753}, :finish {:x 691, :y 815}, :name "cart", :rounded? true}
           {:start {:x 876, :y 705}, :finish {:x 917, :y 736}, :name "fo1", :rounded? true}
           {:start {:x 910, :y 682}, :finish {:x 951, :y 712}, :name "fo2", :rounded? true}
           {:start {:x 942, :y 659}, :finish {:x 984, :y 690}, :name "fo3", :rounded? true}
           {:start {:x 1070, :y 746}, :finish {:x 1113, :y 790}, :name "rp0", :marker? true}
           {:start {:x 1091, :y 747}, :finish {:x 1034, :y 705}, :name "rp1", :marker? true}
           {:start {:x 1091, :y 705}, :finish {:x 1154, :y 747}, :name "rp2", :marker? true}
           {:start {:x 1154, :y 747}, :finish {:x 1113, :y 796}, :name "rp3", :marker? true}
           {:start {:x 1113, :y 796}, :finish {:x 1069, :y 833}, :name "rp4", :marker? true}
           {:start {:x 1069, :y 798}, :finish {:x 1024, :y 758}, :name "rp5", :marker? true}
           {:start {:x 30, :y 500}, :finish {:x 168, :y 530}, :name "r0f", :marker? true}
           {:start {:x 30, :y 545}, :finish {:x 72, :y 575}, :name "r1l", :marker? true}
           {:start {:x 72, :y 545}, :finish {:x 168, :y 575}, :name "r1r", :marker? true}
           {:start {:x 30, :y 545}, :finish {:x 168, :y 575}, :name "r1f"}
           {:start {:x 30, :y 575}, :finish {:x 72, :y 615}, :name "r2l", :marker? true}
           {:start {:x 72, :y 575}, :finish {:x 168, :y 615}, :name "r2r", :marker? true}
           {:start {:x 30, :y 575}, :finish {:x 168, :y 615}, :name "r2f"}
           {:start {:x 30, :y 615}, :finish {:x 72, :y 655}, :name "r3l", :marker? true}
           {:start {:x 72, :y 615}, :finish {:x 168, :y 655}, :name "r3r", :marker? true}
           {:start {:x 30, :y 615}, :finish {:x 168, :y 655}, :name "r3f"}
           {:start {:x 30, :y 655}, :finish {:x 72, :y 695}, :name "r4l", :marker? true}
           {:start {:x 72, :y 655}, :finish {:x 168, :y 695}, :name "r4r", :marker? true}
           {:start {:x 30, :y 655}, :finish {:x 168, :y 695}, :name "r4f"}
           {:start {:x 30, :y 695}, :finish {:x 72, :y 735}, :name "r5l", :marker? true}
           {:start {:x 72, :y 695}, :finish {:x 168, :y 735}, :name "r5r", :marker? true}
           {:start {:x 30, :y 695}, :finish {:x 168, :y 735}, :name "r5f"}
           {:start {:x 30, :y 735}, :finish {:x 72, :y 775}, :name "r6l", :marker? true}
           {:start {:x 72, :y 735}, :finish {:x 168, :y 775}, :name "r6r", :marker? true}
           {:start {:x 30, :y 735}, :finish {:x 168, :y 775}, :name "r6f"}
           {:start {:x 30, :y 775}, :finish {:x 72, :y 815}, :name "r7l", :marker? true}
           {:start {:x 72, :y 775}, :finish {:x 168, :y 815}, :name "r7r", :marker? true}
           {:start {:x 30, :y 775}, :finish {:x 168, :y 815}, :name "r7f"}
           {:start {:x 66, :y 862}, :finish {:x 104, :y 924}, :name "vp-5", :marker? true}
           {:start {:x 105, :y 862}, :finish {:x 135, :y 924}, :name "vp-4", :marker? true}
           {:start {:x 138, :y 862}, :finish {:x 170, :y 924}, :name "vp-3", :marker? true}
           {:start {:x 171, :y 862}, :finish {:x 203, :y 924}, :name "vp-2", :marker? true}
           {:start {:x 204, :y 862}, :finish {:x 240, :y 924}, :name "vp-1", :marker? true}
           {:start {:x 241, :y 862}, :finish {:x 275, :y 924}, :name "vp0", :marker? true}
           {:start {:x 275, :y 862}, :finish {:x 309, :y 924}, :name "vp1", :marker? true}
           {:start {:x 309, :y 862}, :finish {:x 343, :y 924}, :name "vp2", :marker? true}
           {:start {:x 343, :y 862}, :finish {:x 377, :y 924}, :name "vp3", :marker? true}
           {:start {:x 377, :y 862}, :finish {:x 411, :y 924}, :name "vp4", :marker? true}
           {:start {:x 412, :y 862}, :finish {:x 446, :y 924}, :name "vp5", :marker? true}
           {:start {:x 446, :y 862}, :finish {:x 480, :y 924}, :name "vp6", :marker? true}
           {:start {:x 480, :y 862}, :finish {:x 514, :y 924}, :name "vp7", :marker? true}
           {:start {:x 514, :y 862}, :finish {:x 548, :y 924}, :name "vp8", :marker? true}
           {:start {:x 548, :y 862}, :finish {:x 582, :y 924}, :name "vp9", :marker? true}
           {:start {:x 584, :y 862}, :finish {:x 618, :y 924}, :name "vp10", :marker? true}
           {:start {:x 618, :y 862}, :finish {:x 652, :y 924}, :name "vp11", :marker? true}
           {:start {:x 652, :y 862}, :finish {:x 686, :y 924}, :name "vp12", :marker? true}
           {:start {:x 686, :y 862}, :finish {:x 720, :y 924}, :name "vp13", :marker? true}
           {:start {:x 720, :y 862}, :finish {:x 753, :y 924}, :name "vp14", :marker? true}
           {:start {:x 752, :y 862}, :finish {:x 786, :y 924}, :name "vp15", :marker? true}
           {:start {:x 786, :y 862}, :finish {:x 820, :y 924}, :name "vp16", :marker? true}
           {:start {:x 820, :y 862}, :finish {:x 854, :y 924}, :name "vp17", :marker? true}
           {:start {:x 854, :y 862}, :finish {:x 888, :y 924}, :name "vp18", :marker? true}
           {:start {:x 888, :y 862}, :finish {:x 922, :y 924}, :name "vp19", :marker? true}
           {:start {:x 925, :y 862}, :finish {:x 960, :y 924}, :name "vp20", :marker? true}
           {:start {:x 959, :y 862}, :finish {:x 993, :y 924}, :name "vp21", :marker? true}
           {:start {:x 993, :y 862}, :finish {:x 1027, :y 924}, :name "vp22", :marker? true}
           {:start {:x 1027, :y 862}, :finish {:x 1061, :y 924}, :name "vp23", :marker? true}
           {:start {:x 1061, :y 862}, :finish {:x 1095, :y 924}, :name "vp24", :marker? true}
           {:start {:x 1097, :y 862}, :finish {:x 1135, :y 924}, :name "vp25", :marker? true}

           {:start {:x 315, :y 483}, :finish {:x 388, :y 522}, :name "dv@" :rounded? true}
           {:start {:x 358, :y 370}, :finish {:x 434, :y 427}, :name "sv@" :rounded? true}
           {:start {:x 404, :y 534}, :finish {:x 554, :y 606}, :name "sgf@" :rounded? true}
           {:start {:x 512, :y 597}, :finish {:x 602, :y 651}, :name "pv@" :rounded? true}
           {:start {:x 163, :y 632}, :finish {:x 312, :y 718}, :name "gt@" :rounded? true}
           {:start {:x 330, :y 797}, :finish {:x 417, :y 848}, :name "bs@" :rounded? true}
           {:start {:x 714, :y 747}, :finish {:x 861, :y 791}, :name "tw@" :rounded? true}
           {:start {:x 922, :y 699}, :finish {:x 981, :y 732}, :name "fo@" :rounded? true}
           {:start {:x 857, :y 546}, :finish {:x 976, :y 614}, :name "mw@" :rounded? true}
           {:start {:x 875, :y 349}, :finish {:x 951, :y 401}, :name "wv@" :rounded? true}
           {:start {:x 630, :y 455}, :finish {:x 698, :y 490}, :name "do@" :rounded? true}
           {:start {:x 648, :y 456}, :finish {:x 775, :y 527}, :name "hf@" :rounded? true}

           {:start {:x 601, :y 0}, :finish {:x 601, :y 50}, :name "source", :marker? true}]

          :vineyard
          [{:start {:x 30, :y 127}, :finish {:x 114, :y 206}, :name "t"}
           {:start {:x 180, :y 130}, :finish {:x 430, :y 250}, :name "f5"}
           {:start {:x 475, :y 130}, :finish {:x 715, :y 250}, :name "f6"}
           {:start {:x 770, :y 130}, :finish {:x 1020, :y 250}, :name "f7"}
           {:start {:x 1100, :y 138}, :finish {:x 1176, :y 195}, :name "i"}
           {:start {:x 474, :y 255}, :finish {:x 555, :y 314}, :name "y", :rounded? true}
           {:start {:x 597, :y 318}, :finish {:x 664, :y 364}, :name "y-s"}
           {:start {:x 726, :y 277}, :finish {:x 875, :y 375}, :name "s"}
           {:start {:x 928, :y 373}, :finish {:x 1189, :y 458}, :name "workers", :marker? true}
           {:start {:x 612, :y 0}, :finish {:x 612, :y 50}, :name "supply", :marker? true}
           {:start {:x 248, :y 330}, :finish {:x 357, :y 430}, :name "c"}
           {:start {:x 50, :y 389}, :finish {:x 129, :y 481}, :name "n"}
           {:start {:x 819, :y 772}, :finish {:x 961, :y 812}, :name "m"}
           {:start {:x 1020, :y 775}, :finish {:x 1166, :y 818}, :name "l"}
           {:start {:x 84, :y 542}, :finish {:x 124, :y 580}, :name "pr1", :rounded? true}
           {:start {:x 144, :y 543}, :finish {:x 184, :y 580}, :name "pr2", :rounded? true}
           {:start {:x 206, :y 544}, :finish {:x 243, :y 581}, :name "pr3", :rounded? true}
           {:start {:x 137, :y 615}, :finish {:x 175, :y 652}, :name "pr4", :rounded? true}
           {:start {:x 197, :y 615}, :finish {:x 235, :y 653}, :name "pr5", :rounded? true}
           {:start {:x 257, :y 616}, :finish {:x 295, :y 654}, :name "pr6", :rounded? true}
           {:start {:x 40, :y 688}, :finish {:x 80, :y 725}, :name "pr7", :rounded? true}
           {:start {:x 102, :y 689}, :finish {:x 139, :y 727}, :name "pr8", :rounded? true}
           {:start {:x 162, :y 689}, :finish {:x 199, :y 726}, :name "pr9", :rounded? true}
           {:start {:x 428, :y 545}, :finish {:x 467, :y 583}, :name "pw1", :rounded? true}
           {:start {:x 489, :y 546}, :finish {:x 526, :y 583}, :name "pw2", :rounded? true}
           {:start {:x 549, :y 545}, :finish {:x 586, :y 584}, :name "pw3", :rounded? true}
           {:start {:x 381, :y 617}, :finish {:x 418, :y 655}, :name "pw4", :rounded? true}
           {:start {:x 441, :y 618}, :finish {:x 479, :y 656}, :name "pw5", :rounded? true}
           {:start {:x 501, :y 619}, :finish {:x 539, :y 656}, :name "pw6", :rounded? true}
           {:start {:x 404, :y 692}, :finish {:x 442, :y 730}, :name "pw7", :rounded? true}
           {:start {:x 464, :y 691}, :finish {:x 502, :y 729}, :name "pw8", :rounded? true}
           {:start {:x 524, :y 693}, :finish {:x 562, :y 731}, :name "pw9", :rounded? true}
           {:start {:x 637, :y 511}, :finish {:x 669, :y 550}, :name "cr1"}
           {:start {:x 692, :y 512}, :finish {:x 725, :y 551}, :name "cr2"}
           {:start {:x 749, :y 512}, :finish {:x 781, :y 551}, :name "cr3"}
           {:start {:x 823, :y 512}, :finish {:x 856, :y 551}, :name "cr4"}
           {:start {:x 879, :y 512}, :finish {:x 911, :y 551}, :name "cr5"}
           {:start {:x 936, :y 513}, :finish {:x 968, :y 552}, :name "cr6"}
           {:start {:x 1013, :y 514}, :finish {:x 1044, :y 552}, :name "cr7"}
           {:start {:x 1068, :y 514}, :finish {:x 1099, :y 552}, :name "cr8"}
           {:start {:x 1125, :y 514}, :finish {:x 1154, :y 553}, :name "cr9"}
           {:start {:x 638, :y 572}, :finish {:x 666, :y 610}, :name "cw1"}
           {:start {:x 693, :y 573}, :finish {:x 723, :y 611}, :name "cw2"}
           {:start {:x 751, :y 611}, :finish {:x 780, :y 572}, :name "cw3"}
           {:start {:x 826, :y 572}, :finish {:x 855, :y 611}, :name "cw4"}
           {:start {:x 882, :y 611}, :finish {:x 911, :y 574}, :name "cw5"}
           {:start {:x 938, :y 574}, :finish {:x 967, :y 612}, :name "cw6"}
           {:start {:x 1014, :y 613}, :finish {:x 1042, :y 576}, :name "cw7"}
           {:start {:x 1069, :y 575}, :finish {:x 1098, :y 613}, :name "cw8"}
           {:start {:x 1126, :y 613}, :finish {:x 1154, :y 576}, :name "cw9"}
           {:start {:x 825, :y 634}, :finish {:x 854, :y 672}, :name "cb4"}
           {:start {:x 881, :y 673}, :finish {:x 911, :y 636}, :name "cb5"}
           {:start {:x 938, :y 635}, :finish {:x 968, :y 673}, :name "cb6"}
           {:start {:x 1012, :y 673}, :finish {:x 1042, :y 636}, :name "cb7"}
           {:start {:x 1069, :y 636}, :finish {:x 1098, :y 674}, :name "cb8"}
           {:start {:x 1125, :y 674}, :finish {:x 1153, :y 636}, :name "cb9"}
           {:start {:x 1037, :y 698}, :finish {:x 1016, :y 735}, :name "cs7"}
           {:start {:x 1093, :y 736}, :finish {:x 1073, :y 698}, :name "cs8"}
           {:start {:x 1152, :y 698}, :finish {:x 1128, :y 736}, :name "cs9"}]

          })

       (def widget-by-name
         (into {} (map (fn [widget]
                         [(:name widget) widget])
                       (concat (:board widget-data)
                               (:vineyard widget-data)))))

       (def visible-board-widgets (remove :marker? (:board widget-data)))
       (def visible-vineyard-widgets (remove :marker? (:vineyard widget-data)))

       (defn position-in-marker [marker-name displacement]
         (let [{:keys [start finish]} (widget-by-name marker-name)
               width (Math/abs (- (:x finish) (:x start)))
               height (Math/abs (- (:y finish) (:y start)))
               ]
           {:x (+ (:x start) (* width (:x displacement)))
            :y (+ (:y start) (* height (:y displacement)))}))

       ]))
