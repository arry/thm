(ns thm.viti.game-ui.viti-ui
  (:require [reagent.core :as r]
            [re-frame.core :refer [dispatch subscribe]]
            [taoensso.timbre :refer-macros [debug warn warnf]]
            [clojure.string :as str]
            [baking-soda.core :as b]

            [thm.views.common :refer [tabbed-component-ng indexed-component]]
            [thm.views.game-ui-common :refer [zone-of-cards]]

            [thm.utils :refer [fit-to-range svg-arrow
                               bull
                               dispatch-e
                               multipart-name-from-descr
                               last-part-of-multipart]]

            [thm.viti.viti-data :as viti-data]
            [thm.viti.viti-utils :refer [get-figure-id figure-image]]

            [thm.game-ui.card-animator :as card-animator]
            ))

(def GAME-CODE "viti")

(defn vp-marker-name [value]
  (str "vp" (fit-to-range value -5 25)))

(defn rp-marker-name [value]
  (str "rp" value))

(defn vp-displacement [role all-playing-roles]
  ;; XXX i/(n+1) instead
  (if (= role (first all-playing-roles))
    {:x 0.33, :y 0.33}
    {:x 0.66, :y 0.66}))

(defn rp-displacement [role all-playing-roles]
  ;; XXX something else instead
  (if (= role (first all-playing-roles))
    {:x -0.5, :y -0.3}
    {:x 0.5, :y 0.3}))

(defn update-board-dependent-sizes []
  (doseq [index (range (.-length (js/document.getElementsByClassName "board-image")))
          :let [img (aget (js/document.getElementsByClassName "board-image") index)
                div-to-fit (aget (js/document.getElementsByClassName "fit-to-image") index)]
          :when (and img div-to-fit)
          :let [ratio (/ (.-clientWidth img) (.-clientWidth div-to-fit))]]
    (set! (.-transform (.-style div-to-fit)) (str "scale(" ratio ")"))))

(defn do-resize []
  (update-board-dependent-sizes))

(defn px [n]
  (str n "px"))

(defn widget-style [start finish]
  (let [x1 (:x start)  y1 (:y start)
        x2 (:x finish) y2 (:y finish)
        x (min x1 x2) y (min y1 y2)
        w (Math/abs (- x2 x1)) h (Math/abs (- y2 y1))]
    {:left (px x) :top (px y)
     :width (px w) :height (px h)}))

(defn middle-with-displacement [big-beginning big-end small-size displacement-value]
  (let [big-size (Math/abs (- big-end big-beginning))]
    (+ big-beginning
       (/ (- big-size small-size) 2)
       (* small-size (- displacement-value 0.5)))))

(defn beginning-with-displacement [big-beginning big-end small-size displacement-value]
  (let [big-size (Math/abs (- big-end big-beginning))]
    (+ big-beginning
       (* small-size displacement-value))))

(defn fit-into
  "Given the beginning and end coordinate of a big line segment's coordinate,
  and the size of a small line segment, calculate the coordinate at which to
  put the start coordinate of a small segment with a given
  gravity (:start, :center, or :finish) and offset from that, expressed as a
  multiple of small-size (e.g. offsets of 0, 1, 2 make the small sizes in a row
  with no overlap.)"
  [big-b big-e small-size gravity offset]
  (let [a (if (< big-b big-e) big-b big-e)
        b (if (< big-b big-e) big-e big-b)
        big-size (- b a)

        start (+ a (case (or gravity :center)
                     :start 0
                     :finish (- big-size small-size)
                     :center (* 0.5 (- (- b a) small-size))
                     (do (warnf "Unrecognized gravity: %s, defaulting to :center" gravity)
                         (* 0.5 (- (- b a) small-size)))))]
    (+ start (* offset small-size))))

(defn figure-at-marker [figure-id
                        marker-name
                        & [opts]]
  (let [marker (viti-data/widget-by-name marker-name)
        figure-size (or (:figure-size opts) {:width 17
                                             :height 28})
        gravity (or (:gravity opts) {:x :center, :y :center})
        displacement (or (:displacement opts) {:x 0, :y 0})
        fw (:width figure-size)
        fh (:height figure-size)
        bx (:x (:start marker))
        by (:y (:start marker))
        ex (:x (:finish marker))
        ey (:y (:finish marker))

        style {:left (fit-into bx ex fw (:x gravity) (:x displacement))
               :top  (fit-into by ey fh (:y gravity) (:y displacement))
               :width fw
               :height fh}]
    ;; TODO add :title, formatted like "John's vp: 13", for which needs role->player-name.
    [figure-image figure-id {:style style}]))

(def rooster-figure-size {:width 27, :height 30})

(def figure-sizes
  {"o" {:width 34, :height 24} ;; token, even though it doesn't follow the
                               ;; owner-type naming convention, has a unique
                               ;; second letter.

   "b" {:width 14, :height 27}
   "c" {:width 27, :height 27}
   "g" {:width 43, :height 44}
   "i" {:width 20, :height 26}
   "l" {:width 27, :height 23}
   "m" {:width 27, :height 23}
   "n" {:width 21, :height 27}
   "r" {:width 27, :height 30}
   "s" {:width 27, :height 19}
   "t" {:width 27, :height 23}
   "v" {:width 17, :height 29}
   "w" {:width 33, :height 31}
   "y" {:width 27, :height 12}})

(defn figure-size-from-id [figure-id]
  (let [letter (subs figure-id 1 2)]
    (or (figure-sizes letter)
        (warn "Unrecognized figure id to get size: " figure-id))))

(defn vp-figure [primary-role role]
  (let [val @(subscribe [:thm.game-ui/track-var primary-role (str "vp:" role)])
        all-playing-roles @(subscribe [:thm.game-ui/get-all-playing-roles primary-role])]
    [figure-at-marker (get-figure-id role :vp) (vp-marker-name val)
     {:figure-size {:width 17, :height 28}
      :gravity {:x :start, :y :start}
      :displacement (vp-displacement role all-playing-roles)}]))

(defn rp-figure [primary-role role]
  (let [val @(subscribe [:thm.game-ui/track-var primary-role (str "rp:" role)])
        all-playing-roles @(subscribe [:thm.game-ui/get-all-playing-roles primary-role])]
    [figure-at-marker (get-figure-id role :rp) (rp-marker-name val)
     {:figure-size {:width 14, :height 28}
      :gravity {:x :center, :y :center}
      :displacement (rp-displacement role all-playing-roles)}]))

(def all-rooster-spaces
  ["m:r1l" "m:r1r"
   "m:r2l" "m:r2r"
   "m:r3l" "m:r3r"
   "m:r4l" "m:r4r"
   "m:r5l" "m:r5r"
   "m:r6l" "m:r6r"
   "m:r7l" "m:r7r"])

(def all-main-board-action-spaces
  ;; TODO
  ["m:dv1" "m:dv@"
   "m:gt1" "m:gt@"
   "m:bs1" "m:bs@"
   "m:sv1" "m:sv@"
   "m:sgf1" "m:sgf@"
   "m:pv1" "m:pv@"

   "m:do1" "m:do@"
   "m:hf1" "m:hf@"
   "m:tw1" "m:tw@"
   "m:wv1" "m:wv@"
   "m:mw1" "m:mw@"
   "m:fo1" "m:fo@"

   "m:cart"
   ])

(defn -space-with-figures-internal [opts primary-role space-name figures animation-info]
  (let [marker (viti-data/widget-by-name (last-part-of-multipart space-name))
        figure-size rooster-figure-size ;; XXX depends on the figures
        bx (:x (:start marker))
        by (:y (:start marker))
        ex (:x (:finish marker))
        ey (:y (:finish marker))

        space-style {:left (:x (:start marker))
                     :top (:y (:start marker))
                     :width (- (:x (:finish marker)) (:x (:start marker)))
                     :height (- (:y (:finish marker)) (:y (:start marker)))}
        gravity (or (:gravity opts) {:x :center, :y :center})]
    (r/create-class
     {:display-name "-space-with-figures-internal"

      :component-did-mount (fn [this]
                             (card-animator/space-component-did-mount primary-role space-name this))

      :component-will-unmount (fn [this]
                                (card-animator/space-component-will-unmount primary-role space-name))

      :component-did-update (fn [this old-argv]
                              (let [new-argv (r/argv this)
                                    old-animation-info (nth old-argv 5)
                                    new-figures (nth new-argv 4)
                                    new-animation-info (nth new-argv 5)]
                                (when (and (not= old-animation-info new-animation-info)
                                           (some? new-animation-info))
                                  ;; (debug "animation: " primary-role space-name new-animation-info new-figures)
                                  (card-animator/perform-animation-for-space-with-figures primary-role space-name new-animation-info new-figures))))

      :reagent-render
      (fn [opts primary-role space-name figures animation-info]
        [:div.space {:style space-style}
         (indexed-component figures
                            (fn [index figure-id]
                              (let [figure-size (figure-size-from-id figure-id)
                                    fw (:width figure-size)
                                    fh (:height figure-size)]
                                ^{:key figure-id}
                                [figure-image figure-id
                                 {:style {:width fw
                                          :height fh
                                          :opacity (:opacity opts)
                                          :left (- (fit-into bx ex fw (:x gravity)
                                                             (- index (/ (dec (count figures)) 2)))
                                                   bx)
                                          :top (- (fit-into by ey fh (:y gravity) 0) by)}}])))])})))

(defn space-with-figures [primary-role space-descr & [opts]]
  (let [space-name (multipart-name-from-descr space-descr)]
    (let [figures @(subscribe [:thm.game-ui/get-all-figures-in-space primary-role space-name])
          animation-info @(subscribe [:thm.game-ui/space-animation-info primary-role space-name])]
      [-space-with-figures-internal opts primary-role space-name figures animation-info])))

(defn main-board-figures [primary-role]
  (let [all-playing-roles @(subscribe [:thm.game-ui/get-all-playing-roles primary-role])]
    [:div.figures
     [space-with-figures primary-role "m:source" {:opacity 0
                                                  :gravity {:x :center, :y :finish}}]

     (doall (for [role all-playing-roles]
              ^{:key role}
              [vp-figure primary-role role]))

     (doall (for [role all-playing-roles]
              ^{:key role}
              [rp-figure primary-role role]))

     [space-with-figures primary-role "m:r0f"]

     (doall (for [space-name all-rooster-spaces]
              ^{:key space-name}
              [space-with-figures primary-role space-name]))

     (doall (for [space-name all-main-board-action-spaces]
              ^{:key space-name}
              [space-with-figures primary-role space-name]))

     ]))

(def all-token-space-subnames
  ["f5" "f6" "f7" ;; field names because token figures can be created on fields

   "pr1" "pr2" "pr3" "pr4" "pr5" "pr6" "pr7" "pr8" "pr9"
   "pw1" "pw2" "pw3" "pw4" "pw5" "pw6" "pw7" "pw8" "pw9"

   "cr1" "cr2" "cr3" "cr4" "cr5" "cr6" "cr7" "cr8" "cr9"
   "cw1" "cw2" "cw3" "cw4" "cw5" "cw6" "cw7" "cw8" "cw9"
   ,                 "cb4" "cb5" "cb6" "cb7" "cb8" "cb9"
   ,                                   "cs7" "cs8" "cs9"

   ])

(defn vineyard-figures [owner-role primary-role]
  [:div.figures
   [space-with-figures primary-role [owner-role "workers"] {:gravity {:x :center, :y :finish}}]

   [space-with-figures primary-role [owner-role "supply"] {:opacity 0
                                                           :gravity {:x :center, :y :finish}}]

   (doall (for [subname ["t" "n" "i" "c" "s" "m" "l" "y-s"]]
            ^{:key subname}
            [space-with-figures primary-role [owner-role subname]
             {:gravity {:x :center, :y :finish}}]))

   (doall (for [subname all-token-space-subnames]
            ^{:key subname}
            [space-with-figures primary-role [owner-role subname]
             {:gravity {:x :center, :y :center}}]))

   [space-with-figures primary-role [owner-role "y"]]

   ])

(defn widgets [board-id owner-role primary-role]
  (let [data (case board-id
               :main viti-data/visible-board-widgets
               :vineyard viti-data/visible-vineyard-widgets)
        prefix (if (= board-id :main)
                 "m:"
                 (str owner-role ":"))]
    [:div.widgets
     (doall (for [{:keys [start finish name rounded? discard-zone?]} data
                  :let [full-name (str prefix name)
                        active? @(subscribe [:thm.game-ui/widget-active? primary-role full-name])
                        checked? @(subscribe [:thm.game-ui/widget-checked? primary-role full-name])
                        on-click #(cond
                                    active?
                                    (dispatch-e [:thm.game-ui/widget-clicked primary-role full-name] %)

                                    discard-zone?
                                    (dispatch-e [:thm.game-ui/show-modal name] %))]]
              ^{:key full-name}
              [:div.widget {:class [name
                                    (when active? "is-active")
                                    (when checked? "is-checked")
                                    (when rounded? "rounded")]
                            :on-click on-click
                            :style (widget-style start finish)}
               [:div.cover]]))]))

(defn svg-vp-change-arrow [primary-role role]
  (let [var-name (str "vp:" role)
        change @(subscribe [:thm.game-ui/track-recent primary-role var-name])
        now @(subscribe [:thm.game-ui/track-var primary-role var-name])
        all-playing-roles @(subscribe [:thm.game-ui/get-all-playing-roles primary-role])
        old (- now change)]
    (when (and change (not (zero? change)))
      (let [m1 (viti-data/position-in-marker (vp-marker-name old) (vp-displacement role all-playing-roles))
            m2 (viti-data/position-in-marker (vp-marker-name now) (vp-displacement role all-playing-roles))
            color (if (= role "r") "red" "yellow") ;; XXX select a proper color
            ]
        [svg-arrow m1 m2 color {:stroke-width 4
                                :arrow-big-size 20}]))))

(defn svg-container [board-id primary-role]
  (let [all-playing-roles @(subscribe [:thm.game-ui/get-all-playing-roles primary-role])]
    [:div.svg-container
     [:svg {:width "100%" :height "100%"}
      (doall (for [role all-playing-roles]
               ^{:key role}
               [svg-vp-change-arrow primary-role role]))]]))

(defn css-position-by-widget [{:keys [start finish] :as widget}]
  {:left (:x start)
   :top (:y start)
   :width (- (:x finish) (:x start))
   :height (- (:y finish) (:y start))})

(defn decks-and-discards [primary-role]
  [:div.decks-and-discards
   (doall (for [deck-name ["green-deck" "yellow-deck" "purple-deck" "blue-deck"]
                :let [widget (viti-data/widget-by-name deck-name)]]
            ^{:key deck-name}
            [zone-of-cards {:code GAME-CODE :mode :facedown-stack
                            :style (css-position-by-widget widget)}
             primary-role
             deck-name]))

   (doall (for [zone-name ["green-discard" "yellow-discard" "purple-discard" "blue-discard"]
                :let [widget (viti-data/widget-by-name zone-name)]]
            ^{:key zone-name}
            [zone-of-cards {:code GAME-CODE
                            :mode :v-stack
                            :class "v-stack"
                            :style (css-position-by-widget widget)}
             primary-role
             zone-name]))

   ])

(defn board [primary-role]
  [:div.board
   [:img.board-image {:src "/viti-images/main-board.jpg"}]
   [:div.fit-to-image.board
    [decks-and-discards primary-role]
    [svg-container :main primary-role]
    [main-board-figures primary-role]
    [widgets :main nil primary-role]
   ]])

(defn field-zones [owner-role primary-role]
  [:div.field-zones
   (doall (for [field-name viti-data/all-field-names
                :let [widget (viti-data/widget-by-name field-name)]]
            ^{:key field-name}
            [zone-of-cards {:code GAME-CODE :mode :v-row ;; TODO vertical row
                            :style (css-position-by-widget widget)}
             primary-role
             [owner-role field-name]]))])


(defn vineyard [owner-role primary-role]
  [:div.vineyard
   ;; TODO different vineyard images have different colors.
   [:img.board-image {:src "/viti-images/vineyard.jpg"}]
   [:div.fit-to-image.vineyard
    [field-zones owner-role primary-role]
    [vineyard-figures owner-role primary-role]
    [widgets :vineyard owner-role primary-role]]])

(defn one-mamapapa-row [primary-role player-name role]
  [:div.col-6
   [:h4 (figure-image (get-figure-id role :vp)) " " player-name]
   [:div
    [zone-of-cards {:code GAME-CODE :class "horizontal-zone"}
     primary-role [role "starting"]]]
   [:hr]])

(defn mamapapa [primary-role]
  [:div.mamapapa
   [:h3 "Mamas & Papas"]
   [:div.row.mx-2
    (doall (for [[player-name role] @(subscribe [:thm.game-ui/get-ordered-map-of-player-name->role primary-role])]
             ^{:key role}
             [one-mamapapa-row primary-role player-name role]))]])

(defn your-hand [primary-role]
  [:div.your-hand-container
   [zone-of-cards {:code GAME-CODE
                   :class "horizontal-zone"}
    primary-role
    [primary-role "hand"]]

   (when @(subscribe [:thm.game-ui/track-var primary-role "setting-up-mamapapas"])
     [mamapapa primary-role])])

(def activity-marker-symbol
  (str " " (bull)))

(defn with-activity-marker [text tab-id active-play-areas]
  (str text
       (when (contains? active-play-areas tab-id)
         activity-marker-symbol)))

(defn vineyard-tab-title-for-humans [player-name role primary-role tab-id play-area-tabs]
  [:span {:title (str player-name "'s")}
   (figure-image (get-figure-id role :worker)) " "
   (with-activity-marker "vineyard" tab-id play-area-tabs)])

(defn game-container [primary-role]
  (let [play-area-tabs @(subscribe [:thm.game-ui/play-areas-associated-with-your-request primary-role])
        pn->r @(subscribe [:thm.game-ui/get-ordered-map-of-player-name->role primary-role])]
    [:div.game-container.container-fluid
     [tabbed-component-ng :vertical "play-area-selector"
      (subscribe [:thm.game-ui/play-area-active-tab]) :thm.game-ui/activate-play-area-tab nil
      (concat
       [["board" (with-activity-marker "Board" "board" play-area-tabs) [board primary-role]]
        ["your-hand" (with-activity-marker "Your hand" "your-hand" play-area-tabs) [your-hand primary-role]]]
       (for [[player-name role] pn->r
             :let [tab-id (str role ":vineyard")]]
         [tab-id (vineyard-tab-title-for-humans player-name role primary-role tab-id play-area-tabs)
          [vineyard role primary-role]]))]]))
