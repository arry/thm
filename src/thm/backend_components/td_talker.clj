(ns thm.backend-components.td-talker
  (:require [com.stuartsierra.component :as component]
            [taoensso.carmine :as car]
            [taoensso.timbre :refer (debug error warn warnf)]
            [taoensso.nippy :as nippy]
            [thm.game-engine.game-engine :as game-engine]
            [thm.backend-components.carmine :refer [subscribe unsubscribe comp-wcar]]
            [thm.td.td-data :as td-data :refer [td-options]]
            [clojure.data.json :as json]))

;; We talk to the worker pretending there's users connecting via sente, while
;; actually they connect from socketio in cr.  To make sure the uid doesn't
;; clash with sente's (though that one is stringified uuid while socketio
;; creates 20 alpha characters), we prepend it with this prefix for talking to
;; worker.
(def CR-UID-PREFIX "cruid:")

(defn publish-helper [c chan {:keys [uid global gid tag data] :as message-obj}]
  (debug "publish-helper" chan message-obj)
  (let [user-id (if uid
                  (subs uid (count CR-UID-PREFIX))
                  nil)
        message-for-sending {"userId" user-id
                             "global" global
                             "gid" gid
                             "tag" tag
                             "data" data}
        message-str (json/write-str message-for-sending)
        res (comp-wcar (:carmine c) (car/publish chan message-str))]
    (when (not= res 1)
      (warn "Publishing to" chan ", message" message-obj "failed: should have 1 listener at that end but got" res))
    res))

(defn publish [c message-obj]
  (publish-helper c "talker->outside" message-obj))


(defn invoke-worker [c tag opts]
  (let [chan (str (:sub-prefix c) ":all->worker")
        message [tag opts]]
    (comp-wcar (:carmine c) (car/publish chan message))))

(defmulti sub-handler
  (fn [c state tag opts] tag))

(defn subscriber [c [type chan message]]
  (debug "subscriber called with" type chan message)
  (when (= type "message")
    (let [message-obj (json/read-str message)
          new-state (sub-handler c @(:state c) (get message-obj "tag") message-obj)
          ]
      (debug "got new-state from sub-handler" new-state)
      (reset! (:state c) new-state))))

;; Handles messages sent from worker which thinks it sends the messages to the
;; Sente sockets by uid, but actually we'll publish the massaged form of them
;; to outside to be picked up by the cr server.
(defmulti wsub-handler1
  (fn [c state uid tag opts] tag))

(defmethod wsub-handler1 :default
  [c state uid tag opts]
  (debug "unhandled communication from worker" {:uid uid :tag tag :opts opts}))

(defmethod wsub-handler1 :thms.rooms/user-enters
  [& ignore])

(defmethod wsub-handler1 :thms.rooms/user-leaves
  [& ignore])

(defmethod wsub-handler1 :thms/init-app
  [& ignore])

(defmethod wsub-handler1 :thms.rooms/you-enter-room
  [c state uid tag opts]
  (let [{:keys [room more]} opts
        {:keys [gid]} room
        {:keys [client-states your-role]} more
        ]
    (debug "Handling :thms.rooms/you-enter-room" your-role client-states)
    (publish c {:uid uid
                :gid gid
                :tag :initial-client-states
                :data {:client-states client-states
                       :your-role your-role}})))

(defmethod wsub-handler1 :thms.play/deltas
  [c state uid tag opts]
  (let [{:keys [deltas]} opts]
    (debug "Handling :thms.play/deltas" uid deltas)
    (publish c {:uid uid
                :tag :deltas
                :data {:deltas deltas}})))

(defmethod wsub-handler1 :thms.globaltalker/game-finished
  [c state _uid tag opts]
  (debug "Got global game-finished" opts)
  (publish c {:tag "game-finished"
              :global true
              :data opts}))

(defmethod wsub-handler1 :thms.globaltalker/game-created
  [c state _uid tag opts]
  (debug "Got global game-created" opts)
  (publish c {:tag "game-created"
              :global true
              :data opts}))

(defmethod wsub-handler1 :thms.globaltalker/game-updated
  [c state _uid tag opts]
  (debug "Got global game-updated" opts)
  (publish c {:tag "game-updated"
              :global true
              :data opts}))

(defmethod wsub-handler1 :thms.globaltalker/send-your-turn-notification
  [c state _uid tag opts]
  (debug "Got global send-your-turn-notification" opts)
  (publish c {:tag "send-your-turn-notification"
              :global true
              :data opts}))

(defn worker-subscriber [c [type chan message]]
  (when (= type "message")
    (let [[uid [tag opts]] message]
      (wsub-handler1 c @(:state c) uid tag opts)
      )))

(defrecord TdTalker [carmine sub-prefix]
  component/Lifecycle
  (start [component]
    (debug "td-talker component start")
    (let [state (atom {})
          c-w-state (assoc component
                           :state state
                           :sub-prefix sub-prefix)
          app-listener (subscribe carmine "outside->talker"
                                  #(subscriber c-w-state %))
          worker-listener (subscribe carmine (str sub-prefix ":worker->talker")
                                     #(worker-subscriber c-w-state %))
          ]
      (assoc c-w-state
             :app-listener app-listener
             :worker-listener worker-listener)))
  (stop [component]
    (debug "td-talker component stop")
    (when-let [l (:app-listener component)]
      (unsubscribe l))
    (dissoc component :state :sub-prefix :app-listener:worker-listener)))

(defn new-td-talker [sub-prefix & [options]]
  (map->TdTalker (merge {:sub-prefix sub-prefix} options)))

(defmethod sub-handler :default
  [c state tag opts]
  (debug "Unhandled td-talker subscriber message" tag opts)
  (publish c {:tag :unhandled :uid nil :data {:tag tag :opts opts}})
  state)

(defmethod sub-handler "gameCreated"
  [c state tag opts]
  (let [gid (get opts "gid")]
    (debug "gameCreated" gid)
    (invoke-worker c :refresh-game {:gid gid})
    state))

(defmethod sub-handler "gameChangedPlayers"
  [c state tag opts]
  (let [gid (get opts "gid")]
    (debug "game changed players" gid)
    (invoke-worker c :refresh-game {:gid gid})
    state))

(defmethod sub-handler "gameStarted"
  [c state tag opts]
  (let [gid (get opts "gid")]
    (debug "gameStarted" gid)
    (invoke-worker c :refresh-game {:gid gid})
    state))

(defmethod sub-handler "gameCanceled"
  [c state tag opts]
  (let [gid (get opts "gid")]
    (debug "gameCanceled" gid)
    (invoke-worker c :refresh-game {:gid gid})
    state))

(defn uid-from-user-id [user-id]
  ;; user-id is cr socket.io userId; uid is pseudo uid for worker.
  (str CR-UID-PREFIX user-id))

(defn get-client-connected-opts [uid username]
  {:uid uid
   :username username
   :pid "talker"
   :cookie-key "talker-ck"
   :anti-forgery-token "talker-aft"})

(defmethod sub-handler "userEntersGameRoom"
  [c state tag opts]
  (let [username (get opts "username")
        user-id (get opts "userId")
        gid (get opts "gid")
        uid (uid-from-user-id user-id)
        has-runtime-state (get opts "hasRuntimeState")]
    (invoke-worker c :client-connected (get-client-connected-opts uid username))
    (when has-runtime-state
      (invoke-worker c :navigate {:uid uid
                                  :page-desc ["play" (str gid)]}))
    state))

(defmethod sub-handler "giveRuntimeStatesAfterGameStarted"
  [c state tag opts]
  (let [gid (get opts "gid")
        user-ids (get opts "userIds")
        uids (map uid-from-user-id user-ids)]
    (doseq [uid uids]
      (invoke-worker c :navigate {:uid uid
                                  :page-desc ["play" (str gid)]}))
    state))

(defmethod sub-handler "userLeavesGameRoom"
  [c state tag opts]
  (let [username (get opts "username")
        user-id (get opts "userId")
        gid (get opts "gid")
        uid (str CR-UID-PREFIX user-id)]
    (invoke-worker c :client-disconnected {:uid uid
                                           :pid "talker"})
    state))

(defmethod sub-handler "userSendsAnswer"
  [c state tag opts]
  (let [username (get opts "username")
        user-id (get opts "userId")
        gid (get opts "gid")
        uid (str CR-UID-PREFIX user-id)
        answer (get opts "answer")
        role (get opts "role")]
    (invoke-worker c :client-sent-answer {:uid uid
                                          :data {:role role
                                                 :answer answer}})
    state))
