(ns thm.backend-components.ds
  (:require [com.stuartsierra.component :as component]
            [thm.backend-components.carmine :refer [comp-wcar]]
            [thm.utils :refer [now-timestamp map-values]]
            [thm.config :refer [config]]
            [taoensso.carmine :as car]
            [conman.core :as conman]
            [hugsql.core :as hugsql]
            [taoensso.timbre :refer (debug error warn)]
            [clojure.data.json :as json]
            [taoensso.nippy :as nippy]))

(def pool-spec
  {:jdbc-url (:jdbc-url config)})

(hugsql/def-db-fns "sql/queries.sql")

(defn clojurize-key [k]
  (let [s (-> k
              name
              (clojure.string/replace "_" "-"))
        s (if (re-matches #"^is-.*" s)
            (str (clojure.string/replace s #"^is-" "")
                 "?")
            s)]
    (keyword s)))

(defn clojurize-keys
  "Return a new map whose keys have underscores replaced with minuses in their
  names, to be consistent with the Clojure convention of key naming."
  [m]
  (when m
    (into {} (map (fn [[k v]]
                    [(clojurize-key k) v])
                  m))))


(defn json-str [obj]
  (json/write-str obj))

(defn json-from-str [j]
  (json/read-str j :key-fn keyword))

(defn json-from-str-keys-as-strings [j]
  (json/read-str j))

(def sql->clj-transformations
  {:json (fn [v] (when v (json-from-str (.getValue v))))
   :json-keys-as-strings (fn [v] (when v (json-from-str-keys-as-strings (.getValue v))))
   :timestamp (fn [v] (.getTime v))
   :array (fn [v] (vec (.getArray v)))})

(def game-table-key-types
  {:game-settings :json
   :runtime-info :json
   :outcome :json
   :created-at :timestamp
   :last-modified-at :timestamp
   :players :array})

(defn sql->clj [obj key-types]
  "Transform an SQL object returned from a query to a Clojure map representing
  the same object.  key-types is a mapping of a key to its type, by which a
  transformation function will be selected."
  (if (nil? obj)
    nil
    (map-values (fn [k v]
                  (if-let [type (get key-types k)]
                    (apply (sql->clj-transformations type) [v])
                    v))
                (clojurize-keys obj))))

(defn game-sql->clj [obj]
  (-> (sql->clj obj game-table-key-types)
      (update :runtime-info
              ;; Keys of this map should be strings, not keywords, because for
              ;; space-containing usernames serialization using keywords fails.
              update :player-name->role
              #(into {} (map (fn [[k v]] [(name k) v]) %)))))

(defn user-sql->clj [obj]
  (sql->clj obj {:created-at :timestamp
                 :last-seen-at :timestamp
                 :account-settings :json-keys-as-strings
                 :game-settings-on-last-creation :json}))

(defrecord DS [carmine]
  component/Lifecycle
  (start [component]
    (assoc component
           :connection (conman/connect! pool-spec)))
  (stop [component]
    (conman/disconnect! (:connection component))
    (dissoc component :connection)))

(defn new-ds [options]
  (map->DS options))

(defmacro wcar* [c & body]
  `(comp-wcar (:carmine ~c) ~@body))

(defn add-chat-message [c room-name chat-message]
  (wcar* c (car/rpush (str room-name ":chat-messages") chat-message)))

(def CHAT-MESSAGE-LIMIT 200)

;; TODO somewhere in the future, need to remove the old lobby messages, because
;; they may collect to be huge.
(defn get-recent-chat-messages [c room-name]
  (let [low-limit (if (= room-name "lobby")
                    (- CHAT-MESSAGE-LIMIT)
                    0)]
    (wcar* c (car/lrange (str room-name ":chat-messages") low-limit -1))))

(defn get-games [c]
  (map game-sql->clj (q-get-games (:connection c))))

(defn get-game-by-gid [c gid]
  (game-sql->clj (q-get-game-by-gid (:connection c) {:gid gid})))

(defn save-game [c game]
  (let [gid (:gid game)
        new-game (assoc game :last-modified-at (now-timestamp))]
    (wcar* c (car/hset "games" gid new-game))
    new-game))

(defn create-game [c owner-username code kind comment game-settings]
  (debug "create-game" owner-username code kind comment game-settings)
  (let [ret (q-create-game! (:connection c) {:code code
                                             :status "open"
                                             :kind kind
                                             :owner-username owner-username
                                             :comment comment
                                             :game-settings (json-str game-settings)
                                             :players [owner-username]})]
    (game-sql->clj ret)))

(defn game-set-players [c gid players]
  (game-sql->clj (q-game-set-players (:connection c) {:gid gid
                                                      :players players})))

(defn game-set-status [c gid status]
  (game-sql->clj (q-game-set-status (:connection c) {:gid gid
                                                     :status status})))

(defn game-set-runtime-info [c gid runtime-info]
  (game-sql->clj (q-game-set-runtime-info (:connection c) {:gid gid
                                                           :runtime-info (json-str runtime-info)})))

(defn game-set-finished [c gid status outcome runtime-info]
  (game-sql->clj (q-game-set-finished (:connection c) {:gid gid
                                                       :status status
                                                       :outcome (json-str outcome)
                                                       :runtime-info (json-str runtime-info)})))


(defn get-engine-checkpoint [c gid]
  (when-let [res (q-get-engine-checkpoint (:connection c) {:gid gid})]
    (nippy/thaw (get res :engine_checkpoint))))

(defn save-engine-checkpoint! [c gid checkpoint]
  (q-save-engine-checkpoint! (:connection c) {:gid gid
                                              :engine-checkpoint (nippy/freeze checkpoint)}))

(defn add-user!
  [c user]
  (assert (every? #(contains? user %)
                  [:username :password-hash :email])
          "User must have keys: :username :password-hash :email.")
  (q-add-user! (:connection c) user))

(defn user-by-username-or-email [c param]
  (user-sql->clj (q-user-by-username-or-email (:connection c) {:param param})))

(defn user-by-email [c email]
  (user-sql->clj (q-user-by-email (:connection c) {:email email})))

(defn user-by-username [c username]
  (user-sql->clj (q-user-by-username (:connection c) {:username username})))

(defn set-user-account-settings! [c username account-settings]
  (q-set-user-account-settings! (:connection c) {:username username
                                                 :account-settings (json-str account-settings)}))

(defn add-pending-registration! [c email confirmation-code]
  (q-add-pending-registration! (:connection c) {:email email
                                                :confirmation-code confirmation-code}))

(defn pending-registration-by-email [c email]
  (clojurize-keys (q-pending-registration-by-email (:connection c) {:email email})))

(defn pending-registration-by-code [c code]
  (clojurize-keys (q-pending-registration-by-code (:connection c) {:confirmation-code code})))

(defn confirm-pending-registration-by-code! [c code]
  (q-confirm-pending-registration-by-code! (:connection c)
                                            {:confirmation-code code}))

(defn add-password-recovery! [c email code]
  (q-add-password-recovery! (:connection c)
                            {:email email
                             :recovery-code code}))

(defn password-recovery-by-email [c email]
  (clojurize-keys (q-password-recovery-by-email (:connection c)
                                                {:email email})))

(defn password-recovery-by-code [c code]
  (clojurize-keys (q-password-recovery-by-code (:connection c)
                                               {:code code})))

(defn change-user-password! [c username password-hash]
  (q-change-user-password! (:connection c)
                           {:username username
                            :password-hash password-hash}))

(defn remove-password-recovery! [c code]
  (q-remove-password-recovery! (:connection c)
                               {:code code}))

(defn gids-of-active-games-for [c username]
  (map :gid (q-gids-of-active-games-for (:connection c)
                                        {:username username})))

(defn set-game-settings-on-last-creation! [c username code kind game-settings]
  (q-set-game-settings-on-last-creation! (:connection c)
                                         {:username username
                                          :code code
                                          :settings (json-str (assoc game-settings
                                                                     :kind kind))}))

(defn set-personal-notes-content! [c username gid content]
  (q-set-personal-notes-content! (:connection c)
                                 {:username username
                                  :gid gid
                                  :content content}))

(defn get-personal-notes-content [c username gid]
  (get (q-get-personal-notes-content (:connection c)
                                     {:username username
                                      :gid gid})
       :content
       ""))
