(ns thm.td.td-scenarios
  (:require
   [thm.td.td-engine :refer [new-game GAME-CODE]]
   [thm.td.td-data :refer [battlegrounds]]
   [thm.game-engine.game-base :refer [state-after-action-sequence state-after-pending-actions
                          state-after-answer]]
   [thm.game-engine.registry-of-test-scenarios :refer [register-scenarios]]
   ))

(defn new-game* []
  (-> (new-game ["John" "Bill"] {:seed 1})
      (assoc :pending-actions [])))

(def scenarios [])

(defmacro defscenario [n & forms]
  `(let [scenario# {:name ~n
                    :create (fn []
                              ~@forms)}]
     (alter-var-root #'scenarios conj scenario#)))

(defscenario "choose-cards"
  (->
   (new-game*)
   (state-after-action-sequence [:deal-cards "strategy-deck" 5]
                                [:do-event "s" "summee"])))

(defscenario "change-defcon 1"
  (->
   (new-game*)
   (state-after-action-sequence
    [:set-vars
     ["d:u:m" 3]
     ["d:u:p" 4]
     ["d:u:w" 5]]
    [:choose-change-defcon "u" 1])))

(defscenario "change-defcon 2"
  (->
   (new-game*)
   (state-after-action-sequence [:choose-change-defcon "u" 1 {:max-tracks 2}])))

(defscenario "change-defcon p"
  (->
   (new-game*)
   (state-after-action-sequence [:choose-change-defcon "u" 2 {:allowed-tracks #{"p"}}])))

(defscenario "change-defcon opp"
  (->
   (new-game*)
   (state-after-action-sequence [:choose-change-defcon "u" 1 {:whose "s"}])))

(defscenario "choose-remove-inf"
  (->
   (new-game*)
   (update :vars assoc
           "i:u:a" 5
           "i:u:v" 3)
   (state-after-action-sequence
    [:choose-remove-influence "u" "u" 5 :single-bg false (set (keys battlegrounds))])))

(defscenario "choose-remove-inf half"
  (->
   (new-game*)
   (update :vars assoc
           "i:s:c" 5
           "i:s:k" 3)
   (state-after-action-sequence
    [:choose-remove-influence "u" "s" 1 :half false #{"c" "k"}])))

(defscenario "choose-command-onto"
  (->
   (new-game*)
   (update :vars assoc
           "i:u:c" 3)
   (state-after-action-sequence
    [:do-command-onto "u" 3 #{"c" "a" "b"}])))

(defscenario "choose-place exact"
  (->
   (new-game*)
   (state-after-action-sequence
    [:choose-place-influence "u" 2 nil false (set (keys battlegrounds))
     {:exact-amount? true}])))

(defscenario "choose-remove-inf many-bgs"
  (->
   (new-game*)
   (update :vars assoc
           "i:s:c" 5
           "i:s:k" 3)
   (state-after-action-sequence
    [:choose-remove-influence "u" "u" 2 :many-bgs false (set (keys battlegrounds))])))

(defscenario "choose-change-defcon exact"
  (->
   (new-game*)
   (update :vars assoc
           "d:u:p" 1
           "d:u:w" 2)
   (state-after-action-sequence
    [:choose-change-defcon "u" 2 {:mode :deflate-only
                                  :allowed-tracks #{"p" "w"}
                                  :exact-amount? true}])))

(defn fake-request []
  [:choose-place-influence "s" 1 nil false #{"t"}])

(defscenario "reveal aftermath"
  (->
   (new-game*)
   (update :zones assoc-in ["aftermath" :cards] ["tgoa" "fiffif" "maskir"])
   (state-after-action-sequence
    ;; Fake request because scenario runner displays the final state, and I
    ;; want to see behavior as the phase starts.
    (fake-request)
    [:do-aftermath-phase])))

(defscenario "finish game: tie"
  (-> (new-game*)
      (state-after-action-sequence
       (fake-request)
       [:finish-game "tie" "both-war"])))

(defscenario "finish game: win"
  (-> (new-game*)
      (state-after-action-sequence
       (fake-request)
       [:finish-game "win" "war" "s"])))

(defscenario "Intelligence Reports"
  (-> (new-game*)
      (state-after-action-sequence
       [:move-cards "strategy-deck" "u:hand" ["fiffif" "u2ph"]]
       [:move-cards "strategy-deck" "s:hand" ["intrep" "strbal"]]
       [:set-vars
        ["active-player" "s"]
        ["turn" 1]]
       [:do-turn])))

(defscenario "Effects with Personal Letter"
  (-> (new-game*)
      (state-after-action-sequence
       [:add-effect "u" "contai"]
       [:add-effect "u" "tothbr"]
       [:add-effect "u" "nucsub"]
       [:add-effect "u" "sops"])))

(defscenario "choose initiative"
  (-> (new-game ["John" "Bill"] {:seed 1})
      (state-after-pending-actions)
      (state-after-answer "italy" "s")
      (state-after-answer "woropi:2" "u")))

(defscenario "new game"
  (-> (new-game ["John" "Bill"] {:seed 1})
      (state-after-action-sequence
       (fake-request))
      state-after-pending-actions))

(defscenario "Operation Mongoose"
  (-> (new-game*)
      (state-after-action-sequence
       [:move-cards "strategy-deck" "u:hand" ["opemon"]]
       [:set-vars
        ["active-player" "u"]
        ["turn" 1]]
       [:do-turn])))

(defscenario "Saving for aftermath"
  (-> (new-game ["John" "Bill"] {:seed 1})
      (state-after-pending-actions)
      (state-after-answer "italy" "s")
      (state-after-answer "woropi:2" "u")
      (state-after-answer "u")

      (state-after-answer "inofcu")
      (state-after-answer "event")
      (state-after-answer {})
      (state-after-answer {})

      (state-after-answer "maskir")
      (state-after-answer "event")
      (state-after-answer {})

      (state-after-answer "u2ph")
      (state-after-answer "command")
      (state-after-answer ["c" 0])

      (state-after-answer "tothbr")
      (state-after-answer "event")

      (state-after-answer "strbal")
      (state-after-answer "opp-command")
      (state-after-answer false)
      (state-after-answer ["k" 0])

      (state-after-answer "fidcas")
      (state-after-answer "command")
      (state-after-answer ["c" 0])

      (state-after-answer "baofpi")
      (state-after-answer "opp-command")
      (state-after-answer false)
      (state-after-answer ["k" 0])

      (state-after-answer "natlib")
      (state-after-answer "command")


      ))

(defscenario "Discard agendas from in-play"
  (-> (new-game*)
      (state-after-action-sequence
       [:move-cards "agenda-deck" "in-play" ["italy" "woropi:1"]]
       (fake-request)
       [:move-cards "in-play" "agenda-discard" ["italy" "woropi:1"]])))

(defscenario "defcon agendas"
  (-> (new-game*)
      (state-after-action-sequence
       [:move-cards "agenda-deck" "s:agenda" ["italy"]]
       [:move-cards "agenda-deck" "u:agenda" ["woropi:1"]]
       [:set-vars
        ["d:u:w" 4]]
       (fake-request)
       [:do-resolve-agendas-phase]
       )))


(register-scenarios GAME-CODE scenarios)
