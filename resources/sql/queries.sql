-- == Users

-- :name q-user-by-username :? :1
-- :doc Get user by username
select * from users
where lower(username) = lower(:username)

-- :name q-user-by-email :? :1
select * from users
where lower(email) = lower(:email)

-- :name q-add-user! :! :n
insert into users (username, password_hash, email)
values (:username, :password-hash, :email)

-- :name q-user-by-username-or-email :? :1
select * from users
where lower(username) = lower(:param) or lower(email) = lower(:param)

-- :name q-change-user-password! :! :n
update users
set password_hash = :password-hash
where username = :username

-- :name q-set-user-account-settings! :! :n
update users
set account_settings = json(:account-settings)
where username = :username


-- == Pending registrations

-- :name q-add-pending-registration! :! :n
insert into pending_registrations (email, confirmation_code)
values (:email, :confirmation-code)

-- :name q-pending-registration-by-email :? :1
select * from pending_registrations
where lower(email) = lower(:email)

-- :name q-pending-registration-by-code :? :1
select * from pending_registrations
where confirmation_code = :confirmation-code

-- :name q-confirm-pending-registration-by-code! :! :n
update pending_registrations
set is_confirmed = TRUE
where confirmation_code = :confirmation-code


-- == Password recoveries

-- :name q-add-password-recovery! :! :n
insert into password_recoveries (email, recovery_code)
values (:email, :recovery-code)

-- :name q-password-recovery-by-email :? :1
select * from password_recoveries
where email = :email

-- :name q-password-recovery-by-code :? :1
select * from password_recoveries
where recovery_code = :code

-- :name q-remove-password-recovery! :! :n
delete from password_recoveries
where recovery_code = :code


-- == Games

-- :name q-get-games :? :*
select * from games
order by gid

-- :name q-get-game-by-gid :? :1
select * from games
where gid = :gid

-- :name q-create-game! :<! :1
insert into games (code, kind, status, owner_username, comment, game_settings, players)
values (:code, game_time_kind(:kind), :status, :owner-username, :comment, json(:game-settings), array[:v*:players])
returning *

-- :name q-game-set-players :<! :1
update games
set players = array[:v*:players]
where gid = :gid
returning *

-- :name q-game-set-status :<! :1
update games
set status = :status
where gid = :gid
returning *

-- :name q-game-set-runtime-info :<! :1
update games
set runtime_info = json(:runtime-info)
where gid = :gid
returning *

-- :name q-game-set-finished :<! :1
update games
set status = :status, outcome = json(:outcome), runtime_info = json(:runtime-info)
where gid = :gid
returning *

-- :name q-get-engine-checkpoint :? :1
select engine_checkpoint from game_engine_checkpoints
where gid = :gid

-- :name q-save-engine-checkpoint! :! :n
insert into game_engine_checkpoints (gid, engine_checkpoint)
values (:gid, :engine-checkpoint)
on conflict (gid) do update set engine_checkpoint = :engine-checkpoint

-- :name q-gids-of-active-games-for :? :*
select gid from games
where runtime_info->'current-players' @> jsonb('"' || :username || '"')
and status = 'running'
order by gid;

-- :name q-set-game-settings-on-last-creation! :<! :1
update users
set game_settings_on_last_creation = jsonb_set(game_settings_on_last_creation,
'{td}', jsonb(:settings))
where username = :username
returning *

-- :name q-get-personal-notes-content :? :1
select content from personal_notes
where username = :username and gid = :gid

-- :name q-set-personal-notes-content! :! :n
insert into personal_notes (username, gid, content)
values (:username, :gid, :content)
on conflict (username, gid) do update set content = :content
