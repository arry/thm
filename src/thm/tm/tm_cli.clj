(ns thm.tm.tm-cli

  (:require
   [thm.tm.tm-data]
   [thm.game-engine.game-engine]
            [thm.utils :refer [get-var all-cards-in-zone denumber-id get-request-for-role]]
        )

  )

(defn friendly-temperature [temp]
  (* 2 (- temp 15)))

(def rows [[62 62 1]
           [63 63 1]
           [1 5 4]
           [6 11 3]
           [12 18 2]
           [19 26 1]
           [27 35 0]
           [36 43 1]
           [44 50 2]
           [51 56 3]
           [57 61 4]])

(defn engine-to-string [engine]
  (let [cs (thm.game-engine.game-engine/translated-state-for engine "s")

        role "s"

        oxygen (get-var cs "oxygen")
        temperature (get-var cs "temperature")
        remaining-oceans (get-var cs "remaining-oceans")

        globals (format "g %s : o2 %s : temp %s : oceans %s"
                        (get-var cs "generation")
                        oxygen (friendly-temperature temperature) remaining-oceans)

        tile-kind-letter (fn [tile-kind]
                           (case tile-kind
                             "city" "c"
                             "ocean" "o"
                             "greenery" "g"
                             "capital" "C"
                             "eruption" "e"
                             "euro" "E"
                             "factory" "f"
                             "mine" "m"
                             "mohol" "M"
                             "nuclear" "N"
                             "paw" "p"
                             "preserve" "r"
                             "restricted" "R"
                             "?"))

        format-area (fn [area]
                      (let [owner (get-var cs ["t" area "owner"])
                            kind (get-var cs ["t" area "kind"])
                            features (get thm.tm.tm-data/map-features area)
                            type (:type features)
                            bonus (:bonus features)

                            index (format "%02d" area)
                            letter (case type
                                     "l" "."
                                     "w" "_"
                                     "special" (case (:other features)
                                                 "noctis-city" "n"
                                                 "phobos" "P"
                                                 "ganymede" "G"
                                                 "?")
                                     "?")
                            latter (cond
                                  kind
                                  (format "%s%s%s%s"
                                          letter
                                          (tile-kind-letter kind)
                                          (if owner "!"
                                              letter)
                                          letter)

                                  (and bonus (= (count bonus) 2))
                                  (format "%s%s"
                                          index bonus)

                                  (and bonus (= (count bonus) 1))
                                  (format "%s%s%s" index bonus letter)

                                  :else
                                  (format "%s%s%s" index letter letter))


                            ]
                        (format "%s%s" letter latter)))


        field (clojure.string/join "\n"
                                   (for [[start finish offset] rows]
                                     (format "%s%s"
                                             (clojure.string/join "" (repeat (* 5 offset) " "))
                                             (clojure.string/join "     "
                                                                  (for [area (range start (inc finish))]
                                                                    (format-area area))))))

        my-resources
        (clojure.string/join "; "
                             (into [(format "tr: %s" (get-var cs [role "tr"]))]
                                   (for [res thm.tm.tm-data/basic-resources
                                         :let [amount (get-var cs [role "a" res])
                                               prod (get-var cs [role "p" res])]]
                                     (format "%s: %s/%s%s"
                                             res
                                             amount
                                             (if (neg? prod)
                                               ""
                                               "+")
                                             prod))))

        my-zones (let [zones [["choosing-corp"]
                              ["hand"]
                              ["table-corp" {:active? true}]
                              ["table-blue" {:active? true}]
                              ["table-green"]
                              ["table-red"]
                              ["discard" {:non-specific? true}]]

                       fmt-card (fn [card active?]
                                  (if active?
                                    (format "%s %s%s"
                                            (denumber-id (:cid card))
                                            (let [res (get card :res 0)]
                                              (if (pos? res)
                                                res
                                                ""))
                                            (if-let [used? (get card :used?)]
                                              "!"
                                              ""))

                                    (denumber-id card)))
                       ]
                   (clojure.string/join "\n"
                                        (for [zone-descr zones
                                              :let [zone-name-part (first zone-descr)
                                                    {:keys [active? non-specific?]} (second zone-descr)
                                                    zone (if non-specific?
                                                           zone-name-part
                                                           [role zone-name-part])
                                                    cards (all-cards-in-zone cs zone)]
                                              :when (seq cards)]
                                          (format "%s: %s"
                                                  zone-name-part
                                                  (clojure.string/join ", "
                                                                       (map #(fmt-card % active?) cards))


                                                  ))))


        request (get-request-for-role cs role)
        [kind text-descr & args] request

        request-str (format "%s %s\n%s"
                            kind text-descr
                            (with-out-str (clojure.pprint/pprint args)))
        ]

    (clojure.string/join "\n--\n"
                         [globals field my-resources my-zones request-str])

    ))

(defn fmt-card [cid]
  (let [design (thm.tm.tm-data/card-design-by-id (denumber-id cid))
        {:keys [cost type title tags blue effect]} design]
    (format "%s [%s] %s - %s (%s)\n%s\n%s"
            type
            cost (denumber-id cid) title tags
            (or blue "-")
            (or effect "-"))))

(defn engine-more-about-hand [engine]
  (let [cs (thm.game-engine.game-engine/translated-state-for engine "s")
        cids (all-cards-in-zone cs "s:hand")]
    (clojure.string/join "\n===\n"
                         (map fmt-card cids))))
