(ns thm.backend-components.worker
  (:require [com.stuartsierra.component :as component]
            [taoensso.carmine :as car]
            [taoensso.timbre :refer (debug error warn warnf)]
            [taoensso.nippy :as nippy]
            [overtone.at-at :as aa]
            [thm.game-engine.game-engine :as game-engine]
            [thm.backend-components.ds :as ds]
            [thm.backend-components.carmine :refer [subscribe unsubscribe comp-wcar]]
            [thm.utils :refer [dissoc-in now-timestamp game-comparator username-comparator
                               VALIDATION-CONSTS get-form-value game-includes-player? to-integer
                               seq-contains? get-and-dissoc]]
            [thm.config :refer [cookie-name config]]
            [thm.backend-utils.cookies :refer [update-session-cookie]]
            [thm.td.td-data :as td-data :refer [td-options]]
            [thm.game-engine.all-game-modules] ;; Here to make sure that all game scenarios
                                   ;; are properly registered.
            [thm.game-engine.registry-of-test-scenarios]
            [thm.backend-utils.users :as users]
            [thm.backend-utils.emails :as emails]
            [thm.tm.tm-cli :as tm-cli]
            [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]
            [clojure.data.json :as json]))

(defn publish-helper [c chan full-message]
  (let [res (comp-wcar (:carmine c) (car/publish chan full-message))]
    (when (not= res 1)
      (warn "Publishing to" chan ", message" full-message "failed: should have 1 listener at that end but got" res))
    res))

(defn publish-to-talker [c full-message]
  (let [chan (str (:sub-prefix c) ":worker->talker")]
    (comp-wcar (:carmine c) (car/publish chan full-message))))

(defn publish-to-ws [c uid message]
  (let [state @(:state c)
        pid (get-in state [:uid->data uid :pid])
        chan (str (:sub-prefix c) ":worker->" pid)]
    (publish-helper c chan [uid message])))

(defn lobby-room []
  {:name "lobby", :type "lobby"})

(defn home-room [username]
  {:name (str "home-" username), :type "home", :owner-username username})

(defn view-game-room [gid]
  {:name (str "view-game-" gid), :type "view-game", :gid (to-integer gid)})

(defn play-game-room [gid]
  {:name (str "play-game-" gid), :type "play-game", :gid (to-integer gid)})

(defn test-ui-room [username code]
  {:name (str "test-ui:" username ":" code), :type "test-ui", :owner-username username, :code code})

(defn sandbox-room [uid code]
  {:name (str "sandbox:" uid ":" code), :type "sandbox", :uid uid, :code code})

(defn term-room [username]
  {:name (str "term:" username)
   :type "term"
   :username username})

(defn page-desc->room [[page-name & [url-rest]] username uid]
  (letfn [(get-arg [] (first (clojure.string/split url-rest #"#")))]
    (case page-name
      "lobby" (lobby-room)
      "game" (view-game-room (get-arg))
      "play" (play-game-room (get-arg))
      "home" (home-room username)
      "test-ui" (test-ui-room username (get-arg))
      "sandbox" (sandbox-room uid (get-arg))
      "confirm" {:name (str "confirm:" username ":" (get-arg))
                 :type "confirm"
                 :code (get-arg)}
      "reset" {:name (str "reset:" username ":" (get-arg))
               :type "reset"
               :code (get-arg)}
      "account" {:name (str "account:" username)
                 :type "account"
                 :username username}
      "test-term" (term-room username)
      {:name "static" :type "static"})))

(defn to-all-uids [uids msg]
  (map (fn [uid] [uid msg]) uids))

(defn uids-in-room [state room]
  (get-in state [:rooms (:name room) :uids]))

(defn all-in-room [state room msg]
  (to-all-uids (uids-in-room state room) msg))

(defn all-in-rooms [state rooms msg]
  (to-all-uids (apply concat (map #(uids-in-room state %) rooms))
               msg))

(defn all-in-room-fn [state room msg-fn]
  (let [uids (uids-in-room state room)]
    (map (fn [uid]
           [uid (msg-fn uid)]) uids)))

(defn all-in-room-except-uid [uid state room msg]
  (to-all-uids (filter #(not= % uid)
                       (uids-in-room state room))
               msg))

(defn all-view-or-play-game-rooms-with-player [state username]
  (let [gids (for [[gid game] (:game-by-id state)
                   :when (game-includes-player? game username)]
               gid)]
    (concat (map #(view-game-room %) gids)
            (map #(play-game-room %) gids))))

(defn split-n [s n]
  (clojure.string/split s #":" n))

(defn get-username [state uid]
  (get-in state [:uid->data uid :username]))

(defn user-online? [c state username]
  (seq (get (:username->uids state) username)))

(defn update-session [c state uid f & args]
  (let [cookie-key (get-in state [:uid->data uid :cookie-key])
        expiration-seconds (-> config :store :ttl-secs)]
    (apply update-session-cookie cookie-key f args)
    {:new-state state
     :send [[uid [:thms.cookies/update {:name cookie-name
                                        :value cookie-key
                                        :secs expiration-seconds}]]]}))

(defn send-many-emails [c state usernames kind game & args]
  (when (= (:kind game) "async")
    (let [affected-usernames (remove #(user-online? c state %) usernames)]
      (doseq [username affected-usernames
              :let [user (ds/user-by-username (:ds c) username)
                    account-settings (:account-settings user)
                    email (:email user)]
              :when (get account-settings "allow-game-emails")]
        (apply
         (case kind
           :your-turn emails/send-your-turn-notification!
           :join emails/send-player-joins-game-notification!
           :part emails/send-player-parts-game-notification!)
         user email game args)))))

(defn send-players-update-notification! [c state game control that-username]
  (let [username (:owner-username game)]
    (send-many-emails c state [username] control game that-username)))

;; Change state and send necessary messages.  Changes can include `:new-state`,
;; which will get reset in the controller's state atom; `:send`, which is a
;; sequence of [uid message] pairs, which will be sent to appropriate clients.
(defn apply-changes [c changes]
  ;; (debug "apply-changes" changes)
  (when-let [{:keys [new-state send]} changes]
    (when new-state
      ;; (debug "will reset state" new-state)
      (reset! (:state c) new-state))
    (doseq [[uid message] send]
      (when message
        ;; (debug "send1:" uid message)
        (publish-to-ws c uid message)))))

(defn game-controls-for [game username]
  (if (= (:status game) "open")
    (let [players (:players game)
          you-in (some #(= % username) players)
          you-owner (= username (:owner-username game))
          req-players 2]
      {:start (and you-owner
                       (= (count players) req-players))
       :join (and username
                  (< (count players) req-players)
                  (not you-in))
       :part (and you-in
                  (not you-owner))
       :cancel you-owner})
    {}))

(defn update-game-and-notify [c state gid tag changes indices-update & [players]]
  (let [new-game (if (= (first changes) :refreshed)
                   (second changes)
                   (apply (case (first changes)
                            :set-players      ds/game-set-players
                            :set-status       ds/game-set-status
                            :set-runtime-info ds/game-set-runtime-info
                            :set-finished     ds/game-set-finished)
                          (:ds c) gid (rest changes)))
        new-state (assoc-in state [:game-by-id gid] new-game)]
    (when-not (= tag :refreshed)
      (publish-to-talker c [:global-uid [:thms.globaltalker/game-updated
                                         {:gid gid}]]))
    {:new-state (case indices-update
                  :assoc-in-open (assoc-in new-state [:open-game-by-id gid] new-game)
                  :dissoc-in-open (dissoc-in new-state [:open-game-by-id gid])
                  :open->running (-> new-state
                                     (dissoc-in [:open-game-by-id gid])
                                     (assoc-in [:running-game-by-id gid] new-game))
                  :assoc-in-running (assoc-in new-state [:running-game-by-id gid] new-game)
                  :dissoc-in-running (dissoc-in new-state [:running-game-by-id gid])
                  :dissoc-in-open&running (-> new-state
                                              (dissoc-in [:open-game-by-id gid])
                                              (dissoc-in [:running-game-by-id gid]))
                  :ignore new-state)
     :send (let [r (concat (all-in-room-fn state (view-game-room gid)
                                           (fn [uid]
                                             [:thms/game-changed-on-view-game
                                              {:game new-game
                                               :tag tag
                                               :controls (game-controls-for new-game (get-username state uid))}]))
                           (all-in-rooms state (into #{(lobby-room)}
                                                     (map #(home-room %) (if players
                                                                           players
                                                                           (:players new-game))))
                                         [:thms/game-changed {:game new-game
                                                              :indices-update indices-update}]))]
             r)}))

(defn get-game [state gid]
  (get-in state [:game-by-id gid]))

(defmulti sub-handler
  "The sub-handler is a multimethod that returns a hash of changes as per
  `apply-changes` (This is the re-frame's Handler concept, but there a handler
  returns only the new state, whereas ours also returns the messages to send,
  because we don't have Reactions and stuff.)"
  (fn [c state tag opts] tag))

(defn subscriber [c [type chan message]]
  (when (= type "message")
    (let [[tag opts] message
          changes (sub-handler c @(:state c) tag opts)]
      (apply-changes c changes))))

(defn worker-compose
  "Each spec is a vector whose first argument is a function that takes [c
  state] and whatever the rest of spec args are.  This function must return the
  same stuff as `sub-handler`: ie a map with :new-state and :send.  Given an
  initial state and controller, each of those functions will be called, and
  their results collected into one sub-handler stuff."
  [c state & specs]
  (letfn [(f [x spec]
            (let [cur-state (:new-state x)
                  {:keys [new-state send]
                   :or {new-state cur-state}}
                  (apply (first spec) c cur-state (rest spec))]
              {:new-state new-state
               :send (into (:send x) send)}))]
    (reduce f {:new-state state, :send []}
            (remove nil? specs))))

(def default-room-state
  {:uids #{}
   :username->count (sorted-map-by username-comparator)})

(defn create-test-ui-room-state [c state room]
  (let [code (:code room)
        scenarios (thm.game-engine.registry-of-test-scenarios/scenarios-by-code code)]
    (if (seq scenarios)
      (let [scenario (peek scenarios)
            game-state (-> scenario :create (apply []))
            engine (game-engine/new-engine-from-checkpoint code {:state game-state} :test)]
        {:engine engine})
      {:engine nil})))

(defn create-sandbox-room-state [c state room]
  (let [code (:code room)
        uid (:uid room)
        username (or (get-username state uid) "Guest")
        engine (game-engine/new-engine code
                                       [username
                                        (str "evil " username)])]
    {:engine engine}))

(defn send-your-turn-notifications! [c state game old-runtime-info new-runtime-info]
  (when (= (:kind game) "async")
    (let [old-players (:current-players old-runtime-info)
          new-players (:current-players new-runtime-info)
          affected-usernames (->> new-players
                                  (remove #(seq-contains? old-players %))
                                  (filter #(empty? (get (:username->uids state) %))))]
      (doseq [username affected-usernames
              :let [user (ds/user-by-username (:ds c) username)
                    account-settings (:account-settings user)
                    email (:email user)]
              :when (get account-settings "allow-game-emails")]
        (publish-to-talker c [:global-uid [:thms.globaltalker/send-your-turn-notification
                                           {:username username
                                            :email email
                                            :gid (:gid game)}]])))))

(defn create-play-game-room-state [c state room]
  (when-let [game (get-game state (:gid room))]
    (assert (#{"running" "finished" "abandoned" "voided"} (:status game))
            (str "Game must be or have been running to create engine: " game))
    (let [gid (:gid room)
          [engine mode] (if-let [checkpoint (ds/get-engine-checkpoint (:ds c) gid)]
                          [(game-engine/new-engine-from-checkpoint (:code game) checkpoint :real) :restored]
                          (let [engine (game-engine/new-engine (:code game) (:players game) (:game-settings game))]
                            [engine :created]))
          runtime-info (game-engine/engine-runtime-info engine)]
      (when (= mode :created)
        (ds/save-engine-checkpoint! (:ds c) gid (game-engine/create-checkpoint engine))
        (send-your-turn-notifications! c state game nil runtime-info))
      {:engine engine})))

(defn create-room-state [c state room]
  (let [more (case (:type room)
               "test-ui" (create-test-ui-room-state c state room)
               "sandbox" (create-sandbox-room-state c state room)
               "play-game" (create-play-game-room-state c state room)
               "term" (let [engine (game-engine/new-engine "tm"
                                                           [(:username room)]
                                                           {})]
                        {:engine engine})
               nil)]
    (merge default-room-state more)))

(defn get-room-state [c state room]
  (or (get-in state [:rooms (:name room)])
      (create-room-state c state room)))

(def GAME-EXPIRATIONS
  [{:index :open-game-by-id, :kind "live", :hours 24}
   {:index :running-game-by-id, :kind "live", :hours (* 24 3)}
   {:index :open-game-by-id, :kind "async", :hours (* 24 7)}
   {:index :running-game-by-id, :kind "async", :hours (* 24 7)}])

(def MILLISECONDS-IN-AN-HOUR (* 1000 60 60))

(defn expire-games [c state]
  (let [now (now-timestamp)
        expire-one-game (fn [c state gid]
                          (let [game (get-game state gid)]
                            (case (:status game)
                              "open"
                              (update-game-and-notify c state gid "expire" [:set-status "expired"] :dissoc-in-open)

                              "running"
                              (update-game-and-notify c state gid "expire" [:set-status "voided"] :dissoc-in-running)

                              (do (warn "Trying to expire a game that's neither open nor running: " game)
                                  nil))))
        expire-in-one-index (fn [c state index kind hours ]
                              (let [coll (filter #(= (:kind (second %)) kind)
                                                 (get state index))
                                    millis (* MILLISECONDS-IN-AN-HOUR hours)
                                    gid->game (filter (fn [[gid game]]
                                                       (> (- now (:last-modified-at game)) millis))
                                                     coll)]
                                (when (seq gid->game)
                                  (debug "will expire a collection " index kind (vec (map first gid->game)))
                                  (apply worker-compose c state
                                         (map (fn [[gid game]]
                                                [expire-one-game gid])
                                              gid->game)))))]
    (apply worker-compose c state
           (map (fn [{:keys [index kind hours]}]
                  [expire-in-one-index index kind hours])
                GAME-EXPIRATIONS))))

(defn on-expire-games-timer [c]
  (apply-changes c (expire-games c @(:state c))))

;; Worker: this component is intended to run in one process regardless of the
;; number of web-serving processes.  It tracks all the clients (Sente
;; connections) from all those processes and their comings and goings in rooms
;; (which roughly correspond to URL-s), and the communication between rooms and
;; clients.
(defrecord Worker [ds carmine sub-prefix]
  component/Lifecycle
  (start [component]
    (let [games (ds/get-games ds) ;; TODO get only recent games (create or update timestamp is less than 2 weeks)
          games->game-by-id (fn [games]
                             (into (sorted-map-by game-comparator)
                                   (map (fn [g] [(:gid g) g]) games)))
          state (atom {:uid->data {}
                       :username->uids {}
                       :rooms {}
                       :game-by-id     (games->game-by-id games)
                       :open-game-by-id (games->game-by-id
                                         (filter #(= (:status %) "open") games))
                       :running-game-by-id (games->game-by-id
                                            (filter #(= (:status %) "running") games))
                       })
          ;; Need to assoc in two stages because listeners use state and
          ;; carmine from component, and we need listeners in the component.
          c-w-state (assoc component :state state)
          app-listener (subscribe carmine (str sub-prefix ":all->worker")
                                  #(subscriber c-w-state %))
          aa-pool (aa/mk-pool)]
      (aa/every (* 1 MILLISECONDS-IN-AN-HOUR)
                #(on-expire-games-timer c-w-state) aa-pool)
      (assoc c-w-state
             :aa-pool aa-pool
             :app-listener app-listener)))
  (stop [component]
    (when-let [l (:app-listener component)]
      (unsubscribe l))
    (when-let [aa-pool (:aa-pool component)]
      (aa/stop-and-reset-pool! aa-pool))
    (dissoc component :state :app-listener)))

(defn new-worker [sub-prefix & [options]]
  (map->Worker (merge {:sub-prefix sub-prefix} options)))

(defn initial-client-states [engine]
  (let [roles (game-engine/all-playing-roles engine)]
    (zipmap roles
            (map #(game-engine/translated-state-for engine %) roles))))

(defn get-ui-modes-for-user-and-code [user code]
  (get-in (:account-settings user) ["ui-modes" code]))

(defn get-room-specific-init-message [c state room username room-state]
  (case (:type room)
    "lobby" (let [user (ds/user-by-username (:ds c) username)]
              {:open-game-by-id (:open-game-by-id state)
               :running-game-by-id (:running-game-by-id state)
               ;; Should be non-nil to trigger atom swap in create-game-form in cljs/thm/views.cljs
               :game-settings-on-last-creation (or (:game-settings-on-last-creation user) {})})
    "view-game" (let [game (get-game state (:gid room))]
                  {:game game
                   :controls (game-controls-for game username)})
    "home" (let [your-game-by-id (filter (fn [[gid game]]
                                           (game-includes-player? game username))
                                         (:game-by-id state))]
             {:your-game-by-id your-game-by-id})
    "play-game" (let [user (ds/user-by-username (:ds c) username)
                      gid (:gid room)
                      game (get-game state gid)
                      engine (:engine room-state)
                      _ (assert engine (format "Must have engine of play-game room %s: room-state %s" room room-state))
                      your-role (game-engine/get-role-for-username engine username)
                      client-states {your-role (game-engine/translated-state-for engine your-role)}]
                {:game game
                 :active-gids (ds/gids-of-active-games-for (:ds c) username)
                 :your-role your-role
                 :client-states client-states
                 :recent-changes (game-engine/get-recent-changes-for engine your-role)
                 :ui-modes (get-ui-modes-for-user-and-code user (:code game))
                 :personal-notes-content (ds/get-personal-notes-content (:ds c) username gid)})
    "test-ui" (let [code (:code room)]
                (if-let [scenarios (thm.game-engine.registry-of-test-scenarios/scenarios-by-code code)]
                  (let [user (ds/user-by-username (:ds c) username)
                        engine (:engine room-state)
                        client-states (initial-client-states engine)]
                    {:scenario-names (map :name scenarios)
                     :client-states client-states
                     :ui-modes (get-ui-modes-for-user-and-code user (:code room))})
                  {:scenario-names []
                   :client-states {}}))
    "sandbox" (let [user (ds/user-by-username (:ds c) username)
                    engine (:engine room-state)
                    client-states (initial-client-states engine)]
                {:client-states client-states
                 :ui-modes (get-ui-modes-for-user-and-code user (:code room))})
    "confirm" (let [code (:code room)
                    pending-registration (ds/pending-registration-by-code (:ds c) code)]
                {:confirmation-code-valid? (and pending-registration
                                                (not (:confirmed? pending-registration)))})
    "reset" (let [code (:code room)
                  recovery (ds/password-recovery-by-code (:ds c) code)]
              {:code-valid? (not (nil? recovery))})
    "account" (let [username (:username room)
                    user (ds/user-by-username (:ds c) username)
                    account-settings (:account-settings user)]
                {:account-settings (select-keys account-settings ["allow-game-emails"])})
    "term" (let [room-state (get-room-state c state room)
                 engine (:engine room-state)
                 cs (thm.game-engine.game-engine/translated-state-for engine "s")]
             {:cs cs})
    nil))

(defn room-init-message [c state room username room-state]
  [:thms.rooms/you-enter-room
   {:room room
    :usernames (remove nil? (keys (:username->count room-state)))
    :chat-messages (ds/get-recent-chat-messages (:ds c) (:name room))
    :more (get-room-specific-init-message c state room username room-state)}])

(defn add-user-to-room [c state uid room]
  (when room
    (let [username (get-username state uid)
          room-state (get-room-state c state room)
          username->count (:username->count room-state)
          username-count (get username->count username 0)
          entering? (and username (zero? username-count))
          new-username->count (assoc username->count username (inc username-count))
          new-room-state (-> room-state
                             (update :uids conj uid)
                             (assoc :username->count new-username->count))
          init-message (room-init-message c state room username new-room-state)]
      {:new-state (-> state
                      (assoc-in [:uid->data uid :room] room)
                      (assoc-in [:rooms (:name room)] new-room-state))
       :send (concat
              [[uid init-message]]
              (when (and username entering? (not= (:type room) "static"))
                (all-in-room state room
                             [:thms.rooms/user-enters {:username username}])))})))

(defn remove-user-from-room [c state uid room]
  (when room
    (let [username (get-username state uid)
          username->count (get-in state [:rooms (:name room) :username->count])
          username-count (get username->count username)
          leaving? (and username (= username-count 1))
          new-username->count (if leaving?
                                (dissoc username->count username)
                                (assoc username->count username (dec username-count)))
          room-emptied? (empty? new-username->count)]
      {:new-state (cond-> state
                    true (dissoc-in [:uid->data uid :room])
                    room-emptied? (dissoc-in [:rooms (:name room)])
                    (not room-emptied?) (update-in [:rooms (:name room)]
                                                   #(-> %
                                                        (update :uids disj uid)
                                                        (assoc :username->count new-username->count))))
       :send (concat
              (when (and username leaving? (not (= (:type room) "static")))
                (all-in-room-except-uid uid state room
                                        [:thms.rooms/user-leaves {:username username}])))})))

(defn change-room-of-user [c state uid page-desc]
  (let [username (get-username state uid)
        old-room (get-in state [:uid->data uid :room])
        _ (debug "change-room-of-user" uid page-desc)
        new-room (page-desc->room page-desc username uid)]
    (if (= old-room new-room)
      {:send
       [[uid (room-init-message c state new-room username
                                (get-in state [:rooms (:name new-room)]))]]}
      (worker-compose c state
                      [remove-user-from-room uid old-room]
                      [add-user-to-room      uid new-room]))))

(defmethod sub-handler :default
  [c state tag opts]
  (debug "Unhandled worker subscriber message" tag opts)
  {:new-state state, :send []})

(defn get-init-app-message [username anti-forgery-token]
  [:thms/init-app {:username username
                   :anti-forgery-token anti-forgery-token
                   }])

;; XXX in updating username->uids, should dissoc the username key once the set
;; becomes empty, to avoid a memory leak.

(defmethod sub-handler :client-connected
  [c state _ {:keys [uid username pid cookie-key anti-forgery-token]}]
  (debug "client-connected" uid username pid)
  (worker-compose c state
                  [(fn [c state]
                     {:new-state (-> state
                                     (assoc-in [:uid->data uid] {:username username
                                                                 :pid pid
                                                                 :cookie-key cookie-key})
                                     (update :username->uids
                                             update username (fnil conj #{}) uid))
                      :send [[uid (get-init-app-message username anti-forgery-token)]]})]
                  (when (and (some? username) (not= cookie-key "talker-ck"))
                    [update-session uid assoc :last-login (now-timestamp)])))

(defmethod sub-handler :refresh-game
  [c state _ {:keys [gid]}]
  (worker-compose c state
                  [(fn [c state]
                     (let [game (get-game state gid)
                           old-status (:status game)
                           new-game (ds/get-game-by-gid (:ds c) gid)
                           new-state (assoc-in state [:game-by-id gid] new-game)
                           status (:status new-game)
                           indices-update (cond
                                            (= status "open")
                                            :assoc-in-open

                                            (and (= old-status "open")
                                                 (= status "running"))
                                            :open->running

                                            (= status "running")
                                            :assoc-in-running

                                            :else
                                            :dissoc-in-open&running)]
                       (debug "refresh-game" gid old-status new-game indices-update)
                       (update-game-and-notify c state gid :refreshed
                                              [:refreshed new-game]
                                              indices-update)))]))

(defmethod sub-handler :client-disconnected
  [c state _ {:keys [uid pid]}]
  (let [room (get-in state [:uid->data uid :room])
        username (get-in state [:uid->data uid :username])]
    (debug "client-disconnected" uid username pid)
    (worker-compose c state
                    [remove-user-from-room uid room]
                    [(fn [c state] {:new-state (-> state
                                                   (dissoc-in [:uid->data uid])
                                                   (update :username->uids
                                                           update username disj uid))})])))

(defmethod sub-handler :avatar-image-uploaded
  [c state _ username]
  (debug "avatar-image-uploaded" username)
  {:send (all-in-rooms state (into #{(home-room username)}
                                   (all-view-or-play-game-rooms-with-player state username))
                       [:thms.account/avatar-changed username])})

(defn client-logged-in [c state uid username]
  (let [room (get-in state [:uid->data uid :room])
        old-username (get-in state [:uid->date uid :username])]
    (worker-compose c state
                    [remove-user-from-room uid room]
                    [(fn [c state] {:new-state (-> state
                                                   (update :username->uids
                                                           update old-username disj uid)
                                                   (assoc-in [:uid->data uid :username] username)
                                                   (update :username->uids
                                                           update username (fnil conj #{}) uid))})]
                    [add-user-to-room uid room]
                    [update-session uid assoc :username username])))

(defmethod sub-handler :client-logged-in
  [c state _ {:keys [uid username]}]
  (client-logged-in c state uid username))

(defmethod sub-handler :client-logged-out
  [c state _ {:keys [uid]}]
  (let [old-username (get-username state uid)
        room (get-in state [:uid->data uid :room])
        username nil]
    (worker-compose c state
                    [remove-user-from-room uid room]
                    [(fn [c state] {:new-state (-> state
                                                   (update :username->uids
                                                           update old-username disj uid)
                                                   (dissoc-in [:uid->data uid :username])
                                                   (update :username->uids
                                                           update username (fnil conj #{}) uid))})]
                    [add-user-to-room uid room]
                    [update-session uid assoc :username nil])))

(defmethod sub-handler :chat-line
  [c state _ {:keys [uid text]}]
  (if-let [username (get-username state uid)]
    (if-let [room (get-in state [:uid->data uid :room])]
      (let [chat-message {:author username
                          :timestamp (now-timestamp)
                          :text text}]
        (ds/add-chat-message (:ds c) (:name room) chat-message)
        {:send (all-in-room state room
                            [:thms.rooms/new-chat-message chat-message])})
      (error "Chat message in a chatless room" uid))
    (error "Chat message from a guest user" text)))

(defmethod sub-handler :navigate
  [c state _ {:keys [uid page-desc]}]
  (debug "got :navigate" uid page-desc)
  (change-room-of-user c state uid page-desc))

(defn validate-game [form-data]
  (let [option->values (into {} (map (fn [opt]
                                       [(:name opt) (into #{} (map :value (:values opt)))])
                                     td-options))
        problems (atom [])]

    (for [[name value] form-data]
      (if (= name :comment)
        (when-not (<= (count value) (:max-comment-length VALIDATION-CONSTS))
          (swap! problems conj "Comment is too long"))
        (when-not (and (contains? option->values name)
                       (contains? (option->values name) value))
          (swap! problems conj (format "Option '%s' value '%s' is wrong" name value)))))

    (if (seq @problems)
      [false @problems]
      [true])))

(defmethod sub-handler :submit-new-game-form
  [c state _ {:keys [uid form-data]}]
  (if-let [username (get-username state uid)]
    (let [[ok problems] (validate-game form-data)]
      (if ok
        (let [[code kind comment game-settings] (get-and-dissoc form-data :code :kind :comment)
              code (or code "td")
              game (ds/create-game (:ds c) username code kind comment game-settings)
              game-created [:thms.lobby/game-created {:game game}]]
          (ds/set-game-settings-on-last-creation! (:ds c) username code kind game-settings)
          (publish-to-talker c [:global-uid [:thms.globaltalker/game-created
                                             {:gid (:gid game)}]])
          (apply-changes c {:new-state (-> state
                                           (update :game-by-id assoc (:gid game) game)
                                           (update :open-game-by-id assoc (:gid game) game))
                            :send (concat [[uid [:thms.lobby/create-game-ok {:gid (:gid game)}]]]
                                          (all-in-room state (home-room username)
                                                       game-created)
                                          (all-in-room-except-uid uid state (lobby-room)
                                                                  game-created))}))
        (apply-changes c {:send [[uid [:thms.lobby/create-game-error {:message problems}]]]})))))

(defn apply-game-control [c state uid username game control]
  (let [gid (:gid game)]
    (case control
      (:join :part) (let [old-players (:players game)
                          new-players (if (= control :join)
                                        (conj old-players username)
                                        (vec (filter #(not= % username) (:players game))))]
                      (send-players-update-notification! c state game control username)
                      (update-game-and-notify c state gid control
                                              [:set-players new-players]
                                              :assoc-in-open
                                              (when (= control :part) old-players)))
      :cancel (update-game-and-notify c state gid control [:set-status "canceled"] :dissoc-in-open)
      :start  (update-game-and-notify c state gid control [:set-status "running"]  :open->running))))

(defmethod sub-handler :submit-game-control
  [c state _ {:keys [uid control]}]
  (if-let [room (get-in state [:uid->data uid :room])]
    (if-let [game (get-game state (:gid room))]
      (let [username (get-username state uid)
            game-controls (game-controls-for game username)]
        (if (control game-controls)
          (apply-game-control c state uid username game control)))
      (warnf "Room %s doesn't have a game, for uid %s" room uid))
    (warnf "Client %s is not in any room" uid)))

(defn prepare-deltas-message [state room engine tdpr]
  (all-in-room-fn state room
                  (fn [uid]
                    (let [username (get-username state uid)
                          role (game-engine/get-role-for-username engine username)
                          deltas (tdpr role)]
                      (when (seq deltas)
                        [:thms.play/deltas {:deltas deltas}])))))

(defmethod sub-handler :client-sent-answer
  [c state _ {:keys [uid data]}]
  (let [{:keys [role answer]} data
        room (get-in state [:uid->data uid :room])]
    (assert room
            (format "User '%s' must be in room" uid))
    (case (:type room)
      "play-game"
      (let [username (get-username state uid)
            gid (:gid room)
            game (get-game state gid)
            _ (assert (= (:status game) "running")
                      "Game must be running to accept answers")
            room-state (get-room-state c state room)
            engine (:engine room-state)
            actual-role (game-engine/get-role-for-username engine username)

            _ (assert (= actual-role role)
                      "Actual user's role must be same as reported")

            old-runtime-info (game-engine/engine-runtime-info engine)
            {:keys [translated-deltas-per-role]} (game-engine/put-answer! engine role answer)
            new-runtime-info (game-engine/engine-runtime-info engine)]
        (ds/save-engine-checkpoint! (:ds c) gid (game-engine/create-checkpoint engine))
        (send-your-turn-notifications! c state game old-runtime-info new-runtime-info)

        (worker-compose c state
                        [(fn [c state]
                           {:send (prepare-deltas-message state room engine
                                                          translated-deltas-per-role)})]
                        [(fn [c state]
                           (let [runtime-info (game-engine/engine-runtime-info engine)
                                 finished? (game-engine/engine-finished? engine)
                                 outcome (when finished? (game-engine/engine-outcome engine))]
                             (if finished?
                               (publish-to-talker c [:global-uid [:thms.globaltalker/game-finished
                                                                  {:gid gid
                                                                   :outcome outcome}]])
                               (publish-to-talker c [:global-uid [:thms.globaltalker/game-updated
                                                                  {:gid gid
                                                                   :runtime-info runtime-info}]]))
                             (update-game-and-notify c state gid :after-client-answer
                                                     (if finished?
                                                       [:set-finished "finished" outcome runtime-info]
                                                       [:set-runtime-info runtime-info])
                                                     (if finished?
                                                       :dissoc-in-running
                                                       :assoc-in-running))))]))

      ("test-ui" "sandbox")
      (let [engine (get-in state [:rooms (:name room) :engine])
            {:keys [translated-deltas-per-role]} (game-engine/put-answer! engine role answer)]
        {:send (all-in-room state room
                            [:thms.test-ui/deltas translated-deltas-per-role])})

      (warn "trying to answer on the non-engine room: " uid answer room))))

(defmethod sub-handler :test-switch-scenario
  [c state _ {:keys [uid index]}]
  (let [room (get-in state [:uid->data uid :room])
        code (:code room)
        _ (assert (and room (= (:type room) "test-ui"))
                  "Must be in the test-ui room to switch scenarios")
        room-state (get-room-state c state room)
        scenario (get (thm.game-engine.registry-of-test-scenarios/scenarios-by-code code) index)
        game-state (-> scenario :create (apply []))
        engine (game-engine/new-engine-from-checkpoint (:code room) {:state game-state} :test)
        client-states (initial-client-states engine)]
    {:new-state (assoc-in state [:rooms (:name room) :engine] engine)
     :send (all-in-room state room
                        [:thms.test-ui/initial-state client-states])}))

(defmethod sub-handler :thm.test-ui/new-game
  [c state _ {:keys [uid]}]
  (let [room (get-in state [:uid->data uid :room])
        _ (assert (and room (= (:type room) "sandbox"))
                  "Must be in the sandbox room to ask a new game")
        ;; hack: better would be to extract engine creation from room state
        ;; creation, and call that from here.
        engine (:engine (create-sandbox-room-state c state room))
        client-states (initial-client-states engine)]
    {:new-state (assoc-in state [:rooms (:name room) :engine] engine)
     :send (all-in-room state room
                        [:thms.test-ui/initial-state client-states])}))

(defmethod sub-handler :thm.signup/submit-signup-form
  [c state _ {:keys [uid form-data]}]
  (let [email (get-form-value form-data :email)]
    (cond
      (not (re-matches #".*@.*" email))
      {:send [[uid [:thms.signup/signup-failed :invalid-email]]]}

      (ds/user-by-email (:ds c) email)
      {:send [[uid [:thms.signup/signup-failed :user-exists]]]}

      (ds/pending-registration-by-email (:ds c) email)
      {:send [[uid [:thms.signup/signup-failed :unconfirmed-registration-exists]]]}

      :else
      (let [confirmation-code (users/create-confirmation-code)]
        (ds/add-pending-registration! (:ds c) email confirmation-code)
        (emails/send-signup-confirmation-code! email confirmation-code)
        {:send [[uid [:thms.signup/confirmation-email-sent]]]}))))

(defmethod sub-handler :thm.signup/submit-confirm-registration-form
  [c state _ {:keys [uid form-data]}]
  (let [pending-registration (ds/pending-registration-by-code (:ds c) (:confirmation-code form-data))
        [user error-messages] (users/validate-signup-form-data (:ds c) form-data pending-registration)]
    (if (seq error-messages)
      {:send [[uid [:thms.signup/confirm-failed {:error-messages error-messages}]]]}
      (let [{:keys [username password email]} user
            password-hash (users/get-password-hash password)]
        (ds/add-user! (:ds c) {:username username
                               :password-hash password-hash
                               :email email})
        (ds/confirm-pending-registration-by-code! (:ds c) (:confirmation-code form-data))
        (worker-compose c state
                        [client-logged-in uid username]
                        [(fn [c state]
                           {:send [[uid [:thms.signup/confirm-successful {:username username}]]
                                   ;; TODO in future maybe send also the get-init-app-message
                                   ;; for user prefs and such
                                   ]})])))))

(defmethod sub-handler :thm.reset-password/submit-forgot-form
  [c state _ {:keys [uid form-data]}]
  (let [username-or-email (get-form-value form-data :username-or-email)
        user (ds/user-by-username-or-email (:ds c) username-or-email)]
    (if user
      (if-let [recovery (ds/password-recovery-by-email (:ds c) (:email user))]
        {:send [[uid [:thms.reset-password/forgot-failed :recovery-exists]]]}
        (let [code (users/create-password-recovery-code)]
          (ds/add-password-recovery! (:ds c) (:email user) code)
          (emails/send-password-recovery! (:email user) code)
          {:send [[uid [:thms.reset-password/reset-email-sent]]]}))
      {:send [[uid [:thms.reset-password/forgot-failed :no-user]]]})))


(defmethod sub-handler :thm.reset-password/submit-reset-form
  [c state _ {:keys [uid form-data]}]
  (let [code (get-form-value form-data :reset-code)
        recovery (ds/password-recovery-by-code (:ds c) code)
        ;; It's ok to fail harshly because the only way to get here is by
        ;; cheating, i.e. submitting reset form with bad code.
        _ (assert recovery (str "Must have recovery by code " code))
        user (ds/user-by-email (:ds c) (:email recovery))
        username-or-email (get-form-value form-data :username-or-email)
        password (get form-data :password "")
        username (:username user)

        errors (cond-> []
                 (not (or (= username username-or-email)
                          (= (:email user) username-or-email)))
                 (conj [:wrong-username-or-email])

                 ;; XXX the following two cases are duplicated from
                 ;; users/validate-form-data.
                 (< (count password) (:min-password-length VALIDATION-CONSTS))
                 (conj [:min-password (:min-password-length VALIDATION-CONSTS) (count password)])

                 (> (count password) (:max-password-length VALIDATION-CONSTS))
                 (conj [:max-password (:max-password-length VALIDATION-CONSTS) (count password)]))]
    (if (seq errors)
      {:send [[uid [:thms.reset-password/reset-failed errors]]]}
      (let [password-hash (users/get-password-hash password)]
        ;; XXX do this in transaction
        (ds/change-user-password! (:ds c) username password-hash)
        ;; XXX it's not the best idea to remove stuff forever, because a trail
        ;; of actions would be useful.
        (ds/remove-password-recovery! (:ds c) code)
        (worker-compose c state
                        [client-logged-in uid username]
                        [(fn [c state]
                           {:send [[uid [:thms.reset-password/reset-successful {:username username}]]]})])
        ;; TODO in future maybe send also the get-init-app-message
        ;; for user prefs and such
        ))))

(defmethod sub-handler :thm.game-ui/abandon-game
  [c state _ {:keys [uid]}]
  (let [username (get-username state uid)
        room (get-in state [:uid->data uid :room])
        gid (:gid room)
        _ (assert (= (:type room) "play-game")
                  "User must be in play-game room to abandon")
        game (get-game state gid)
        _ (assert (= (:status game) "running")
                  "Game must be running to abandon")
        _ (assert (game-includes-player? game username)
                  "User must be a player in the game to abandon")

        new-runtime-info (assoc (:runtime-info game)
                                :current-players [])
        outcome ["abandoned" "abandoned-by" username]]
    (worker-compose c state
                    [update-game-and-notify gid :abandoned
                     [:set-finished "abandoned" outcome new-runtime-info]
                     :dissoc-in-running]
                    [(fn [c state]
                       {:send (all-in-room state (play-game-room gid)
                                           [:thms.game-ui/game-abandoned
                                            ;; XXX this is a hack that relies
                                            ;; on the fact that only change in
                                            ;; outcome matters for updating
                                            ;; play room's ui.  Better get the
                                            ;; new game from
                                            ;; update-game-and-notify called a
                                            ;; bit earlier.
                                            (assoc game :outcome outcome)])})])))

(defmethod sub-handler :thmc.game-ui/ui-layout-chosen
  [c state _ {:keys [uid data]}]
  (let [username (get-username state uid)
        {:keys [value code user-agent]} data
        user (ds/user-by-username (:ds c) username)
        account-settings (:account-settings user)
        ui-modes (get account-settings "ui-modes" {})
        new-ui-modes (assoc-in ui-modes [code user-agent] value)
        new-account-settings (assoc account-settings
                                    "ui-modes" new-ui-modes)]
    (debug "ui-layout-chosen" username code value new-account-settings)
    (ds/set-user-account-settings! (:ds c) username new-account-settings)
    {}))

(defmethod sub-handler :thmc.play-game/save-personal-notes
  [c state _ {:keys [uid data]}]
  (let [username (get-username state uid)
        _ (assert (some? username)
                  "Personal notes are only for registered users")
        room (get-in state [:uid->data uid :room])
        gid (:gid room)
        _ (assert (= (:type room) "play-game")
                  "Personal notes are linked only to the real games")
        content data
        user (ds/user-by-username (:ds c) username)]
    (ds/set-personal-notes-content! (:ds c) username gid content)
    ;; XXX send confirmation to this user?
    {}))

(defmethod sub-handler :thmc.account/submit-account-settings-form
  [c state _ {:keys [uid form-data]}]
  (debug "submit account settings form" uid form-data)
  (let [username (get-username state uid)
        user (ds/user-by-username (:ds c) username)
        new-account-settings (assoc (:account-settings user)
                                    "allow-game-emails" (get form-data "allow-game-emails"))]
    (ds/set-user-account-settings! (:ds c) username new-account-settings)
    {:send [[uid [:thms.account/account-settings-applied]]]}))

(defmethod sub-handler :thmc.term/new-tm-game
  [c state _ {:keys [uid data]}]
  (let [username (get-username state uid)
        seed (:seed data)

        room (term-room username)
        engine (game-engine/new-engine "tm"
                                       [username]
                                       {:seed seed})

        room-state (get-room-state c state room)
        new-room-state (assoc room-state :engine engine)

        cs (thm.game-engine.game-engine/translated-state-for engine "s")
        str-rep (tm-cli/engine-to-string engine)

        ]
    {:new-state (-> state
                    (assoc-in [:rooms (:name room)] new-room-state))
     :send (all-in-room state room
                        [:thms.term/new-tm-game-started {:seed seed
                                                         :str-rep str-rep
                                                         :cs cs}])}))

(defmethod sub-handler :thmc.term/get-engine-str-rep
  [c state _ {:keys [uid data]}]
  (let [username (get-username state uid)
        room (term-room username)
        room-state (get-room-state c state room)
        engine (:engine room-state)
        str-rep (tm-cli/engine-to-string engine)]
    {:send (all-in-room state room
                        [:thms.term/answering-get-engine-rep-str {:str-rep str-rep}])}))


(defmethod sub-handler :thmc.term/put-answer
  [c state _ {:keys [uid data]}]
  (let [answer (:answer data)
        username (get-username state uid)
        room (term-room username)
        room-state (get-room-state c state room)
        engine (:engine room-state)
        {:keys [translated-deltas-per-role]} (game-engine/put-answer! engine "s" answer)
        cs (thm.game-engine.game-engine/translated-state-for engine "s")
        str-rep (tm-cli/engine-to-string engine)]
    {:send (all-in-room state room
                        [:thms.term/after-put-answer {:tdpr translated-deltas-per-role
                                                      :cs cs
                                                      :str-rep str-rep}])}))
