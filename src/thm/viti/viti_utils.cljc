(ns thm.viti.viti-utils
  (:require [clojure.string :as str]
            [clojure.set]
            [thm.utils :refer [denumber-id
                               get-var
                               unrecognized
                               all-figures-in-space]]))

(def figure-type->abbreviation {:worker "w"
                                :grande "g"
                                :vp "v"
                                :rooster "r"
                                :rp "b"

                                :trellis "t"
                                :windmill "n"
                                :irrigation "i"
                                :cottage "c"
                                :tasting-room "s"
                                :medium-cellar "m"
                                :large-cellar "l"
                                :yoke "y"})

(def figure-abbreviation->type (clojure.set/map-invert figure-type->abbreviation))

(defn get-figure-type [figure-id]
  (figure-abbreviation->type (subs figure-id 1 2)))

(defn get-figure-id [figure-role figure-type & [number]]
  (str (if (= figure-role :neutral)
         "n"
         figure-role)
       (figure-type->abbreviation figure-type)
       (when (some? number)
         (str ":" number))))

(defn figure-image [figure-id & [opts]]
  [:img.figure (merge opts
                      {:src (str "/viti-images/figures/" (denumber-id figure-id) ".png")}
                      (when (= (subs figure-id 1 2) "g")
                        {:class "grande"}))])

(defn state-timing [state]
  {:year (get-var state "year"),
   :season (get-var state "season")})

(defn available-figures-of-type [state role figure-type]
  (filter #(= (subs % 1 2) (figure-type->abbreviation figure-type))
          (all-figures-in-space state [role "workers"])))

(defn get-n-available-figures-of-type [state role figure-type]
  (count (available-figures-of-type state role figure-type)))

(defn get-first-available-figure-of-type [state role figure-type]
  (first (available-figures-of-type state role figure-type)))
