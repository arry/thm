(set-env!
 :source-paths   #{"src" "test" "sass"}
 :resource-paths #{"resources"}
 :dependencies '[[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.11.60"]
                 [org.clojure/data.json "2.4.0"]

                 [environ "1.2.0"]
                 [boot-environ "1.2.0"]
                 [adzerk/boot-cljs "2.1.5" :scope "test"]
                 [adzerk/boot-reload "0.6.1" :scope "test"]
                 [adzerk/boot-cljs-repl   "0.4.0" :scope "test"]
                 [tolitius/boot-check "0.1.13" :scope "test"]

                 [cider/piggieback "0.5.3"  :scope "test"]
                 [weasel                  "0.7.1"  :scope "test"]

                 [org.clojure/tools.namespace "1.3.0"]

                 [org.danielsz/system "0.4.7"]
                 [nrepl "0.9.0"]

                 [http-kit "2.7.0-alpha1"]
                 [ring/ring-core "1.4.0"] ;; not upgraded, 1.6.3.  The new version doesn't load files in dev mode.
                 [ring/ring-defaults "0.1.5"] ;; not upgraded, 0.3.1.  The new one causes problems with cookies.
                 [compojure "1.7.0"]

                 [com.taoensso/encore "2.94.0"] ; dependencies for other taoensso modules
                 [com.taoensso/sente "1.12.0"] ;; websockets
                 [com.taoensso/timbre "4.10.0"] ;; logging
                 [org.slf4j/slf4j-api "1.8.0-beta2"]
                 [org.slf4j/slf4j-simple "1.8.0-beta2"]
                 [com.cognitect/transit-clj  "0.8.303"]
                 [com.cognitect/transit-cljs "0.8.256"]
                 [com.taoensso/nippy "2.14.0"]  ;; serialization
                 [com.taoensso/carmine "2.18.0"] ; Redis

                 [com.layerware/hugsql "0.4.8"] ;; database
                 [org.postgresql/postgresql "42.2.2"]
                 [conman "0.7.7"]

                 [crypto-password "0.2.0"]
                 [cprop "0.1.11"]
                 [hiccup "2.0.0-alpha1"]

                 [overtone/at-at "1.2.0"] ;; repeated actions

                 [deraen/boot-sass "0.5.5"]

                 ;; Client-side dependencies
                 [baking-soda "0.2.0"]
                 [reagent "0.8.2-SNAPSHOT" :exclusions [cljsjs/react
                                                        cljsjs/react-dom
                                                        cljsjs/create-react-class]]
                 [cljsjs/react "16.3.2-0"]
                 [cljsjs/react-dom "16.3.2-0"]
                 [cljsjs/react-transition-group "2.3.1-0"]
                 [cljsjs/react-popper "0.10.4-0"]
                 [cljsjs/create-react-class "15.6.3-1"]

                 [cljsjs/react-flip-move "3.0.1-1"] ;; experimental

                 [re-frame "0.10.5" :exclusions [org.clojure/clojurescript]]
                 [cljs-ajax "0.7.3"]

                 [org.flatland/ordered "1.5.7"]
                 ;; [venantius/accountant "0.1.7"] ;; Navigation (secretary <->
                 ;; html history): dependency not included because it's copied
                 ;; and adapted to thm_accountant.
                 ])


(require
 '[tolitius.boot-check :as check]
 '[environ.boot :refer [environ]]

 '[boot.repl]
 '[adzerk.boot-cljs      :refer [cljs]]
 '[adzerk.boot-cljs-repl :refer [cljs-repl start-repl]]
 '[adzerk.boot-reload    :refer [reload]]
 '[thm.systems :refer [dev-system prod-system]]
 '[system.boot :refer [system run]]
 '[thm.config :refer [config]]
 '[deraen.boot-sass :refer [sass]]
 )

(swap! boot.repl/*default-dependencies*
       concat '[[cider/cider-nrepl "0.28.5"]])

(swap! boot.repl/*default-middleware*
       conj 'cider.nrepl/cider-middleware)

(deftask dev
  "Run a restartable system in the Repl"
  []
  (comp
   (watch :verbose true)
   ;;(system :sys #'dev-system :auto true :files ["handler.clj"])
   (reload :on-jsload 'thm.core/init!)
   (cljs :source-map true
         :compiler-options {:asset-path "/main.out"})
   (sass)
   (repl :server true :init-ns 'thm.user)))

(deftask dev-cljs-repl
  "Run a restartable system in the Repl, with cljs repl"
  []
  (comp
   (watch :verbose true)
   ;;(system :sys #'dev-system :auto true :files ["handler.clj"])
   (reload :on-jsload 'thm.core/init!)
   (cljs-repl)
   (cljs :source-map true
         :compiler-options {:asset-path "/main.out"})
   (sass)))

(deftask dev-run
  "Run a dev system from the command line"
  []
  (comp
   (cljs :compiler-options {:asset-path "/main.out"})
   (run :main-namespace "thm.core" :arguments [#'dev-system])
   (wait)))

(deftask prod-run
  "Run a prod system from the command line"
  []
  (comp
   (cljs :optimizations :advanced)
   (sass :output-style :compressed)
   (run :main-namespace "thm.core" :arguments [#'prod-system])
   (wait)))

(deftask build
  "Builds an uberjar of this project that can be run with java -jar"
  []
  (comp
   (aot :namespace '#{thm.core})
   (cljs :optimizations :advanced)
   (sass :output-style :compressed)
   (pom :project 'thm
        :version "1.0.0")
   (uber)
   (jar :main 'thm.core)
   (target :dir #{"target"})))
