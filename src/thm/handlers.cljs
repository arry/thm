(ns thm.handlers
  (:require [thm.db :as db]
            [re-frame.core :as re-frame :refer (reg-event-db reg-event-fx dispatch)]
            [reagent.core :as r]
            [re-frame.registrar :refer (clear-handlers)]
            [taoensso.timbre :refer-macros (tracef debugf infof warnf errorf debug)]
            [taoensso.sente  :as sente :refer (cb-success?)]
            [thm.views.marker :as marker]
            [thm.views.game_ui :as ui]
            [thm.views.views :refer [set-initial-account-settings!]]
            [thm.client-ws-handler :refer (handle-ws-message)]
            [thm.game-ui.game-client :as game-client]
            [thm.utils :refer [game-comparator
                               username-comparator
                               game-includes-player?
                               short-game-name
                               first-index
                               has-your-request?
                               map-values
                               get-request-for-role
                               get-user-agent-string
                               on-mobile-browser?
                               to-integer
                               denumber-id
                               get-request-for-role
                               find-first
                               all-cards-in-zone
                               now-timestamp
                               get-game-code-from-page-arg
                               get-your-request
                               set-answer
                               seq-contains?
                               remove-from-vector-at-index
                               ]]
            [thm-accountant.core :as accountant]
            [goog.net.cookies :as cks]
            [clojure.string :as str]

            ;; td-specific
            [thm.utils :refer [state-version-is-at-least?
                               get-var
                               dispatch-e]]
            [thm.td.td-data :as td-data]
            [thm.td.td-utils :as td-utils :refer [get-track-board-widget-value]]


            ))

(defn ^:export set-cookie [name value secs]
  (debug "called set-cookie " name value secs)
  (goog.net.cookies.set name value secs "/"))

(defn map-values-inverted-args [m f]
  (map-values f m))

(defn sente-send [db data]
  (debug "sente-send" data)
  (let [send-fn (-> db :sente :send-fn)]
    (send-fn data)))

(clear-handlers :event)

(reg-event-db
 :initialize-db
 (fn  [_ _]
   db/default-db))

(reg-event-db
 :initialize-sente
 (fn [db event]
   (let [sente (sente/make-channel-socket! "/chsk" {:type :auto})]
     (sente/start-chsk-router! (:ch-recv sente) handle-ws-message)
     (assoc db
            :sente (select-keys sente [:chsk :ch-recv :send-fn :state])))))

(reg-event-db
 :navigate
 (fn [db [_ page-desc]]
   (when (:ws-open? db)
     (sente-send db [:thmc/navigate page-desc]))
   (assoc db
          :page-loading? true
          :page-desc page-desc)))

(reg-event-db
 :ws-state-changed
 (fn [db [_ ws-open?]]
   (when ws-open?
     (sente-send db [:thmc/navigate (:page-desc db)]))
   (assoc db :ws-open? ws-open?)))

(reg-event-db
 :reloaded
 (fn [db [_]]
   (when (:ws-open? db)
     (sente-send db [:thmc/navigate (:page-desc db)]))
   (dispatch [:do-resize])
   db))

(reg-event-db
 :key-press
 (fn [db [_ e]]
   (case (first (:page-desc db))
     "marker" (marker/handle-key-press e)
     "test-proxier" (let [key (.-key e)]
                      (js/console.log "yay got key" key)

                      (case key
                        "ArrowRight" (dispatch [:thm.proxier/switch-card 1])
                        "ArrowLeft"  (dispatch [:thm.proxier/switch-card -1])
                        nil))
     nil)
   db))

(reg-event-db
 :resize
 (fn [db [_]]
   ;; XXX this check should be somewhere else.
   (when (#{"test-ui" "sandbox" "play"} (first (:page-desc db)))
     (ui/handle-resize))
   db))

(reg-event-db
 :thms.cookies/update
 (fn [db [_ data]]
   (let [{:keys [name value secs]} data]
     (set-cookie name value secs))
   db))

(defn get-game-code [db]
  (if (#{"test-ui" "sandbox"} (first (:page-desc db)))
    (get-game-code-from-page-arg (second (:page-desc db)))
    (:code (:thm.play-game/game db))))

(reg-event-db
 :do-resize
 (fn [db [_]]
   ;; XXX this check shouldn't be duplicated.
   (when (#{"test-ui" "sandbox" "play"} (first (:page-desc db)))
     (let [code (get-game-code db)]
       (ui/do-resize code)))
   db))

(reg-event-db
 :ws
 (fn [db [_ kind & args]]
   (sente-send db (into [kind] args))
   db))

(defn get-only-request-role [cs]
  (when (and (:request-map cs)
             (= (count (:request-map cs)) 1))
    (first (keys (:request-map cs)))))

(defn the-only-role-that-needs-attention [db]
  (let [those-that-need (->> (vals (:thm.game-ui/client-states db))
                             (filter (fn [cs]
                                       (or (= (get-only-request-role cs) (:your-role cs))
                                           (:paused? cs)))))]
    (when (= (count those-that-need) 1)
      (-> those-that-need first :your-role))))

(defn initial-your-role [client-states]
  (if-let [r (and client-states
                  (first client-states)
                  (get-only-request-role (val (first client-states))))]
    r
    (first (sort (keys client-states)))))

;; == Game UI handlers

;; TODO for chat-n-log and play-area tabs, include primary role (only matters
;; for sandbox and test-ui).
(reg-event-db
 :thm.game-ui/activate-chat-n-log-tab
 (fn [db [_ tab-name]]
   (assoc db :thm.game-ui/chat-n-log-active-tab tab-name)))

(reg-event-db
 :thm.game-ui/activate-play-area-tab
 (fn [db [_ tab-name]]
   (r/next-tick #(dispatch [:do-resize]))
   (assoc db :thm.game-ui/play-area-active-tab tab-name)))

(reg-event-db
 :thm.game-ui/on-play-area-tab-hidden
 (fn [db [_ primary-role tab-name default-tab-name]]
   (if (= (:thm.game-ui/play-area-active-tab db) tab-name)
     (do
       (dispatch [:thm.game-ui/activate-play-area-tab default-tab-name])
       db)
     db)))

(reg-event-db
 :thm.game-ui/activate-one-of-play-area-tabs
 (fn [db [_ tab-names]]
   (r/next-tick #(dispatch [:do-resize]))
   (if (contains? (set tab-names) (:thm.game-ui/play-area-active-tab db))
     db
     (assoc db :thm.game-ui/play-area-active-tab (first tab-names)))))

(reg-event-db
 :thm.game-ui/cursor-on-big-card
 (fn [db [_ yes?]]
   (assoc-in db [:thm.game-ui/preview :cursor-on-big-card] yes?)))

(reg-event-db
 :thm.game-ui/cursor-on-small-card
 (fn [db [_ yes? & [id additional-class]]]
   (update db :thm.game-ui/preview
           #(cond-> %
              true (assoc :cursor-on-small-card yes?)
              yes? (assoc :id id, :additional-class additional-class)))))

;; XXX appropriating the property name for cursor-on-small-card for card
;; preview functionality on touch devices.  This seems like the fastest way at
;; the time; a better way is to make explicit a property whether to display
;; card preview.
(reg-event-db
 :thm.game-ui/toggle-card-preview
 (fn [db [_ id additional-class]]
   (let [preview (:thm.game-ui/preview db)
         this-shown? (and (:cursor-on-small-card preview)
                          (= (:id preview) id))
         yes? (not this-shown?)]
     (update db :thm.game-ui/preview
             #(cond-> %
                true (assoc :cursor-on-small-card yes?)
                yes? (assoc :id id)
                yes? (assoc :additional-class additional-class))))))

(reg-event-db
 :thm.game-ui/set-answer
 (fn [db [_ primary-role answer]]
   (assoc-in db [:thm.game-ui/client-states primary-role :answer] answer)))

(defn get-client-state-for [db primary-role]
  (get (:thm.game-ui/client-states db)
       primary-role))

(reg-event-db
 :thm.game-ui/reset-answer
 (fn [db [_ primary-role answer]]
   (let [cs (get-client-state-for db primary-role)
         [kind text-descr & args :as req]
         (get-request-for-role cs primary-role)
         answer (case kind
                  :choose-place-influence {}
                  :choose-remove-influence {}
                  :choose-change-defcon {}
                  nil)]
     (assoc-in db [:thm.game-ui/client-states primary-role :answer] answer))))

(reg-event-db
 :thm.game-ui/send-answer
 (fn [db [_ primary-role]]
   (sente-send db [:thmc.game-ui/my-answer {:answer (get-in db [:thm.game-ui/client-states primary-role :answer])
                                            :role primary-role}])
   (assoc-in db [:thm.game-ui/client-states primary-role :answer-sent?] true)))

(reg-event-db
 :thm.game-ui/proceed-from-pause
 (fn [db [_ primary-role]]
   (update-in db [:thm.game-ui/client-states primary-role] game-client/proceed-from-pause)))

(defn testing? [db]
  (#{"test-ui" "sandbox"} (first (:page-desc db))))

(reg-event-db
 :thm.game-ui/apply-next-delta
 (fn [db [_ primary-role progress]]
   (let [result (update-in db [:thm.game-ui/client-states primary-role]
                           game-client/apply-next-delta progress)]
     (when (testing? result)
       (when-let [r (the-only-role-that-needs-attention result)]
         (dispatch [:thm.test-ui/switch-role r])))
     result)))

(reg-event-db
 :thm.game-ui/set-ui-sizes
 (fn [db [_ value]]
   (assoc db :thm.game-ui/ui-sizes value)))

(reg-event-db
 :thm.game-ui/switch-ui-layout
 (fn [db [_ value]]
   (when (some? (:username db))
     (sente-send db [:thmc.game-ui/ui-layout-chosen {:value value
                                                     :code (get-game-code db)
                                                     :user-agent (get-user-agent-string)}]))
   (assoc db :thm.game-ui/layout value)))

;; td-specific handlers (TODO move them to their own file)

(reg-event-db
 :thm.game-ui.td/bg-chosen-for-command
 (fn [db [_ primary-role bg]]
   (let [cs (get-client-state-for db primary-role)
         [role req] (first (:request-map cs))
         [kind _text-descr cubes] req
         [lower higher] (td-utils/get-allowed-command-spread cs role bg cubes
                                                             (if (= kind :choose-command)
                                                               :normal
                                                               :onto))
         amount (cond (not= (first (:answer cs)) bg) higher
                      (= (second (:answer cs)) lower) higher
                      :else (dec (second (:answer cs))))]
     (update-in db [:thm.game-ui/client-states primary-role]
                assoc :answer [bg amount],
                :spread [lower higher]))))

(reg-event-db
 :thm.game-ui.td/bg-chosen-for-place-influence
 (fn [db [_ primary-role bg]]
   (let [cs (get-client-state-for db primary-role)
         req (get-request-for-role cs primary-role)
         [_kind _text-descr max max-per defcon? bgs] req
         potential-answer (update (:answer cs) bg (fnil inc 0))]
     (if (td-utils/good-answer-for-choose-place-influence?
          cs primary-role [max max-per defcon? bgs] potential-answer)
       (update-in db [:thm.game-ui/client-states primary-role]
                  assoc :answer potential-answer)
       db))))

(defn on-board-widget-clicked [cs {:keys [name bg? track] :as widget}]
  (if (= name "af")
    (if (state-version-is-at-least? cs td-data/SEPARATE-AFTERMATH-VERSION)
      (dispatch [:thm.game-ui/show-modal (if (= (get-var cs "phase") "aftermath")
                                           :aftermath-reveal
                                           :your-aftermath)])
      (when (= (get-var cs "phase") "aftermath")
        (dispatch [:thm.game-ui/show-modal :aftermath-reveal])))

    (when-let [[kind text-descr & args :as req] (get-your-request cs)]
      (case kind
        (:choose-command :choose-command-onto)
        ,(dispatch [:thm.game-ui.td/bg-chosen-for-command (:your-role cs) name])

        :choose-change-defcon (let [[whose max-tracks allowed-tracks lower higher exact-amount?] args
                                    bw-value (get-track-board-widget-value name)
                                    cur-value (get-var cs ["d" whose track])
                                    to-set (- bw-value cur-value)
                                    cur-answer (:answer cs)
                                    answer (if (= max-tracks 2)
                                             (let [pot (assoc cur-answer track to-set)]
                                               (if (> (count pot) 2)
                                                 {track to-set}
                                                 pot))
                                             {track to-set})]
                                (set-answer (:your-role cs) answer))
        :choose-place-influence (dispatch [:thm.game-ui.td/bg-chosen-for-place-influence (:your-role cs) name])

        :choose-remove-influence
        , (let [[whose max policy defcon? allowed-bgs] args
                cur-answer (:answer cs)
                answer (case policy
                         :single-bg (cond
                                      (empty? cur-answer) {name 1}
                                      (not (contains? cur-answer name)) {name 1}
                                      (and (< (get cur-answer name) max)
                                           (< (get cur-answer name) (get-var cs ["i" whose name])))
                                      (update cur-answer name inc)
                                      :else cur-answer)
                         :many-bgs (if (and (< (reduce + (vals cur-answer)) max)
                                            (< (get cur-answer name 0) (get-var cs ["i" whose name])))
                                     (update cur-answer name (fnil inc 0))
                                     cur-answer)
                         :half (if (or (empty? cur-answer)
                                       (not (contains? cur-answer name)))
                                 {name 1}
                                 cur-answer))]
            (set-answer (:your-role cs) answer))

        nil))))

(reg-event-fx
 :thm.game-ui.td/board-widget-clicked
 (fn [{:keys [db]} [_ primary-role widget]]
   (on-board-widget-clicked (get-client-state-for db primary-role) widget)
   {}))

(reg-event-fx
 :thm.game-ui/card-activated
 (fn [{:keys [db]} [_ primary-role cid]]
   (let [cs (get-client-state-for db primary-role)
         [kind text-descr & args :as req] (get-your-request cs)]
     (case kind
       :choose-card (set-answer primary-role cid)
       :choose-card-and-mode (set-answer primary-role [cid nil])
       :choose-cards (let [answer (or (:answer cs) #{})]
                       (if (contains? answer cid)
                         (set-answer primary-role (disj answer cid))
                         (set-answer primary-role (conj answer cid))))

       :choose-thing (let [[things options] args
                           helper (:request-helper cs)
                           things (get (:bucket->things helper) cid)]
                       (if (:multilevel? options)
                         (dispatch [:thm.game-ui/show-modal {:kind :choose-thing-of-card,
                                                             :cid cid
                                                             :things things}])
                         (do (when (> (count things) 1)
                               ;; XXX probably this check should be done earlier, when building the helper.
                               (warnf "A single-level request shouldn't have several things for cid: %s, things %s" cid things))
                             (set-answer primary-role (:tid (first things))))))
       :choose-things (let [[things options] args
                            helper (:request-helper cs)
                            answer (or (:answer cs) [])
                            things (get (:bucket->things helper) cid)
                            tid (-> helper
                                    :bucket->things
                                    (get cid)
                                    first
                                    :tid)
                            higher (:higher options)]
                        (if (seq-contains? answer tid)
                          (set-answer primary-role (filterv #(not= % tid) answer))
                          (if (and (some? higher) (= (count answer) higher))
                            (set-answer primary-role (conj (subvec answer 1) tid))
                            (set-answer primary-role (conj answer tid)))))
       :choose-cards-flexible (let [[request-zone-name cids options] args
                                    helper (:request-helper cs)
                                    answer (or (:answer cs) [])
                                    higher (:higher helper)]
                                (if (seq-contains? answer cid)
                                  (set-answer primary-role (filterv #(not= % cid) answer))
                                  (if (= (count answer) higher)
                                    (set-answer primary-role (conj (subvec answer 1) cid))
                                    (set-answer primary-role (conj answer cid))))))
     {})))

(reg-event-db
 :thm/send-chat-line
 (fn [db [_ line]]
   (when-not (#{"test-ui" "sandbox"} (first (:page-desc db)))
     (sente-send db [:thmc/chat-line line]))
   db))

(reg-event-db
 :thm.game-ui/navigate-to-site-from-finished-game
 (fn [db [_]]
   (accountant/navigate! (case (first (:page-desc db))
                           "test-ui" "/home"
                           "sandbox" (if (:username db)
                                       "/home"
                                       "/")
                           (str "/game/" (:gid (:thm.play-game/game db)))))
   db))

(reg-event-db
 :thm.game-ui/toggle-play-menu
 (fn [db [_]]
   (let [now? (:thm.game-ui/play-menu-open? db)]
     (assoc db
            :thm.game-ui/play-menu-open? (not now?)))))

(defn show-modal [db data]
  (let [modal-data (if (map? data)
                     data
                     {:kind data})
        stack (get db :thm.game-ui/modal-stack [])]
    (assoc db :thm.game-ui/modal-stack (conj stack modal-data))))

(reg-event-db
 :thm.game-ui/show-modal
 (fn [db [_ data]]
   (show-modal db data)))

(defn compact-ui-layout? [db]
  (= (:thm.game-ui/layout db) "compact"))

(reg-event-db
 :thm.game-ui/show-choose-mode-modal
 (fn [db [_ req]]
   (if (compact-ui-layout? db)
     (show-modal db {:kind :choose-mode
                     :request req})
     db)))

(reg-event-db
 :thm.game-ui/hide-modal
 (fn [db [_]]
   (let [stack (get db :thm.game-ui/modal-stack [])]
     (when (empty? stack)
       (errorf "Trying to hide modal without any shown"))
     (let [new-stack (pop stack)]
       (assoc db :thm.game-ui/modal-stack new-stack)))))

(reg-event-db
 :thm.game-ui/do-abandon-game
 (fn [db [_]]
   (sente-send db [:thmc.game-ui/abandon-game])
   (assoc db :loading? true)))

(reg-event-db
 :thms.game-ui/game-abandoned
 (fn [db [_ game]]
   (assoc db
        :loading? false
        :thm.play-game/game game
        :thm.game-ui/client-states (map-values
                                  #(assoc %2 :outcome (:outcome game))
                                  (:thm.game-ui/client-states db)))))

(reg-event-db
 :thm.game-ui.compact/switch-pane
 (fn [db [_ id]]
   (cond-> db
     (and (compact-ui-layout? db)
          (= id "chat"))
     (assoc :thm.game-ui.compact/new-chat-message-alert? nil)

     true
     (assoc :thm.game-ui.compact/active-pane id))))

(reg-event-db
 :thm.game-ui.compact/show-extended-player-plaque
 (fn [db [_ data]]
   (show-modal db (merge {:kind :extended-player-plaque}
                         data))))

(reg-event-db
 :thm.game-ui/show-personal-notes
 (fn [db [_]]
   (show-modal db {:kind :personal-notes
                   :content (:thm.play-game/personal-notes-content db)})))

(reg-event-db
 :thm.game-ui/save-personal-notes
 (fn [db [_ content]]
   (sente-send db [:thmc.play-game/save-personal-notes content])
   (assoc db :thm.play-game/personal-notes-content content)))

(reg-event-db
 :thm.game-ui/widget-clicked
 (fn [db [_ primary-role widget-name]]
   (let [cs (get-client-state-for db primary-role)
         [kind text-descr & args :as req] (get-your-request cs)
         request-helper (:request-helper cs)]
     (case kind
       :choose-space (do (set-answer (:your-role cs) widget-name)
                         db)

       :choose-spaces
       (let [{:keys [lower higher]} request-helper
             answer (or (:answer cs) [])
             index (first-index #(= % widget-name) answer)]
         (set-answer
          primary-role
          (cond (some? index)
                (remove-from-vector-at-index answer index)

                (= (count answer) higher)
                (-> answer
                    (subvec 1)
                    (conj widget-name))

                :else
                (conj answer widget-name)))
         db)

       :choose-spaces-from-bags
       (let [bags (:bags request-helper)
             n-bags (count bags)
             answer (or (:answer cs)
                        (vec (repeat n-bags [])))
             bag-index (get (:space-name->bag-index request-helper)
                            widget-name)
             answer-bag (get answer bag-index)

             index-in-answer (first-index #(= % widget-name) answer-bag)

             new-answer-bag (if (nil? index-in-answer)
                              (if (= (count answer-bag)
                                     (:amount (get bags bag-index)))
                                ;; Adding the new item will exceed required
                                ;; amount: remove oldest.
                                (conj (subvec answer-bag 1) widget-name)
                                (conj answer-bag widget-name))
                              (remove-from-vector-at-index answer-bag index-in-answer))

             new-answer (assoc answer bag-index new-answer-bag)]
         (set-answer primary-role new-answer)
         db)


       db))))

;; == Login handlers

(reg-event-db
 :thm.login/submit-login-form
 (fn [db [_ form-data]]
   (sente-send db [:thmc.login/submit-login-form form-data])
   (assoc db :loading? true)))

(reg-event-db
 :thms.login/login-successful
 (fn [db [_ data]]
   (accountant/navigate! "/home")
   (assoc db :loading? false
          :username (:username data)
          :thm.login/login-failure? false)))

(reg-event-db
 :thms.login/login-failure
 (fn [db [_ _data]]
   (assoc db :loading? false
          :thm.login/login-failure? true)))

(reg-event-db
 :thms/init-app
 (fn [db [_ data]]
   (assoc db
          :username (:username data)
          :anti-forgery-token (:anti-forgery-token data))))

;; == Account page handlers

(reg-event-db
 :thm.account/submit-logout-form
 (fn [db [_]]
   (sente-send db [:thmc.account/submit-logout-form {}])
   db))

(reg-event-db
 :thms.account/logout-successful
 (fn [db [_]]
   (accountant/navigate! "/")
   (dissoc db :username)))

(reg-event-db
 :thm.account/submit-account-settings-form
 (fn [db [_ form-data]]
   (sente-send db [:thmc.account/submit-account-settings-form form-data])
   (assoc db
          :loading? true
          :thm.account/settings-applied? false)))

(reg-event-db
 :thms.account/account-settings-applied
 (fn [db [_]]
   (assoc db :loading? false
          :thm.account/settings-applied? true)))

(reg-event-db
 :thm.account/avatar-file-too-large
 (fn [db [_]]
   ;; Keyword inside string to match the form sent from the server.
   (assoc db :thm.account/avatar-upload-result ":file-too-large")))

(reg-event-db
 :thm.account/starting-avatar-upload
 (fn [db [_]]
   (assoc db
          :thm.account/avatar-upload-result nil
          :loading? true)))

(reg-event-db
 :thm.account/avatar-upload-finished
 (fn [db [_ error-message]]
   (-> db
       (update :thm.account/avatar-update-timestamps assoc (:username db) (now-timestamp))
       (assoc
        :thm.account/avatar-upload-result error-message
        :loading? false))))

(reg-event-db
 :thms.account/avatar-changed
 (fn [db [_ username]]
   (update db :thm.account/avatar-update-timestamps assoc username (now-timestamp))))

;; == Handlers common for all rooms

(defn game-active? [game]
  (contains? #{"open" "running"} (:status game)))

(defn prepare-initial-client-states [client-states]
  (into {} (map (fn [[r cs]]
                  [r (game-client/prepare-initial-state cs)])
                client-states)))

(def DEFAULT-UI-LAYOUT "compact")

(defn ui-layout-from-room-data [more]
  (if-let [ui-layout (get (:ui-modes more) (get-user-agent-string))]
    ui-layout
    (if (on-mobile-browser?)
      "compact"
      "classic")))

(reg-event-db
 :thms.rooms/you-enter-room
 (fn [db [_ {:keys [room chat-messages usernames more]}]]
   (let [x (assoc db
                  :page-loading? false
                  :room room
                  :chat-messages chat-messages
                  :usernames (apply sorted-set-by username-comparator usernames))]
     (case (:type room)
       "lobby" (assoc x
                      :thm.lobby/open-game-by-id (into (sorted-map-by game-comparator)
                                                       (:open-game-by-id more))
                      :thm.lobby/running-game-by-id (into (sorted-map-by game-comparator)
                                                          (:running-game-by-id more))
                      :thm.lobby/game-settings-on-last-creation (:game-settings-on-last-creation more))
       "view-game" (-> x
                       (assoc :thm.view-game/game (:game more))
                       (assoc :thm.view-game/controls (:controls more)))
       "home" (let [groups (group-by #(game-active? (second %))
                                     (:your-game-by-id more))]
                (assoc x
                       :thm.home/active-game-by-id (into (sorted-map-by game-comparator)
                                                         (get groups true))
                       :thm.home/past-game-by-id (into (sorted-map-by game-comparator)
                                                       (get groups false))))
       "play-game" (assoc x
                          :thm.play-game/game (:game more)
                          :thm.game-ui/preview {}
                          :thm.play-game/active-gids (vec (:active-gids more))
                          :thm.game-ui/layout (ui-layout-from-room-data more)

                          :thm.game-ui/your-role (:your-role more)
                          ;; XXX duplication with :thms.play/initial-state
                          :thm.game-ui/client-states (cond-> (:client-states more)
                                                       true prepare-initial-client-states
                                                       true (update (:your-role more) assoc :recent (:recent-changes more))
                                                       ;; XXX it's a hack to add outcome at this stage: better to inform
                                                       ;; the engine when game is abandoned, so it updates the outcome and
                                                       ;; sends it translated naturally.  We need outcome in the
                                                       ;; translated state because in ui, we rely on it to forbid
                                                       ;; widget activation.
                                                       (:outcome (:game more)) (map-values-inverted-args
                                                                                #(assoc %2 :outcome (:outcome (:game more)))))
                          :thm.play-game/personal-notes-content (or (:personal-notes-content more) ""))

       "test-ui" (assoc x
                        :thm.game-ui/layout (ui-layout-from-room-data more)
                        :thm.test-ui/scenario-names (:scenario-names more)
                        :thm.game-ui/client-states (prepare-initial-client-states (:client-states more))
                        :thm.game-ui/your-role (initial-your-role (:client-states more)))
       "sandbox" (-> x
                     (assoc
                      :thm.game-ui/layout (ui-layout-from-room-data more)
                      :thm.game-ui/client-states (prepare-initial-client-states (:client-states more))
                      :thm.game-ui/your-role (initial-your-role (:client-states more)))
                     (show-modal :sandbox-invitation))
       "confirm" (assoc x
                        :thm.signup/confirmation-code-valid? (:confirmation-code-valid? more))

       "reset" (assoc x
                      :thm.reset-password/code-valid? (:code-valid? more))

       "account" (do
                   (set-initial-account-settings! (:account-settings more))
                   (assoc x
                          :thm.account/settings-applied? false))

       "term" (assoc x
                     :thm.term/cs (:cs more))

       x))))

(reg-event-db
 :thms.rooms/user-enters
 (fn [db [_ {:keys [username]}]]
   (update db :usernames conj username)))

(reg-event-db
 :thms.rooms/user-leaves
 (fn [db [_ {:keys [username]}]]
   (update db :usernames disj username)))

;; XXX duplication, but not exact, with clj/thm/emails.clj
(defn build-notification [tag game]
  (case tag
    :join [(str "User joins your game")
           (str "User "
                (second (:players game)) ;; XXX for games with more players, should get the joined name correctly
                " joins your game " (short-game-name game) ". "
                "It is now ready to start.")] ;; XXX for games with more players, it isn't always the case
    :part [(str "User parts your game")
           (str "A user parts your game " (short-game-name game) ". "
                "It can no longer be started.")]
    :started ["Game started"
              (str "Your game " (short-game-name game) " has started.")]
    :your-turn ["Your turn"
                (str "It's your turn in the game " (short-game-name game) ".")]

    ["Unknown notification"
     (str "Trying to send an unknown notification: " tag ". "
          "Please report it.")]))

(defn notify [tag game]
  (let [do-notify (fn []
                    (let [[title body] (build-notification tag game)
                          notification (js/Notification. title
                                        #js{:body body
                                            :icon "/favicon.ico"})]
                      (aset notification "onclick"
                            (fn []
                              (this-as this
                                (.close this))
                              (js/window.focus)))))]
    (when (and js/window.Notification
               (not= (aget js/Notification "permission") "denied")
               (not (.hasFocus js/document)))
      (if (= (aget js/Notification "permission") "granted")
        (do-notify)
        (js/Notification.requestPermission (fn [permission]
                                             (if (= permission "granted")
                                               (do-notify))))))))

(reg-event-db
 :thms.rooms/new-chat-message
 (fn [db [_ chat-message]]
   (cond-> db
     true
     (update :chat-messages conj chat-message)

     (and (compact-ui-layout? db)
          (not (= (:thm.game-ui.compact/active-pane db) "chat")))
     (assoc :thm.game-ui.compact/new-chat-message-alert? true))))

;; == Global handlers

(reg-event-db
 :thm.global/toggle-navbar
 (fn [db [_]]
   (let [now? (:thm.global/navbar-open? db)]
     (assoc db
            :thm.global/navbar-open? (not now?)))))

;; == Lobby handlers

(reg-event-db
 :thm.lobby/activate-tab
 (fn [db [_ tab-name]]
   (assoc db :thm.lobby/active-tab tab-name)))

(reg-event-db
 :thm.lobby/submit-new-game-form
 (fn [db [_ form-data]]
   (sente-send db [:thmc.lobby/submit-new-game-form form-data])
   (assoc db :loading? true)))

(reg-event-db
 :thms.lobby/create-game-ok
 (fn [db [_ {:keys [gid]}]]
   (accountant/navigate! (str "/game/" gid))
   (assoc db :loading? false)))

(reg-event-db
 :thms.lobby/create-game-error
 (fn [db [_ {:keys [message]}]]
   (assoc db
          :loading? false
          :thm.lobby/create-game-error message)))

(reg-event-db
 :thms.lobby/game-created
 (fn [db [_ {:keys [game]}]]
   (case (-> db :room :type)
     "home"
     (update db :thm.home/active-game-by-id assoc (:gid game) game)

     ;; TODO report it in the chat.
     "lobby"
     (update db :thm.lobby/open-game-by-id assoc (:gid game) game)

     db)))

;; == View-game handlers

(reg-event-db
 :thm.view-game/game-control
 (fn [db [_ control]]
   (sente-send db [:thmc.view-game/game-control control])
   (assoc db :loading? true)))

(reg-event-db
 :thms/game-changed
 (fn [db [_ {:keys [game indices-update]}]]
   (let [gid (:gid game)]
     (case (-> db :room :type)
       "home"
       (cond
         (not (game-includes-player? game (:username db)))
         (update db :thm.home/active-game-by-id dissoc gid)

         (game-active? game)
         (update db :thm.home/active-game-by-id assoc gid game)

         (contains? (:thm.home/active-game-by-id db) gid)
         (-> db
             (update :thm.home/active-game-by-id dissoc gid)
             (update :thm.home/past-game-by-id assoc gid game))

         :else
         (update db :thm.home/past-game-by-id assoc gid game))

       "lobby"
       (case indices-update
         :assoc-in-open (update db :thm.lobby/open-game-by-id
                                assoc gid game)
         :dissoc-in-open (update db :thm.lobby/open-game-by-id
                                 dissoc gid)
         :open->running (-> db
                            (update :thm.lobby/open-game-by-id
                                    dissoc gid)
                            (update :thm.lobby/running-game-by-id
                                    assoc gid game))
         :assoc-in-running (update db :thm.lobby/running-game-by-id
                                   assoc gid game)
         :dissoc-in-running (update db :thm.lobby/running-game-by-id
                                    dissoc gid)
         :dissoc-in-open&running (-> db
                                     (update :thm.lobby/open-game-by-id
                                             dissoc gid)
                                     (update :thm.lobby/running-game-by-id
                                             dissoc gid))
         :ignore db)

       db))))

(reg-event-db
 :thms/game-changed-on-view-game
 (fn [db [_ {:keys [game tag controls]}]]
   (case tag
     :join (when (= (:username db) (:owner-username game)) (notify :join game))
     :part (when (= (:username db) (:owner-username game)) (notify :part game))
     :start (do
              (notify :started (:thm.view-game/game db))
              (accountant/navigate! (str "/play/" (:gid game))))
     nil)
   (assoc db
          :loading? false
          :thm.view-game/game game
          :thm.view-game/controls controls)))

;; == Play page handlers

(reg-event-db
 :thms.play/deltas
 (fn [db [_ {:keys [deltas]}]]
   (let [your-role (:thm.game-ui/your-role db)
         new-db (update-in db [:thm.game-ui/client-states your-role] game-client/deltas-arrived deltas)
         new-cs (get-in new-db [:thm.game-ui/client-states your-role])]
     (when (or (has-your-request? new-cs)
               (:paused? new-cs)) ;; XXX probably wrong, because a second
                                  ;; notification will be displayed when the
                                  ;; other player clicks through to the next
                                  ;; request, while this player hasn't unpaused
                                  ;; yet.  The better way would be to trigger
                                  ;; notification only in game-client, in
                                  ;; deltas-arrived.
       (notify :your-turn (:thm.play-game/game db)))
     new-db)))

;; == Test TD UI page handlers

(reg-event-db
 :thms.test-ui/deltas
 (fn [db [_ tdpr]]
   (let [res (reduce (fn [cur-db [r deltas]]
                       (update-in cur-db [:thm.game-ui/client-states r] game-client/deltas-arrived deltas))
                     db
                     tdpr)]
     (when-let [r (the-only-role-that-needs-attention res)]
       (dispatch [:thm.test-ui/switch-role r]))
     res)))

(reg-event-db
 :thm.test-ui/switch-scenario
 (fn [db [_ index]]
   (sente-send db [:thmc.test-ui/switch-scenario index])
   (assoc db :thm.game-ui/play-menu-open? false, :thm.game-ui.test/scenario-index index)))

(reg-event-db
 :thm.test-ui/set-display-all-client-states-on-one-page
 (fn [db [_ value]]
   (assoc db :thm.test-ui/display-all-client-states-on-one-page value)))

(reg-event-db
 :thm.test-ui/redo-current-scenario
 (fn [db [_]]
   (let [index (or (get db :thm.game-ui.test/scenario-index)
                   ;; XXX duplication with (peek scenarios) in
                   ;; worker/create-test-ui-room-state.
                   (dec (count (get db :thm.test-ui/scenario-names))))]
     (sente-send db [:thmc.test-ui/switch-scenario index])
     (assoc db :thm.game-ui/play-menu-open? false))))

(reg-event-db
 :thms.test-ui/initial-state
 (fn [db [_ client-states]]
   (assoc db
          :thm.game-ui/client-states (prepare-initial-client-states client-states)
          :thm.game-ui/your-role (initial-your-role client-states))))

(reg-event-db
 :thm.test-ui/switch-role
 (fn [db [_ role]]
   (if (:thm.test-ui/display-all-client-states-on-one-page db)
     db
     ;; The client states are in different tabs, and updating the size only works
     ;; when the tab is visible; so issue a resize once the tab is visible.
     (do
       (r/next-tick #(dispatch [:do-resize]))
       (assoc db
              :thm.game-ui/your-role role
              :thm.game-ui/play-menu-open? false)))))

(reg-event-db
 :thm.test-ui/new-game
 (fn [db [_ role]]
   (sente-send db [:thmc.test-ui/new-game])
   db))

;; == Signup handlers

(reg-event-db
 :thm.signup/submit-signup-form
 (fn [db [_ form-data]]
   (sente-send db [:thmc.signup/submit-signup-form form-data])
   (assoc db :loading? true)))

(reg-event-db
 :thms.signup/signup-failed
 (fn [db [_ error-message]]
   (assoc db
          :loading? false
          :thm.signup/signup-error-message error-message)))

(reg-event-db
 :thms.signup/confirmation-email-sent
 (fn [db [_ _]]
   (assoc db
          :loading? false
          :thm.signup/signup-error-message nil
          :thm.signup/confirmation-email-sent? true)))

(reg-event-db
 :thm.signup/submit-confirm-registration-form
 (fn [db [_ form-data]]
   (sente-send db [:thmc.signup/submit-confirm-registration-form form-data])
   (assoc db :loading? true)))

(reg-event-db
 :thms.signup/confirm-failed
 (fn [db [_ {:keys [error-messages]}]]
   (assoc db
          :loading? false
          :thm.signup/confirm-error-messages error-messages)))

(reg-event-db
 :thms.signup/confirm-successful
 (fn [db [_ {:keys [username]}]]
   (accountant/navigate! "/lobby")
   (assoc db
          :username username
          :loading? false
          :thm.signup/confirm-error-messages nil)))

;; == Reset password handlers

(reg-event-db
 :thm.reset-password/submit-forgot-form
 (fn [db [_ form-data]]
   (sente-send db [:thmc.reset-password/submit-forgot-form form-data])
   (assoc db :loading? true)))

(reg-event-db
 :thms.reset-password/forgot-failed
 (fn [db [_ error-message]]
   (assoc db
          :loading? false
          :thm.reset-password/forgot-error-message error-message)))

(reg-event-db
 :thms.reset-password/reset-email-sent
 (fn [db [_ _]]
   (assoc db
          :loading? false
          :thm.reset-password/forgot-error-message nil
          :thm.reset-password/reset-email-sent? true)))

(reg-event-db
 :thm.reset-password/submit-reset-form
 (fn [db [_ form-data]]
   (sente-send db [:thmc.reset-password/submit-reset-form form-data])
   (assoc db :loading? true)))

(reg-event-db
 :thms.reset-password/reset-failed
 (fn [db [_ error-messages]]
   (assoc db :loading? false
          :thm.reset-password/error-messages error-messages)))

(reg-event-db
 :thms.reset-password/reset-successful
 (fn [db [_ {:keys [username]}]]
   (accountant/navigate! "/home")
   (assoc db
          :username username
          :loading? false
          :thm.reset-password/error-messages nil
          :thm.reset-password/reset-successful? true)))

;; Handlers for the terminal emulator widget

(reg-event-db
 :thm.term/on-arrow-up
 (fn [db [_ e]]
   (update db :thm.term/state
           (fn [st]
             (if (zero? (:history-index st))
               st
               (-> st
                   (update :history-index dec)
                   (assoc :input (get (:history st) (dec (:history-index st))))))))))

(reg-event-db
 :thm.term/on-arrow-down
 (fn [db [_ e]]
   (update db :thm.term/state
           (fn [st]
             (cond
               (= (:history-index st) (count (:history st)))
               st

               (= (:history-index st) (dec (count (:history st))))
               (-> st
                   (update :history-index inc)
                   (assoc :input ""))

               :else
               (-> st
                   (update :history-index inc)
                   (assoc :input (get (:history st) (inc (:history-index st))))))))))


(reg-event-db
 :thm.term/did-mount
 (fn [db [_ dom-node]]
   (update db :thm.term/state
           assoc :dom-node dom-node)))

(reg-event-db
 :thm.term/on-submit
 (fn [db [_]]
   (let [term-state (:thm.term/state db)]
     (dispatch [:thm.term/handle-input (:input term-state)]))
   (update db :thm.term/state
           #(-> %
                (assoc :input "")
                (update :history conj (:input %))
                (assoc :history-index (inc (count (:history %))))
                (update :lines conj (str (:prompt %) (:input %)))))))

(reg-event-db
 :thm.term/on-input-changed
 (fn [db [_ value]]
   (update db :thm.term/state
           assoc :input value)))

(reg-event-db
 :thm.term/add-output-line
 (fn [db [_ line]]
   (update db :thm.term/state
           update :lines conj line)))


(reg-event-db
 :thm.term/add-output-image
 (fn [db [_ line]]
   (update db :thm.term/state
           update :lines conj [:span
                               [:img {:width 150
                                      :src "/tm-images/projects/209_smaast.jpg"}]
                               [:img {:width 150
                                      :src "/tm-images/projects/1_cotrca.jpg"}]
                               ]
           )))


(def term-commands (atom {}))

(defn register-term-command [name-or-names description args f]
  (let [names (if (sequential? name-or-names)
                name-or-names
                [name-or-names])
        cmd {:names names
             :description description
             :args args
             :f f}]
    (doseq [name names]
      (swap! term-commands assoc name cmd))))

(defn term-output [line]
  (dispatch [:thm.term/add-output-line line]))

(register-term-command
 "help"
 "display available commands"
 "(n/a)"
 (fn [& args]
   (doseq [name (sort (keys @term-commands))
         :let [cmd (get @term-commands name)]]
     (term-output (str name " " (:args cmd) ": " (:description cmd))))))

(register-term-command
 ["exit" "quit"]
 "return to site"
 "(n/a)"
 (fn [& args]
   (accountant/navigate! "/home")))

(register-term-command
 "clear"
 "clear terminal"
 "(n/a)"
 (fn [& args]
   (dispatch [:thm.term/clear])))

(register-term-command
 "image"
 "display test image"
 "(n/a)"
 (fn [& args]
   (dispatch [:thm.term/add-output-image]
   )))

(register-term-command
 "new"
 "start a new tm game"
 "[seed]"
 (fn [& [seed]]
   (let [seed (to-integer seed)]
     (dispatch [:thm.term/new-tm-game {:seed seed}]))))

(reg-event-db
 :thm.term/new-tm-game
 (fn [db [_ options]]
   (sente-send db [:thmc.term/new-tm-game options])
   db))

(reg-event-db
 :thm.term/clear
 (fn [db [_]]
   (update db :thm.term/state
           update :lines empty)))

(reg-event-db
 :thm.term/handle-input
 (fn [db [_ input]]
   (let [parts (-> input
                   (str/trim)
                   (str/split #"\s+"))
         [name & args] parts]
     (if-let [cmd (get @term-commands name)]
       (apply (:f cmd) args)
       (term-output (str "Unknown command: `" name "'.  Type `help' to see commands."))))
   db))

(reg-event-db
 :thms.term/new-tm-game-started
 (fn [db [_ options]]
   (term-output (:str-rep options))

   (assoc db :thm.term/cs (:cs options))))

(register-term-command
 "v"
 "view current game"
 "(n/a)"
 (fn [& any]
   (dispatch [:thm.term/get-engine-str-rep])))

(reg-event-db
 :thm.term/get-engine-str-rep
 (fn [db [_]]
   (sente-send db [:thmc.term/get-engine-str-rep {}])
   db))

(reg-event-db
 :thms.term/answering-get-engine-rep-str
 (fn [db [_ options]]
   (term-output (:str-rep options))
   db))

(register-term-command
 "h"
 "view hand as images"
 "(n/a)"
 (fn [& any]
   (dispatch [:thm.term/view-hand-as-images])))

(defn display-row-of-tm-card-images [db cids]
  (let [structure (into [:span]
                        (map (fn [cid]
                               [:img {:width 150
                                      :src (str "/tm-images/projects/" cid ".jpg")}])
                             cids))
        ]
    (update db :thm.term/state
            update :lines conj structure)))

(reg-event-db
 :thm.term/view-hand-as-images
 (fn [db [_]]
   (let [cs (:thm.term/cs db)
         cids (map (comp denumber-id :cid) (all-cards-in-zone cs "s:hand"))]
     (display-row-of-tm-card-images db cids))))

(register-term-command
 "c"
 "view specified card images"
 "card, card, ..."
 (fn [& cids]
   (dispatch [:thm.term/view-specified-tm-card-images cids])))


(reg-event-db
 :thm.term/view-specified-tm-card-images
 (fn [db [_ cids]]
   (display-row-of-tm-card-images db cids)))

(register-term-command
 "a"
 "answer current request"
 "(depends on the request)"
 (fn [& args]
   (dispatch [:thm.term/answer-current-request args])))

(defn massage-answer [cs input]
  (let [main (first input)]
    (if (= main "*cancel*")
      main
      (let [[kind text-descr & args :as req] (get-request-for-role cs "s")
            match (fn [input list-of-strings]
                    (find-first #(re-find (re-pattern (str "^" input)) %)
                                list-of-strings))]
        (case kind
          :choose-card
          (match (str main)
                 (apply concat (vals (first args))))

          :choose-cards-flexible
          (let [[request-zone-name cids options] args
                cids (if (:all-in-zone? options)
                       (map :cid (all-cards-in-zone cs request-zone-name))
                       cids)]
            (mapv #(match % cids) input))

          :choose-thing
          (match (str main)
                      (map :tid (first args)))

          :choose-mode
          (match main (first args))

          :choose-payment-alternatives
          (mapv to-integer input)

          :choose-number
          (to-integer main)

          :choose-number-from-list
          (to-integer main)

          :yn
          (if (re-find #"^y" main)
            true
            false)

          (str main)
          )))))

(reg-event-db
 :thm.term/answer-current-request
 (fn [db [_ args]]
   (let [cs (:thm.term/cs db)
         client-answer (massage-answer cs args)]
     (sente-send db [:thmc.term/put-answer {:answer client-answer}])

     db)))

(reg-event-db
 :thms.term/after-put-answer
 (fn [db [_ options]]
   (let [deltas (get (:tdpr options) "s")
         deltas-str (clojure.string/join "\n" (map str deltas))]
     (term-output deltas-str))
   (term-output "===")
   (term-output (:str-rep options))

   (assoc db :thm.term/cs (:cs options))))

(reg-event-db
 :thm.proxier/switch-card
 (fn [db [_ change]]
   (let [index (get-in db [:thm.proxier/state :card-index] 0)
         new-index (mod (+ index change) (count thm.tm.tm-data/project-cards))]
     (assoc-in db [:thm.proxier/state :card-index] new-index))))
