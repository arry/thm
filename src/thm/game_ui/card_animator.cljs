(ns thm.game-ui.card-animator
  (:require [taoensso.timbre :refer-macros (debug warnf warn)]
            [reagent.core :as r]
            [clojure.string :as str]
            [thm.utils :refer [multipart-name-from-descr str-join-non-nil]]
            ))

"
Animate movement of card from zone to zone.  The technique used is FLIP, the
same as in flip-move
(https://github.com/joshwcomeau/react-flip-move).  While flip-move animates the
nodes under the single parent, this must work with a node moving from one
parent to another.

This is slightly hacky beause it bypasses the re-frame framework and instead
relies on simple callbacks from, on one hand, game_client which knows which
zones are about to update, and on the other, reagent components which represent
the actual zones and hold the DOM nodes to be animated.
"

(defn jsdebug [& args]
  (apply js/console.debug args))

(def state-per-role (atom {}))

(defn delta-application-started! [role]
  (swap! state-per-role update role
         #(-> %
             (assoc :cid->rect {})
             (assoc :should-animate {}))))

(defn zone-component-did-mount [role zone-descr reagent-component zone-mode]
  (swap! state-per-role update role
         update :zones
         assoc (multipart-name-from-descr zone-descr) {:rc reagent-component
                                                  :mode zone-mode}))

(defn space-component-did-mount [role space-descr reagent-component]
  (swap! state-per-role update role
         update :spaces
         assoc (multipart-name-from-descr space-descr) {:rc reagent-component}))

(defn zone-component-will-unmount [role zone-descr]
  (swap! state-per-role update role
         update :zones
         dissoc (multipart-name-from-descr zone-descr)))

(defn space-component-will-unmount [role space-descr]
  (swap! state-per-role update role
         update :spaces
         dissoc (multipart-name-from-descr space-descr)))

(defn get-zone-dom-node [role zone-descr]
  (let [rc (get-in @state-per-role [role :zones (multipart-name-from-descr zone-descr) :rc])]
    (when (and (some? rc)
               (some? (r/dom-node rc)))
      (r/dom-node rc))))

(defn get-space-dom-node [role space-descr]
  (let [rc (get-in @state-per-role [role :spaces (multipart-name-from-descr space-descr) :rc])]
    (when (and (some? rc)
               (some? (r/dom-node rc)))
      (r/dom-node rc))))

(defn get-node-rect [node]
  (when (some? node)
    (.getBoundingClientRect node)))

(defn animate-rect->rect [node old-rect new-rect fade-mode]
  (let [dx (- (.-x old-rect) (.-x new-rect))
        dy (- (.-y old-rect) (.-y new-rect))

        new-width (.-width new-rect)

        scale (if (= new-width 0)
                1
                (/ (.-width old-rect) new-width))

        need-translate? (not (and (zero? dx) (zero? dy)))
        need-scale? (not= scale 1)

        initial-opacity (case fade-mode
                          :appearing 0
                          :disappearing 1
                          nil)
        target-opacity (case fade-mode
                         :appearing 1
                         :disappearing 0
                         nil)]
    (when (or need-translate? need-scale?)
      (jsdebug "animating node rect->rect" node dx dy scale fade-mode)
      (aset (.-style node)
            "transform"
            (str-join-non-nil " " [(when need-translate?
                                     (str "translate(" dx "px," dy "px)"))
                                   (when need-scale?
                                     (str "scale(" scale ")"))]))
      (aset (.-style node)
            "transition" "")

      (when fade-mode
        (aset (.-style node)
              "opacity" initial-opacity))

      (r/next-tick (fn []
                     (aset (.-style node)
                           "transform" "")
                     (when fade-mode
                       (aset (.-style node)
                             "opacity" target-opacity))
                     (aset (.-style node)
                           "transition" (str-join-non-nil "," ["transform 0.5s"
                                                               (when fade-mode
                                                                 "opacity 0.5s")])))))))

(defn map-children-of-node [node f]
  (let [children (.-children node)]
    (map (fn [i]
           (f i (aget children i)))
         (range (.-length children)))))

(defn iterate-children-of-node [node f]
  (let [children (.-children node)]
    (doseq [i (range (.-length children))]
      (f i (aget children i)))))


(defn get-original-rects-by-cids [role zone-name cids all-cards]
  (let [result
        (when-let [zone-node (get-zone-dom-node role zone-name)]
          (into {} (map-children-of-node zone-node (fn [i child-node]
                                                     (let [card (get all-cards i)]
                                                       [(:cid card) (get-node-rect child-node)])))))]
    result))

(defn get-original-rects-by-figure-ids [role space-name all-figure-ids]
  (let [result
        (when-let [zone-node (get-space-dom-node role space-name)]
          (into {} (map-children-of-node zone-node (fn [i child-node]
                                                     (let [figure-id (get all-figure-ids i)]
                                                       [figure-id (get-node-rect child-node)])))))]
    result))

(defn get-original-rects-by-sequence [role zone-name cids]
  (when-let [zone-node (get-zone-dom-node role zone-name)]
    (into {} (map-children-of-node zone-node (fn [i child-node]
                                               [(get cids i) (get-node-rect child-node)])))))

(defn get-original-rects-by-zone [role zone-name cids]
  (when-let [zone-node (get-zone-dom-node role zone-name)]
    (into {} (map (fn [cid]
                    [cid (get-node-rect zone-node)])
                  cids))))

(defn get-original-rects-by-space [role space-name cids]
  (when-let [node (get-space-dom-node role space-name)]
    (into {} (map (fn [cid]
                    [cid (get-node-rect node)])
                  cids))))

(defn perform-animation [role zone-name animation-info new-cards]
  (when-let [node (get-zone-dom-node role zone-name)]
    (let [alternate-cids (:alternate-cids animation-info)
          set-of-cids (set (:animate-cids animation-info))
          fade-mode (:fade-mode animation-info)
          should-animate? (fn [cid]
                            (if (:animate-the-remainder-in-place? animation-info)
                              true
                              (contains? set-of-cids cid)))]
      (iterate-children-of-node node (fn [i child-node]
                                       (let [cid (if alternate-cids
                                                   (get alternate-cids i)
                                                   (:cid (get new-cards i)))]
                                         (when (should-animate? cid)
                                           (let [new-rect (get-node-rect child-node)
                                                 old-rect (get-in animation-info [:cid->old-rect cid])]
                                             (when (and new-rect old-rect)
                                               (animate-rect->rect child-node old-rect new-rect fade-mode))))))))))

(defn perform-animation-for-space-with-figures [role space-name animation-info new-figures]
  (when-let [node (get-space-dom-node role space-name)]
    (let [set-of-figure-ids (set (:animate-figure-ids animation-info))
          should-animate? (fn [figure-id]
                            (if (:animate-the-remainder-in-place? animation-info)
                              true
                              (contains? set-of-figure-ids figure-id)))]
      (iterate-children-of-node node (fn [i child-node]
                                       (let [figure-id (get new-figures i)]
                                         (when (should-animate? figure-id)
                                           (let [new-rect (get-node-rect child-node)
                                                 old-rect (get-in animation-info [:figure-id->old-rect figure-id])]
                                             (when (and new-rect old-rect)
                                               (animate-rect->rect child-node old-rect new-rect
                                                                   (:fade-mode animation-info)))))))))))
