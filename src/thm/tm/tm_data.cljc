(ns thm.tm.tm-data
  (:require [thm.utils :refer [shorten-title map-cross]])
  )

(def corp-cards
  [{:title "Tharsis Republic"
    :category ""
    :money 40
    :tags "b"
    :first-action [:place-city],
    :first "As your first action in the game, place a city tile."
    :b-triggered [{:t :anyone-places-city-on-mars, :a [:inc-prod "m" 1]}
                  {:t :you-place-city, :a [:gain 3 "m"]}]
    :second "Effect: When any city tile is placed ON MARS, increase your ME production 1 step. When you place a city tile, gain 3 ME."}
   {:title "Thorgate"
    :category ""
    :money 48
    :tags "n"
    :first "You start with 1 energy production."
    :b-discount [:energy 3]
    :second "Effect: When playing a power card OR THE STANDARD PROJECT POWER PLANT, you pay 3 ME less for it."}
   {:title "Phobolog"
    :category ""
    :money 23
    :tags "t"
    :first "You start with 10 titanium."
    :b-special :titanium-worth-1-more
    :second "Effect: Your titanium resources are each worth 1 ME extra."}
   {:title "Saturn Systems"
    :category "a"
    :money 42
    :tags "j"
    :first "You start with 1 titanium production."
    :b-triggered {:t :anyone-puts-jovian-tag-in-play, :a [:inc-prod "m" 1]}
    :second "Effect: Each time any Jovian tag is put into play, including this, increase your ME production 1 step."}
   {:title "United Nations Mars Initative"
    :category ""
    :money 40
    :tags "e"
    :first "-"
    :b-action [:unmi]
    :second "Action: If your Terraform Rating was raised this generation, you may pay 3 ME to raise it 1 step more."}
   {:title "Teractor"
    :category "a"
    :money 60
    :tags "e"
    :first "-"
    :b-discount [:earth 3]
    :second "Effect: When playing an Earth card, you pay 3 ME less for it."}
   {:title "Mining Guild"
    :category ""
    :money 30
    :tags "bb"
    :imm [:inc-prod "s" 1]
    :first "You start with 5 steel and 1 steel production."
    :b-triggered {:t :you-get-metal-placement-bonus, :a [:inc-prod "s" 1]}
    :second "Effect: Each time you get any steel or titanium as a placement bonus on the map, increase your steel production 1 step."}
   {:title "Helion"
    :category ""
    :money 42
    :tags "t"
    :first "You start with 3 heat production."
    :b-special :use-heat-as-me
    :second "Effect: You may use heat as ME. You may not use ME as heat."}
   {:title "Interplanetary Cinematics"
    :category ""
    :money 30
    :tags "b"
    :first "You start with 20 steel."
    :b-triggered {:t :you-play-event, :a [:gain 2 "m"]}
    :second "Effect: Each time you play an event, you gain 2 ME."}
   {:title "Inventrix"
    :category ""
    :money 45
    :tags "s"
    :first-action [:draw-cards 3],
    :first "As your first action in the game, draw 3 cards."
    :b-special :tweak-global-requirements
    :second "Effect: Your temperature, oxygen, and ocean requirements are +2 or -2 steps, your choice in each case."}
   {:title "Credicor"
    :category ""
    :money 57
    :tags "-"
    :first "-"
    :b-triggered {:t :you-pay-20+, :a [:gain 4 "m"]}
    :second "Effect: After you pay for a card or standard project with a basic cost of 20 ME or more, you gain 4 ME."}
   {:title "Ecoline"
    :category ""
    :money 36
    :tags "p"
    :first "You start with 2 plant production and 3 plants."
    :b-special :cheaper-standard-greenery
    :second "Effect: You may always pay 7 plants, instead of 8, to place 1 greenery."}
   #_{:title "Beginner Corporation"
    :category ""
    :money 42
    :tags "-"
    :first "Instead of participating in the first research phase, draw 10 cards for free."
    :second "-"}])

(def project-cards
  [{:vp 2,
    :category "",
    :tags "jb",
    :number 1,
    :type "g",
    :title "Colonizer Training Camp",
    :req "ox5",
    :cost 8,
    :effect "2 vp"}
   {:category "a",
    :tags "j",
    :number 2,
    :type "g",
    :title "Asteroid Mining Consortium",
    :imm [:multi [:dec-any-prod "t" 1] [:inc-prod "t" 1]],
    :req "T",
    :vp 1,
    :cost 13,
    :effect "Decrease any titanium production 1 step and increase your own 1 step. 1 vp."}
   {:imm [:multi [:inc-prod "e" 1] [:raise-temperature 1]],
    :category "",
    :tags "nb",
    :number 3,
    :type "g",
    :title "Deep Well Heating",
    :cost 13,
    :effect "Increase your energy production 1 step. Increase temperature 1 step."}
   {:imm
    [:multi [:dec-prod "m" 1] [:dec-any-prod "h" 1] [:inc-prod "p" 2]],
    :number 4,
    :title "Cloud Seeding",
    :cost 11,
    :req "w3",
    :tags "-"
    :type "g",
    :category "",
    :effect "Decrease your ME production 1 step and any heat production 1 step. Increase your plant production 2 steps."}
   {:category "",
    :tags "s",
    :number 5,
    :type "b",
    :title "Search for Life",
    :max-vp 3
    :vp-formula [3 1 :this-resources],
    :b-action [:multi [:spend 1 "m"] [:special-sefoli]],
    :req "ox6",
    :blue
    "Action: Spend 1 ME to reveal the top card of the draw deck.  If that card has a microbe tag, add a science resource here.",
    :cost 3,
    :effect "3 VPs if you have one or more science resources here."}
   {:category "a",
    :tags "s",
    :number 6,
    :type "b",
    :title "Inventors' Guild",
    :b-action [:look-buy-or-discard],
    :blue
    "Action: Look at the top card and either buy it or discard it.",
    :cost 9,
    :effect "-"}
   {:category "",
    :tags "b",
    :number 7,
    :type "b",
    :title "Martian Rails",
    :b-action [:multi [:spend 1 "e"] [:gain-formula "m" [1 1 :city-on-mars]]],
    :blue
    "Action: Spend 1 energy to gain 1 ME for each city tile ON MARS.",
    :cost 13,
    :effect "-"}
   {:category "",
    :tags "cb",
    :number 8,
    :type "g",
    :title "Capital",
    :vp-formula [1 1 :adjacent-ocean],
    :imm
    [:multi
     [:dec-prod "e" 2]
     [:inc-prod "m" 5]
     [:place "capital" :like-city]],
    :req "w4",
    :cost 26,
    :effect "Place this [capital] tile. Decrease your energy production 2 steps and increase your ME production 5 steps. 1 ADDITIONAL VP FOR EACH OCEAN TILE ADJACENT TO THIS CITY TILE."}
   {:imm
    [:multi [:raise-temperature 1] [:gain 2 "t"] [:remove-any 3 "p"]],
    :number 9,
    :title "Asteroid",
    :cost 14,
    :tags "t1",
    :type "r",
    :category "",
    :effect "Raise temperature 1 step and gain 2 titanium. Remove up to 3 plants from any player."}
   {:imm
    [:multi [:raise-temperature 1] [:place-ocean] [:remove-any 3 "p"]],
    :number 10,
    :title "Comet",
    :cost 21,
    :tags "t1",
    :type "r",
    :category "",
    :effect "Raise temperature 1 step and place an ocean tile. Remove up to 3 plants from any player."}
   {:imm
    [:multi [:raise-temperature 2] [:gain 4 "t"] [:remove-any 4 "p"]],
    :number 11,
    :title "Big Asteroid",
    :cost 27,
    :tags "t1",
    :type "r",
    :category "",
    :effect "Raise temperature 2 steps and gain 4 titanium. Remove up to 4 plants from any player."}
   {:category "",
    :tags "jt",
    :number 12,
    :type "b",
    :title "Water Import from Europa",
    :b-action [:multi [:spend-with-titanium 12 "m"] [:place-ocean]],
    :blue
    "Action: Pay 12 ME to place an ocean tile.  TITANIUM MAY BE USED as if playing a space card.",
    :cost 25,
    :effect "1 vp for each Jovian tag you have."}
   {:category "a",
    :tags "tb",
    :number 13,
    :type "b",
    :title "Space Elevator",
    :b-action [:multi [:spend 1 "s"] [:gain 5 "m"]],
    :imm [:inc-prod "t" 1],
    :vp 2,
    :blue "Action: Spend 1 steel to gain 5 ME.",
    :cost 27,
    :effect "Increase your titanium production 1 step. 2 vp."}
   {:category "a",
    :tags "sb",
    :number 14,
    :type "b",
    :title "Development Center",
    :b-action [:multi [:spend 1 "e"] [:draw-cards 1]],
    :blue "Action: Spend 1 energy to draw a card.",
    :cost 11,
    :effect "-"}
   {:category "",
    :tags "b",
    :number 15,
    :type "b",
    :title "Equatorial Magnetizer",
    :b-action [:multi [:dec-prod "e" 1] [:raise-tr 1]],
    :blue
    "Action: Decrease your energy production 1 step to increase your terraform rating 1 step.",
    :cost 11,
    :effect "-"}
   {:category "",
    :tags "cb",
    :number 16,
    :type "g",
    :title "Domed Crater",
    :imm
    [:multi
     [:gain 3 "p"]
     [:dec-prod "e" 1]
     [:inc-prod "m" 3]
     [:place-city]],
    :req "ox7",
    :vp 1,
    :cost 24,
    :effect "Gain 3 plants and place a city tile. Decrease your energy production 1 step and increase ME production 3 steps. 1 vp."}
   {:imm
    [:multi
     [:dec-prod "e" 1]
     [:inc-prod "m" 3]
     [:place-named-city :noctis]],
    :number 17,
    :title "Noctis City",
    :cost 18,
    :tags "cb",
    :type "g",
    :category "",
    :effect "Decrease your energy production 1 step and increase your ME production 3 steps.  Place a city tile ON THE RESERVED AREA, disregarding normal placement restrictions."}
   {:category "",
    :tags "jt",
    :number 18,
    :type "g",
    :title "Methane from Titan",
    :imm [:multi [:inc-prod "h" 2] [:inc-prod "p" 2]],
    :req "o2",
    :vp 2,
    :cost 28,
    :effect "Increase your heat production 2 steps and your plant production 2 steps.  2 VP."}
   {:imm
    [:multi
     [:place-ocean]
     [:choose
      {:choice-tag :gain-3-p, :imm [:gain 3 "p"]}
      {:choice-tag :add-another-microbes-3, :imm [:add-another "microbes" 3]}
      {:choice-tag :add-another-animals-2, :imm [:add-another "animals" 2]}]],
    :number 19,
    :title "Imported Hydrogen",
    :cost 16,
    :tags "et1",
    :type "r",
    :category "",
    :effect "Gain 3 plants, or add 3 microbes or 2 animals to ANOTHER card. Place an ocean tile."}
   {:category "",
    :tags "scb",
    :number 20,
    :b-discount [:any-card 1],
    :type "b",
    :title "Research Outpost",
    :imm [:place-city :not-adjacent-to-any],
    :blue "Effect: When you play a card, you pay 1 ME less for it.",
    :cost 18,
    :effect "Place a city tile NEXT TO NO OTHER TILE."}
   {:category "",
    :tags "tc",
    :number 21,
    :type "g",
    :title "Phobos Space Haven",
    :imm [:multi [:inc-prod "t" 1] [:place-named-city :phobos]],
    :vp 3,
    :cost 25,
    :effect "Increase your titanium production 1 step and place a city tile ON THE RESERVED AREA.  3 VP."}
   {:imm [:multi [:place-ocean] [:dec-prod "m" 2] [:inc-prod "h" 3]],
    :number 22,
    :title "Black Polar Dust",
    :cost 15,
    :tags "-",
    :type "g",
    :category "",
    :effect "Place an ocean tile. Decrease your ME production 2 steps and increase your heat production 3 steps."}
   {:category "",
    :tags "p",
    :b-triggered {:t :anyone-places-ocean, :a [:gain 2 "p"]},
    :number 23,
    :type "b",
    :title "Arctic Algae",
    :imm [:gain 1 "p"],
    :req "tx-12",
    :blue "Effect: When anyone places an ocean tile, gain 2 plants.",
    :cost 12,
    :effect "Gain 1 plant."}
   {:category "",
    :tags "a",
    :number 24,
    :type "b",
    :title "Predators",
    :b-action [:multi [:remove-another 1 "animals"] [:add-this "animals" 1]],
    :req "o11",
    :vp-formula [1 1 :this-resources],
    :blue
    "Action: Remove 1 animal from any card and add it to this card.",
    :cost 14,
    :effect "1 VP per animal on this card."}
   {:category "",
    :tags "t",
    :number 25,
    :b-discount [:space 2],
    :type "b",
    :title "Space Station",
    :vp 1,
    :blue
    "Effect: When you play a space card, you pay 2 ME less for it.",
    :cost 10,
    :effect "1 VP."}
   {:category "",
    :tags "pb",
    :number 26,
    :type "g",
    :title "Eos Chasma National Park",
    :imm
    [:multi [:add-another "animals" 1] [:gain 3 "p"] [:inc-prod "m" 2]],
    :req "t-12",
    :vp 1,
    :cost 16,
    :effect "Add 1 animal TO ANY ANIMAL CARD.  Gain 3 plants.  Increase your ME production 2 steps.  1 VP."}
   {:vp 4,
    :number 27,
    :title "Interstellar Colony Ship",
    :cost 24,
    :req "s5",
    :tags "et1",
    :type "r",
    :category "",
    :effect "4 VP."}
   {:category "",
    :tags "t",
    :number 28,
    :type "b",
    :title "Security Fleet",
    :vp-formula [1 1 :this-resources],
    :b-action [:multi [:spend 1 "t"] [:add-this "fighter" 1]],
    :blue
    "Action: Spend 1 titanium to add 1 fighter resource to this card.",
    :cost 12,
    :effect "1 VP for each fighter resource on this card."}
   {:imm [:multi [:place-city] [:dec-prod "e" 1] [:inc-prod "m" 3]],
    :number 29,
    :title "Cupola City",
    :cost 16,
    :req "ox9",
    :tags "cb",
    :type "g",
    :category "",
    :effect "Place a city tile.  Decrease your enegy production 1 step and increase your ME production 3 steps."}
   {:imm [:multi [:dec-prod "m" 2] [:inc-prod "h" 2] [:inc-prod "e" 2]],
    :number 30,
    :title "Lunar Beam",
    :cost 13,
    :tags "en",
    :type "g",
    :category "",
    :effect "Decrease your ME production 2 steps and increase your heat production and energy production 2 steps each."}
   {:category "",
    :tags "t",
    :b-triggered {:t :you-play-space-event, :a [:multi [:gain 3 "m"] [:gain 3 "h"]]},
    :number 31,
    :type "b",
    :title "Optimal Aerobraking",
    :blue
    "Effect: When you play a space event, you gain 3 ME and 3 heat.",
    :cost 7,
    :effect "-"}
   {:imm [:multi [:place-city] [:dec-prod "e" 2] [:inc-prod "s" 2]],
    :number 32,
    :title "Underground City",
    :cost 18,
    :tags "cb",
    :type "g",
    :category "",
    :effect "Place a city tile. Decrease your energy production 2 steps and increase your steel production 2 steps."}
   {:category "",
    :tags "sm",
    :number 33,
    :type "b",
    :title "Regolith Eaters",
    :b-action [:choose
               {:choice-tag :add-this-microbes-1, :imm [:add-this "microbes" 1]}
               {:choice-tag :remove-2-microbes->raise-oxygen, :imm [:multi [:remove-this "microbes" 2] [:raise-oxygen 1]]}],
    :blue
    "Action: Add 1 microbe to this card, or remove 2 microbes from this card to raise oxygen level 1 step.",
    :cost 13,
    :effect "-"}
   {:category "",
    :tags "sm",
    :number 34,
    :type "b",
    :title "GHG Producing Bacteria",
    :b-action [:choose
               {:choice-tag :add-this-microbes-1, :imm [:add-this "microbes" 1]}
               {:choice-tag :remove-2-microbes->raise-temperature, :imm [:multi [:remove-this "microbes" 2] [:raise-temperature 1]]}],
    :req "o4",
    :blue
    "Action: Add 1 microbe to this card, or remove 2 microbes to raise temperature 1 step.",
    :cost 8,
    :effect "-"}
   {:category "",
    :tags "m",
    :number 35,
    :type "b",
    :title "Ants",
    :vp-formula [1 2 :this-resources],
    :b-action [:multi [:remove-another 1 "microbes"] [:add-this "microbes" 1]],
    :req "o4",
    :blue
    "Action: Remove 1 microbe from any card to add 1 to this card.",
    :cost 9,
    :effect "1 VP per 2 microbes on this card."}
   {:imm [:raise-tr 2],
    :number 36,
    :title "Release of Inert Gases",
    :cost 14,
    :tags "1",
    :type "r",
    :category "",
    :effect "Raise your teraform rating 2 steps."}
   {:imm
    [:multi
     [:raise-tr 2]
     [:raise-temperature 1]
     [:inc-prod-formula "p" [1 4 :special-barrier-at-3-plant-tags]]],
    :number 37,
    :title "Nitrogen-Rich Asteroid",
    :cost 31,
    :tags "t1",
    :type "r",
    :category "",
    :effect "Raise your terraforming rating 2 steps and temperature 1 step. Increase your plant production 1 step, or 4 step if you have 3 plant tags."}
   {:category "",
    :tags "b",
    :b-triggered {:t :anyone-places-city, :a [:gain 2 "m"]},
    :number 38,
    :type "b",
    :title "Rover Construction",
    :blue "Effect: When any city tile is placed, gain 2 ME.",
    :cost 8,
    :effect "1 VP."}
   {:imm [:multi [:raise-temperature 3] [:gain 4 "s"] [:remove-any 8 "p"]],
    :number 39,
    :title "Deimos Down",
    :cost 31,
    :tags "t1",
    :type "r",
    :category "",
    :effect "Raise temperature 3 steps and gain 4 steel.  Remove up to 8 plants from any player."}
   {:category "",
    :tags "jt",
    :number 40,
    :type "g",
    :title "Asteroid Mining",
    :imm [:inc-prod "t" 2],
    :vp 2,
    :cost 30,
    :effect "Increase your titanium production 2 steps.  2 VP."}
   {:category "",
    :tags "b",
    :number 41,
    :type "g",
    :title "Food Factory",
    :imm [:multi [:dec-prod "p" 1] [:inc-prod "m" 4]],
    :vp 1,
    :cost 12,
    :effect "Decrease your plant production 1 step and increase your ME production 4 steps.  1 VP."}
   {:imm [:inc-prod "p" 1],
    :number 42,
    :title "Archaebacteria",
    :cost 6,
    :req "tx-18",
    :tags "m",
    :type "g",
    :category "",
    :effect "Increase your plant production 1 step."}
   {:imm [:multi [:dec-prod "e" 1] [:inc-prod "h" 3]],
    :number 43,
    :title "Carbonate Processing",
    :cost 6,
    :tags "b",
    :type "g",
    :category "",
    :effect "Decrease your energy production 1 step and increase your heat production 3 steps."}
   {:category "",
    :tags "sb",
    :number 44,
    :type "g",
    :title "Natural Preserve",
    :imm
    [:multi [:place "preserve" :not-adjacent-to-any] [:inc-prod "m" 1]],
    :req "ox4",
    :vp 1,
    :cost 9,
    :effect "Place this [preserve] tile NEXT TO NO OTHER TILE.  Increase your ME production 1 step.  1 VP."}
   {:imm [:multi [:dec-prod "m" 2] [:inc-prod "e" 3]],
    :number 45,
    :title "Nuclear Power",
    :cost 10,
    :tags "nb",
    :type "g",
    :category "",
    :effect "Decrease your ME production 2 steps and increase your energy production 3 steps."}
   {:category "",
    :tags "n",
    :number 46,
    :type "g",
    :title "Lightning Harvest",
    :imm [:multi [:inc-prod "e" 1] [:inc-prod "m" 1]],
    :req "s3",
    :vp 1,
    :cost 8,
    :effect "Increase your energy production and your ME production 1 step each.  1 VP."}
   {:imm [:multi [:gain 1 "p"] [:inc-prod "p" 2]],
    :number 47,
    :title "Algae",
    :cost 10,
    :req "w5",
    :tags "p",
    :type "g",
    :category "",
    :effect "Gain 1 plant and increase your plant production 2 steps."}
   {:imm [:inc-prod "p" 1],
    :number 48,
    :title "Adapted Lichen",
    :cost 9,
    :tags "p",
    :type "g",
    :category "",
    :effect "Increase your plant production 1 step."}
   {:category "a",
    :tags "m",
    :number 49,
    :type "b",
    :title "Tardigrades",
    :vp-formula [1 4 :this-resources],
    :b-action [:add-this "microbes" 1],
    :blue "Action: Add 1 microbe to this card.",
    :cost 4,
    :effect "1 VP per 4 microbes on this card."}
   {:imm [:choose
          {:choice-tag :remove-another-animals-2, :imm [:remove-another 2 "animals"]}
          {:choice-tag :remove-any-5-p, :imm [:remove-any 5 "p"]}],
    :number 50,
    :title "Virus",
    :cost 1,
    :tags "m1",
    :type "r",
    :category "a",
    :effect "Remove up to 2 animals or 5 plants from any player."}
   {:category "a",
    :tags "jt",
    :number 51,
    :type "g",
    :title "Miranda Resort",
    :imm [:inc-prod-formula "m" [1 1 :your-earth-tag]],
    :vp 1,
    :cost 12,
    :effect "Increase your ME production 1 step for each Earth tag you have. 1 vp."}
   {:category "",
    :tags "a",
    :number 52,
    :type "b",
    :title "Fish",
    :vp-formula [1 1 :this-resources],
    :b-action [:add-this "animals" 1],
    :imm [:dec-any-prod "p" 1],
    :req "t+2",
    :blue "Action: Add 1 animal to this card.",
    :cost 9,
    :effect "Decrease any plant production 1 step. 1 VP for each animal on this card."}
   {:category "",
    :tags "-",
    :number 53,
    :type "g",
    :title "Lake Marineris",
    :imm [:place-oceans 2],
    :req "t0",
    :vp 2,
    :cost 18,
    :effect "Place 2 ocean tiles.  2 vp."}
   {:category "",
    :tags "a",
    :number 54,
    :type "b",
    :title "Small animals",
    :vp-formula [1 2 :this-resources],
    :b-action [:add-this "animals" 1],
    :imm [:dec-any-prod "p" 1],
    :req "o6",
    :blue "Action: Add 1 animal to this card.",
    :cost 6,
    :effect "Decrease any plant production 1 step. 1 vp per 2 animals on this card."}
   {:category "",
    :tags "p",
    :number 55,
    :type "g",
    :title "Kelp Farming",
    :imm [:multi [:inc-prod "m" 2] [:inc-prod "p" 3] [:gain 2 "p"]],
    :req "w6",
    :vp 1,
    :cost 17,
    :effect "Increase your ME production 2 steps and your plant production 3 steps. Gain 2 plants.  1 vp."}
   {:imm [:inc-prod "s" 1],
    :number 56,
    :title "Mine",
    :cost 4,
    :tags "b",
    :type "g",
    :category "a",
    :effect "Increase your steel production 1 step."}
   {:category "a",
    :tags "jt",
    :number 57,
    :type "g",
    :title "Vesta Shipyard",
    :imm [:inc-prod "t" 1],
    :vp 1,
    :cost 15,
    :effect "Increase your titanium production 1 step. 1 vp."}
   {:category "",
    :tags "jtn",
    :number 58,
    :type "g",
    :title "Beam from a Thorium Asteroid",
    :imm [:multi [:inc-prod "h" 3] [:inc-prod "e" 3]],
    :req "j1",
    :vp 1,
    :cost 32,
    :effect "Increase your heat production and energy production 3 steps each. 1 vp."}
   {:category "",
    :tags "p",
    :number 59,
    :type "g",
    :title "Mangrove",
    :imm [:place-greenery :water-spot-ignoring-restrictions],
    :req "t+4",
    :vp 1,
    :cost 12,
    :effect "Place a greenery tile ON AN AREA RESERVED FOR OCEAN and raise oxygen 1 step. Disregard normal placement restrictions for this. 1 vp."}
   {:category "",
    :tags "p",
    :number 60,
    :type "g",
    :title "Trees",
    :imm [:multi [:inc-prod "p" 3] [:gain 1 "p"]],
    :req "t-4",
    :vp 1,
    :cost 13,
    :effect "Increase your plant production 3 steps.  Gain 1 plant.  1 vp."}
   {:imm [:multi [:dec-any-prod "s" 1] [:inc-prod "s" 1]],
    :number 61,
    :title "Great Escarpment Consortium",
    :cost 6,
    :req "S",
    :tags "-",
    :type "g",
    :category "a",
    :effect "Decrease any steel production 1 step and increase your own 1 step."}
   {:imm [:gain 5 "s"],
    :number 62,
    :title "Mineral Deposit",
    :cost 5,
    :tags "1",
    :type "r",
    :category "a",
    :effect "Gain 5 steel."}
   {:imm [:multi [:raise-oxygen 1] [:remove-any 2 "p"] [:gain 2 "s"]],
    :number 63,
    :title "Mining Expedition",
    :cost 12,
    :tags "1",
    :type "r",
    :category "",
    :effect "Raise oxygen 1 step. Remove 2 plants from any player. Gain 2 steel."}
   {:imm [:place-mine-inc-prod :spot-with-metal-near-your-tile]
    :number 64,
    :title "Mining Area",
    :cost 4,
    :tags "b",
    :type "g",
    :category "a",
    :effect "Place this [mine] tile on an area with a steel or titanium placement bonus, adjacent to another of your tiles. Increase your production of that resource step."}
   {:imm [:multi [:dec-prod "e" 1] [:inc-prod "s" 2]],
    :number 65,
    :title "Building Industries",
    :cost 6,
    :tags "b",
    :type "g",
    :category "a",
    :effect "Decrease your energy production 1 step and increase your steel production 2 steps."}
   {:imm [:place-marker :non-reserved],
    :number 66,
    :title "Land Claim",
    :cost 1,
    :tags "1",
    :type "r",
    :category "a",
    :effect "Place your marker on a non-reserved area. Only you may place a tile here."}
   {:imm [:place-mine-inc-prod :spot-with-metal]
    :number 67,
    :title "Mining Rights",
    :cost 9,
    :tags "b",
    :type "g",
    :category "",
    :effect "Place this [mine] tile on an area with a steel or titanium placement bonus.  Increase that production 1 step."}
   {:imm [:inc-prod "m" 2],
    :number 68,
    :title "Sponsors",
    :cost 6,
    :tags "e",
    :type "g",
    :category "a",
    :effect "Increase your ME production 2 steps."}
   {:category "a",
    :tags "b",
    :number 69,
    :type "b",
    :title "Electro Catapult",
    :b-action [:multi [:choose
                       {:choice-tag :spend-1-p->gain-7-m, :imm [:spend 1 "p"]}
                       {:choice-tag :spend-1-s->gain-7-m, :imm [:spend 1 "s"]}] [:gain 7 "m"]],
    :imm [:dec-prod "e" 1],
    :req "ox8",
    :vp 1,
    :blue "Action: Spend 1 plant or 1 steel to gain 7 ME.",
    :cost 17,
    :effect "Decrease your energy production 1 step. 1 vp."}
   {:category "a",
    :tags "e",
    :number 70,
    :b-discount [:any-card 2],
    :type "b",
    :title "Earth Catapult",
    :vp 2,
    :blue "Effect: When you play a card, you pay 2 ME less for it.",
    :cost 23,
    :effect "2 vp."}
   {:category "a",
    :tags "s",
    :number 71,
    :type "b",
    :b-special :steel-and-titanium-worth-1-more,
    :title "Advanced Alloys",
    :blue
    "Effect: Each titanium you have is worth 1 ME extra. Each steel you have is worth 1 ME extra.",
    :cost 9,
    :effect "-"}
   {:category "",
    :tags "a",
    :number 72,
    :type "b",
    :title "Birds",
    :vp-formula [1 2 :this-resources],
    :b-action [:add-this "animals" 1],
    :imm [:dec-any-prod "p" 2],
    :req "o13",
    :blue "Action: Add an animal to this card.",
    :cost 10,
    :effect "Decrease any plant production 2 steps. 1 VP for each animal on this card."}
   {:category "",
    :tags "sb",
    :b-triggered {:t :you-play-science, :a [:may [:multi [:discard-card] [:draw-cards 1]]]},
    :number 73,
    :type "b",
    :title "Mars University",
    :vp 1,
    :blue
    "Effect: When you play a science tag, including this, you may discard a card from hand to draw a card.",
    :cost 8,
    :effect "1 vp."}
   {:category "a",
    :tags "sm",
    :b-triggered {:t :you-play-amp, :a [:choose
                                        {:choice-tag :gain-1-p, :imm [:gain 1 "p"]}
                                        {:choice-tag :add-that-resource, :imm [:add-that 1]}]},
    :number 74,
    :type "b",
    :title "Viral Enhancers",
    :blue
    "Effect: When you play a plant, microbe, or an animal tag, including this, gain 1 plant or add 1 resource TO THAT CARD.",
    :cost 9,
    :effect "-"}
   {:imm [:multi [:gain 2 "p"] [:raise-oxygen 1] [:place-ocean]],
    :number 75,
    :title "Towing a Comet",
    :cost 23,
    :tags "t1",
    :type "r",
    :category "",
    :effect "Gain 2 plants.  Raise oxygen level 1 step and place an ocean tile."}
   {:category "",
    :tags "nt",
    :number 76,
    :type "b",
    :title "Space Mirrors",
    :b-action [:multi [:spend 7 "m"] [:inc-prod "e" 1]],
    :blue
    "Action: Spend 7 ME to increase your energy production 1 step.",
    :cost 3,
    :effect "-"}
   {:imm [:multi [:inc-prod "e" 1] [:gain 2 "t"]],
    :number 77,
    :title "Solar Wind Power",
    :cost 11,
    :tags "stn",
    :type "g",
    :category "",
    :effect "Increase your energy production 1 step and gain 2 titanium."}
   {:imm [:place-oceans 2],
    :number 78,
    :title "Ice Asteroid",
    :cost 23,
    :tags "t1",
    :type "r",
    :category "",
    :effect "Place 2 ocean tiles."}
   {:category "a",
    :tags "sn",
    :number 79,
    :b-discount [:space 2],
    :type "b",
    :title "Quantum Extractor",
    :imm [:inc-prod "e" 4],
    :req "s4",
    :blue
    "Effect: When you play a space card, you pay 2 ME less for it.",
    :cost 13,
    :effect "Increase you energy production 4 steps."}
   {:imm
    [:multi
     [:raise-temperature 2]
     [:place-oceans 2]
     [:remove-any 6 "p"]],
    :number 80,
    :title "Giant Ice Asteroid",
    :cost 36,
    :tags "t1",
    :type "r",
    :category "",
    :effect "Raise temperature 2 steps and place 2 ocean tiles. Remove up to 6 plants from any player."}
   {:category "",
    :tags "jtc",
    :number 81,
    :type "g",
    :title "Ganymede Colony",
    :vp-formula [1 1 :your-jovian-tag],
    :imm [:place-named-city :ganymede],
    :cost 20,
    :effect "Place a city ON THE RESERVED AREA. 1 VP per Jovian tag you have."}
   {:category "a",
    :tags "jt",
    :number 82,
    :type "g",
    :title "Callisto Penal Mines",
    :imm [:inc-prod "m" 3],
    :vp 2,
    :cost 24,
    :effect "Increase your ME production 3 steps. 2 vp."}
   {:imm [:inc-prod "e" 3],
    :number 83,
    :title "Giant Space Mirror",
    :cost 17,
    :tags "nt",
    :type "g",
    :category "",
    :effect "Increase your energy production 3 steps."}
   {:vp 1,
    :number 84,
    :title "Trans-Neptune Probe",
    :cost 6,
    :tags "st",
    :type "g",
    :category "a",
    :effect "1 vp"}
   {:category "a",
    :tags "b",
    :number 85,
    :type "g",
    :title "Commerical District",
    :vp-formula [1 1 :adjacent-city],
    :imm [:multi [:dec-prod "e" 1] [:inc-prod "m" 4] [:place "euro"]],
    :cost 16,
    :effect "Decrease your energy production 1 step and increase your ME production 4 steps.  Place this [euro] tile.  1 VP PER ADJACENT CITY TILE."}
   {:imm [:duplicate-production-of-your-building],
    :number 86,
    :title "Robotic Workforce",
    :cost 9,
    :tags "s",
    :type "g",
    :category "a",
    :effect "Duplicate only the production box of one of your building cards."}
   {:imm [:multi [:inc-prod "p" 1] [:gain 3 "p"]],
    :number 87,
    :title "Grass",
    :cost 11,
    :req "t-16",
    :tags "p",
    :type "g",
    :category "",
    :effect "Increase your plant production 1 step. Gain 3 plants."}
   {:imm [:multi [:inc-prod "p" 1] [:gain 1 "p"]],
    :number 88,
    :title "Heather",
    :cost 6,
    :req "t-14",
    :tags "p",
    :type "g",
    :category "",
    :effect "Increase your plant production 1 step. Gain 1 plant."}
   {:imm [:multi [:dec-prod "m" 1] [:inc-prod "e" 2]],
    :number 89,
    :title "Peroxide Power",
    :cost 7,
    :tags "nb",
    :type "g",
    :category "",
    :effect "Decrease your ME production 1 step and increase your energy production 2 steps."}
   {:category "a",
    :tags "ss",
    :number 90,
    :type "g",
    :title "Research",
    :imm [:draw-cards 2],
    :vp 1,
    :cost 11,
    :effect "Counts as playing 2 science cards.  Draw 2 cards.  1 vp."}
   {:category "a",
    :tags "s",
    :number 91,
    :type "g",
    :title "Gene Repair",
    :imm [:inc-prod "m" 2],
    :req "s3",
    :vp 2,
    :cost 12,
    :effect "Increase your ME production 2 steps.  2 vp."}
   {:category "a",
    :tags "jt",
    :number 92,
    :type "g",
    :title "Io Mining Industries",
    :vp-formula [1 1 :your-jovian-tag],
    :imm [:multi [:inc-prod "t" 2] [:inc-prod "m" 2]],
    :cost 41,
    :effect "Increase your titanium production 2 steps and your ME Production 2 steps. 1 VP per Jobian tag you have."}
   {:imm [:multi [:inc-prod "p" 2] [:gain 2 "p"]],
    :number 93,
    :title "Bushes",
    :cost 10,
    :req "t-10",
    :tags "p",
    :type "g",
    :category "",
    :effect "Increase your plant production 2 steps.  Gain 2 plants."}
   {:category "a",
    :tags "sn",
    :number 94,
    :b-discount [:space 2],
    :type "b",
    :title "Mass Converter",
    :imm [:inc-prod "e" 6],
    :req "s5",
    :blue
    "Effect: When you play a space card, you pay 2 ME less for it.",
    :cost 8,
    :effect "Increase your energy production 6 steps."}
   {:category "a",
    :tags "sb",
    :number 95,
    :type "b",
    :title "Physics Complex",
    :vp-formula [2 1 :this-resources],
    :b-action [:multi [:spend 6 "e"] [:add-this "science" 1]],
    :blue
    "Action: Spend 6 energy to add a science resource to this card.",
    :cost 12,
    :effect "2 vp for each science resource on this card."}
   {:imm [:gain-formula "p" [1 1 :city-in-play]],
    :number 96,
    :title "Greenhouses",
    :cost 6,
    :tags "pb",
    :type "g",
    :category "",
    :effect "Gain 1 plant for each city tile in play."}
   {:category "",
    :tags "e",
    :number 97,
    :type "g",
    :title "Nuclear Zone",
    :imm [:multi [:place "nuclear" :land-spot] [:raise-temperature 2]],
    :vp -2,
    :cost 10,
    :effect "Place this [nuclear] tile and raise temperature 2 steps.  -2 vp."}
   {:category "a",
    :tags "b",
    :number 98,
    :type "g",
    :title "Tropical Resort",
    :imm [:multi [:dec-prod "h" 2] [:inc-prod "m" 3]],
    :vp 2,
    :cost 13,
    :effect "Decrease your heat production 2 steps and increase your ME production 3 steps. 2 vp."}
   {:imm [:inc-prod-formula "m" [1 1 :each-space-tag-of-opponents]],
    :number 99,
    :title "Toll Station",
    :cost 12,
    :tags "t",
    :type "g",
    :category "a",
    :effect "Increase your ME production 1 step for each space tag your OPPONENTS have."}
   {:imm [:multi [:dec-prod "m" 1] [:inc-prod "e" 1]],
    :number 100,
    :title "Fueled Generators",
    :cost 1,
    :tags "nb",
    :type "g",
    :category "",
    :effect "Decrease your ME production 1 step and increase your energy production 1 step."}
   {:category "",
    :tags "b",
    :number 101,
    :type "b",
    :title "Ironworks",
    :b-action [:multi [:spend 4 "e"] [:gain 1 "s"] [:raise-oxygen 1]],
    :blue
    "Action: Spend 4 energy to gain 1 steel and increase oxygen 1 step.",
    :cost 11,
    :effect "-"}
   {:imm [:inc-prod-formula "e" [1 1 :your-power-tag]],
    :number 102,
    :title "Power Grid",
    :cost 18,
    :tags "n",
    :type "g",
    :category "",
    :effect "Increase your energy production 1 step for each power tag you have, including this."}
   {:category "",
    :tags "b",
    :number 103,
    :type "b",
    :title "Steelworks",
    :b-action [:multi [:spend 4 "e"] [:gain 2 "s"] [:raise-oxygen 1]],
    :blue
    "Action: Spend 4 energy to gain 2 steel and increase oxygen 1 step.",
    :cost 15,
    :effect "-"}
   {:category "",
    :tags "b",
    :number 104,
    :type "b",
    :title "Ore Processor",
    :b-action [:multi [:spend 4 "e"] [:gain 1 "t"] [:raise-oxygen 1]],
    :blue
    "Action: Spend 4 energy to gain 1 titanium and increase oxygen 1 step.",
    :cost 13,
    :effect "-"}
   {:category "a",
    :tags "e",
    :number 105,
    :b-discount [:earth 3],
    :type "b",
    :title "Earth Office",
    :blue
    "Effect: When you play an Earth tag, you pay 3 ME less for it.",
    :cost 1,
    :effect "-"}
   {:imm [:inc-prod "m" 3],
    :number 106,
    :title "Acquired Company",
    :cost 10,
    :tags "e",
    :type "g",
    :category "a",
    :effect "Increase your ME production 3 steps."}
   {:imm [:gain-formula "m" [1 1 :each-event-ever-played]],
    :number 107,
    :title "Media Archives",
    :cost 8,
    :tags "e",
    :type "g",
    :category "a",
    :effect "Gain 1 ME for each event EVER PLAYED by all players."}
   {:category "",
    :tags "cb",
    :number 108,
    :type "g",
    :title "Open City",
    :imm
    [:multi
     [:dec-prod "e" 1]
     [:inc-prod "m" 4]
     [:gain 2 "p"]
     [:place-city]],
    :req "o12",
    :vp 1,
    :cost 23,
    :effect "Decrease your energy production 1 step and increase your ME production 4 steps. Gain 2 plants and place a city tile. 1 vp."}
   {:category "a",
    :tags "e",
    :b-triggered {:t :you-play-event, :a [:gain 3 "m"]},
    :number 109,
    :type "b",
    :title "Media Group",
    :blue "Effect: After you play an event card, you gain 3 ME.",
    :cost 6,
    :effect "-"}
   {:category "a",
    :tags "e",
    :number 110,
    :type "b",
    :title "Business Network",
    :b-action [:look-buy-or-discard],
    :imm [:dec-prod "m" 1],
    :blue
    "Action: Look at the top card and either buy it or discard it.",
    :cost 4,
    :effect "Decrease your ME production 1 step."}
   {:imm [:look-take 4 2],
    :number 111,
    :title "Business Contacts",
    :cost 7,
    :tags "e1",
    :type "r",
    :category "a",
    :effect "Look at the top 4 card from the deck. Take 2 of them into hand and discard the other 2."}
   {:category "a",
    :tags "e1",
    :number 112,
    :type "r",
    :title "Bribed Committee",
    :imm [:raise-tr 2],
    :vp -2,
    :cost 7,
    :effect "Raise your terraform rating 2 steps. -2 vp."}
   {:category "",
    :tags "nb",
    :number 113,
    :type "g",
    :title "Solar Power",
    :imm [:inc-prod "e" 1],
    :vp 1,
    :cost 11,
    :effect "Increase your energy production 1 step. 1 vp."}
   {:vp 2,
    :number 114,
    :title "Breathing Filters",
    :cost 11,
    :req "o7",
    :tags "s",
    :type "g",
    :category "",
    :effect "2 vp"}
   {:imm [:choose
          {:choice-tag :inc-prod-p-1, :imm [:inc-prod "p" 1]}
          {:choice-tag :inc-prod-e-2, :imm [:inc-prod "e" 2]}],
    :number 115,
    :title "Artificial Photosynthesis",
    :cost 12,
    :tags "s",
    :type "g",
    :category "",
    :effect "Increase your plant production 1 step or your energy production 2 steps."}
   {:category "",
    :tags "b",
    :number 116,
    :type "g",
    :title "Artificial Lake",
    :imm [:place-ocean :land-spot],
    :req "t-6",
    :vp 1,
    :cost 15,
    :effect "Place 1 ocean tile ON AN AREA NOT RESERVED FOR OCEAN. 1 vp."}
   {:imm [:inc-prod "e" 2],
    :number 117,
    :title "Geothermal Power",
    :cost 11,
    :tags "nb",
    :type "g",
    :category "",
    :effect "Increase your energy production 2 steps."}
   {:category "",
    :tags "p",
    :number 118,
    :type "g",
    :title "Farming",
    :imm [:multi [:inc-prod "m" 2] [:inc-prod "p" 2] [:gain 2 "p"]],
    :req "t+4",
    :vp 2,
    :cost 16,
    :effect "Increase your ME production 2 steps and your plant production 2 steps. Gain 2 plants. 2 vp."}
   {:vp 1,
    :number 119,
    :title "Dust Seals",
    :cost 2,
    :req "wx3",
    :tags "-",
    :type "g",
    :category "",
    :effect "1 vp."}
   {:imm
    [:multi
     [:dec-prod "e" 1]
     [:inc-prod "m" 2]
     [:place-city :near-2-cities]],
    :number 120,
    :title "Urbanized Area",
    :cost 10,
    :tags "cb",
    :type "g",
    :category "",
    :effect "Decrease your energy produciton 1 step and increase your ME production 2 steps. Place a city tile ADJACENT TO AT LEAST 2 OTHER CITY TILES."}
   {:imm
    [:choose
     {:choice-tag :remove-any-3-t, :imm [:remove-any 3 "t"]}
     {:choice-tag :remove-any-4-s, :imm [:remove-any 4 "s"]}
     {:choice-tag :remove-any-7-m, :imm [:remove-any 7 "m"]}],
    :number 121,
    :title "Sabotage",
    :cost 1,
    :tags "1",
    :type "r",
    :category "a",
    :effect "Remove up to 3 titanium from any player, or 4 steel, or 7 ME."}
   {:imm [:multi [:spend 1 "p"] [:inc-prod "p" 1]],
    :number 122,
    :title "Moss",
    :cost 4,
    :req "w3",
    :tags "p",
    :type "g",
    :category "",
    :effect "Lose 1 plant. Increase your plant production 1 step."}
   {:category "a",
    :tags "b",
    :number 123,
    :type "b",
    :title "Industrial Center",
    :b-action [:multi [:spend 7 "m"] [:inc-prod "s" 1]],
    :imm [:place "factory" :near-city],
    :blue "Action: Spend 7 ME to increase your steel production 1 step.",
    :cost 4,
    :effect "Place this [factory] tile ADJACENT TO A CITY TILE."}
   {:imm [:choose
          {:choice-tag :steal-from-any-s-2, :imm [:steal-from-any "s" 2]}
          {:choice-tag :steal-from-any-m-3, :imm [:steal-from-any "m" 3]}],
    :number 124,
    :title "Hired Raiders",
    :cost 1,
    :tags "1",
    :type "r",
    :category "a",
    :effect "Steal up to 2 steel, or 3 ME from any player."}
   {:category "a",
    :tags "-",
    :number 125,
    :type "g",
    :title "Hackers",
    :imm
    [:multi [:dec-prod "e" 1] [:dec-any-prod "m" 2] [:inc-prod "m" 2]],
    :vp -1,
    :cost 3,
    :effect "Decrease your energy production 1 step and any ME production 2 steps. Increase your ME production 2 steps. -1 vp."}
   {:imm [:multi [:dec-prod "e" 1] [:inc-prod "h" 4]],
    :number 126,
    :title "GHG Factories",
    :cost 11,
    :tags "b",
    :type "g",
    :category "",
    :effect "Decrease your energy production 1 step and increase your heat production 4 steps."}
   {:imm [:place-ocean],
    :number 127,
    :title "Subterranean Reservoir",
    :cost 11,
    :tags "1",
    :type "r",
    :category "",
    :effect "Place 1 ocean tile."}
   {:category "",
    :tags "ap",
    :b-triggered {:t :you-play-ap, :a [:add-this "animals" 1]},
    :number 128,
    :type "b",
    :title "Ecological Zone",
    :vp-formula [1 2 :this-resources],
    :imm [:place "paw" :near-greenery],
    :req "g",
    :blue
    "Effect: When you play an animal or a plant tag (including these 2), add an animal to this card.",
    :cost 12,
    :effect "Place this [paw] tile ADJACENT TO ANY GREENERY TILE. 1 vp per 2 animals on this card."}
   {:category "",
    :req "o5",
    :tags "-",
    :number 129,
    :type "g",
    :title "Zeppelins",
    :imm [:inc-prod-formula "m" [1 1 :city-on-mars]],
    :vp 1,
    :cost 13,
    :effect "Increase your ME production 1 step for each city tile ON MARS.  1 vp."}
   {:imm [:inc-prod-formula "p" [1 2 :your-microbe-tag]],
    :number 130,
    :title "Worms",
    :cost 8,
    :req "o4",
    :tags "m",
    :type "g",
    :category "",
    :effect "Increase your plant production 1 step for every 2 microbe tags you have, including this."}
   {:category "",
    :tags "m",
    :b-triggered {:t :you-play-amp, :a [:add-this "microbes" 1]},
    :number 131,
    :type "b",
    :title "Decomposers",
    :vp-formula [1 3 :this-resources],
    :req "o3",
    :blue
    "Effect: When you plan an animal, plant, or microbe tag, including this, add a microbe to this card.",
    :cost 5,
    :effect "1 vp per 3 microbes on this card."}
   {:imm [:inc-prod "e" 3],
    :number 132,
    :title "Fusion Power",
    :cost 14,
    :req "n2",
    :tags "snb",
    :type "g",
    :category "",
    :effect "Increase your energy production 3 steps."}
   {:category "",
    :tags "m",
    :number 133,
    :type "b",
    :title "Symbiotic Fungus",
    :b-action [:add-another "microbes" 1],
    :req "t-14",
    :blue "Action: Add a microbe to ANOTHER card.",
    :cost 4,
    :effect "-"}
   {:category "",
    :tags "m",
    :number 134,
    :type "b",
    :title "Extreme-Cold Fungus",
    :b-action [:choose
               {:choice-tag :gain-1-p, :imm [:gain 1 "p"]}
               {:choice-tag :add-another-microbes-2, :imm [:add-another "microbes" 2]}],
    :req "tx-10",
    :blue "Action: Gain 1 plant or add 2 microbes to ANOTHER card.",
    :cost 13,
    :effect "-"}
   {:vp 3,
    :number 135,
    :title "Advanced Ecosystems",
    :cost 11,
    :req "A",
    :tags "pma",
    :type "g",
    :category "",
    :effect "3 vp."}
   {:category "",
    :tags "nb",
    :number 136,
    :type "g",
    :title "Great Dam",
    :imm [:inc-prod "e" 2],
    :req "w4",
    :vp 1,
    :cost 12,
    :effect "Increase your energy production 2 steps. 1 vp."}
   {:imm [:inc-prod-formula "m" [1 1 :your-earth-tag]],
    :number 137,
    :title "Cartel",
    :cost 8,
    :tags "e",
    :type "g",
    :category "a",
    :effect "Increase your ME Production 1 step for each Earth tag you have, including this."}
   {:imm
    [:multi
     [:dec-prod "e" 2]
     [:inc-prod "s" 2]
     [:inc-prod "t" 1]
     [:raise-oxygen 2]],
    :number 138,
    :title "Strip Mine",
    :cost 25,
    :tags "b",
    :type "g",
    :category "",
    :effect "Decrease your energy production 2 steps. Increase your steel production 2 steps and your titanium production 1 step. Raise oxygen 2 steps."}
   {:category "",
    :tags "n",
    :number 139,
    :type "g",
    :title "Wave Power",
    :imm [:inc-prod "e" 1],
    :req "w3",
    :vp 1,
    :cost 8,
    :effect "Increase your energy production 1 step. 1 vp."}
   {:imm [:multi [:raise-temperature 2] [:place "eruption" :lavflo]],
    :number 140,
    :title "Lava Flows",
    :cost 18,
    :tags "1",
    :type "r",
    :category "",
    :effect "Raise temperature 2 steps and place this [eruption] tile ON EITHER THARSIS THOLUS, ASCRAEUS MONS, PAVONIS MONS OR ARSIA MONS."}
   {:imm [:inc-prod "e" 1],
    :number 141,
    :title "Power Plant",
    :cost 4,
    :tags "nb",
    :type "g",
    :category "",
    :effect "Increase your energy production 1 step."}
   {:imm [:multi [:inc-prod "h" 4] [:place "mohole" :water-spot]],
    :number 142,
    :title "Mohole Area",
    :cost 20,
    :tags "b",
    :type "g",
    :category "",
    :effect "Increase your heat production 4 steps.  Place this [mohole] tile ON AN AREA RESERVED FOR OCEAN."}
   {:category "",
    :tags "et1",
    :number 143,
    :type "r",
    :title "Large Convoy",
    :imm
    [:multi
     [:draw-cards 2]
     [:place-ocean]
     [:choose
      {:choice-tag :gain-5-p, :imm [:gain 5 "p"]}
      {:choice-tag :add-another-animals-4, :imm [:add-another "animals" 4]}]],
    :vp 2,
    :cost 36,
    :effect "Place an ocean tile and draw 2 cards. Gain 5 plants, or add 4 animals to ANOTHER card. 2 vp."}
   {:imm [:inc-prod "t" 1],
    :number 144,
    :title "Titanium Mine",
    :cost 7,
    :tags "b",
    :type "g",
    :category "a",
    :effect "Increase your titanium production 1 step."}
   {:category "",
    :tags "nb",
    :number 145,
    :type "g",
    :title "Tectonic Stress Power",
    :imm [:inc-prod "e" 3],
    :req "s2",
    :vp 1,
    :cost 18,
    :effect "Increase your energy production 3 steps. 1 vp."}
   {:imm [:multi [:spend 2 "p"] [:inc-prod "p" 2]],
    :number 146,
    :title "Nitrophilic Moss",
    :cost 8,
    :req "w3",
    :tags "p",
    :type "g",
    :category "",
    :effect "Lose 2 plants. Increase your plant production 2 steps."}
   {:category "",
    :tags "a",
    :b-triggered {:t :you-place-greenery, :a [:add-this "animals" 1]},
    :number 147,
    :type "b",
    :title "Herbivores",
    :vp-formula [1 2 :this-resources],
    :imm [:multi [:add-this "animals" 1] [:dec-any-prod "p" 1]],
    :req "o8",
    :blue
    "Effect: When you place a greenery tile, add an animal to this card.",
    :cost 12,
    :effect "Add 1 animal to this card. Decrease any plant production 1 step. 1 vp per 2 animals on this card."}
   {:imm [:inc-prod-formula "p" [1 1 :your-plant-tag]],
    :number 148,
    :title "Insects",
    :cost 9,
    :req "o6",
    :tags "m",
    :type "g",
    :category "",
    :effect "Increase your plant production 1 step for each plant tag you have."}
   {:imm [:add-resource-where-has-resource 1],
    :number 149,
    :title "CEO's Favorite Project",
    :cost 1,
    :tags "1",
    :type "r",
    :category "a",
    :effect "Add 1 resource to a card with at least 1 resource on it."}
   {:category "a",
    :tags "s",
    :number 150,
    :b-discount [:any-card 2],
    :type "b",
    :title "Anti-Gravity Technology",
    :req "s7",
    :vp 3,
    :blue "Effect: When you play a card, you pay 2 ME less for it.",
    :cost 14,
    :effect "3 vp."}
   {:imm [:multi [:dec-prod "m" 1] [:gain 10 "m"]],
    :number 151,
    :title "Investment Loan",
    :cost 3,
    :tags "e1",
    :type "r",
    :category "a",
    :effect "Decrease your ME production 1 step.  Gain 10 ME."}
   {:imm [:convert-prod-any-amount "h" "m"],
    :number 152,
    :title "Insulation",
    :cost 2,
    :tags "-",
    :type "g",
    :category "",
    :effect "Decrease your heat production any number of steps and increase your ME production the same number of steps."}
   {:category "",
    :tags "s",
    :number 153,
    :type "b",
    :b-special :tweak-global-requirements,
    :title "Adaptation Technology",
    :vp 1,
    :blue
    "Effect: Your global requirements are +2 or -2 steps, your choice in each case.",
    :cost 12,
    :effect "1 vp"}
   {:category "a",
    :req "t0",
    :tags "-",
    :number 154,
    :type "b",
    :title "Caretaker Contract",
    :b-action [:multi [:spend 8 "h"] [:raise-tr 1]],
    :blue
    "Action: Spend 8 heat to increase your terraform rating 1 step.",
    :cost 3,
    :effect "-"}
   {:imm [:inc-prod "p" 2],
    :number 155,
    :title "Designed Microorganisms",
    :cost 16,
    :req "tx-14",
    :tags "sm",
    :type "g",
    :category "",
    :effect "Increase your plant production 2 steps."}
   {:category "a",
    :tags "s",
    :b-triggered {:t :you-pay-for-standard, :a [:gain 3 "m"]},
    :number 156,
    :type "b",
    :title "Standard Technology",
    :blue
    "Effect: After you pay for a standard project, except selling patents, you gain 3 ME.",
    :cost 6,
    :effect "-"}
   {:category "",
    :tags "m",
    :number 157,
    :type "b",
    :title "Nitrite Reducing Bacteria",
    :b-action [:choose
               {:choice-tag :add-this-microbes-1, :imm [:add-this "microbes" 1]}
               {:choice-tag :remove-3-microbes->raise-tr, :imm [:multi [:remove-this "microbes" 3] [:raise-tr 1]]}],
    :imm [:add-this "microbes" 3],
    :blue
    "Action: Add 1 microbe to this card, or remove 3 microbes to increase your TR 1 step.",
    :cost 11,
    :effect "Add 3 microbes to this card."}
   {:imm [:multi [:inc-prod "e" 1] [:inc-prod "s" 1]],
    :number 158,
    :title "Industrial Microbes",
    :cost 12,
    :tags "mb",
    :type "g",
    :category "",
    :effect "Increase your energy production and your steel production 1 step each."}
   {:imm [:inc-prod "p" 1],
    :number 159,
    :title "Lichen",
    :cost 7,
    :req "t-24",
    :tags "p",
    :type "g",
    :category "",
    :effect "Increase your plant production 1 step."}
   {:imm [:multi [:dec-any-prod "e" 1] [:inc-prod "e" 1]],
    :number 160,
    :title "Power Supply Consortium",
    :cost 5,
    :req "n2",
    :tags "n",
    :type "g",
    :category "a",
    :effect "Decrease any energy production 1 step and increase your own 1 step."}
   {:imm [:multi [:draw-cards 1] [:place-ocean]],
    :number 161,
    :title "Convoy from Europa",
    :cost 15,
    :tags "t1",
    :type "r",
    :category "",
    :effect "Place 1 ocean tile and draw 1 card."}
   {:imm [:multi [:inc-prod "h" 1] [:gain 3 "h"]],
    :number 162,
    :title "Imported GHG",
    :cost 7,
    :tags "et1",
    :type "r",
    :category "",
    :effect "Increase your heat production 1 step and gain 3 heat."}
   {:imm
    [:multi
     [:raise-tr 1]
     [:gain 4 "p"]
     [:add-another "microbes" 3]
     [:add-another "animals" 2]],
    :number 163,
    :title "Imported Nitrogen",
    :cost 23,
    :tags "et1",
    :type "r",
    :category "",
    :effect "Raise your TR 1 step and gain 4 plants. Add 3 microbes to ANOTHER card and 2 animals to ANOTHER card."}
   {:imm [:inc-prod "h" 1],
    :number 164,
    :title "Micro-Mills",
    :cost 3,
    :tags "-",
    :type "g",
    :category "",
    :effect "Increase your heat production 1 step."}
   {:imm [:multi [:dec-prod "e" 4] [:inc-prod "p" 2] [:raise-tr 3]],
    :number 165,
    :title "Magnetic Field Generators",
    :cost 20,
    :tags "b",
    :type "g",
    :category "",
    :effect "Decrease your energy production 4 steps and increase your plant production 2 steps. Raise your TR 3 steps."}
   {:category "",
    :tags "t",
    :number 166,
    :b-discount [:space 2],
    :type "b",
    :title "Shuttles",
    :imm [:multi [:dec-prod "e" 1] [:inc-prod "m" 2]],
    :req "o5",
    :vp 1,
    :blue
    "Effect: When you play a space card, you pay 2 ME less for it.",
    :cost 10,
    :effect "Decrease your energy production 1 step and increase your ME production 2 steps. 1 vp."}
   {:imm [:inc-prod "h" 2],
    :number 167,
    :title "Import of Advanced GHG",
    :cost 9,
    :tags "et1",
    :type "r",
    :category "",
    :effect "Increase your heat production 2 steps."}
   {:category "",
    :tags "nb",
    :number 168,
    :type "g",
    :title "Windmills",
    :imm [:inc-prod "e" 1],
    :req "o7",
    :vp 1,
    :cost 6,
    :effect "Increase your energy production 1 step. 1 vp."}
   {:category "",
    :tags "p",
    :number 169,
    :type "g",
    :title "Tundra Farming",
    :imm [:multi [:inc-prod "p" 1] [:inc-prod "m" 2] [:gain 1 "p"]],
    :req "t-6",
    :vp 2,
    :cost 16,
    :effect "Increase your plant production 1 step and your ME production 2 steps. Gain 1 plant. 2 vp."}
   {:imm
    [:multi
     [:add-another "microbes" 2]
     [:inc-prod "h" 3]
     [:inc-prod "p" 1]],
    :number 170,
    :title "Aerobraked Ammonia Asteroid",
    :cost 26,
    :tags "t1",
    :type "r",
    :category "",
    :effect "Add 2 microbes to ANOTHER card.  Increase your heat production 3 steps and your plant production 1 step."}
   {:imm [:multi [:dec-prod "e" 2] [:inc-prod "p" 1] [:raise-tr 1]],
    :number 171,
    :title "Magnetic Field Dome",
    :cost 5,
    :tags "b",
    :type "g",
    :category "",
    :effect "Decrease your energy production 2 steps and increase your plant production 1 step. Raise your terraform rating 1 step."}
   {:category "",
    :tags "ea",
    :b-triggered {:t :anyone-places-city, :a [:add-this "animals" 1]},
    :number 172,
    :type "b",
    :b-special :no-removing-animals-from-here,
    :title "Pets",
    :vp-formula [1 2 :this-resources],
    :imm [:add-this "animals" 1],
    :blue
    "Effect: When any city tile is placed, add an animal to this card.  Animals may not be removed from this card.",
    :cost 10,
    :effect "Add 1 animal to this card. 1 VP per 2 animals here."}
   {:category "a",
    :tags "-",
    :number 173,
    :type "b",
    :b-special :no-removing-your-amp,
    :title "Protected Habitats",
    :blue "Opponents may not remove your plants, animals, or microbes.",
    :cost 5,
    :effect "-"}
   {:imm
    [:multi
     [:inc-prod "m" 2]
     [:place-greenery :water-spot-ignoring-restrictions]],
    :number 174,
    :title "Protected Valley",
    :cost 23,
    :tags "pb",
    :type "g",
    :category "",
    :effect "Increase your ME production 2 steps. Place a greenery tile ON AN AREA RESERVED FOR OCEAN, disregarding normal placement restrictions, and increase oxygen 1 step."}
   {:imm [:inc-prod-formula "m" [1 1 :your-space-tag]],
    :number 175,
    :title "Satellites",
    :cost 10,
    :tags "t",
    :type "g",
    :category "a",
    :effect "Increase your ME production 1 step for each space tag you have, including this."}
   {:category "",
    :tags "pb",
    :number 176,
    :type "g",
    :title "Noctis Farming",
    :imm [:multi [:inc-prod "m" 1] [:gain 2 "p"]],
    :req "t-20",
    :vp 1,
    :cost 10,
    :effect "Increase your ME production 1 step and gain 2 plants. 1 vp."}
   {:category "",
    :tags "b",
    :number 177,
    :type "b",
    :title "Water Splitting Plant",
    :b-action [:multi [:spend 3 "e"] [:raise-oxygen 1]],
    :req "w2",
    :blue "Action: Spend 3 energy to raise oxygen 1 step.",
    :cost 12,
    :effect "-"}
   {:category "",
    :tags "nb",
    :number 178,
    :type "g",
    :title "Heat Trappers",
    :imm [:multi [:dec-any-prod "h" 2] [:inc-prod "e" 1]],
    :vp -1,
    :cost 6,
    :effect "Decrease any heat production 2 steps and increase your energy production 1 step. -1 vp."}
   {:category "",
    :tags "b",
    :number 179,
    :type "g",
    :title "Soil Factory",
    :imm [:multi [:dec-prod "e" 1] [:inc-prod "p" 1]],
    :vp 1,
    :cost 9,
    :effect "Decrease your energy production 1 step and increase your plant production 1 step. 1 vp."}
   {:imm [:multi [:dec-prod "e" 1] [:inc-prod "t" 1] [:inc-prod "m" 1]],
    :number 180,
    :title "Fuel Factory",
    :cost 6,
    :tags "b",
    :type "g",
    :category "a",
    :effect "Decrease your energy production 1 step and increase your titanium production and your ME production 1 step each."}
   {:imm [:place-ocean],
    :number 181,
    :title "Ice Cap Melting",
    :cost 5,
    :req "t+2",
    :tags "1",
    :type "r",
    :category "",
    :effect "Place 1 ocean tile."}
   {:category "a",
    :tags "cb",
    :number 182,
    :type "g",
    :title "Corporate Stronghold",
    :imm [:multi [:dec-prod "e" 1] [:inc-prod "m" 3] [:place-city]],
    :vp -2,
    :cost 11,
    :effect "Decrease your energy production 1 step and increase your ME production 3 steps. Place a city tile. -2 vp."}
   {:category "",
    :tags "eb",
    :number 183,
    :type "g",
    :title "Biomass Combustors",
    :imm [:multi [:dec-any-prod "p" 1] [:inc-prod "e" 2]],
    :req "o6",
    :vp -1,
    :cost 4,
    :effect "Decrease any plant production 1 step and increase your energy production 2 steps. -1 vp."}
   {:category "",
    :tags "a",
    :number 184,
    :type "b",
    :title "Livestock",
    :vp-formula [1 1 :this-resources],
    :b-action [:add-this "animals" 1],
    :imm [:multi [:dec-prod "p" 1] [:inc-prod "m" 2]],
    :req "o9",
    :blue "Action: Add 1 animal to this card.",
    :cost 13,
    :effect "Decrease your plant production 1 step and increase your ME production 2 steps. 1 vp for each animal on this card."}
   {:category "a",
    :tags "seb",
    :b-triggered {:t :you-play-science, :a
                  [:choose
                   {:choice-tag :add-this-science-1, :imm [:add-this "science" 1]}
                   {:choice-tag :remove-1-science->draw-card, :imm [:multi [:remove-this "science" 1] [:draw-cards 1]]}]},
    :number 185,
    :type "b",
    :title "Olympus Conference",
    :vp 1,
    :blue
    "When you play a science tag, including this, either add a science resource to this card, or remove a science resource from this card to draw a card.",
    :cost 10,
    :effect "1 vp"}
   {:category "a",
    :tags "-",
    :number 186,
    :type "g",
    :title "Rad-Suits",
    :imm [:inc-prod "m" 1],
    :req "c2",
    :vp 1,
    :cost 6,
    :effect "Increase your ME production 1 step. 1 vp."}
   {:category "",
    :tags "b",
    :number 187,
    :type "b",
    :title "Aquifer Pumping",
    :b-action [:multi [:spend-with-steel 8 "m"] [:place-ocean]],
    :blue
    "Action: Spend 8 ME to place 1 ocean tile. STEEL MAY BE USED as if you were playing a building card.",
    :cost 18,
    :effect "-"}
   {:category "",
    :tags "1",
    :number 188,
    :type "r",
    :title "Flooding",
    :imm [:multi [:place-ocean] [:remove-any-from-adj-owner 4]],
    :vp -1,
    :cost 7,
    :effect "Place an ocean tile. IF THERE ARE TILES ADJACENT TO THIS OCEAN TILE, YOU MAY REMOVE 4 ME FROM THE OWNER OF ONE OF THOSE TILES. -1 vp."}
   {:imm [:inc-prod-formula "e" [1 1 :city-in-play]],
    :number 189,
    :title "Energy Saving",
    :cost 15,
    :tags "n",
    :type "g",
    :category "",
    :effect "Increase your energy production 1 step for each city tile in play."}
   {:imm
    [:multi
     [:spend 5 "h"]
     [:choose
      {:choice-tag :gain-4-p, :imm [:gain 4 "p"]}
      {:choice-tag :add-another-animals-2, :imm [:add-another "animals" 2]}]],
    :number 190,
    :title "Local Heat Trapping",
    :cost 1,
    :tags "1",
    :type "r",
    :category "",
    :effect "Spend 5 heat to either gain 4 plants, or to add 2 animals to ANOTHER card."}
   {:imm [:place-ocean],
    :number 191,
    :title "Permafrost Extraction",
    :cost 8,
    :req "t-8",
    :tags "1",
    :type "r",
    :category "",
    :effect "Place 1 ocean tile."}
   {:imm [:look-take 3 1],
    :number 192,
    :title "Invention Contest",
    :cost 2,
    :tags "s1",
    :type "r",
    :category "a",
    :effect "Look at the top 3 cards from the deck. Take 1 of them into hand and discard the other 2."}
   {:imm [:place-greenery],
    :number 193,
    :title "Plantation",
    :cost 15,
    :req "s2",
    :tags "p",
    :type "g",
    :category "",
    :effect "Place a greenery tile and raise oxygen 1 step."}
   {:category "a",
    :tags "nb",
    :number 194,
    :type "b",
    :title "Power Infrastructure",
    :b-action [:spend-any-gain-same "e" "m"],
    :blue
    "Action: Spend any amount of energy to gain that amount of ME.",
    :cost 4,
    :effect "-"}
   {:category "a",
    :tags "1",
    :number 195,
    :type "r",
    :title "Indentured Workers",
    :imm [:next-card-this-gen-8m-less],
    :vp -1,
    :cost 0,
    :effect "The next card you play this generation costs 8 ME less. -1 vp."}
   {:category "a",
    :tags "st",
    :number 196,
    :type "g",
    :title "Lagrange Observatory",
    :imm [:draw-cards 1],
    :vp 1,
    :cost 9,
    :effect "Draw 1 card. 1 vp."}
   {:category "a",
    :tags "jt",
    :number 197,
    :type "g",
    :title "Terraforming Ganymede",
    :imm [:raise-tr-formula [1 1 :your-jovian-tag]],
    :vp 2,
    :cost 33,
    :effect "Raise your TR 1 step for each Jovian tag you have, including this. 2 vp."}
   {:category "",
    :tags "et",
    :number 198,
    :type "g",
    :title "Immigration Shuttles",
    :vp-formula [1 3 :city-in-play],
    :imm [:inc-prod "m" 5],
    :cost 31,
    :effect "Increase your ME production 5 steps. 1 vp for every 3rd city in play."}
   {:category "a",
    :tags "s",
    :number 199,
    :type "b",
    :title "Restricted area",
    :b-action [:multi [:spend 2 "m"] [:draw-cards 1]],
    :imm [:place "restricted" :land-spot],
    :blue "Action: Spend 2 ME to draw a card.",
    :cost 11,
    :effect "Place this [restricted] tile."}
   {:category "",
    :tags "cb",
    :b-triggered {:t :anyone-places-city, :a [:inc-prod "m" 1]},
    :number 200,
    :type "b",
    :title "Immigrant City",
    :imm [:multi [:dec-prod "e" 1] [:dec-prod "m" 2] [:place-city]],
    :blue
    "Effect: Each time a city tile is placed, including this, increase your ME production 1 step.",
    :cost 13,
    :effect "Decrease your energy production 1 step and decrease your ME production 2 steps. Place a city tile."}
   {:category "a",
    :tags "n",
    :number 201,
    :type "g",
    :title "Energy Tapping",
    :imm [:multi [:dec-any-prod "e" 1] [:inc-prod "e" 1]],
    :vp -1,
    :cost 3,
    :effect "Decrease any energy production 1 step and increase your own 1 step. -1 vp."}
   {:category "",
    :tags "b",
    :number 202,
    :type "b",
    :title "Underground Detonations",
    :b-action [:multi [:spend 10 "m"] [:inc-prod "h" 2]],
    :blue
    "Action: Spend 10 ME to increase your heat production 2 steps.",
    :cost 6,
    :effect "-"}
   {:imm [:inc-prod "h" 7],
    :number 203,
    :title "Soletta",
    :cost 35,
    :tags "t",
    :type "g",
    :category "",
    :effect "Increase your heat production 7 steps."}
   {:imm [:draw-cards 2],
    :number 204,
    :title "Technology Demonstration",
    :cost 5,
    :tags "st1",
    :type "r",
    :category "a",
    :effect "Draw 2 cards."}
   {:imm [:multi [:dec-prod "e" 1] [:raise-tr 2]],
    :number 205,
    :title "Rad-Chem Factory",
    :cost 8,
    :tags "b",
    :type "g",
    :category "",
    :effect "Decrease your energy production 1 step. Raise your terraform rating 2 steps."}
   {:imm [:next-card-this-gen-tweak-global-requirements],
    :number 206,
    :title "Special Design",
    :cost 4,
    :tags "s1",
    :type "r",
    :category "",
    :effect "The next card you play this generation is +2 or -2 in global requirements, your choice."}
   {:category "a",
    :tags "sb",
    :number 207,
    :type "g",
    :title "Medical Lab",
    :imm [:inc-prod-formula "m" [1 2 :your-building-tag]],
    :vp 1,
    :cost 13,
    :effect "Increase your ME production 1 step for every 2 building tags you have, including this. 1 vp."}
   {:category "a",
    :tags "sb",
    :number 208,
    :type "b",
    :title "AI Central",
    :b-action [:draw-cards 2],
    :imm [:dec-prod "e" 1],
    :req "s3",
    :vp 1,
    :blue "Action: Draw 2 cards.",
    :cost 21,
    :effect "Decrease your energy production 1 step. 1 vp."}
   {:imm [:multi [:raise-temperature 1] [:remove-any 2 "p"]],
    :number 209,
    :title "Small Asteroid",
    :cost 10,
    :tags "t1",
    :type "r",
    :category "p",
    :effect "Increase temperature 1 step. Remove up to 2 plants from any player."}])

;; For the future (don't think like implementing it now, and it's a promo anyway)
#_{:number 210
    :title "Self-Replicating Robots"
    :cost 7
    :req "s2"
    :tags "-"
    :type "b"
    :category "p"
    :effect "-"
    :blue "Action: Reveal and place a SPACE OR BUILDING card here from hand, and place 2 resources on it, OR double the resources on a card here.  Effect: Cards here may be played as if from hand with its cost reduced by the number of resources on it."}

(defn cid-from-props [props]
  (-> props :title shorten-title))

(def all-corp-cids
  (map cid-from-props corp-cards))

(def all-project-cids
  (map cid-from-props project-cards))


;        01  02  03  04  05
;      06  07  08  09  10  11
;    12  13  14  15  16  17  18
;  19  20  21  22  23  24  25  26
;27  28  29  30  31  32  33  34  35
;  36  37  38  39  40  41  42  43
;    44  45  46  47  48  49  50
;      51  52  53  54  55  56
;        57  58  59  60  61

(def card-map (into {} (map (fn [props] [(cid-from-props props) props])
                            (concat corp-cards project-cards))))

(defn card-design-by-id [id]
  (get card-map id))

(def map-adjacency
  {1 [2 7 6]
   2 [3 8 7 1]
   3 [4 9 8 2]
   4 [5 10 9 3]
   5 [11 10 4]
   6 [1 7 13 12]
   7 [1 2 8 14 13 6]
   8 [2 3 9 15 14 7]
   9 [3 4 10 16 15 8]
   10 [4 5 11 17 16 9]
   11 [5 18 17 10]
   12 [6 13 20 19]
   13 [6 7 14 21 20 12]
   14 [7 8 15 22 21 13]
   15 [8 9 16 23 22 14]
   16 [9 10 17 24 23 15]
   17 [10 11 18 25 24 16]
   18 [11 26 25 17]
   19 [12 20 28 27]
   20 [12 13 21 29 28 19]
   21 [13 14 22 30 29 20]
   22 [14 15 23 31 30 21]
   23 [15 16 24 32 31 22]
   24 [16 17 25 33 32 23]
   25 [17 18 26 34 33 24]
   26 [18 35 34 25]
   27 [19 28 36]
   28 [19 20 29 37 36 27]
   29 [20 21 30 38 37 28]
   30 [21 22 31 39 38 29]
   31 [22 23 32 40 39 30]
   32 [23 24 33 41 40 31]
   33 [24 25 34 42 41 32]
   34 [25 26 35 43 42 33]
   35 [26 43 34]
   36 [27 28 37 44]
   37 [28 29 38 45 44 36]
   38 [29 30 39 46 45 37]
   39 [30 31 40 47 46 38]
   40 [31 32 41 48 47 39]
   41 [32 33 42 49 48 40]
   42 [33 34 43 50 49 41]
   43 [34 35 50 42]
   44 [36 37 45 51]
   45 [37 38 46 52 51 44]
   46 [38 39 47 53 52 45]
   47 [39 40 48 54 53 46]
   48 [40 41 49 55 54 47]
   49 [41 42 50 56 55 48]
   50 [42 43 56 49]
   51 [44 45 52 57]
   52 [45 46 53 58 57 51]
   53 [46 47 54 59 58 52]
   54 [47 48 55 60 59 53]
   55 [48 49 56 61 60 54]
   56 [49 50 61 55]
   57 [51 52 58]
   58 [52 53 59 57]
   59 [53 54 60 58]
   60 [54 55 61 59]
   61 [55 56 60]})

(def map-features
  {1 {:type "l" :bonus "ss"}
   2 {:type "w" :bonus "ss"}
   3 {:type "l"}
   4 {:type "w" :bonus "c"}
   5 {:type "w"}

   6 {:type "l"}
   7 {:type "l" :bonus "s" :other "lava-flows"}
   8 {:type "l"}
   9 {:type "l"}
   10 {:type "l"}
   11 {:type "w" :bonus "cc"}

   12 {:type "l" :bonus "c" :other "lava-flows"}
   13 {:type "l"}
   14 {:type "l"}
   15 {:type "l"}
   16 {:type "l"}
   17 {:type "l"}
   18 {:type "l" :bonus "s"}

   19 {:type "l" :bonus "pt" :other "lava-flows"}
   20 {:type "l" :bonus "p"}
   21 {:type "l" :bonus "p"}
   22 {:type "l" :bonus "p"}
   23 {:type "l" :bonus "pp"}
   24 {:type "l" :bonus "p"}
   25 {:type "l" :bonus "p"}
   26 {:type "w" :bonus "pp"}
   27 {:type "l" :bonus "pp" :other "lava-flows"}
   28 {:type "l" :bonus "pp"}
   29 {:type "special" :bonus "pp" :other "noctis-city"}
   30 {:type "w" :bonus "pp"}
   31 {:type "w" :bonus "pp"}
   32 {:type "w" :bonus "pp"}
   33 {:type "l" :bonus "pp"}
   34 {:type "l" :bonus "pp"}
   35 {:type "l" :bonus "pp"}

   36 {:type "l" :bonus "p"}
   37 {:type "l" :bonus "pp"}
   38 {:type "l" :bonus "p"}
   39 {:type "l" :bonus "p"}
   40 {:type "l" :bonus "p"}
   41 {:type "w" :bonus "p"}
   42 {:type "w" :bonus "p"}
   43 {:type "w" :bonus "p"}

   44 {:type "l"}
   45 {:type "l"}
   46 {:type "l"}
   47 {:type "l"}
   48 {:type "l"}
   49 {:type "l" :bonus "p"}
   50 {:type "l"}

   51 {:type "l" :bonus "ss"}
   52 {:type "l"}
   53 {:type "l"}
   54 {:type "l" :bonus "c"}
   55 {:type "l"}
   56 {:type "l" :bonus "t"}

   57 {:type "l" :bonus "s"}
   58 {:type "l" :bonus "ss"}
   59 {:type "l"}
   60 {:type "l"}
   61 {:type "w" :bonus "tt"}

   62 {:type "special" :other "ganymede"}
   63 {:type "special" :other "phobos"}})

(def N-SURFACE-AREAS 61)

(def zones-data
  [{:name "choosing-corp", :v :you, :per-player? true}
   {:name "hand", :v :you, :per-player? true}
   {:name "table-corp", :v :all, :facing :up, :per-player? true, :a true}
   {:name "table-blue", :v :all, :facing :up, :per-player? true, :a true}
   {:name "table-green", :v :all, :facing :up, :per-player? true}
   {:name "table-red", :v :all, :facing :down, :per-player? true}
   {:name "revealed", :v :all, :facing :up}
   {:name "deck", :v :none, :facing :down}
   {:name "corp-deck", :v :none, :facing :down}
   ;; XXX discard visibility is :none for non-solitaire
   {:name "discard", :v :all, :facing :down}])

(def solo-role "s")

(def all-roles [solo-role])

(def OXYGEN-TRACK {:starting 0, :goal 14})
(def TEMPERATURE-TRACK {:starting 0, :goal 19, :human-min -30, :human-step 2})
(def REMAINING-OCEANS {:starting 9, :goal 0})

(defn card-short-title [card]
  (-> card :title shorten-title))

(defn card-rep [card]
  (str (:number card) "_" (card-short-title card)))
;; We shorten names of megacredits, steel, titanium, plants, energy, heat to
;; reduce space of game log, because the values of these vars will change a
;; lot.

(def basic-resources  ["m"          "s"    "t"       "p"     "e"     "h"])
