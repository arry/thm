(ns thm.views.common
  (:require [reagent.core :as r]
            [baking-soda.core :as b]
            [re-frame.core :refer [dispatch subscribe]]
            [thm.utils :refer [bull dispatch-e html-entity format-timestamp
                               unrecognized]]
            [thm.texts :as t]
            [taoensso.timbre :refer-macros (debug warn)]
            [cljsjs/react] [react-dom] [react-flip-move] ;; The previous two are the deps for flip-move
            [clojure.string :as str])
  )

(defn indexed-component
  "Create a React-friendly (non-lazy) collection that consists of components
  returned from f, which is called with indices and values from sequential
  collection."
  [coll f]
  (when coll
    (doall (map-indexed (fn [i v]
                          (f i v))
                        coll))))

(defn autoscroller
  "A component that displays a container, given by el-name, with children,
  whose value is given by contents-reaction, and whose appearance is rendered
  by render-one-f.  This component will auto-scroll to bottom when a child is
  added; but only if its scroll position was already at the bottom."
  [el-name contents-reaction render-one-f]
  (let [do-scroll (fn [node]
                    (aset node "scrollTop" (.-scrollHeight node))
                    (aset node "_shouldScroll" false))]
    (r/create-class
     {:component-did-mount (fn [this]
                             (let [node (r/dom-node this)]
                               (do-scroll node)))
      :component-will-update (fn [this new-argv]
                               (let [props (r/props this)
                                     node (r/dom-node this)]
                                 ;; XXX probably save this in component props instead of on the node.
                                 (aset node "_shouldScroll"
                                       (= (+ (.-scrollTop node) (.-offsetHeight node))
                                          (.-scrollHeight node)))))
      :component-did-update (fn [this old-argv]
                              (let [node (r/dom-node this)]
                                (when (aget node "_shouldScroll")
                                  (do-scroll node))))
      :reagent-render (fn []
                        [el-name {:class "autoscroller"}
                         (doall (map-indexed (fn [i v]
                                               ^{:key i}
                                               [render-one-f v])
                                             @contents-reaction))])})))

(defn loader []
  [:img {:src "/images/loading.gif"}])

(defn loader-when-loading []
  (let [*loading? (subscribe [:loading?])]
    (fn []
      [:span (when @*loading? [loader])])))

(defn behind-page-loader [component]
  (let [*page-loading? (subscribe [:page-loading?])]
    (if @*page-loading?
      [loader]
      component)))

(defn behind-loader [component]
  (let [*loading? (subscribe [:loading?])]
    (if @*loading?
      [loader]
      component)))

(defn chat []
  (let [*chat-messages (subscribe [:chat-messages])
        submit-chat-form (fn [browser-event]
                           (let [form (.-target browser-event)
                                 input (aget (.-children form) 0)
                                 line (.-value input)]
                             (when (re-find #"\S" line)
                               (aset input "value" "")
                               (dispatch [:thm/send-chat-line line]))
                             (.preventDefault browser-event)))]
    (fn []
      [behind-page-loader
       [:div.chat
        [autoscroller :ul *chat-messages
         (fn [{:keys [author timestamp text]}]
           [:li
            "[" [:span.timestamp (format-timestamp timestamp)] "] "
            [:b.author author] ": "
            [:span text]])]
        [:form {:on-submit submit-chat-form}
         [:input {:type "text"}]]]])))

(defn event->human-href [event]
  (str "#" (name (first event))
       (when (-> event rest count pos?) (bull))
       (str/join (bull) (map #(if (fn? %) (str %)
                                  %)
                                  (rest event)))))

(defn action-link
  "Usage:
  [action-link element-props event-to-dispatch & contents]
  [action-link event-to-dispatch & contents]

  Create a Reagent component which is a link, which when clicked, dispatches an
  event and suppresses the browser event so URL bar isn't changed.  If
  element-props map is supplied, it's added to the :a element's props."
  [ep-or-event & other]
  (if (or (nil? ep-or-event)
          (map? ep-or-event))
    (let [element-props ep-or-event
          event-to-dispatch (first other)
          contents (rest other)]
      (into [:a (merge {:href (event->human-href event-to-dispatch)
                        :on-click #(dispatch-e event-to-dispatch %)}
                       element-props)]
            contents))
    (let [element-props {}
          event-to-dispatch ep-or-event
          contents other]
      (apply action-link element-props event-to-dispatch contents))))

(defn dropdown-action-link
  [options event-to-dispatch & contents]
  (into [:a.dropdown-item (merge {:href (event->human-href event-to-dispatch)
                                  :on-click (fn [e]
                                              (dispatch [:thm.game-ui/toggle-play-menu])
                                              (dispatch-e event-to-dispatch e))}
                                 options)]
        contents))

(defn link
  [href & contents]
  (let [f (first contents)]
    (if (map? f)
      (into [:a (merge {:href href} f)]
            (rest contents))
      (into [:a {:href href}]
            contents))))

(defn dropdown-link
  [href & contents]
  (into [:a.dropdown-item {:href href
                           :on-click #(dispatch [:thm.game-ui/toggle-play-menu])}]
        contents))

(defn outlink [href text]
  [:a.external {:href href :target "_blank" :rel "noopener noreferrer"} text])

(defn connection-status [*ws-open?]
  (if @*ws-open?
    [:span.connection-status.connected]
    [:span.connection-status.disconnected]))

(defn active-if [what *active-tab index]
  (if (or (= what @*active-tab)
          (and (nil? @*active-tab)
               (= index 0)))
    "active"
    ""))

(defn tabbed-component
  "Create a div with a .tabbed-component class, which consists of tab headers
  and tab contents.  Tabs are switched via dispatching the activate-message;
  current tab is determined by results of *active-tab subscription.  Triplets
  are tab-id, tab header (component rendered under the tab header div), and tab
  content (component rendered under the tab-content div)."
  [*active-tab activate-message triplets]
  [:div.tabbed-component
   [b/Nav {:tabs true}
    (indexed-component (remove nil? triplets)
                       (fn [i [id component _content-component]]
                         [b/NavItem {:key id}
                          [b/NavLink {:class (str (active-if id *active-tab i))
                                      :href (str "#" id)
                                      :on-click #(dispatch-e [activate-message id] %)}
                           component]]))]
   [b/TabContent {:active-tab @*active-tab}
    (indexed-component (remove nil? triplets)
                       (fn [i [id _header-component component]]
                         [b/TabPane {:tab-id id
                                     :key id
                                     :class (active-if id *active-tab i)}
                          component]))]])

(defn tabbed-component-ng
  "Like tabbed-component, but with more parameters:

  - class-name (it is put into class of the container div)

  - orientation: when :horizontal, tabs are horizontal and above contents, and
  when :vertical, tabs are vertical and to the left of contents.

  - on-tab-header-click: a callback function to be called after the
  activate-message is dispatched.

  "
  [orientation class-name
   *active-tab activate-message on-tab-header-click
   id+title+content-triplets]
  (let [triplets (filter some? id+title+content-triplets)
        active-tab (or @*active-tab (first (first triplets)))]
    [:div.tabbed-component-ng {:class [class-name (if (= orientation :vertical)
                                                    "vertical"
                                                    "horizontal")]}
     [b/Nav {:tabs true}
      (doall (for [[id title content :as triplet] triplets]
               ^{:key id}
               [b/NavItem
                [b/NavLink {:class-name (when (= active-tab id) "active")
                            :on-click #(do (dispatch-e [activate-message id] %)
                                           (when on-tab-header-click
                                             (on-tab-header-click)))}
                 title]]))]
     [b/TabContent {:active-tab active-tab}
      (doall (for [[id title content :as triplet] triplets]
               ^{:key id}
               [b/TabPane {:tab-id id}
                content]))]]))

(defn -avatar-img-helper [username & [more]]
  (if (str/blank? username)
    [:img.avatar {:src ""}]
    [:img.avatar {:src (str "/avatar/" username ".png"
                            (when (some? more)
                              (str "?" more)))}]))

(defn avatar-img [username]
  ;; Changing a parameter after the question mark forces Reagent to re-render
  ;; the image.
  (let [*timestamp (subscribe [:thm.account/avatar-update-timestamp-for username])]
    (fn [username]
      [-avatar-img-helper username @*timestamp])))

(defn your-avatar-img []
  (let [*username (subscribe [:username])
        *timestamp (subscribe [:thm.account/your-avatar-update-timestamp])]
    (fn []
      [-avatar-img-helper @*username @*timestamp])))

(defn my-button [arg1 & r]
  (let [[props contents] (if (map? arg1)
                           [arg1 r]
                           [{} (into [arg1] r)])
        props (update props :class #(if (re-find #"btn-(primary|success|info|warning|danger)" (or % ""))
                                      (str "btn " %)
                                      (str "btn btn-light " %)))]
    (into [:button props]
     contents)))

;; XXX the following two functions are very similar, but I don't know how to
;; unify them.
(defn outcome-for-humans [code [mode submode winner :as outcome]]
  (case mode
    "tie" [:span [:b (t/outcome-mode mode :full)]
           (t/text code :outcome-submode submode :full)]
    "win" [:span [:b winner (t/outcome-mode mode :full)]
           (t/text code :outcome-submode submode :full)]
    "abandoned" [:span (t/outcome-mode mode :full) [:b winner]]
    (unrecognized "mode of outcome" outcome)))

(defn past-outcome-for-humans [code [mode submode winner :as outcome]]
  (case mode
    "tie" [:span [:b (t/outcome-mode mode :short)]
           (t/text code :outcome-submode submode :short)]
    "win" [:span [:b winner] (t/outcome-mode mode :short)
           "("
           (t/text code :outcome-submode submode :short)
           ")."]
    "abandoned" [:span (t/outcome-mode mode :short) [:b winner] "."]
    (unrecognized "mode of past outcome" outcome)))

(defn not-found []
  [:div.container
   [:div "Not found " (bull)
    [link "/" "To main page"]]])

(defn modal-content-wrapper [title body & [footer]]
  [:div
   [b/ModalHeader {:toggle #(dispatch-e [:thm.game-ui/hide-modal] %)}
    title]
   [b/ModalBody
    body]
   (when (some? footer)
     [b/ModalFooter
      footer])])

(defn connection-indicator [connected?]
  (if connected?
    [:span.connected {:title "Online"} "\u2713"]
    [:span.disconnected {:title "Offline"} "\u2717"]))

(defn dispatch-resize-when-mounted [child-rendering-fn]
  (r/create-class
   {:component-did-mount (fn []
                           (dispatch [:resize]))
    :reagent-render child-rendering-fn}))

(def flip-move (r/adapt-react-class js/FlipMove))

(defn game-logo-src [game]
  (case (:code game)
    "td" "/images/td-logo.png"
    "viti" "/viti-images/viti-logo.png"
    "/images/unknown-card.png"))
