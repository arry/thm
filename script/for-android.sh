#!/bin/sh


# This script creates a compiled jar file that has a subset of thm site
# relevant to the TM native app and moves it to that app's project directory.

FROM_ROOT=~/home/clojure/thm
TO_ROOT=~/home/clojure/little
ANDROID_PROJECT_ROOT=~/home/android-projects/TerraformingMars

cp $FROM_ROOT/src/clj/thm/game_base.clj $TO_ROOT/src/thm
cp $FROM_ROOT/src/clj/thm/game_engine.clj $TO_ROOT/src/thm
cp $FROM_ROOT/src/cljc/thm/utils.cljc $TO_ROOT/src/thm
cp $FROM_ROOT/src/clj/thm/tm/tm_engine.clj $TO_ROOT/src/thm/tm
cp $FROM_ROOT/src/cljc/thm/tm/tm_data.cljc $TO_ROOT/src/thm/tm

cd $TO_ROOT
lein jar
cp $TO_ROOT/target/little-0.1.0-SNAPSHOT.jar $ANDROID_PROJECT_ROOT/app/libs/

echo "New jar is here: $ANDROID_PROJECT_ROOT/app/libs/little-0.1.0-SNAPSHOT.jar"
