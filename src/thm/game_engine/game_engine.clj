(ns thm.game-engine.game-engine
  "Common functionality for game engines."
  (:require [taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug)]
            [thm.game-engine.game-base :refer [new-lens zone-visible-to-role?
                                   translate-state translate-deltas translate-answer
                                   game-definition-by-code
                                   supply-answer perform-pending-actions
                                   all-playing-roles++ lens-creation-options
                                   ]]
            [clojure.edn]
            [thm.utils :refer [k-map finished? get-request-for-role player-name-by-role
                               map-values get-zone make-rnd-from-options now-timestamp]]
            )
  )


;; === Engine: atom that keeps a state and deals with communication.

(defn new-engine [code player-names & [opts]]
  (let [game-definition (game-definition-by-code code)
        state ((:new-game game-definition) player-names opts)
        {:keys [state deltas]} (perform-pending-actions state)
        engine (atom {:state state
                      :waypoints [] ;; records of each speculative state and deltas leading to it in the chain of requests
                      ;; role currently executing the chain of requests
                      :actor (when (= (count (:request-map state)) 1)
                               (first (keys (:request-map state))))
                      :past-deltas (into [[:text :*commit-timestamp* (now-timestamp)]]
                                    deltas) ;; all committed deltas
                      :step 0 ;; for tracking progress: each time a request chain is committed, step is increased
                      :lenses {}})]
    engine))

(defn create-checkpoint [engine]
  (let [m @engine]
    m))

(defn new-engine-from-checkpoint [code checkpoint mode]
  (let [m (cond-> checkpoint
            (nil? (:code (:state checkpoint))) ;; Grandfather old td games to get a correct code.
            (update :state assoc :code "td")

            (and (= mode :test)
                 (= (count (:request-map (:state checkpoint))) 1))
            (assoc :actor (first (keys (:request-map (:state checkpoint)))))

            (nil? (:waypoints checkpoint))
            (assoc :waypoints [])

            (nil? (:past-deltas checkpoint))
            (assoc :past-deltas []))]
    (atom m)))

(defn create-str-checkpoint [engine]
  (str (create-checkpoint engine)))

;; XXX This loses the predeterminedness of the stored java.util.Random values.
(defn new-engine-from-str-checkpoint [code str-checkpoint mode]
  (new-engine-from-checkpoint code
                              (clojure.edn/read-string
                               {:default (fn [t v]
                                           (if (and (= t 'object)
                                                    (vector? v)
                                                    (= (first v) 'java.util.Random))
                                             (make-rnd-from-options {})
                                             (do (warnf "Unknown tagged something: %s %s" t v)
                                                 nil)))}
                               str-checkpoint)
                              mode))


(defn- get-lens-for [engine role]
  (let [result (or (get-in @engine [:lenses role])
                   (let [state (:state @engine)]
                     (new-lens role (:code state)
                               (lens-creation-options state))))]
    (if (:code result)
      result
      (assoc result :code (or (:code (:state @engine)) "td")))))

(defn- get-current-state [engine-snapshot]
  (let [{:keys [state waypoints]} engine-snapshot]
    (if (empty? waypoints)
      state
      (:state (peek waypoints)))))

(defn- get-last-waypoint [engine-snapshot]
  (peek (:waypoints engine-snapshot)))

(defn translated-state-for [engine role]
  (let [{:keys [actor state waypoints past-deltas]} @engine
        cur-lens (get-lens-for engine role)
        waypoint (peek waypoints)
        [state these-past-deltas] (if (= role actor)
                                    (if waypoint
                                      [(:state waypoint) (into past-deltas (:deltas waypoint))]
                                      [state past-deltas])
                                    [state past-deltas])
        {:keys [result lens]} (translate-state cur-lens state these-past-deltas)]
    (swap! engine assoc-in [:lenses role] lens)
    result))

(defn translated-deltas-for [engine role deltas]
  (let [cur-lens (get-lens-for engine role)
        {:keys [lens result]} (translate-deltas cur-lens (:state @engine) deltas)]
    (swap! engine assoc-in [:lenses role] lens)
    result))

(defn delta-allows-cancel? [engine delta role]
  (let [[kind & args] delta]
    (if (= kind :cards-moved)
      (let [[from to cards] args
            state (:state @engine)
            from-visible? (zone-visible-to-role? (get-zone state from) role)
            to-visible? (zone-visible-to-role? (get-zone state to) role)
            result (not
                    ;; When cards are moved from invisible to visible zone, that's new
                    ;; information, and that prevents canceling.
                    (and (not from-visible?) to-visible?))]
        result)
      true)))

(defn determine-can-cancel [engine deltas role]
  (every? #(delta-allows-cancel? engine % role) deltas))

(defn all-playing-roles [engine]
  (let [state (:state @engine)]
    (all-playing-roles++ state)))

;; TODO translated-deltas-per-role should include the special *observer* role.

(defn handle-noncancel-answer [engine role client-answer]
  (let [lens (get-lens-for engine role)
        state (get-current-state @engine)
        request (get-request-for-role state role)
        server-answer (translate-answer lens client-answer request)
        _ (assert (some? server-answer)
                  (format "Cannot translate client answer %s: %s" client-answer lens))
        _ (debugf "handle-noncancel-answer: server-answer %s, role %s" (pr-str server-answer) (pr-str role))
        {:keys [state delta]} (supply-answer state role server-answer)
        {:keys [state deltas]} (perform-pending-actions state)
        new-deltas (into [[:text :*commit-timestamp* (now-timestamp)] delta] deltas)
        request-map (:request-map state)
        last-delta (peek new-deltas)
        last-delta-kind (first last-delta)
        {:keys [actor waypoints past-deltas step]} @engine

        ;; Clear waypoints; send to the actor only the new deltas,
        ;; while to the laggards old+new deltas.
        commit (fn [new-actor]
                 (let [old-deltas (apply concat (map :deltas waypoints))
                       tdpr (k-map #(translated-deltas-for engine %
                                                           (if (= % actor)
                                                             new-deltas
                                                             (concat old-deltas new-deltas)))
                                   (all-playing-roles engine))]
                   (swap! engine assoc
                          :state (assoc state :can-cancel? false)
                          :past-deltas (into past-deltas (concat old-deltas new-deltas))
                          :actor new-actor
                          :waypoints []
                          :step ((fnil inc 0) step))
                   {:state state,
                    :translated-deltas-per-role tdpr}))

        ;; Add waypoint: send only to the actor new deltas, while others don't
        ;; know anything.
        add-waypoint (fn []
                       (let [can-cancel? (determine-can-cancel engine new-deltas actor)]
                         (swap! engine update
                                :waypoints conj {:state (assoc state :can-cancel? can-cancel?)
                                                 :deltas new-deltas})
                         {:state state,
                          :translated-deltas-per-role
                          {actor (translated-deltas-for engine actor
                                                        (conj new-deltas [:-can-cancel-request? can-cancel?]))}}))
        ]
    (cond
      (#{:game-finished :request-answered} last-delta-kind)
      (commit nil)

      (not= last-delta-kind :request-put)
      (do
        (warnf "Last delta should be game-finished, request-answered or request-put: %s"
               {:new-deltas new-deltas})
        ;; Still continue, because this can happen for dev purposes.
        (commit nil))

      (>= (count (second last-delta)) 2)
      (commit nil)

      (not= (first (keys (second last-delta))) actor)
      (commit (first (keys (second last-delta))))

      :else
      (add-waypoint))))

(defn reverse-delta [[kind & args :as delta]]
  (case kind
    :text [:remove-last-text]
    :var-changed (let [[var-name amount new-value] args]
                   [:var-changed var-name (- amount) (- new-value amount)])
    :cards-created (let [[zn cards cids] args]
                     [:cards-destroyed zn cids cards])
    :cards-destroyed (let [[zn cids cards] args]
                       [:cards-created zn cards cids])
    :cards-moved (let [[from to cids] args]
                   [:cards-moved to from cids])
    :ns-cards-moved (let [[from to cids delta-opts] args]
                      ;; XXX doesn't handle the facedown change
                      [:ns-cards-moved to from cids nil])
    :var-set (let [[var-name value old-value] args]
               [:var-set var-name old-value value])
    :var-unset (let [[var-name old-value] args]
                 [:var-set var-name old-value nil])
    :vars-set (let [[& var-new-old-triplets] args]
                (into [:vars-set] (map (fn [[k new old]]
                                         [k old new])
                                       var-new-old-triplets)))
    :card-props-set (let [[zn cid prop-new-old-triplets] args]
                      [:card-props-set zn cid (mapv (fn [[k new old]]
                                                      [k old new])
                                                    prop-new-old-triplets)])
    :card-props-unset (let [[zn cid prop-old-pairs] args]
                        [:card-props-set zn cid (mapv (fn [[k old]]
                                                        [k old nil])
                                                      prop-old-pairs)])
    :card-props-changed (let [[zn cid prop-change-new-triplets] args
                              untriplets (mapv (fn [[prop change new]]
                                                 [prop (- change) (- new change)])
                                               prop-change-new-triplets)]
                          [:card-props-changed zn cid untriplets])
    :figure-created (let [[space-name figure-id index] args]
                      [:figure-destroyed space-name figure-id index])
    :figure-destroyed (let [[space-name figure-id index] args]
                        [:figure-created space-name figure-id index])
    :figure-moved (let [[from-name to-name figure-id from-index to-index options] args]
                    [:figure-moved to-name from-name figure-id to-index from-index options])
    (:request-put
     :deck-shuffled
     :request-answered
     :game-finished
     :pause) nil))

(defn reverse-deltas [deltas]
  (->> deltas
       reverse
       (map reverse-delta)
       (remove nil?)))

(defn handle-cancel
  [engine role]
  (let [cur-state (get-current-state @engine)
        request (get-request-for-role cur-state role)]
    (assert request (format "Must have a request for player %s" role))
    (assert (:can-cancel? cur-state) "Request must be cancelable")
    (let [{:keys [waypoints actor state]} @engine
          new-waypoints (pop waypoints)
          last-waypoint (peek waypoints)
          deltas (:deltas last-waypoint)
          new-state (get-current-state {:state state
                                        :waypoints new-waypoints})
          request-map (:request-map new-state)
          deltas-to-send (vec (concat
                               [[:request-answered actor]]
                               (reverse-deltas deltas)
                               [[:request-put request-map]
                                [:-can-cancel-request? (:can-cancel? new-state)]]))]
      (swap! engine assoc
             :waypoints new-waypoints)
      {:state state
       :translated-deltas-per-role {actor
                                    (translated-deltas-for engine actor deltas-to-send)}})))

(defn put-answer!
  "This is the main method of interaction with the engine.  When the game isn't
  finished, whenever we can observe the engine, it's waiting for an answer.
  Putting an answer into it produces a sequence of deltas as the engine is run.
  Not all those deltas are visible immediately to the clients, though: when one
  player performs a chain of requests, only he sees the changes it produces.
  This function returns (a hash with) translated-deltas-per-role, which are to
  be sent to the clients."
  [engine role client-answer]
  (if (= client-answer "*cancel*")
    (handle-cancel engine role)
    (handle-noncancel-answer engine role client-answer)))

(defn player-role [state player-name]
  (get (:player-name->role state) player-name))

(defn get-role-for-username [engine username]
  (let [state (:state @engine)]
    (or (player-role state username) "*observer*")))

(defn engine-finished? [engine]
  (finished? (:state @engine)))

(defn engine-outcome [engine]
  (:outcome (:state @engine)))

(defn runtime-info [state]
  (let [game-definition (game-definition-by-code (:code state))]
    {:player-name->role (:player-name->role state)
     :current-players (if (finished? state)
                        []
                        (sort (map #(player-name-by-role state %)
                                   (keys (:request-map state)))))
     :timing ((:state-timing game-definition) state)}))

(defn engine-runtime-info [engine]
  (merge (runtime-info (:state @engine))
         {:step (:step @engine)}))

(defn get-recent-deltas [deltas]
  (let [counter (atom 0)
        current (atom nil)
        get-mark (fn [delta]
                   (if (= (first delta) :request-put)
                     (vec (sort (keys (second delta))))
                     :none))
        partitioner (fn [delta]
                      (when (and (= (first delta) :request-put)
                                 (not= @current (get-mark delta)))
                        (reset! current (get-mark delta))
                        (swap! counter inc))
                      @counter)
        ;; Each group has a number of deltas, which start either from
        ;; beginning, or with one player's request-put, and continues until
        ;; either other player's request-put or to end.
        groups (partition-by partitioner deltas)
        c (count groups)]
    (if (= (first (peek deltas)) :game-finished)
      (last groups)
      (first (drop (- c 2) groups)))))

(defn get-recent-changes-for [engine role]
  (let [{:keys [waypoints actor state past-deltas]} @engine
        relevant-deltas (if (and (= actor role) (seq waypoints))
                          (peek waypoints)
                          (get-recent-deltas past-deltas))
        var-changeds (filter #(= (first %) :var-changed) relevant-deltas)]
    (reduce (fn [cur [_ var-name amount new-value :as delta]]
              (update cur var-name (fnil + 0) amount))
            {}
            var-changeds)))
