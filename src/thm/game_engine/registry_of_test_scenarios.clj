(ns thm.game-engine.registry-of-test-scenarios)

(defonce ^:dynamic *scenarios-by-code* (atom {}))

(defn register-scenarios [code scenarios]
  (swap! *scenarios-by-code* assoc code scenarios))

(defn scenarios-by-code [code]
  (get @*scenarios-by-code* code))
