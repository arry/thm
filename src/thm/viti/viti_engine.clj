(ns thm.viti.viti-engine
  "Engine for Viticulture."
  (:require
   [thm.game-engine.game-base :refer [defaction-coded def-answer-validator
                          has-action? perform-sequence
                          perform-action set-vars text do-nothing
                          all-playing-roles++
                          register-game-definition]]
   [thm.game-engine.game-engine :as game-engine]
   [taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug spy)]
   [clojure.string :as str]
   [thm.utils :refer [one-dim-map
                      two-dim-map
                      make-rnd-from-options
                      prepare-zones
                      shuffle-with-random
                      first-index
                      get-var
                      denumber-id
                      all-cards-in-zone
                      only-role&answer
                      get-all-figures-in-space
                      to-integer
                      last-part-of-multipart
                      multipart-name-from-descr
                      shift-so-it-starts-with
                      find-first
                      split-multipart
                      not-empty?
                      n-cards-in-zone
                      sum
                      range-inclusive
                      seq-contains?
                      ]]
   [thm.viti.viti-utils :refer [get-figure-id
                                get-figure-type
                                state-timing
                                get-n-available-figures-of-type
                                get-first-available-figure-of-type
                                ]]
   [thm.viti.viti-data :refer [zones-data
                               green-designs yellow-designs purple-designs blue-designs mama-designs papa-designs
                               cid->design
                               all-field-names]]
   ))

(def GAME-CODE "viti")

(def VERSION 1)

(def all-roles ["r" "y"]) ;; XXX

;; == Utils

(def all-wine-recipes
  [{:wine-kind :red
    :min-space 1
    :red 1
    :white 0
    :grapes [:red]}
   {:wine-kind :white
    :min-space 1
    :red 0
    :white 1
    :grapes [:white]}
   {:wine-kind :blush
    :min-space 4
    :red 1
    :white 1
    :grapes [:white :red]}
   {:wine-kind :sparkling
    :min-space 7
    :red 2
    :white 1
    :grapes [:white :red :red]}])

(def wine-kind->recipe (into {} (map (fn [recipe]
                                       [(:wine-kind recipe) recipe])
                                     all-wine-recipes)))


(defn grape-color [grape]
  (first grape))

(defn grape-value [grape]
  (second grape))

(defn create-grape [color value]
  [color value])


(defn all-playing-roles [state]
  (:ordered-roles state))

(defn design-by-cid [cid]
  (cid->design (denumber-id cid)))

(def structures-data
  [{:name :trellis, :space "t", :cost 2}
   {:name :windmill, :space "n", :cost 5}
   {:name :irrigation, :space "i", :cost 3}
   {:name :cottage, :space "c", :cost 4}
   {:name :tasting-room, :space "s", :cost 6}
   {:name :medium-cellar, :space "m", :cost 4}
   {:name :large-cellar, :space "l", :cost 6, :prereq :medium-cellar}
   {:name :yoke, :space "y-s", :cost 2}])

(def structure->data (into {} (map (fn [data]
                                     [(:name data) data])
                                   structures-data)))

(def space->structure-name (into {} (map (fn [data]
                                           [(:space data) (:name data)])
                                         structures-data)))

(defn get-structure-space-name [role structure-name]
  (str role ":" (-> structure->data structure-name :space)))

(defn get-rooster-left-space [number]
  (str "m:r" number "l"))

(defn get-rooster-right-space [number]
  (str "m:r" number "r"))

(defn get-rooster-full-space [number]
  (str "m:r" number "f"))

(defn get-rooster-season-space [season number]
  ((case season
     "summer" get-rooster-left-space
     "winter" get-rooster-right-space
     (assert false (str "Bad season for rooster space: " season)))
   number))

(defn get-rooster-other-season-space [season number]
  (get-rooster-season-space (if (= season "winter") "summer" "winter") number))

(defn get-player-available-workers-space [role]
  (str role ":workers"))

(def rooster-rest-space "m:r0f")

(defn get-number-of-rooster-space [space-name]
  ;; m:r1f
  ;; 01234
  (to-integer (subs space-name 3 4)))

(defn get-role-of-figure [figure-id]
  (when (some? figure-id)
    (subs figure-id 0 1)))

(defn space-is-free? [state space-name]
  (empty? (get-all-figures-in-space state space-name)))

(defn space-is-occupied? [state space-name]
  (not-empty? (get-all-figures-in-space state space-name)))

(defn space-name-from-wine-by-parts [role kind value]
  (str role ":c"
       (case kind
         :red "r"
         :white "w"
         :blush "b"
         :sparkling "s"
         (assert false (str "Unrecognized wine kind: " kind)))
       value))

(defn space-name-from-wine [role wine]
  (space-name-from-wine-by-parts role (:kind wine) (:value wine)))


(def crushpad-color->abbreviation {:red "r" :white "w"})

(defn crushpad-space-name-of-value [role color value]
  (str role ":" "p" (crushpad-color->abbreviation color) value))

(defn find-free-crushpad-space-number [state role color value]
  (find-first (fn [number]
                (space-is-free? state (crushpad-space-name-of-value role color number)))
   (range value 0 -1)))

(def all-grape-values (range-inclusive 1 9))

(defn space-name-from-grape-by-parts [role color value]
  (str role ":p"
       (case color
         :red "r"
         :white "w"
         (assert false (str "Unrecognized grape color: " color)))
       value))

(defn space-name-from-grape [role grape]
  ;; eg ["y" [:white 2] => "y:pw2"
  (space-name-from-grape-by-parts role (grape-color grape) (grape-value grape)))


(defn can-sell-any-grape? [state role]
  (some #(or
          (space-is-occupied? state
                              (crushpad-space-name-of-value role :red %))
          (space-is-occupied? state
                              (crushpad-space-name-of-value role :white %)))
        all-grape-values))

(defn collect-planted-vines-per-zone [state role]
  (let [result (atom {})]
    (doseq [field-name all-field-names
            :let [zone-name (multipart-name-from-descr [role field-name])]
            cid (all-cards-in-zone state zone-name)]
      (swap! result update zone-name (fnil conj []) cid))
    @result))

(defn has-structure? [state role structure-name]
  (space-is-occupied? state (get-structure-space-name role structure-name)))

(defn field-harvested-var [role field-name]
  ;; fh is short for field harvested
  (multipart-name-from-descr ["fh" role field-name]))

(defn field-unharvested-this-year? [state role field-name]
  (zero? (get-var state (field-harvested-var role field-name) 0)))

(defn field-red-value-var [role field-name]
  ["-frv" role field-name])

(defn field-white-value-var [role field-name]
  ["-fwv" role field-name])

(defn get-field-red-value [state role field-name]
  (get-var state (field-red-value-var role field-name) 0))

(defn get-field-white-value [state role field-name]
  (get-var state (field-white-value-var role field-name) 0))

(defn get-field-total-value [state role field-name]
  (+ (get-field-red-value state role field-name)
     (get-field-white-value state role field-name)))


(defn collect-harvestable-fields [state role]
  (filter (fn [field-name]
            ;; TODO and has at least one available crushpad space of matching
            ;; or lowest value (and warn if harvests two and wastes one of
            ;; them).
            (and (field-unharvested-this-year? state role field-name)
                 (pos? (get-field-total-value state role field-name))))
          all-field-names))

(defn has-harvestable-field? [state role]
  (not-empty? (collect-harvestable-fields state role)))

(defn can-harvest-field? [state role _bonus-applies?]
  ;; LATER with friendly variant and bonus-applies?, check that there are at
  ;; least two.
  (has-harvestable-field? state role))

(defn has-any-planted-vine? [state role]
  (not-empty? (collect-planted-vines-per-zone state role)))

(defn can-perform-yoke? [state role]
  (and (has-structure? state role :yoke)
       (or (has-any-planted-vine? state role)
           (can-harvest-field? state role false))))


;; == New game

(defn prepare-deck [zones deck-name designs rnd]
  (assoc-in zones [deck-name :cards] (vec (shuffle-with-random (map :cid designs) rnd))))

(defn get-player-supply-space [role]
  (str role ":supply"))

(defn new-game [player-names & [opts]]
  (assert (= (count player-names) 2) "Must have exactly two players")
  (let [rnd (make-rnd-from-options opts)
        ;; XXX take into account players' color preferences, or randomize the
        ;; roles.
        all-playing-roles (vec (shuffle-with-random all-roles rnd))
        player-name->role (zipmap player-names all-playing-roles)
        role->player-name (clojure.set/map-invert player-name->role)
        first-player (first all-playing-roles)]
    {:code GAME-CODE
     :version VERSION
     :ordered-roles all-playing-roles
     :player-name->role player-name->role
     :role->player-name role->player-name
     :vars (into (sorted-map) (merge {"first-player" first-player
                                      "year" 1
                                      "season" "setup"
                                      "-counter" 1} ;; next grapes/wine token counter
                                     (one-dim-map "vp:" all-playing-roles 0)
                                     (one-dim-map "rp:" all-playing-roles 0)
                                     (one-dim-map "lira:" all-playing-roles 0)
                                     (two-dim-map "nc:" all-playing-roles ["green" "yellow" "purple" "blue"] 0)))
     :spaces (into {rooster-rest-space (mapv #(get-figure-id % :rooster) all-playing-roles)
                    (get-rooster-right-space 7) [(get-figure-id :neutral :worker)]}
                   (mapcat (fn [role]
                             [[(get-player-available-workers-space role)
                               [(get-figure-id role :worker 1)
                                (get-figure-id role :worker 2)
                                (get-figure-id role :grande)]]

                              [(get-player-supply-space role) [(get-figure-id role :worker 3)
                                                               (get-figure-id role :worker 4)
                                                               (get-figure-id role :worker 5)

                                                               ;; XXX all names from structures-data
                                                               (get-figure-id role :trellis)
                                                               (get-figure-id role :windmill)
                                                               (get-figure-id role :irrigation)
                                                               (get-figure-id role :cottage)
                                                               (get-figure-id role :tasting-room)
                                                               (get-figure-id role :medium-cellar)
                                                               (get-figure-id role :large-cellar)
                                                               (get-figure-id role :yoke)]]])
                           all-playing-roles))
     :zones (-> (prepare-zones zones-data all-playing-roles)
                (prepare-deck "green-deck" green-designs rnd)
                (prepare-deck "yellow-deck" yellow-designs rnd)
                (prepare-deck "purple-deck" purple-designs rnd)
                (prepare-deck "blue-deck" blue-designs rnd)
                (prepare-deck "mama-deck" mama-designs rnd)
                (prepare-deck "papa-deck" papa-designs rnd))
     :pending-actions [[:text :game-starts]
                       :setup
                       :main-sequence]
     :rnd rnd}))

;; == Action helpers

(defn space-available? [state space-name]
  (empty? (get-all-figures-in-space state space-name)))

(def all-wakeup-numbers (vec (range 1 8)))

(defn get-available-rooster-spaces [state]
  ;; Note: we place roosters to the left space (r1l), while ask to choose full
  ;; space (r1f).
  (->> all-wakeup-numbers
       (filter #(space-available? state (get-rooster-left-space %)))
       (map get-rooster-full-space)))

(defn who-does-season-turn-next [state season number]
  (let [numbers (if (or (nil? number) (= number 7))
                  all-wakeup-numbers
                  (shift-so-it-starts-with all-wakeup-numbers (inc number)))
        matching-rows (for [number numbers
                            :let [figures (get-all-figures-in-space state (get-rooster-season-space season number))]
                            :when (not-empty? figures)
                            :let [figure (first figures)]
                            :when (= (get-figure-type figure) :rooster)]
                        [number (get-role-of-figure figure)])]
    (if (empty? matching-rows)
      nil
      (first matching-rows))))

(defn find-player-rooster-number [state role season]
  (find-first (fn [number]
                (= (-> (get-all-figures-in-space state
                                                 (get-rooster-season-space season number))
                       first
                       get-role-of-figure)
                   role))
              all-wakeup-numbers))

(defn get-id-of-first-worker-in-supply [state role]
  (find-first #(= (get-figure-type %) :worker)
   (get-all-figures-in-space state (get-player-supply-space role))))

(defn has-worker-in-supply? [state role]
  (some? (get-id-of-first-worker-in-supply state role)))

;; == Game actions

(defmacro defaction
  [n & forms]
  `(defaction-coded ~GAME-CODE ~n ~@forms))

(defaction :setup [state]
  (perform-sequence state
                    [:setup-mamapapas]))

(defaction :setup-mamapapas [state]
  (perform-sequence state
                    [:set-var "setting-up-mamapapas" true]
                    [:deal-mamapapas]
                    [:apply-mamas]
                    [:apply-papas]
                    [:set-var "setting-up-mamapapas" false]))

(defaction :do-in-player-order [state action-tag]
  (apply perform-sequence state
         (map (fn [role]
                [action-tag role])
              (shift-so-it-starts-with (:ordered-roles state)
                                       (get-var state "first-player")))))

(defaction :deal-mamapapas [state]
  (perform-action state
                  [:do-in-player-order :deal-one-mamapapa]))

(defaction :deal-one-mamapapa [state role]
  (perform-sequence state
                    [:draw-cards "mama-deck" [role "starting"] 1]
                    [:draw-cards "papa-deck" [role "starting"] 1]))

(defaction :apply-mamas [state]
  (perform-sequence state
                    [:do-in-player-order :apply-one-mama]))

(defaction :apply-one-mama [state role]
  (let [cid (first (all-cards-in-zone state [role "starting"]))
        design (design-by-cid cid)
        _ (assert (= (:type design) "mama")
                  (format "The first dealt starting card should be a mama but is %s" design))
        effects (:effects design)

        actions (for [effect [:two-lira :g :y :p :b]
                      :let [amount (get effects effect 0)]
                      :when (pos? amount)]
                  (case effect
                    :two-lira [:gain-lira role (* 2 amount)]
                    :g [:viti-draw-cards role "green-deck" amount]
                    :y [:viti-draw-cards role "yellow-deck" amount]
                    :p [:viti-draw-cards role "purple-deck" amount]
                    :b [:viti-draw-cards role "blue-deck" amount]))]
    (apply perform-sequence state
           [:text :mama-is-applied role (denumber-id cid)]
           actions)))

(defaction :change-var-logged [state var-descr amount text-descr-part]
  (perform-sequence state
                    [:change-var var-descr amount]
                    [:log-var-change var-descr amount text-descr-part]))

(defaction :log-var-change [state var-descr amount text-descr-part]
  (perform-sequence state
                    (-> [:text]
                        (into text-descr-part)
                        (conj amount)
                        (conj (get-var state var-descr)))))

(defaction :gain-lira [state role amount]
  (perform-sequence state
                    [:change-var-logged ["lira" role] amount [:gains-lira role]]))

(defaction :pay-lira-silently [state role amount]
  (perform-sequence state
                    [:change-var ["lira" role] (- amount)]))

(defaction :space-gain-1-lira [state role _bonus-applies?]
  (perform-sequence state
                    [:gain-lira role 1]))

(defaction :apply-papas [state]
  (perform-sequence state
                    [:do-in-player-order :apply-one-papa]))

(defn mode-from-papa-lira [lira]
  (assert (<= 1 lira 6)
          (str "Papa or-lira should be from 1 to 6 but got " lira))
  [:lira lira])

(defaction :apply-one-papa [state role]
  (let [cid (last (all-cards-in-zone state [role "starting"]))
        design (design-by-cid cid)
        _ (assert (= (:type design) "papa")
                  (format "The last dealt starting card should be a papa but is %s" design))
        {:keys [base-lira or-stuff or-lira]} design
        stuff-mode or-stuff
        lira-mode (mode-from-papa-lira or-lira)]
    (perform-sequence state
                      [:text :papa-is-applied role (denumber-id cid)]
                      [:gain-lira role base-lira]
                      [:easy-put-request role
                       [:choose-mode :choose-papa-effect [stuff-mode lira-mode]]
                       [:on-papa-effect-chosen]])))

(defaction :on-papa-effect-chosen [state]
  (let [[role mode] (only-role&answer state)]
    (perform-action state
                    (if (and (vector? mode) (= (first mode) :lira))
                      [:gain-lira role (second mode)]
                      (case mode
                        :one-vp [:gain-vp role 1]

                        :worker [:gain-available-worker role]

                        (:cottage
                         :irrigation
                         :medium-cellar
                         :tasting-room
                         :trellis
                         :windmill
                         :yoke) [:build-structure-for-free role mode]

                        (assert false
                                (str "Unrecognized papa effect mode: " mode)))))))

(defaction :gain-vp [state role amount]
  (perform-sequence state
                    [:change-var-logged ["vp" role] amount [:gains-vp role]]))

(defaction :gain-rp [state role amount]
  (perform-sequence state
                    [:change-var-with-limits ["rp" role] amount 0 5]
                    [:log-var-change ["rp" role] amount [:gains-rp role]]))

(defaction :gain-available-worker [state role]
  (perform-sequence state
                    [:text :gains-available-worker role]
                    [:move-figure
                     (get-player-supply-space role)
                     (get-player-available-workers-space role)
                     (get-id-of-first-worker-in-supply state role)]))

(defaction :do-build-structure [state role structure-name]
  (perform-sequence state
                    [:move-figure
                     (get-player-supply-space role)
                     (get-structure-space-name role structure-name)
                     (get-figure-id role structure-name)]))

(defaction :build-structure-for-free [state role structure-name]
  (perform-sequence state
                    [:text :builds-structure role structure-name]
                    [:do-build-structure role structure-name]))

(defaction :main-sequence [state]
  (perform-sequence state
                    [:text :year-starts (get-var state "year")]
                    :do-spring
                    :do-summer
                    :do-fall
                    :do-winter
                    :year-end
                    :check-game-end))

(defaction :season-starts [state season]
  (perform-sequence state
                    [:text :season-starts season]
                    [:set-var "season" season]))

(defaction :do-spring [state]
  (perform-sequence state
                    [:season-starts "spring"]
                    [:do-in-player-order :place-rooster-get-bonus]))

(defaction :place-rooster-get-bonus [state role]
  (let [spaces (get-available-rooster-spaces state)]
    (perform-sequence state
                      [:easy-put-request role
                       [:choose-space :choose-rooster-space spaces {}]
                       [:on-rooster-space-chosen]])))

(defaction :on-rooster-space-chosen [state]
  (let [[role space-name] (only-role&answer state)
        number (get-number-of-rooster-space space-name)]
    (perform-sequence state
                      [:text :chooses-wakeup-row role number]
                      [:move-figure rooster-rest-space (get-rooster-left-space number)
                       (get-figure-id role :rooster)]
                      [:apply-wakeup-bonus role number])))

(def wakeup-bonuses
  {1 [:nothing]
   2 [:draw "green-deck"]
   3 [:draw "purple-deck"]
   4 [:gain-lira 1]
   5 [:choose-draw ["yellow-deck" "blue-deck"]]
   6 [:gain-vp 1]
   7 [:gain-neutral-worker]})

(defaction :apply-wakeup-bonus [state role number]
  (let [[bonus-tag bonus-arg] (wakeup-bonuses number)]
    (perform-sequence state
                      (case bonus-tag
                        :nothing [:do-nothing]
                        :draw [:viti-draw-cards role bonus-arg 1]
                        :gain-lira [:gain-lira role bonus-arg]
                        :choose-draw [:choose-draw-cards role bonus-arg 1]
                        :gain-vp [:gain-vp role bonus-arg]
                        :gain-neutral-worker [:gain-neutral-worker role]
                        [:do-nothing]))))

(def card-type->discard-name
  {"green" "green-discard"
   "yellow" "yellow-discard"
   "purple" "purple-discard"
   "blue" "blue-discard"})

(def deck-or-discard->card-type
  {"green-discard" "green"
   "yellow-discard" "yellow"
   "purple-discard" "purple"
   "blue-discard" "blue"

   "green-deck" "green"
   "yellow-deck" "yellow"
   "purple-deck" "purple"
   "blue-deck" "blue"})

(defaction :viti-draw-cards [state role deck-name amount]
  ;; TODO if the deck runs out, reshuffle; if there are not enough cards to
  ;; draw, try reshuffle; if deck and discard are empty, draw less.  In :text,
  ;; actual amount drawn / mention reshuffle / mention not enough cards.
  ;; Also TODO later, actual drawn cards, translated only for you.
  (perform-sequence state
                    [:text :draws-cards role deck-name amount]
                    [:update-n-cards-of-type role (deck-or-discard->card-type deck-name) amount]
                    [:draw-cards deck-name [role "hand"] amount]))

(defaction :viti-move-card [state from to cid]
  (perform-action state [:viti-move-cards from to [cid]]))

(defaction :update-n-cards-of-type [state role card-type amount]
  (perform-action state
                  [:change-var ["nc" role card-type] amount]))

(defaction :update-n-cards-of-type-by-cids [state role cids sign]
  (let [groups (group-by (fn [cid]
                           (:type (design-by-cid cid)))
                         cids)]
    (apply perform-sequence state
           (for [[card-type group-cids] groups]
             [:update-n-cards-of-type role card-type (* sign (count group-cids))]))))


(defaction :viti-move-cards [state from-descr to-descr cids]
  (let [[from1 from2] (split-multipart from-descr)
        [to1 to2] (split-multipart to-descr)]
    (perform-sequence state
                      (when (= from2 "hand")
                        [:update-n-cards-of-type-by-cids from1 cids -1])

                      (when (= to2 "hand")
                        [:update-n-cards-of-type-by-cids to1 cids +1])

                      [:move-cards from-descr to-descr cids])))




(defaction :choose-draw-cards [state role deck-names amount]
  (assert (= amount 1) "For now only 1 card is supported")
  (perform-sequence state
                    [:easy-put-request role
                     [:choose-space :choose-deck-to-draw (mapv #(str "m:" %) deck-names) {}]
                     [:on-choose-draw-deck-chosen]]))

(defaction :on-choose-draw-deck-chosen [state]
  (let [[role space-name] (only-role&answer state)
        deck-name (last-part-of-multipart space-name)]
    (perform-sequence state
                      [:viti-draw-cards role deck-name 1])))

(def neutral-worker-rest-space
  (get-rooster-right-space 7))

(defaction :gain-neutral-worker [state role]
  (perform-sequence state [:move-figure
                           neutral-worker-rest-space
                           (get-player-available-workers-space role)
                           (get-figure-id :neutral :worker)]))

(defaction :do-worker-placement-season [state season number]
  (if-let [[number role] (who-does-season-turn-next state season number)]
    (perform-sequence state
                      [:do-turn role season]
                      [:do-worker-placement-season season number])
    (do-nothing state)))

(defn can-draw-card? [state role deck-name]
  ;; It's assumed that as soon as the deck runs out, the discard is reshuffled;
  ;; so if the deck is empty it means the discard is empty too.
  (pos? (n-cards-in-zone state deck-name)))

(defn can-draw-vine? [state role _bonus-applies?]
  (can-draw-card? state role "green-deck"))

(defn can-draw-wine-order? [state role _bonus-applies?]
  (can-draw-card? state role "purple-deck"))

(defaction :space-draw-vine [state role bonus-applies?]
  (perform-sequence state
                    [:viti-draw-cards role "green-deck" (if bonus-applies? 2 1)]))

(defaction :space-draw-wine-order [state role bonus-applies?]
  (perform-sequence state
                    [:viti-draw-cards role "purple-deck" (if bonus-applies? 2 1)]))

(defn tasting-room-applied-var [role]
  (str "-tra:" role))

(defn has-wine? [state role wine-kind value]
  (space-is-occupied? state (space-name-from-wine-by-parts role wine-kind value)))

(defn has-any-wine? [state role]
  (not-empty? (for [recipe all-wine-recipes
                    value (range-inclusive (:min-space recipe) 9)
                    :when (has-wine? state role (:wine-kind recipe) value)]
                :has)))

(defaction :check-tasting-room-bonus [state role]
  (if (and (has-structure? state role :tasting-room)
           (zero? (get-var state (tasting-room-applied-var role) 0))
           (has-any-wine? state role))
    (perform-sequence state
                      [:text :gets-tasting-room-bonus role]
                      [:set-var (tasting-room-applied-var role) 1]
                      [:gain-vp role 1])
    (do-nothing state)))



(defaction :space-give-tour [state role bonus-applies?]
  (perform-sequence state
                    [:gain-lira role (if bonus-applies? 3 2)]
                    [:check-tasting-room-bonus role]))

(defn get-player-lira [state role]
  (get-var state ["lira" role]))

(defn get-buildable-structures [state role bonus-applies?]
  (let [effective-lira (+ (get-player-lira state role)
                          (if bonus-applies? 1 0))]
    (filter (fn [{:keys [cost prereq name]}]
              (and (not (has-structure? state role name))
                   (>= effective-lira cost)
                   (or (nil? prereq)
                       (has-structure? state role prereq))))
            structures-data)))

(defn can-build-any-structure? [state role bonus-applies?]
  (not-empty? (get-buildable-structures state role bonus-applies?)))

(defaction :space-build-structure [state role bonus-applies?]
  (let [space-names (->> (get-buildable-structures state role bonus-applies?)
                         (mapv (fn [structure]
                                 (str role ":" (:space structure)))))]
    (assert (not-empty? space-names)
            (str "Must have any structures to build: " role bonus-applies?))
    (perform-sequence state
                      (when bonus-applies?
                        [:gain-lira role 1])
                      [:easy-put-request role
                       [:choose-space :choose-structure-to-build
                        space-names]
                       [:on-structure-to-build-space-chosen]])))

(defn get-structure-name-from-space [space-name]
  (let [[role subname] (split-multipart space-name)
        result (space->structure-name subname)]
    (assert (some? result)
            (str "Must have a structure for space name " space-name))
    result))

(defaction :on-structure-to-build-space-chosen [state]
  (let [[role space-name] (only-role&answer state)
        structure-name (get-structure-name-from-space space-name)
        cost (-> structure->data structure-name :cost)]
    (perform-sequence state
                      [:text :builds-structure-for-cost role structure-name cost]
                      [:pay-lira-silently role cost]
                      [:do-build-structure role structure-name])))

(defn can-play-any-summer-visitor? [state role _bonus-applies?]
  ;; TODO collect summer visitors, filter playable ones
  false)


(defn can-buy-field? [state role]
  ;; TODO
  false)

(defn can-sell-field? [state role]
  ;; TODO
  false)

(defn can-sell-grapes-or-deal-in-fields? [state role _bonus-applies?]
  (or (can-sell-any-grape? state role)
      (can-buy-field? state role)
      (can-sell-field? state role)))

(defn vine-value [vine-design]
  (+ (:red vine-design) (:white vine-design)))

(defn vine-value-by-cid [vine-cid]
  (vine-value (design-by-cid vine-cid)))



(defaction :update-field-values-by-cid [state role field-name vine-cid & [options]]
  (let [{:keys [red white]} (design-by-cid vine-cid)
        [red white] (if (:negative? options)
                      [(- red) (- white)]
                      [red white])]
    (perform-sequence state
                      [:change-var (field-red-value-var role field-name) red]
                      [:change-var (field-white-value-var role field-name) white])))

(defn field-capacity [field-name]
  (case field-name
    "f5" 5
    "f6" 6
    "f7" 7
    (assert false
            (str "Unrecognized field name: " field-name))))

(defn meets-vine-prerequisites? [state role vine-design]
  (case (:req vine-design)
    :trel (has-structure? state role :trellis)
    :irr (has-structure? state role :irrigation)
    :trel+irr (and (has-structure? state role :trellis)
                   (has-structure? state role :irrigation))
    true))

(defn collect-fields-where-vine-is-plantable [state role vine-design]
  (let [vine-value (vine-value vine-design)]
    (filter (fn [field-name]
              ;; TODO and not sold
              (<= (+ (get-field-total-value state role field-name)
                     vine-value)
                  (field-capacity field-name)))
            all-field-names)))

(defn is-plantable? [state role vine-design]
  (and (meets-vine-prerequisites? state role vine-design)
       (not-empty? (collect-fields-where-vine-is-plantable state role vine-design))))

(defn collect-plantable-cids [state role]
  (for [cid (all-cards-in-zone state [role "hand"])
        :let [design (design-by-cid cid)]
        :when (and (= (:type design) "green")
                   (is-plantable? state role design))]
    cid))

(defn can-plant-any-vine? [state role _bonus-applies?]
  (not-empty? (collect-plantable-cids state role)))

(defn hand-zone-name [role]
  (str role ":hand"))

(def WORKER-COST 4)

(defn can-train-worker? [state role bonus-applies?]
  (and (has-worker-in-supply? state role)
       (>= (get-player-lira state role) (if bonus-applies? (dec WORKER-COST) WORKER-COST))))

(defaction :choose-plant-vine [state role options]
  (let [cids (collect-plantable-cids state role)]
    (if (empty? cids)
      (if (:skip-if-impossible? options)
        (perform-sequence state
                          [:text :cannot-plant-second-vine role])
        (assert false
                (str "Must have plantable vines: " role)))
      (perform-sequence state
                        [:easy-put-request role [:choose-card :choose-vine-to-plant
                                                 {(hand-zone-name role) cids}]
                         [:on-vine-to-plant-chosen]]))))

(defn get-field-space-name [role field-name]
  (str role ":" field-name))

(defaction :on-vine-to-plant-chosen [state]
  (let [[role cid] (only-role&answer state)
        design (design-by-cid cid)
        field-names (collect-fields-where-vine-is-plantable state role design)]
    (assert (not-empty? field-names)
            ("Must have fields where to plant a vine: " role cid))
    (perform-sequence state
                      [:easy-put-request role
                       [:choose-space [:choose-field-to-plant-vine (denumber-id cid)]
                        (mapv #(get-field-space-name role %) field-names)]
                       [:on-field-to-plant-vine-chosen cid]])))

(defn get-field-name-from-field-space-name [space-name]
  (last-part-of-multipart space-name))

(defaction :on-field-to-plant-vine-chosen [state vine-cid]
  (let [[role field-space-name] (only-role&answer state)
        field-name (get-field-name-from-field-space-name field-space-name)]
    (perform-sequence state
                      [:text :plants-vine role (denumber-id vine-cid) field-name]
                      [:do-plant-vine role vine-cid field-name])))

(defn windmill-applied-var [role]
  (str "-wa:" role))

(defaction :check-windmill-bonus [state role]
  (if (and (has-structure? state role :windmill)
           (zero? (get-var state (windmill-applied-var role) 0)))
    (perform-sequence state
                      [:text :gets-windmill-bonus role]
                      [:set-var (windmill-applied-var role) 1]
                      [:gain-vp role 1])
    (do-nothing state)))

(defaction :do-plant-vine [state role vine-cid field-name]
  (perform-sequence state
                    [:viti-move-card [role "hand"]
                     [role field-name]
                     vine-cid]
                    [:update-field-values-by-cid role field-name vine-cid]
                    [:check-windmill-bonus role]))

(defaction :space-plant-vine [state role bonus-applies?]
  (perform-sequence state
                    [:choose-plant-vine role {}]
                    (when bonus-applies?
                      [:choose-plant-vine role {:skip-if-impossible? true}])))

(defaction :space-fill-wine-order [state role bonus-applies?]
  (perform-sequence state
                    (when bonus-applies?
                      [:gain-vp role 1])
                    [:choose-fill-order role]))

(defaction :space-train-worker [state role bonus-applies?]
  (perform-sequence state
                    (when bonus-applies?
                      [:gain-lira role 1])
                    [:pay-lira-silently role WORKER-COST]
                    [:text :trains-worker-for-cost role WORKER-COST]
                    [:move-figure
                     (get-player-supply-space role)
                     "m:tw@"
                     (get-id-of-first-worker-in-supply state role)]))





(defn can-play-any-winter-visitor? [state role _bonus-applies?]
  ;; TODO
  false)


(defn get-wine-values-by-kind-highest-to-lowest [state role wine-kind]
  (filterv
   (fn [value]
     (has-wine? state role wine-kind value))
   (range 9 (dec (:min-space (wine-kind->recipe wine-kind))) -1)))

(defn is-fillable? [state role order-design]
  (let [wines (:wines order-design)]
    (every?
     (fn [[kind required-values]]
       (let [actual-values (get-wine-values-by-kind-highest-to-lowest state role kind)]
         (and (>= (count actual-values) (count required-values))
              ;; Note: relying on the fact that the same-color wines on the
              ;; order cards are always sequential, like b5 b6.  N-th highest
              ;; value is covers the minimum required value.
              (>= (get actual-values (dec (count required-values)))
                  (first required-values)))))
     wines)))

(defn collect-fillable-wine-orders [state role]
  (for [cid (all-cards-in-zone state [role "hand"])
        :let [design (design-by-cid cid)]
        :when (and (= (:type design) "purple")
                   (is-fillable? state role design))]
    cid))

(defn can-fill-any-wine-order? [state role _bonus-applies?]
  (not-empty? (collect-fillable-wine-orders state role)))

(defaction :choose-fill-order [state role]
  (let [cids (collect-fillable-wine-orders state role)]
    (if (empty? cids)
      (assert false
              (str "Must have fillable orders: " role))
      (perform-sequence state
                        [:easy-put-request role [:choose-card :choose-order-to-fill
                                                 {(hand-zone-name role) cids}]
                         [:on-order-to-fill-chosen]]))))

(defn find-wines-to-fill-order-design [state role order-design]
  (let [wines (:wines order-design)]
    (vec (for [[wine-kind required-values] wines
               :let [actual-values (->> (get-wine-values-by-kind-highest-to-lowest state role wine-kind)
                                        reverse
                                        ;; Note: see `is-fillable?`.
                                        (drop-while #(< % (first required-values)))
                                        (take (count required-values)))]
               value actual-values]
           {:kind wine-kind
            :value value}))))

(defaction :disappear-wines [state role wines]
  (let [supply-space (get-player-supply-space role)]
  (apply perform-sequence state
         (mapcat (fn [wine]
                   (let [space-name (space-name-from-wine role wine)
                         token (first (get-all-figures-in-space state space-name))]
                     [[:move-figure space-name supply-space token]
                      [:destroy-figure supply-space token]]))
                 wines))))

(defaction :on-order-to-fill-chosen [state]
  (let [[role cid] (only-role&answer state)
        design (design-by-cid cid)
        wines (find-wines-to-fill-order-design state role design)]
    (perform-sequence state
                      [:text :fills-order role (denumber-id cid) wines]
                      [:viti-move-card [role "hand"] "purple-discard" cid]
                      [:disappear-wines role wines]
                      [:gain-vp role (:vp design)]
                      [:gain-rp role (:rp design)])))

(defaction :choose-harvest-field [state role options]
  (let [field-names (collect-harvestable-fields state role)]
    (if (empty? field-names)
      (if (:skip-if-impossible? options)
        (perform-sequence state
                          [:text :cannot-harvest-second-field role])
        (assert false
                (str "Must have harvestable fields: " role)))
      (perform-sequence state
                        [:easy-put-request role [:choose-space :choose-field-to-harvest
                                                 (mapv #(get-field-space-name role %) field-names)]
                         :on-field-to-harvest-chosen]))))

(defaction :on-field-to-harvest-chosen [state]
  (let [[role field-space-name] (only-role&answer state)
        field-name (get-field-name-from-field-space-name field-space-name)]
    (perform-sequence state
                      [:text :harvests-field role field-name]
                      [:do-harvest-field role field-name])))

(defaction :do-harvest-field [state role field-name]
  (let [red (get-field-red-value state role field-name)
        white (get-field-white-value state role field-name)]
    (perform-sequence state
                      [:set-var (field-harvested-var role field-name) 1]
                      (when (pos? red)
                        [:add-grape-token-from-field role :red red field-name])
                      (when (pos? white)
                        [:add-grape-token-from-field role :white white field-name]))))

(defn get-next-token-id [state]
  (str "token:" (get-var state "-counter")))

(defaction :update-next-token-id [state]
  (perform-action state [:change-var "-counter" 1]))

(defaction :add-grape-token-from-field [state role color value field-name]
  (let [actual-value (find-free-crushpad-space-number state role color value)]
    (if (nil? actual-value)
      (perform-sequence state
                        [:text :wastes-grape role color value])
      (let [figure-id (get-next-token-id state)]
        (perform-sequence state
                          (if (= actual-value value)
                            [:text :adds-grape-token role color value]
                            [:text :adds-devalued-grape-token role color actual-value value])
                          [:create-figure (get-field-space-name role field-name) figure-id]
                          [:update-next-token-id]
                          [:move-figure (get-field-space-name role field-name)
                           (crushpad-space-name-of-value role color actual-value)
                           figure-id])))))

(defaction :space-harvest-field [state role bonus-applies?]
  (perform-sequence state
                    [:choose-harvest-field role {}]
                    (when bonus-applies?
                      [:choose-harvest-field role {:skip-if-impossible? true}])))

(defn get-n-grapes-of-color [state role color]
  (count (filter #(space-is-occupied? state
                                      (crushpad-space-name-of-value role color %))
                 all-grape-values)))

(defn has-enough-grapes? [state role color amount]
  (or (zero? amount) ;; this check is not strictly necessary, but here for
                     ;; optimization.
      (>= (get-n-grapes-of-color state role color) amount)))

(defn can-make-wine-by-recipe? [state role recipe]
  ;; TODO and has space enough in the cellar.  TODO for combined wines, make
  ;; sure that has necessary structures, and there is a sum that fits into the
  ;; minimum requirement. (e.g. 1r+1w cannot make a blush wine because the min
  ;; is 4.)
  (and (has-enough-grapes? state role :red (:red recipe))
       (has-enough-grapes? state role :white (:white recipe))))

(defn get-applicable-recipes [state role]
  (filter #(can-make-wine-by-recipe? state role %)
          all-wine-recipes))

(defn can-make-any-wine? [state role _bonus-applies?]
  ;; LATER in friendly mode, check that there are three wine making
  ;; possibilities.
  (not-empty? (get-applicable-recipes state role)))

(defaction :space-make-wine [state role bonus-applies?]
  (perform-sequence state
                    [:choose-make-one-wine-token role {}]
                    [:choose-make-one-wine-token role {:can-skip? true, :skip-if-impossible? true, :n 2}]
                    ;; TODO when skipped or auto-skipped second, silently auto-skip third.
                    (when bonus-applies?
                      [:choose-make-one-wine-token role {:can-skip? true, :skip-if-impossible? true, :n 3}])))


(defn collect-grape-values [state role color]
  (filterv
   (fn [value]
     (let [space-name (space-name-from-grape-by-parts role color value)]
       (space-is-occupied? state space-name)))
   (range-inclusive 1 9)))

(defn cellar-space-is-empty? [state role wine-kind value]
  (space-is-free? state (space-name-from-wine-by-parts role wine-kind value)))

(defn has-sufficient-cellar? [state role value]
  (cond (>= value 7) (has-structure? state role :large-cellar)
        (>= value 4) (has-structure? state role :medium-cellar)
        :else true))

(defn build-wine-devaluation-mapping
  "For a given recipe (a cellar row), returns a mapping of a wine value to the
  actual value that would get placed into that row were we to make this wine.

  E.g., when there are sparkling wines at 7 and 9:

  1 2 3 4 5 6 7 8 9
  - - - - - - s . s

  the resulting mapping is {7 nil, 8 8, 9 8}: if we are to make a 7-value
  sparkling wine, we cannot place it in the cellar; a 8-value will fit at
  8-slot, and a 9-value will devalue to a 8-slot as well.

  "
  [state role recipe]
  (reduce (fn [result value]
            (assoc result
                   value (if (cellar-space-is-empty? state role (:wine-kind recipe) value)
                           value
                           (get result (dec value)))))
          {} (range-inclusive (:min-space recipe) 9)))

(defn has-grape-by-parts? [state role color value]
  (space-is-occupied? state (space-name-from-grape-by-parts role color value)))

(defn build-wine-making-variants-map
  "This function returns a map of maps:

  wine-kind -> wine-value -> list of variants

  Wine making variant is a vector of grapes, e.g. [[:red 2] [:white 3]] for a
  blush-5 wine.

  If a player has red-2, red-3, white-2, and white-3 grapes, there are two
  variants to make a blush wine: [[:red 2] [:white 3]] and [[:red 3] [:white
  2]].  This list will be under :blush -> 5."

  [state role wine-kind highest-wine grape-set]
  (let [devaluation (build-wine-devaluation-mapping state role (wine-kind->recipe wine-kind))
        all-variants (case wine-kind
                            :red (for [v all-grape-values]
                                   [[:red v]])
                            :white (for [v all-grape-values]
                                     [[:white v]])
                            :blush (for [r all-grape-values
                                         w all-grape-values]
                                     [[:red r], [:white w]])
                            :sparkling (for [r1 all-grape-values
                                             r2 (range-inclusive (inc r1) 9)
                                             w all-grape-values]
                                         [[:red r1]
                                          [:red r2]
                                          [:white w]]))]
    (reduce (fn [result variant]
              (if-not (every? grape-set variant)
                result
                (let [wine-value (sum (map grape-value variant))
                      actual-value (get devaluation (min highest-wine wine-value))]
                  (if (nil? actual-value)
                    result
                    (let [row (get result actual-value [])
                          new-row (conj row variant)]
                      (assoc result actual-value new-row))))))
            (sorted-map) all-variants)))

(defn get-all-grapes-in-possession [state role]
  (for [color [:red :white]
        value all-grape-values
        :when (has-grape-by-parts? state role color value)]
    (create-grape color value)))

(defn build-set-of-grapes-in-possession [state role]
  (into #{} (get-all-grapes-in-possession state role)))

(defn get-player-highest-wine-value [state role]
  (cond (has-structure? state role :large-cellar) 9
        (has-structure? state role :medium-cellar) 6
        :else 3))

(defn collect-all-wine-making-variants [state role]
  (let [highest-wine (get-player-highest-wine-value state role)
        grape-set (build-set-of-grapes-in-possession state role)]
    (into {} (for [{:keys [wine-kind]} all-wine-recipes]
               [wine-kind (build-wine-making-variants-map state role wine-kind
                                                          highest-wine grape-set)]))))

(defn wine-spaces-from-all-variants [role all-variants]
  (for [[wine-kind variants] all-variants
        wine-value (keys variants)]
    (space-name-from-wine-by-parts role wine-kind wine-value)))

;; This is the beginning of another, more user-friendly, take on the
;; wine-making procedure.  The old one requires you to select first the recipe,
;; then the tokens by that recipe, and can fail if there's no space in the
;; cellar, or the chosen grapes are too low.  In the new one, you select the
;; empty cellar space, then the grapes to match it.  I've put it on hold
;; because it was taking too long, and the old take is enough for the demo
;; purposes.

(defaction :choose-make-one-wine-token-take2 [state role {:keys [can-skip? skip-if-impossible? n]}]
  (let [all-variants (collect-all-wine-making-variants state role)
        spaces (wine-spaces-from-all-variants role all-variants)]
    (if (empty? spaces)
      (if skip-if-impossible?
        (perform-sequence state
                          [:text :cannot-make-wine-token role n])
        (assert false
                (str "Must have at least one makeable wine space " role)))
      (perform-sequence state
                        [:easy-put-request role
                         [:choose-space :choose-wine-to-make
                          spaces]
                         [:on-wine-to-make-space-chosen all-variants]]))))

(defn grape-from-space-name [space-name]
  ;; eg y:pw2 => [:white 2]
  (let [subname (last-part-of-multipart space-name)
        color-letter (subs subname 1 2)
        value-character (subs subname 2 3)]
    [(if (= color-letter "r") :red :white)
     (to-integer value-character)]))

(defn wine-from-space-name [space-name]
  ;; eg y:cb8 => {:kind :blush, :value 8}
  (let [subname (last-part-of-multipart space-name)
        kind-letter (subs subname 1 2)
        value-character (subs subname 2 3)]
    {:kind (case kind-letter
              "r" :red
              "w" :white
              "b" :blush
              "s" :sparkling
              (assert false (str "Unrecognized wine kind letter in space " space-name)))
     :value (to-integer value-character)}))

(defn simple-wine? [wine]
  (#{:red :white} (:kind wine)))

(defaction :on-wine-to-make-space-chosen [state all-variants]
  (let [[role space-name] (only-role&answer state)
        wine (wine-from-space-name space-name)

        variants (get-in all-variants [(:kind wine) (:value wine)])]
    (if (and (simple-wine? wine)
             (= (count variants) 1)
             (= (grape-value (first (first variants))) (:value wine)))
      (perform-sequence state
                        [:do-make-wine role (:kind wine) (first variants)])
      (perform-sequence state
                        [:choose-wine-making-variant role wine variants]))))

(defn build-space-variants-from-grape-variants
  "From grape-variants, which is a sequence of sequences of grapes, create a
  corresponding vector of vectors of space names."
  [role grape-variants]
  (mapv (fn [variant]
          (mapv #(space-name-from-grape role %) variant))
        grape-variants))

(defaction :choose-wine-making-variant [state role wine variants]
  (let [space-variants (build-space-variants-from-grape-variants role variants)]
    (debug "from variants got space-variants" variants space-variants)
    (perform-sequence state
                      [:easy-put-request role
                       [:choose-several-spaces-from-variants [:choose-grapes-to-make-wine wine]
                        space-variants]
                       [:on-wine-making-variant-chosen wine]])))

(defaction :on-wine-making-variant-chosen [state wine]
  (let [[role spaces] (only-role&answer state)
        grapes (map grape-from-space-name spaces)]
    (perform-sequence state
                      [:do-make-wine role (:kind wine) grapes])))

(defaction :choose-make-one-wine-token [state role {:keys [can-skip? skip-if-impossible? n]}]
  (let [recipes (get-applicable-recipes state role)]
    (if (empty? recipes)
      (if skip-if-impossible?
        (perform-sequence state
                          [:text :cannot-make-wine-token role n])
        (assert false
                (str "Must have at least one applicable wine recipe: " role)))
      (let [modes (cond-> (mapv :wine-kind recipes)
                    can-skip? (conj :skip))]
        (perform-sequence state
                          [:easy-put-request role
                           [:choose-mode :choose-wine-recipe-to-apply modes]
                           :on-wine-recipe-chosen])))))

(defn collect-grape-spaces [state role color]
  (vec (for [value all-grape-values
             :let [space-name (crushpad-space-name-of-value role color value)]
             :when (space-is-occupied? state space-name)]
         space-name)))

(defaction :on-wine-recipe-chosen [state]
  (let [[role mode] (only-role&answer state)]
    (if (= mode :skip)
      (perform-sequence state
                        [:text :doesnt-make-wine role])
      (let [recipe (wine-kind->recipe mode)]
        (perform-sequence state
                          [:easy-put-request role
                           [:choose-spaces-from-bags :choose-grape-tokens
                            (cond-> []
                              (pos? (:red recipe))
                              (conj {:spaces (collect-grape-spaces state role :red)
                                     :amount (:red recipe)})

                              (pos? (:white recipe))
                              (conj {:spaces (collect-grape-spaces state role :white)
                                     :amount (:white recipe)}))]
                           [:on-grapes-chosen recipe]])))))

(defaction :on-grapes-chosen [state recipe]
  (let [[role answer] (only-role&answer state)
        all-spaces (apply concat answer)
        all-grapes (map grape-from-space-name all-spaces)]
    (perform-sequence state
                      [:do-make-wine role (:wine-kind recipe) all-grapes])))

(def min-wine-space-for-kind
  {:sparkling 7
   :blush 4
   :white 1
   :red 1})

(defn find-free-wine-space-number [state role wine-kind value]
  (find-first (fn [number]
                (space-is-free? state (space-name-from-wine-by-parts role wine-kind number)))
              (range (min (get-player-highest-wine-value state role) value)
                     (dec (min-wine-space-for-kind wine-kind))
                     -1)))

(defaction :move-single-figure [state from-space to-space]
  (let [figures (get-all-figures-in-space state from-space)]
    (assert (= (count figures) 1)
            (str "Must have exactly one figure, but got " (count figures) ": " figures))
    (perform-action state [:move-figure from-space to-space (first figures)])))

(defaction :do-make-wine [state role wine-kind all-grapes]
  (let [all-grapes (vec all-grapes)
        total (sum (map grape-value all-grapes))

        grape-spaces (map #(space-name-from-grape role %) all-grapes)

        wine-number (find-free-wine-space-number state role wine-kind total)
        wine {:kind wine-kind
              :value wine-number}
        wine-space (space-name-from-wine role wine)]
    (if (nil? wine-space)
      ;; XXX this is bad, because it wastes grapes after it has told that
      ;; action is available, and presents the wasteful choices.
      (perform-sequence state
                        [:text :wastes-wine role wine-kind all-grapes])
      (perform-sequence state
                        (if (= wine-number total)
                          [:text :makes-wine role wine all-grapes]
                          [:text :makes-wine-devalued role wine total all-grapes])
                        [:move-single-figure (first grape-spaces) wine-space]
                        [:destroy-grapes-in-spaces role (rest grape-spaces)]))))

(defaction :space-sgf [state role bonus-applies?]
  (let [modes (cond-> []
                (can-sell-any-grape? state role)
                (conj :sell-grapes)

                (can-buy-field? state role)
                (conj :buy-field)

                (can-sell-field? state role)
                (conj :sell-field))]
    (perform-sequence state
                      (when bonus-applies?
                        [:gain-vp role 1])
                      [:easy-put-request role
                       [:choose-mode :choose-sgf-mode
                        modes]
                       :on-sgf-mode-chosen])))

(defaction :on-sgf-mode-chosen [state]
  (let [[role mode] (only-role&answer state)]
    (perform-sequence state
                      (case mode
                        :sell-grapes [:choose-sell-grapes role]
                        :buy-field [:choose-buy-field role]
                        :sell-field [:choose-sell-field role]
                        (assert false
                                (str "Unrecognized sgf mode: " mode))))))

(defaction :choose-sell-grapes [state role]
  (let [grapes (get-all-grapes-in-possession state role)
        space-names (mapv #(space-name-from-grape role %) grapes)]
    (perform-sequence state
                      [:easy-put-request role
                       [:choose-spaces :choose-grapes-to-sell
                        space-names
                        {:lower 1, :higher (count space-names)}]
                       :on-grapes-to-sell-chosen])))

(def grape-value->sell-value
  {1 1, 2 1, 3 1
   4 2, 5 2, 6 2
   7 3, 8 3, 9 3})

(defn grape-sell-value [grape]
  (grape-value->sell-value (grape-value grape)))

(defaction :on-grapes-to-sell-chosen [state]
  (let [[role space-names] (only-role&answer state)
        grapes (map grape-from-space-name space-names)
        total (sum (map grape-sell-value grapes))]
    (perform-sequence state
                      [:text :sells-grapes role grapes total]
                      [:destroy-grapes-in-spaces role space-names]
                      [:gain-lira role total])))

(defaction :destroy-grapes-in-spaces [state role space-names]
  (let [supply-space (get-player-supply-space role)]
    (perform-sequence state
                      (into [:action-sequence]
                            (mapcat (fn [grape-space]
                                      (let [token (first (get-all-figures-in-space state grape-space))]
                                        [[:move-figure grape-space supply-space token]
                                         [:destroy-figure supply-space token]]))
                                    space-names)))))


(def action-spaces
  {"summer"
   [{:prefix "dv" ;; draw vine
     :can-perform? can-draw-vine?
     :action-tag :space-draw-vine}
    {:prefix "gt" ;; give tour
     :can-perform? (constantly true)
     :action-tag :space-give-tour}
    {:prefix "bs" ;; build structure
     :can-perform? can-build-any-structure?
     :action-tag :space-build-structure}
    {:prefix "sv" ;; play summer visitor
     :can-perform? can-play-any-summer-visitor?
     :action-tag :space-play-summer-visitor}
    {:prefix "sgf" ;; sell 1+ grapes or buy/sell one field
     :can-perform? can-sell-grapes-or-deal-in-fields?
     :action-tag :space-sgf}
    {:prefix "pv" ;; plant vine
     :can-perform? can-plant-any-vine?
     :action-tag :space-plant-vine}]

   "winter"
   [{:prefix "do"
     :can-perform? can-draw-wine-order?
     :action-tag :space-draw-wine-order}
    {:prefix "tw"
     :can-perform? can-train-worker?
     :action-tag :space-train-worker}
    {:prefix "hf"
     :can-perform? can-harvest-field?
     :action-tag :space-harvest-field}
    {:prefix "mw"
     :can-perform? can-make-any-wine?
     :action-tag :space-make-wine}
    {:prefix "wv"
     :can-perform? can-play-any-winter-visitor?
     :action-tag :space-play-winter-visitor}
    {:prefix "fo"
     :can-perform? can-fill-any-wine-order?
     :action-tag :space-fill-wine-order}]})

(defaction :space-yoke [state role _bonus-applies?]
  (let [modes (cond-> []
                (has-any-planted-vine? state role)
                (conj :uproot)

                (has-harvestable-field? state role)
                (conj :harvest))]
    (perform-sequence state
                      [:easy-put-request role
                       [:choose-mode :choose-yoke-mode
                        modes]
                       :on-yoke-mode-chosen])))

(defaction :on-yoke-mode-chosen [state]
  (let [[role mode] (only-role&answer state)]
    (perform-sequence state
                      (case mode
                        :uproot [:choose-uproot-vine role]
                        :harvest [:choose-harvest-field role {}]
                        (assert false
                                (str "Unrecognized yoke mode: " mode))))))

(def cart-space
  {:action-tag :space-gain-1-lira})

(defaction :choose-uproot-vine [state role]
  (let [zone->cids (collect-planted-vines-per-zone state role)]
    (perform-sequence state
                      [:easy-put-request role
                       [:choose-card :choose-vine-to-uproot
                        zone->cids]
                       [:on-vine-to-uproot-chosen zone->cids]])))

(defn find-zone-with-cid [zone->cids cid]
  (first (find-first (fn [[zone cids]]
                       (seq-contains? cids cid))
                     zone->cids)))

(defn get-field-name-from-zone-name [zone-name]
  (last-part-of-multipart zone-name))

(defaction :on-vine-to-uproot-chosen [state zone->cids]
  (let [[role cid] (only-role&answer state)
        zone-name (find-zone-with-cid zone->cids cid)
        field-name (get-field-name-from-zone-name zone-name)]
    (perform-sequence state
                      [:text :uproots-vine role (denumber-id cid) field-name]
                      [:viti-move-card zone-name [role "hand"] cid]
                      [:update-field-values-by-cid role field-name cid {:negative? true}])))

(def yoke-space
  {:action-tag :space-yoke})

(def space-prefix->definition
  (into {"cart" cart-space
         "yoke" yoke-space} (map (fn [x]
                                   [(:prefix x) x])
                                 (apply concat (vals action-spaces)))))

(defn has-available-regular-worker? [state role]
  (pos? (get-n-available-figures-of-type state role :worker)))

(defn has-available-grande-worker? [state role]
  (pos? (get-n-available-figures-of-type state role :grande)))

(defn has-any-workers-available? [state role]
  (not-empty? (get-all-figures-in-space state
                                        (get-player-available-workers-space role))))

(defn get-action-of-space [role space-name]
  (let [[board subname] (split-multipart space-name)]
    (if (= board "m")
      (let [[definition bonus-applies?]
            (if (= subname "cart")
              [cart-space false]

              (let [prefix (subs subname 0 (dec (count subname)))
                    suffix (subs subname (dec (count subname)))]
                [(get space-prefix->definition prefix) (= suffix "2")]))]
        (assert (some? definition)
                (str "Cannot find definition of a space by name " space-name))
        [(:action-tag definition) role bonus-applies?])
      (do (assert (= subname "y")
                  (str "Only yoke action space is on the player vineyard, but got " space-name))
          [(:action-tag yoke-space) role false]))))

(defn get-yoke-action-space-name [role]
  (str role ":y"))

(defn collect-activatable-season-spaces [state role season]
  (filterv some? (concat (for [{:keys [prefix can-perform? action-tag]} (get action-spaces season)
                               ;; TODO if there are 3-4 players, also include suffix "2"; 5-6, "3".
                               :let [space-name (str "m:" prefix "1")
                                     bonus-applies? false #_(TODO prefix is 2)]]
                           (if (space-is-free? state space-name)
                             (when (can-perform? state role bonus-applies?)
                               space-name)
                             (when (and (has-available-grande-worker? state role)
                                        (can-perform? state role bonus-applies?))
                               (let [art-space-name (str "m:" prefix "@")]
                                 art-space-name))))
                         ["m:cart"
                          (let [space-name (get-yoke-action-space-name role)]
                            (when (and (space-is-free? state space-name)
                                       (can-perform-yoke? state role))
                              space-name))])))

(defaction :do-turn [state role season]
  (if (not (has-any-workers-available? state role))
    (perform-sequence state
                      [:auto-pass role season])
    (let [space-names (collect-activatable-season-spaces state role season)
          summer? (= season "summer")]
      (perform-sequence state
                        [:easy-put-request role
                         [:choose-space (if summer?
                                          :choose-summer-action-or-pass
                                          :choose-winter-action)
                          space-names
                          (when summer? {:can-pass? true})]
                         [:on-worker-placement-turn-chosen season]]))))

(defn space-is-for-grande-only? [space-name]
  (str/ends-with? space-name "@"))

(defaction :on-worker-placement-turn-chosen [state season]
  (let [[role answer] (only-role&answer state)]
    (perform-sequence state
                      (if (= answer "*pass*")
                        [:action-sequence
                         [:text :player-passes role]
                         [:move-rooster-to-signify-pass role season]]
                        [:action-sequence
                         [:send-a-worker-to-space role answer]
                         [:perform-space-action role answer]]))))

(defaction :auto-pass [state role season]
  (perform-sequence state
                    [:text :player-passes-for-lack-of-workers role]
                    [:move-rooster-to-signify-pass role season]))

(defaction :send-a-worker-to-space [state role space-name]
  (perform-sequence state
                    (cond (space-is-for-grande-only? space-name)
                          (do (assert (has-available-grande-worker? state role)
                                      ("Must have the grande worker available if the grande-only space is chosen: " role space-name))
                              [:send-grande-worker role space-name])

                          (and (has-available-regular-worker? state role)
                               (has-available-grande-worker? state role))
                          [:choose-worker-to-send role space-name]

                          (has-available-regular-worker? state role)
                          [:send-regular-worker role space-name]

                          :else
                          (do (assert (has-available-grande-worker? state role)
                                      (str "Must have the grande worker available if no regular workers are: " role space-name))
                              [:send-grande-worker role space-name]))))

(defaction :perform-space-action [state role space-name]
  (let [action (get-action-of-space role space-name)]
    (perform-action state action)))

(defaction :send-grande-worker [state role space-name]
  (perform-sequence state
                    [:text :sends-worker-of-type-to role :grande space-name]
                    [:move-figure
                     (get-player-available-workers-space role)
                     space-name
                     (get-figure-id role :grande)]))

(defn get-first-available-player-worker [state role]
  (get-first-available-figure-of-type state role :worker))

(defaction :send-regular-worker [state role space-name]
  (perform-sequence state
                    [:text :sends-worker-of-type-to role :regular space-name]
                    [:move-figure
                     (get-player-available-workers-space role)
                     space-name
                     (get-first-available-player-worker state role)]))

(defaction :move-rooster-to-signify-pass [state role season]
  (let [number (find-player-rooster-number state role season)]
    (perform-sequence state
                      [:move-figure
                       (get-rooster-season-space season number)
                       (get-rooster-other-season-space season number)
                       (get-figure-id role :rooster)])))

(defaction :do-summer [state]
  (perform-sequence state
                    [:season-starts "summer"]
                    [:do-worker-placement-season "summer" nil]))

(defaction :choose-worker-to-send [state role space-name]
  (perform-sequence state
                    [:easy-put-request role
                     [:choose-mode :choose-worker-to-send [:regular :grande]]
                     [:on-worker-to-send-chosen space-name]]))

(defaction :on-worker-to-send-chosen [state space-name]
  (let [[role mode] (only-role&answer state)]
    (perform-sequence state
                      (if (= mode :regular)
                        [:send-regular-worker role space-name]
                        (do (assert (= mode :grande)
                                    (str "Must answer only with :regular or :grande when choosing a worker to send: " mode))
                            [:send-grande-worker role space-name])))))


(defaction :do-fall [state]
  (let [roles (for [number all-wakeup-numbers
                    :let [figures (get-all-figures-in-space state (get-rooster-right-space number))]
                    :when (and (not-empty? figures)
                               (= (get-figure-type (first figures)) :rooster))
                    :let [owner (get-role-of-figure (first figures))]]
                owner)]
    (apply perform-sequence state
           [:season-starts "fall"]
           (map (fn [role]
                  [:perform-fall-draw role])
                roles))))

(defaction :do-winter [state]
  (perform-sequence state
                    [:season-starts "winter"]
                    [:do-worker-placement-season "winter" nil]))

(defaction :perform-fall-draw [state role]
  ;; TODO if has cottage, choose two decks
  (perform-sequence state
                    [:choose-draw-cards role ["yellow-deck" "blue-deck"] 1]))

(defaction :create-token [state space-name]
  (let [figure-id (get-next-token-id state)]
    (perform-sequence state
                      [:create-figure
                       space-name
                       figure-id]
                      [:update-next-token-id])))

(defaction :year-end [state]
  (perform-sequence state
                    :age-grapes-and-wines
                    :retrieve-workers
                    :collect-rp
                    :discard-excess-cards-from-hands
                    :pass-first-player-marker-ccw
                    :remove-roosters
                    :clear-per-year-vars
                    :check-game-end))

(defaction :age-grapes-and-wines [state]
  (apply perform-sequence state
         [:text :players-age-grapes-and-wines]
         (for [role (all-playing-roles state)
               value (range 8 0 -1)]
           [:action-sequence
            [:try-age-grape role :red value]
            [:try-age-grape role :white value]
            [:try-age-wine role :red value]
            [:try-age-wine role :white value]
            [:try-age-wine role :blush value]
            [:try-age-wine role :sparkling value]])))

(defaction :try-age-grape [state role color value]
  (let [this-space-name (crushpad-space-name-of-value role color value)
        next-space-name (crushpad-space-name-of-value role color (inc value))
        figures (get-all-figures-in-space state this-space-name)]
    (if (and (not-empty? figures)
             (empty? (get-all-figures-in-space state next-space-name)))
      (perform-sequence state
                        [:move-figure this-space-name next-space-name (first figures)])
      (do-nothing state))))

(defn get-all-action-spaces [state]
  (concat (for [season ["summer" "winter"]
                {:keys [prefix]} (get action-spaces season)
                suffix ["1" "@"] ;; TODO with more players also 2 and/or 3
                :let [space-name (str "m:" prefix suffix)]]
            space-name)
          ["m:cart"]
          (for [role (all-playing-roles state)
                :let [space-name (get-yoke-action-space-name role)]]
            space-name)))

(defaction :try-age-wine [state role wine-kind value]
  (let [this-space-name (space-name-from-wine-by-parts role wine-kind value)
        next-value (inc value)
        next-space-name (space-name-from-wine-by-parts role wine-kind next-value)
        figures (get-all-figures-in-space state this-space-name)]
    (if (and (not-empty? figures)
             (case next-value
               7 (has-structure? state role :large-cellar)
               4 (has-structure? state role :medium-cellar)
               true)
             (empty? (get-all-figures-in-space state next-space-name)))
      (perform-sequence state
                        [:move-figure this-space-name next-space-name (first figures)])
      (do-nothing state))))

(defaction :retrieve-workers [state]
  (apply perform-sequence state
         [:text :players-retrieve-workers]
         (for [space (get-all-action-spaces state)
               figure (get-all-figures-in-space state space)
               :let [owner (get-role-of-figure figure)
                     target-space (if (= owner "n")
                                    neutral-worker-rest-space
                                    (get-player-available-workers-space owner))]]
           [:move-figure space target-space figure {:disable-animation? true}])))

(defaction :collect-rp [state]
  (apply perform-sequence state
         [:text :players-collect-rp]
         (for [role (all-playing-roles state)
               :let [rp (get-var state ["rp" role])]
               :when (pos? rp)]
           [:gain-lira role rp])))

(def HAND-LIMIT 7)

(defaction :discard-excess-cards-from-hands [state]
  (let [request-map (into {} (for [role (all-playing-roles state)
                                   :let [zn (str role ":hand")
                                         cards (all-cards-in-zone state zn)
                                         excess (- (count cards) HAND-LIMIT)]
                                   :when (pos? excess)]
                               [role [:choose-cards-flexible [:choose-cards-to-discard excess]
                                      zn nil
                                      {:all-in-zone? true
                                       :lower excess
                                       :higher excess}]]))]
    (if (empty? request-map)
      (do-nothing state)
      (perform-sequence state
                        [:put-request request-map]
                        [:on-cards-to-discard-chosen]))))

(defaction :on-cards-to-discard-chosen [state]
  (apply perform-sequence state
         (for [[role answer] (:answers state)]
           [:viti-discard-cards role answer])))


(defaction :viti-discard-cards [state role cids]
  (let [groups (group-by (fn [cid]
                           (:type (design-by-cid cid)))
                         cids)]
    (apply perform-sequence state
           [:text :discards-cards role (mapv denumber-id cids)]
           (for [[type group-cids] groups
                 :let [discard-name (card-type->discard-name type)
                       _ (assert (some? discard-name)
                                 (str "Must have a discard for card type " type ": " role cids))]]
             ;; TODO and update the publicly-visible n cards of this type
             [:viti-move-cards [role "hand"] discard-name group-cids]))))

(defaction :pass-first-player-marker-ccw [state]
  (let [role (get-var state "first-player")
        all-roles (all-playing-roles state)
        index (first-index #(= % role) all-roles)
        _ (assert (some? index)
                  (str "Must have index of first player: " role all-roles))
        new-index (mod (dec index) (count all-roles))
        new-role (get all-roles new-index)]
    (perform-sequence state
                      [:text :first-player-marker-passed-to new-role]
                      [:set-var "first-player" new-role])))

(defaction :remove-roosters [state]
  (apply perform-sequence state
         [:text :players-remove-roosters]
         (for [number all-wakeup-numbers
               :let [space-name (get-rooster-left-space number)
                     figures (get-all-figures-in-space state space-name)]
               :when (not-empty? figures)
               :let [figure-id (first figures)]
               :when (= (get-figure-type figure-id) :rooster)
               :let [owner (get-role-of-figure figure-id)]]
           [:move-figure space-name rooster-rest-space figure-id])))

(defaction :clear-per-year-vars [state]
  (apply perform-sequence state
         (concat (for [role (all-playing-roles state)
                       var [(windmill-applied-var role)
                            (tasting-room-applied-var role)]
                       :when (some? (get-var state var))]
                   [:unset-var var])

                 (for [role (all-playing-roles state)
                       field-name all-field-names
                       :let [var (field-harvested-var role field-name)]
                       :when (some? (get-var state var))]
                   [:unset-var var]))))

(def TARGET-VP 20)

(defaction :check-game-end [state]
  (perform-action state
                  (if (some #(>= (get-var state ["vp" %]) TARGET-VP)
                            (all-playing-roles state))
                    [:game-over]
                    [:go-to-next-year])))

(defaction :go-to-next-year [state]
  (perform-sequence state
                    [:change-var "year" 1]
                    :main-sequence))

(defn get-total-value-of-wine [state role]
  (sum (for [recipe all-wine-recipes
             value (range-inclusive (:min-space recipe) 9)
             :when (has-wine? state role (:wine-kind recipe) value)]
         value)))

(defn get-total-value-of-grapes [state role]
  (sum (for [color [:red :white]
             value all-grape-values
             :when (has-grape-by-parts? state role color value)]
         value)))

(defn filter-having-max-of [coll f]
  (let [max-value (apply max (map f coll))]
    (filter #(= (f %) max-value) coll)))

(defaction :game-over [state]
  (let [data (for [role (all-playing-roles state)]
               {:role role
                :vp (get-var state ["vp" role])
                :lira (get-player-lira state role)
                :wine (get-total-value-of-wine state role)
                :grapes (get-total-value-of-grapes state role)})

        by-vp (filter-having-max-of data :vp)

        make-single-win (fn [submode coll]
                          {:mode "win"
                           :submode submode
                           :winner-role (:role (first coll))})

        result (if (= (count by-vp) 1)
                 (make-single-win "most-vp" by-vp)
                 (let [by-lira (filter-having-max-of by-vp :lira)]
                   (if (= (count by-lira) 1)
                     (make-single-win "most-lira" by-lira)
                     (let [by-wine (filter-having-max-of by-lira :wine)]
                       (if (= (count by-wine) 1)
                         (make-single-win "best-wine" by-wine)
                         (let [by-grapes (filter-having-max-of by-wine :grapes)]
                           (if (= (count by-grapes) 1)
                             (make-single-win "best-grapes" by-grapes)
                             {:mode "tie"
                              :submode "shared-win"
                              :winner-roles (mapv :role by-grapes)})))))))
        {:keys [mode submode winner-role]} result]
    ;; TODO add support for multiple winners.
    (perform-sequence state
                      [:finish-game mode submode winner-role])))

;; == Hooks to external world

(def game-definition {:new-game new-game
                      :state-timing state-timing
                      :has-spaces? true})

(register-game-definition GAME-CODE game-definition)

(defn new-engine [& args]
  (apply game-engine/new-engine GAME-CODE args))

(defn new-engine-from-checkpoint [checkpoint mode]
  (game-engine/new-engine-from-checkpoint GAME-CODE checkpoint mode))

(defmethod all-playing-roles++ GAME-CODE [state]
  (all-playing-roles state))
