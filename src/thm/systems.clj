(ns thm.systems
  (:require
   [com.stuartsierra.component :as component]
   [thm.server-side-handler :refer [app event-msg-handler*]]
   [environ.core :refer [env]]
   [taoensso.sente.server-adapters.http-kit :refer [get-sch-adapter]]
   (system.components
    [http-kit :refer [new-web-server]]
    )
   [thm.config :refer [config]]
   (thm.backend-components
    [ds :refer [new-ds]]
    [sente :refer [new-channel-sockets]]
    [carmine :refer [new-carmine]]
    [worker :refer [new-worker]]
    [td-talker :refer [new-td-talker]]
    [backend-listener :refer [new-backend-listener]]
     )))

(defn dev-system []
  (-> (component/system-map
       :sente (new-channel-sockets event-msg-handler* (get-sch-adapter)
                                   {:user-id-fn #(:client-id %)})
       :carmine (new-carmine (:redis config))
       :ds (new-ds {})
       :worker (new-worker "dev")
       :td-talker (new-td-talker "dev")
       :backend-listener (new-backend-listener "dev")
       :web (new-web-server (Integer. (:http-port config)) app))
      (component/system-using
       {:ds [:carmine]
        :backend-listener [:sente :carmine]
        :web [:backend-listener]
        :worker [:ds :carmine]
        :td-talker [:carmine]})))

(defn prod-system []
  (-> (component/system-map
       :sente (new-channel-sockets event-msg-handler* (get-sch-adapter)
                                   {:user-id-fn #(:client-id %)})
       :carmine (new-carmine (:redis config))
       :ds (new-ds {})
       :worker (new-worker "prod")
       :td-talker (new-td-talker "prod")
       :backend-listener (new-backend-listener "prod")
       :web (new-web-server (Integer. (or (:http-port config) 8008)) app)
       )
      (component/system-using
       {:ds [:carmine]
        :backend-listener [:sente :carmine]
        :web [:backend-listener]
        :worker [:ds :carmine]
        :td-talker [:carmine]})))
