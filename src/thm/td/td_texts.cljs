(ns thm.td.td-texts
  (:require [thm.td.td-data :as td-data]
            [thm.utils :refer [denumber-id shorten-title-for-humans]]
            [clojure.string :as str]
            [taoensso.timbre :refer-macros (debug warn)]))

(def us-symbol "★")
(def su-symbol "☭")

(defn symbol-of-role [r]
  (if (= r "u") us-symbol su-symbol))

(defn role-for-humans [role]
  (if (= role "u") "USA" "USSR"))

(defn zone-name-for-humans [zone-name]
  (case zone-name
    "strategy-discard" "Strategy discard pile"
    "agenda-discard" "Agenda discard pile"
    ("aftermath" "aftermath-reveal") "Aftermath"
    "agenda-deck" "Agenda deck"
    "strategy-deck" "Strategy deck"
    ;; Note: Not all zones are here because it's used only in displaying a zone
    ;; popup and in :deck-shuffled.
    (str "(unknown zone: " zone-name ")")))

(defn battleground-for-humans [bg]
  (:title (td-data/battlegrounds bg)))

(defn defcon-track-for-humans [track]
  (td-data/defcon-tracks track))

(defn card-alignment-for-humans [alignment]
  ({"n" "UN" "s" "USSR" "u" "USA"} alignment))

(defn card-play-mode-for-humans [mode & short?]
  (case mode
    ("command", "opp-command") "Command"
    ("command+pl", "opp-command+pl") (if short?
                                       "Command+Letter"
                                       "Command, with Personal Letter")
    "event" (if short? "Event" "the Event")))

(def agenda-short-names-set (set (map denumber-id td-data/all-agenda-cids)))
(def strategy-short-names-set (set td-data/all-strategy-cids))

(defn big-card-img-src [cid]
  (let [short (denumber-id cid)]
    (cond
      (= short "*back*")
      (str "/images/agenda-big/ab-b.jpg")

      (contains? agenda-short-names-set short)
      (str "/images/agenda-big/ab-" short ".jpg")

      (contains? strategy-short-names-set short)
      (str "/images/strategy-big/sb-" short ".jpg")

      :else
      "/images/perlet-big/pb-perlet.jpg")))

(defn small-card-img-src [cid]
  (if (nil? cid)
    (do (warn "Nil card id!")
        "/images/perlet-crop/pc-perlet.jpg")
    (let [short (denumber-id cid)]
      (cond
        (= short "*back*")
        (str "/images/agenda-crop/ac-b.jpg")

        (contains? agenda-short-names-set short)
        (str "/images/agenda-crop/ac-" short ".jpg")

        (contains? strategy-short-names-set short)
        (str "/images/strategy-crop/sc-" short ".jpg")

        :else
        "/images/perlet-crop/pc-perlet.jpg"))))

(defn card-title-by-id [cid]
  (:title (td-data/card-design-by-denumbered-id (denumber-id cid))))

(defn short-card-title [cid]
  (let [title (:title (td-data/card-design-by-denumbered-id (denumber-id cid)))]
    (shorten-title-for-humans title)))

(def fns
  {:outcome-submode
   (fn [submode translation-kind]
     (get (case submode
            "both-war"
            ["Both sides have caused the nuclear war."
             "Both sides have caused the nuclear war."]

            "war"
            ["Opponent has caused the nuclear war."
             "opponent caused the nuclear war"]

            "more-prestige"
            ["They have more Prestige." ;; XXX gender
             "more Prestige"]

            "has-letter"
            ["They have the Personal Letter." ;; XXX gender
             "control of Personal Letter"])
          (if (= translation-kind :full) 0 1)))

   :role role-for-humans

   :timing
   (fn [timing-obj]
     (let [{:keys [round phase turn active-player]} timing-obj]
       (str "Round " round
            (when phase
              (if turn
                (str " - " (role-for-humans active-player)
                     " card #" turn)
                (str " - " (case phase
                             "agenda" "Agenda Phase"
                             "strategy" "Strategy Phase"
                             "wo-bonus" "World Opinion Bonus"
                             "resolve-agendas" "Resolve Agendas"
                             "aftermath" "Aftermath"
                             (do (warn "Unknown phase " phase) phase))))))))

   :symbol-of-role symbol-of-role

   :big-card-img-src big-card-img-src
   :small-card-img-src small-card-img-src

   :card-title-by-id card-title-by-id

   })
