(ns thm.db)

(def default-db {:page-desc ["/"]
                 :thm.term/state  {:input ""
                                   :lines ["Welcome to hacker mode.  Type `help' to see commands."]
                                   :prompt "$ "
                                   :history []
                                   :history-index 0}
                 })
