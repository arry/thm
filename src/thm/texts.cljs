(ns thm.texts
  (:require [thm.td.td-texts :as td-texts]
            [thm.viti.viti-texts :as viti-texts]
            [clojure.string :as str]
            [thm.utils :refer [bull]]
            [taoensso.timbre :refer-macros (debug warn)]
            ))


(defn outcome-mode [outcome-mode translation-kind]
  (case outcome-mode
    "tie" (if (= translation-kind :full)
            "Game finishes in a tie: "
            "Tie: ")
    "win" (if (= translation-kind :full)
            " wins the game: "
            " won ")
    "abandoned" (if (= translation-kind :full)
                  "The game is abandoned by "
                  "Abandoned by ")))

;; XXX the game-code to texts transformation is repeated from the GameUiModule
;; system.  So far we cannot go through the module system because it would
;; cause a circular dependency.
(def mapping {"td" td-texts/fns
              "viti" viti-texts/fns})

(defn text [code text-kind & args]
  (let [make-default (fn [warning]
                       (warn warning)
                       (str/join (bull) (into [text-kind] args)))
        fns (get mapping code)]
    (if fns
      (if-let [fn (get fns text-kind)]
        (do #_(debug "Got text fn: " code text-kind " -- args " args)
            (apply fn args))
        (make-default (str "Unknown text-kind " text-kind " for game code " code)))
      (make-default (str "Unrecognized game code: " code " - for text " text-kind args)))))
