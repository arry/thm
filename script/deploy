#!/usr/bin/env perl

use strict;
use warnings;
use 5.010;

use File::Spec::Functions qw(catfile);
use File::Path qw(make_path);
use Term::ANSIColor;
use FindBin;

my $build_dir = "build";
my $tmp_dir = "build-tmp";

my $remote = "arry\@138.68.81.128"; # should take from from config

my $task = shift // 'all';

make_build_dir();

if ($task eq 'uberjar' || $task eq 'all') {
    print "Building uberjar\n";
    build_uberjar();
    copy_uberjar();
}

if ($task eq 'files' || $task eq 'all') {
    copy_public_files();
    chmod 0775, "$tmp_dir/public"; # rsync does its own thing with permissions,
                                   # but we need world-visible.
    copy_all_to_build();
}

sub notify {
    my $text = shift;
    system("notify-send -t 1500 '$text'")
}

if ($task eq 'remote' || $task eq 'all') {
    notify('Ready to push to remote');
    copy_build_to_remote();

    # TODO run migrations

    restart_services();
}

report_all_right();

sub make_build_dir {
    make_path($build_dir);
    make_path($tmp_dir);
    for (<$tmp_dir/*>) {
        wrap_system("rm -rf $_");
    }
}

sub wrap_system {
    my @args = @_;
    my $result = system @args;
    unless ($result == 0) {
        warn "Failed to execute `@args'\n";
        print color 'red';
        print "Deployment failed\n";
        print color 'reset';
        notify('Deployment failed');
        exit $result;
    }
}


sub copy {
    my $src_dir = shift;
    my $target_dir = shift;
    my @specs = @_;

    for my $spec (@specs) {
        my ($file_name, @args);
        if (ref $spec eq 'ARRAY') {
            ($file_name, @args) = @{ $spec };
        }
        else {
            $file_name = $spec;
        }
        my $src = catfile($src_dir, $file_name);
        if (-d $src) {
            $src = "$src/";
        }
        my $dest = catfile($target_dir, $file_name);
        my $joined_args = join ' ', @args;
        my $command = <<"END";
rsync -az --exclude '*.orig' @args $src $dest
END
        warn "run: $command\n";
        wrap_system($command);
    }
}

sub build_uberjar {
    wrap_system("boot build");
}

sub copy_uberjar {
    wrap_system("rsync -a target/thm-1.0.0.jar $tmp_dir/project.jar");
    wrap_system("rsync -a config/config.prod.edn $tmp_dir");
}

sub copy_public_files {
    my @specs = (
        'resources/',
        'target/main.js',
    );

    my $dest = "$tmp_dir/public";

    wrap_system(<<"END"
rsync -az --exclude '*.orig' --exclude 'config.*' --exclude 'sql' resources/ $dest
END
);

    wrap_system(<<"END"
rsync -az --exclude '*.orig' --exclude 'config.*' --exclude 'sql' target/css $dest
END
);

    wrap_system(<<"END"
rsync -az --exclude '*.orig' target/main.js $dest
END
);

}

sub copy_all_to_build {
    wrap_system("rsync -avz --delete ${tmp_dir}/ $build_dir");
}

sub copy_build_to_remote {
    wrap_system("rsync -avz --progress ${build_dir}/ $remote:/var/www/thm");
}

sub restart_services {
    notify('Ready to restart services');
    my @commands = (
        'sudo systemctl restart thm.service',
    );
    for my $cmd (@commands) {
        my $command = "ssh $remote '$cmd'";
        warn "run: $command\n";
        wrap_system "$command";
    }
}

sub report_all_right {
    print color 'green';
    print "Deployment is successful\n";
    print color 'reset';
}
