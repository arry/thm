(ns thm.game-ui.game-ui-module
  (:require [taoensso.timbre :refer-macros (debug warn)]))

(defmulti do-resize (fn [code]
                      code))

(defmethod do-resize :default [code]
  (warn "Unrecognized game code for do-resize:" code))

(defprotocol GameUiModule
  (get-game-code [_])

  (your-request-code-for-humans [_this text-descr primary-role])

  (mode-for-humans [_ mode text-descr])

  (answer-for-humans [_ answer req])

  (other-request-code-for-humans [_ player-name text-descr])

  (game-container [_ primary-role])

  (log-item-for-humans [_ log-item log-helper])

  (small-role-identifier [_ role])

  (player-plaque-info-line [_ primary-role role])

  (sidebar-global-info [_ primary-role])

  (compact-player-plaque [_ primary-role role player-name usernames])

  (compact-game-container [_ primary-role])

  (your-hand [_ primary-role])

  (has-left-hand-of-choose-mode-modal? [_ req])

  (left-hand-of-choose-mode-modal [_ req])

  (extended-player-plaque-info [_ modal-data])

  (modal-content [_ modal-data primary-role])

  (get-text [_ text-descr])

  (get-pane-associated-with-request [_ request your-role])

  (get-pane-associated-with-delta [_ delta your-role])

  (get-play-areas-associated-with-request [_ request your-role]
    "Can return a string which is the name of the play area tab, or a sequence
     of the names.")

  (get-play-area-associated-with-delta [_ delta your-role])

  (get-animation-kind-for-zone-name [_ zone-name your-role])

  (get-delay-of-var [_ var-name])

  (get-delay-of-vars [_ var-names])

  (state-timing-for-humans [_ state])

  (on-var-set [_ primary-role var-name value])

  )

(defprotocol GameUiWithSpaces
  (get-space-visibility [_ space-name primary-role])

  (get-play-area-of-space [_ space-name])

  (get-replaced-from-space-name [_ to-space-name]
    "When in a figure animation, the source and destination spaces are on the
    different play area tabs, get a zone on the destination tab from which the
    figure will visually animate."))
