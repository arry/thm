-- Deploy sqitch/sqitch.plan:users to pg

BEGIN;

CREATE TABLE users (
    username TEXT PRIMARY KEY,
    password_hash TEXT NOT NULL,
    email TEXT NOT NULL UNIQUE,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    has_avatar BOOLEAN DEFAULT FALSE,
    last_seen_at TIMESTAMP DEFAULT current_timestamp
);

CREATE TABLE pending_registrations (
    email TEXT NOT NULL UNIQUE,
    confirmation_code TEXT NOT NULL,
    is_confirmed BOOLEAN DEFAULT FALSE
);

CREATE TABLE password_recoveries (
    email TEXT NOT NULL UNIQUE,
    recovery_code TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

COMMIT;
