(ns thm.views.game-ui-common
  (:require [reagent.core :as r]
            [re-frame.core :refer [dispatch subscribe]]
            [clojure.string :as str]
            [taoensso.timbre :refer-macros (debug warn warnf)]

            [thm.game-ui.card-animator :as card-animator]

            [thm.utils :refer [forbid-widget-activation?
                               on-mobile-browser?
                               dispatch-e
                               denumber-id
                               n-cards-in-zone
                               size-style
                               set-answer
                               get-your-request
                               seq-contains?
                               multipart-name-from-descr
                               parse-text-descr
                               send-answer
                               ]]
            [thm.texts :as t]
            [thm.views.common :refer [indexed-component
                                      action-link
                                      ]]
            ))

(defn card-preview-additional-class-from-event [browser-event]
  (let [x (.-clientX browser-event)
        w (.-clientWidth (js/document.getElementById "container"))]
    ;; When mouse is on the left, preview is on the right, hence here it's
    ;; reversed.
    (if (< x (/ w 2)) "right" "left")))

(defn display-card-preview [id browser-event]
  (let [additional-class (card-preview-additional-class-from-event browser-event)]
    (dispatch-e [:thm.game-ui/cursor-on-small-card true id additional-class]
                browser-event)))

(defn toggle-card-preview [id browser-event]
  (let [additional-class (card-preview-additional-class-from-event browser-event)]
    (dispatch-e [:thm.game-ui/toggle-card-preview id additional-class]
                browser-event)))

(defn hide-card-preview [browser-event]
  (dispatch-e [:thm.game-ui/cursor-on-small-card false] browser-event))


(def LONG-TAP-MILLISECONDS 1000)

(defn with-short-and-long-tap [on-short-tap on-long-tap]
  (let [state (atom {})
        clear-timeout (fn []
                        (when-let [timeout-id (:timeout-id @state)]
                          (js/window.clearTimeout timeout-id)
                          (swap! state dissoc :timeout-id)))
        f (fn [e]
            (on-long-tap e)
            (clear-timeout)
            (swap! state assoc :long-tap true))
        set-timeout (fn [e]
                      (swap! state assoc
                             :timeout-id (js/window.setTimeout #(f e) LONG-TAP-MILLISECONDS)))]
    {:on-touch-start (fn [e]
                       (swap! state assoc :started true, :out false, :long-tap false)
                       (.persist e) ;; see https://facebook.github.io/react/docs/events.html#event-pooling
                       (set-timeout e))
     :on-touch-end (fn [e]
                     (when (and (:started @state) (not (:out @state)) (not (:long-tap @state)))
                       (on-short-tap e))
                     (swap! state assoc :out false, :started false)
                     (clear-timeout))
     :on-touch-cancel (fn [e]
                        (swap! state assoc :out true)
                        (clear-timeout))}))

(defn with-card-preview [cid props & [options]]
  (let [on-activation (:on-activation options)
        additional (if (on-mobile-browser?)
                       (if on-activation
                         (with-short-and-long-tap
                           #(do (on-activation %)
                                (hide-card-preview %))
                           #(toggle-card-preview cid %))
                         {:on-touch-start #(toggle-card-preview cid %)})
                       (merge {:on-mouse-enter #(display-card-preview cid %)
                               :on-mouse-leave #(hide-card-preview %)}
                              (when on-activation
                                {:on-click on-activation})))
        result (merge props additional)]
    result))

(defn view-whole-zone [modal-kind]
  (dispatch [:thm.game-ui/show-modal modal-kind]))

(defn one-card-in-zone [props primary-role zone-descr index card]
  (r/create-class
   {:display-name "one-card-in-zone"
    :reagent-render (fn [props primary-role zone-descr index card]
                      (let [cid (:cid card)
                            code (:code props)
                            ui-layout @(subscribe [:thm.game-ui/layout])
                            active? @(subscribe [:thm.game-ui/card-active? primary-role cid zone-descr])
                            checked? @(subscribe [:thm.game-ui/card-checked? primary-role cid zone-descr])

                            request-is-choose-card-to-play? @(subscribe [:thm.game-ui.td/request-is-choose-card-to-play? primary-role])

                            base-div-props {:class (cond-> ["card-crop"]
                                                     active? (conj "is-active")
                                                     checked? (conj "is-checked"))}

                            ;; XXX Hack: 2-stage card play in td.
                            ;; The card play request is done in two step: choose card, then choose mode.
                            ;; I don't want to change server side, but in the new ui paradigm, it's better
                            ;; when the options of card play appear immediately after selecting the card.
                            ;; Hence we send the answer immediately only in that case.
                            hack? (and (= code "td")
                                       active?
                                       request-is-choose-card-to-play?)

                            div-props (if (= ui-layout "compact")
                                        (merge base-div-props
                                               {:on-click (fn [e]
                                                            (if hack?
                                                              (send-answer primary-role [cid nil])
                                                              (dispatch-e [:thm.game-ui/show-modal
                                                                           {:kind :look-at-card
                                                                            :cid cid
                                                                            :actions [(when active?
                                                                                        {:for-humans "Choose"
                                                                                         :short-name "choose"
                                                                                         :on-chosen #(dispatch [:thm.game-ui/card-activated primary-role cid])})
                                                                                      (when-let [modal-kind (:modal-kind props)]
                                                                                        {:for-humans "View whole zone"
                                                                                         :short-name "view-whole-zone"
                                                                                         :on-chosen #(view-whole-zone modal-kind)})]}]
                                                                          e)))})
                                        (with-card-preview cid
                                          base-div-props
                                          (when active? {:on-activation #(dispatch [:thm.game-ui/card-activated primary-role cid])})))]
                        [:div div-props
                         [:img {:src (t/text code :small-card-img-src cid)}]
                         [:div.cover]]))}))

(defn -zone-of-cards-internal [props mode primary-role zone-name iterable-cards animation-info]
  (r/create-class
   {:display-name "-zone-of-cards-internal"

    :component-did-mount (fn [this]
                           (card-animator/zone-component-did-mount primary-role zone-name this mode))

    :component-will-unmount (fn [this]
                              (card-animator/zone-component-will-unmount primary-role zone-name))

    :component-did-update (fn [this old-argv]
                            (let [new-argv (r/argv this)
                                  old-animation-info (nth old-argv 6)
                                  new-iterable-cards (nth new-argv 5)
                                  new-animation-info (nth new-argv 6)]
                              (when (and (not= old-animation-info new-animation-info)
                                         (some? new-animation-info))
                                (card-animator/perform-animation primary-role zone-name new-animation-info new-iterable-cards))))

    :reagent-render
    (fn [props mode primary-role zone-name iterable-cards animation-info]
      (let [class (:class props)
            alternate-cids (:alternate-cids animation-info)]
        (if (= mode :invisible)
          [:div {:class class}]
          (when (or (coll? alternate-cids)
                    (coll? iterable-cards))
            [:div.zone.clearfix {:class class
                                 :style (:style props)}
             (indexed-component (if (coll? alternate-cids)
                                  (map (fn [cid] {:cid cid}) alternate-cids)
                                  iterable-cards)
                                (fn [index card]
                                  ^{:key (:cid card)}
                                  [one-card-in-zone props primary-role zone-name index card]))

             (when (= mode :facedown-stack)
               [:div.card-count (:card-count props)])
             ]))))}))

(defn zone-of-cards [props primary-role zone-descr]
  (let [zone-name (multipart-name-from-descr zone-descr)
        mode (or (:mode props) :all-cards)
        all-cards @(subscribe [:thm.game-ui/all-cards-in-zone primary-role zone-descr])
        iterable-cards (case mode
                         (:all-cards :v-row :v-stack) all-cards
                         :i-facedown-row (do
                                           (assert (number? all-cards)
                                                   (str "all-cards should be number but is " all-cards zone-descr))
                                           ;; XXX facedown ids should be assigned from game-client
                                           (mapv (fn [_] {:cid (str "*back*:" (gensym ""))}) (range all-cards)))
                         (:i-non-holding-place :v-non-holding-place) []
                         :last-card (if (empty? all-cards)
                                      []
                                      [(last all-cards)])
                         :facedown-stack []
                         :invisible []
                         (do (warnf "Unrecognized mode for zone-of-cards: %s, defaulting to :all-cards" mode)
                             all-cards))
        animation-info @(subscribe [:thm.game-ui/zone-animation-info primary-role zone-name])]
    [-zone-of-cards-internal (cond-> props
                               (= mode :facedown-stack)
                               (assoc :card-count all-cards))
     mode primary-role zone-name iterable-cards animation-info]))

(defn card-title-link [cid child-element]
  (let [*ui-layout (subscribe [:thm.game-ui/layout])]
    (fn [cid child-element]
      (if cid
        (let [prop (if (= @*ui-layout "compact")
                     {:href (str "#" cid)
                      :on-click #(dispatch-e [:thm.game-ui/show-modal {:kind :look-at-card
                                                                       :cid cid}] %)}
                     (with-card-preview cid {:href (str "#" cid)
                                             :on-click #(.preventDefault %)}))]
          [:a prop child-element])
        (do (warn "Got nil card id: " child-element)
            [:a "unknown card id"])))))

(defn zone-link [primary-role modal text zn]
  [action-link [:thm.game-ui/show-modal modal]
   (str text " (" @(subscribe [:thm.game-ui/n-cards-in-zone primary-role zn]) ")")])
