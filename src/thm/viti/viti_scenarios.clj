(ns thm.viti.viti-scenarios
  (:require
   [thm.viti.viti-engine :refer [new-game GAME-CODE
                                 crushpad-space-name-of-value
                                 space-name-from-wine-by-parts]]
   [thm.game-engine.game-base :refer [state-after-action-sequence
                                      state-after-pending-actions
                                      state-after-answer
                                      defaction-coded
                                      perform-sequence
                                      ]]
   [thm.game-engine.registry-of-test-scenarios :refer [register-scenarios]]
   ))

(defn new-game* []
  (-> (new-game ["John" "Bill"] {:seed 1})
      (assoc :pending-actions [])))

(def scenarios [])

(defmacro defscenario [n & forms]
  `(let [scenario# {:name ~n
                    :create (fn []
                              ~@forms)}]
     (alter-var-root #'scenarios conj scenario#)))

(defscenario "new-game"
  (-> (new-game ["John" "Bill"] {:seed 1})
      (state-after-pending-actions)))

(defn fake-request [& [role]]
  [:easy-put-request (or role "r")
   [:yn :fake-request nil]
   [:do-nothing]])

(defscenario "change vp"
  (-> (new-game*)
      (state-after-action-sequence
       (fake-request)
       [:change-var "vp:r" 1]
       [:change-var "vp:y" -2])))

(defscenario "move rooster"
  (-> (new-game*)
      (state-after-action-sequence
       [:easy-put-request "r"
        [:choose-space :choose-rooster-space ["m:r1f" "m:r2f" "m:r3f" "m:r4f" "m:r5f" "m:r6f" "m:r7f"] {}]
        [:do-nothing]]
       [:move-figure "m:r0f" "m:r2l" "rr"]
       (fake-request)
       [:move-figure "m:r0f" "m:r4l" "yr"]
       (fake-request)
       [:move-figure "m:r2l" "m:r2r" "rr"]
       [:move-figure "m:r4l" "m:r4r" "yr"]
       (fake-request)
       [:move-figure "m:r2r" "m:r0f" "rr"]
       [:move-figure "m:r4r" "m:r0f" "yr"])))

(defscenario "change rp"
  (-> (new-game*)
      (state-after-action-sequence
       (fake-request)
       [:change-var "rp:r" 1])))

(defscenario "setup"
  (-> (new-game*)
      (state-after-action-sequence
       :setup)))

(defscenario "spring"
  (-> (new-game*)
      (state-after-action-sequence [:do-spring])))

(defscenario "summer - fall - winter"
  (-> (new-game*)
      (state-after-action-sequence
       [:set-var "lira:r" 10]
       [:set-var "lira:y" 2]
       [:viti-move-cards "green-deck" "y:hand" ["sangio:1" "pinot:1"]]
       [:move-figure "m:r0f" "m:r1l" "yr"]
       [:move-figure "m:r0f" "m:r2l" "rr"]
       [:create-token (crushpad-space-name-of-value "y" :white 1)]
       [:create-token (crushpad-space-name-of-value "y" :white 8)]
       [:create-token (crushpad-space-name-of-value "y" :red 4)]
       [:create-token (crushpad-space-name-of-value "y" :red 5)]
       [:do-build-structure "y" :tasting-room]
       [:create-token (space-name-from-wine-by-parts "y" :sparkling 8)]
       [:do-build-structure "y" :windmill]
       [:do-summer]
       [:do-fall]
       [:do-winter]
       [:year-end])))

(defscenario "animate plant vine"
  (-> (new-game*)
      (state-after-action-sequence
       [:viti-move-cards "green-deck" "y:hand" ["sangio:1" "malvas:1"]]
       (fake-request "y"))
      (assoc :answers {"y" "y:f5"})
      (state-after-action-sequence
       [:on-field-to-plant-vine-chosen "sangio:1"]
       [:space-plant-vine "y" false])))

(defaction-coded GAME-CODE :place-test-roosters [state side]
  (perform-sequence state
                    [:move-figure "m:r0f" "m:r1r" "yr"]
                    [:move-figure "m:r0f" "m:r2r" "rr"]))

(defscenario "winter"
  (-> (new-game*)
      (state-after-action-sequence
       [:set-var "lira:r" 10]
       [:set-var "lira:y" 2]
       [:place-test-roosters "r"]
       [:do-winter]
       [:year-end])))

(defscenario "harvest & yoke"
  (-> (new-game*)
      (state-after-action-sequence
       [:viti-move-cards "green-deck" "y:hand" ["sangio:1" "malvas:1" "malvas:2" "malvas:3" "malvas:4"
                                           "trebbi:1"]]
       [:do-plant-vine "y" "sangio:1" "f5"]
       [:do-plant-vine "y" "malvas:1" "f5"]
       [:do-plant-vine "y" "malvas:2" "f6"]
       [:do-plant-vine "y" "trebbi:1" "f7"]
       [:do-build-structure "y" :yoke]
       [:place-test-roosters "r"]
       [:do-winter]
       )))

(defscenario "make wine"
  (-> (new-game*)
      (state-after-action-sequence
       [:create-token (crushpad-space-name-of-value "y" :white 1)]
       [:create-token (crushpad-space-name-of-value "y" :white 2)]
       [:create-token (crushpad-space-name-of-value "y" :white 3)]
       [:create-token (crushpad-space-name-of-value "y" :red 1)]
       [:create-token (crushpad-space-name-of-value "y" :red 2)]
       [:create-token (crushpad-space-name-of-value "y" :red 3)]
       [:create-token (crushpad-space-name-of-value "y" :white 6)]
       [:do-build-structure "y" :medium-cellar]
       [:do-build-structure "y" :large-cellar]
       [:place-test-roosters "r"]
       [:do-winter])))


(defscenario "fill orders"
  (-> (new-game*)
      (state-after-action-sequence
       [:create-token (space-name-from-wine-by-parts "y" :white 1)]
       [:create-token (space-name-from-wine-by-parts "y" :white 2)]
       [:create-token (space-name-from-wine-by-parts "y" :white 3)]
       [:create-token (space-name-from-wine-by-parts "y" :red 1)]
       [:create-token (space-name-from-wine-by-parts "y" :red 2)]
       [:create-token (space-name-from-wine-by-parts "y" :red 3)]
       [:create-token (space-name-from-wine-by-parts "y" :white 6)]
       [:create-token (space-name-from-wine-by-parts "y" :blush 4)]
       [:create-token (space-name-from-wine-by-parts "y" :blush 7)]
       [:create-token (space-name-from-wine-by-parts "y" :sparkling 7)]
       [:create-token (space-name-from-wine-by-parts "y" :sparkling 8)]
       [:viti-move-cards "purple-deck" "y:hand" ["r1w3" "r2w2b5" "r2w2" "r2w4" "w2s8" "r3w1" "s9"]]

       [:do-build-structure "y" :medium-cellar]
       [:do-build-structure "y" :large-cellar]
       [:place-test-roosters "r"]
       [:do-winter]
       [:year-end]
       )))

(defscenario "year end"
  (-> (new-game*)
      (state-after-action-sequence
       [:create-token (space-name-from-wine-by-parts "y" :white 8)]
       [:create-token (space-name-from-wine-by-parts "y" :white 9)]
       [:create-token (space-name-from-wine-by-parts "y" :red 1)]
       [:create-token (space-name-from-wine-by-parts "y" :red 3)]
       [:create-token (space-name-from-wine-by-parts "y" :white 6)]
       [:create-token (space-name-from-wine-by-parts "y" :blush 4)]
       [:create-token (space-name-from-wine-by-parts "y" :blush 7)]
       [:create-token (space-name-from-wine-by-parts "y" :sparkling 8)]
       [:create-token (crushpad-space-name-of-value "y" :white 6)]
       [:viti-move-cards "purple-deck" "y:hand" ["r1w3" "r2w2b5" "r2w2" "r2w4" "w2s8" "r3w1" "s9"]]
       [:viti-move-cards "green-deck" "y:hand" ["malvas:1" "malvas:2"]]
       [:viti-move-cards "green-deck" "r:hand" ["saubla:2" "saubla:3" "saubla:4" "saubla:5" "cabsau:1" "cabsau:2" "cabsau:3"
                                           "cabsau:4" "chardo:1" ]]
       (fake-request "y")
       [:year-end])))

(defscenario "game end - win"
  (-> (new-game*)
      (state-after-action-sequence
       [:set-var "vp:y" 21]
       [:set-var "vp:r" 20]
       (fake-request "y")
       [:year-end])))

(defscenario "game end - tiebreak"
  (-> (new-game*)
      (state-after-action-sequence
       [:set-var "vp:y" 20]
       [:set-var "vp:r" 20]
       [:set-var "lira:r" 5]
       (fake-request "y")
       [:year-end])))

(register-scenarios GAME-CODE scenarios)
