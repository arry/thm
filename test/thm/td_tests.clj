(ns thm.td-tests
  (:use
        [thm.td.td-engine :exclude [defaction]]
        thm.td.td-utils
        thm.game-engine.game-base)
  (:require
   [thm.utils :refer (all-cards-in-zone get-var get-request-for-role)]
   [taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug)]
   [thm.game-engine.game-engine :refer [player-role]]))

(defn new-game* []
  (-> (new-game ["John" "Bill"] {:seed 1})
      (assoc :pending-actions [])))

(defn state-for-event [r event & [f]]
  (cond-> (new-game*)
    true (state-after-action-sequence [:deal-cards "strategy-deck" 5])
    f (f)
    true (state-after-action-sequence [:do-event r event])))

;; (fact "draw agendas interaction"
;;       (let [st (-> (new-game*)
;;                    (state-after-action-sequence :do-draw-agendas-phase)
;;                    (state-after-answer "politi:1" "u")
;;                    (state-after-answer "woropi:1" "s")
;;                    (state-after-pending-actions))]
;;         (get-in st [:zones "u:agenda" :cards]) => (just ["politi:1"])
;;         (get-in st [:zones "s:agenda" :cards]) => (just ["woropi:1"])))

;; (fact "SOPs"
;;       (let [st (state-for-event "u" "sops")]
;;         (fact "start the effect"
;;               (get-var st "ef:u") => ["sops"])
;;         (fact "do a command action"
;;               (get-request-for-role (state-after-action-sequence st [:do-command "u" 3]) "u")
;;               => [:choose-command [:choose-command-t 4] 4])))

;; (fact "Speach to the Nation"
;;       (let [st (state-for-event "u" "sttn")]
;;         (fact (get-request-for-role st "u") => (just [:choose-place-influence anything
;;                                                       3 2 false #{"l" "v" "u"} nil]))))

;; (fact "Fidel Castro"
;;       (let [st (state-for-event "s" "fidcas")]
;;         (fact (get-request-for-role st "s") => (just [:choose-place-influence anything
;;                                                       3 nil false #{"c" "k"} nil]))))

;; (fact "Close Allies"
;;       (let [st (state-for-event "u" "cloall")]
;;         (fact (get-request-for-role st "u") => (just [:choose-place-influence anything
;;                                                       2 nil false #{"k" "i" "t"} nil]))))

;; (fact "Nuclear Submarines"
;;       (let [st (state-for-event "u" "nucsub")]
;;         (fact (get-request-for-role st "u") => (just [:choose-place-influence anything
;;                                                       2 nil false #{"c" "a" "b"} nil]))))

;; (fact "Scramble"
;;       (let [st (state-for-event "u" "scramb")]
;;         (fact (get-request-for-role st "u") => (just [:choose-place-influence anything
;;                                                       3 1 false (nine-of string?) nil]))))

;; (fact "EXCOMM"
;;       (let [st (state-for-event "u" "excomm")]
;;         (fact (get-request-for-role st "u") =>
;;               (just [:choose-place-influence anything
;;                      4 2 true #{"a" "b" "c"
;;                                 "k" "l"
;;                                 "u" "v"} nil])))
;;       (let [st (state-for-event "u" "excomm"
;;                                 #(update % :vars
;;                                          assoc
;;                                          "i:u:a" 3
;;                                          "i:u:v" 2))]
;;         (fact "when US has influence in more battlegrounds "
;;          (get-request-for-role st "u") =>
;;               (just [:choose-place-influence anything
;;                      4 2 true #{ "b" "c"
;;                                 "k" "l"
;;                                 "u" } nil]))))

;; (fact "Lessons of Munich"
;;       (let [st (state-for-event "u" "leofmu")]
;;         (fact (get-request-for-role st "u") => (just [:choose-place-influence anything
;;                                                       4 2 false #{"b" "i" "t"} nil]))))

;; (fact "Quarantine"
;;       (let [st (state-for-event "u" "quaran")]
;;         (fact (get-request-for-role st "u") => (just [:choose-place-influence anything
;;                                                       2 nil false #{"a"} nil]))))

;; (fact "Moscow is our Brain"
;;       (let [st (state-for-event "s" "miob")]
;;         (fact (get-request-for-role st "s") =>
;;               (just [:choose-place-influence anything
;;                      4 2 true #{"b" "c"} nil]))))

;; (fact "Strategic Balance"
;;       (let [st (state-for-event "s" "strbal")]
;;         (fact (get-request-for-role st "s") => (just [:choose-place-influence anything
;;                                                       3 nil true #{"a"} nil]))))

;; (fact "Defensive Missiles"
;;       (let [st (state-for-event "s" "defmis")]
;;         (fact (get-request-for-role st "s") => (just [:choose-place-influence anything
;;                                                       2 nil true #{"u" "v"} nil]))))

;; (fact "The Guns of August"
;;       (let [st (state-for-event "u" "tgoa"
;;                                 #(update % :vars
;;                                          assoc
;;                                          "d:u:m" 5))]
;;         (fact (get-request-for-role st "u") =>
;;               (just [:choose-change-defcon [:choose-change-defcon-t "u" 1 #{"m" "p" "w"}] "u" 1 #{"m" "p" "w"} -2 2 nil]))
;;         (let [st (state-after-answer st {"m" 2})]
;;           (fact (get-var st "d:u:m") => 7)
;;           (fact (get-request-for-role st "u") => (just [:choose-command anything 1])))))

;; (fact "Intelligence Reports"
;;       (let [st (state-for-event "u" "intrep")]
;;         (fact (all-cards-in-zone st "u:hand") => (contains "fidcas"))
;;         (fact (get-request-for-role st "u")
;;               => (just [:choose-mode
;;                         [:intrep-play-or-discard "fidcas"]
;;                         ["play" "discard"]]))
;;         (fact (-> st
;;                   (state-after-answer "play")
;;                   (get-request-for-role "u"))
;;               => (has-prefix [:choose-mode [:choose-mode-of-play "fidcas"]]))

;;         (fact (-> st
;;                   (state-after-answer "discard")
;;                   (all-cards-in-zone "strategy-discard"))
;;               => (contains "fidcas"))))

;; (fact "Summit Meeting"
;;       (let [st (state-for-event "u" "summee")]
;;         (fact (get-request-for-role st "u") => (just [:choose-cards [:summee-choose-cards-to-discard]
;;                                                       "u:hand"
;;                                                       ["defmis" "inofcu" "u2ph" "baofpi" "strbal"]]))))

;; ;; Note: no test for A Face-Saver because what could go wrong?

;; (fact "Fifty-Fifty"
;;       (let [res (-> (new-game*)
;;                     (perform-sequence [:fiffif "u"]))]
;;         (:deltas res) => (just [[:text :fiffif-no-dominator]]))

;;       (let [st (-> (new-game*)
;;                    (assoc-in [:vars "i:s:v"] 3)
;;                    (perform-sequence [:fiffif "u"])
;;                    :state)]
;;         (get-request-for-role st "s") => (just [:choose-change-defcon anything
;;                                                 "s" 2 #{"m" "p" "w"} -1 1 nil])

;;         (let [st (state-after-answer st {"m" -1, "p" 1})]
;;           (get-var st "d:s:m") => 0
;;           (get-var st "d:s:p") => 1)))

;; (fact "U Thant"
;;       (let [st (state-for-event "u" "utha" #(update % :vars
;;                                                     assoc "d:u:m" 5, "d:u:p" 4, "d:u:w" 3))]
;;         (map #(get-var st %) ["d:u:m" "d:u:p" "d:u:w"]) => [4 3 2]))

;; (fact "Containment"
;;       (let [st (state-for-event "u" "contai")]
;;         (fact "start the effect"
;;               (get-var st "ef:s") => ["contai"])

;;         (fact "limit lower bound on a request"
;;               (let [st (-> st
;;                            (assoc-in [:vars "initiative-player"] "s")
;;                            (update-in [:zones "s:hand" :cards] conj "tgoa")
;;                            (state-after-action-sequence :play-strategy-cards)
;;                            (state-after-answer ["tgoa" nil])
;;                            (state-after-answer "event"))]
;;                 (fact (get-request-for-role st "s")
;;                       => (just [:choose-change-defcon anything "s" 1 #{"m" "p" "w"} 0 2 nil]))))

;;         (fact "Cannot play U Thant"
;;               (let [st (-> st
;;                            (update :vars assoc "initiative-player" "s",
;;                                    "d:s:m" 3, "d:s:p" 3, "d:s:w" 3)
;;                            (update-in [:zones "s:hand" :cards] conj "utha")
;;                            (state-after-action-sequence :play-strategy-cards)
;;                            (state-after-answer ["utha" nil]))]
;;                 (get-request-for-role st "s")
;;                 => (just [:choose-mode anything ["command"]])))))

;; (fact "Mathematical Precision"
;;       (let [st (state-for-event "u" "matpre")]
;;         (get-request-for-role st "u")
;;         => (just [:choose-change-defcon anything "u" 1 #{"p"} -2 2 nil])))

;; (fact "Public Protests"
;;       (let [st (state-for-event "u" "pubpro")]
;;         (get-request-for-role st "u")
;;         => (just [:choose-remove-influence [:choose-remove-influence-t "u" 5 :single-bg false]
;;                   "u" 5 :single-bg false #{"i" "t"}])
;;         (-> (state-after-answer st {"i" 1})
;;             (get-var "i:u:i")) => 0))

;; (fact "Operation Mongoose"
;;       (let [st (state-for-event "u" "opemon"
;;                                 #(update % :vars assoc "d:u:m" 6))]
;;         (get-var st "prestige") => -1

;;         (get-request-for-role st "s")
;;         => (just [:choose-change-defcon anything "u" 1 (three-of string?) -1 1 nil])

;;         (-> (state-after-answer st {"m" 1})
;;             (get-var "d:u:m")) => 7))

;; (fact "Air Strike"
;;       (fact "No influence, no remove option"
;;             (let [st (state-for-event "u" "airstr"
;;                                       #(update % :vars assoc "i:s:c" 0, "i:s:k" 0))]
;;               (get-request-for-role st "u") => (just [:choose-mode :airstr-remove-or-place
;;                                                    ["place"]])))

;;       (let [st (state-for-event "u" "airstr"
;;                                 #(update % :vars assoc "i:s:c" 3, "i:s:k" 2))]
;;         (get-request-for-role st "u")
;;         => (just [:choose-mode :airstr-remove-or-place
;;                   ["remove" "place"]])

;;         (let [st (state-after-answer st "remove")]
;;           (get-request-for-role st "u")
;;           => (just [:choose-remove-influence [:choose-remove-influence-t "s" 1 :half false]
;;                     "s" 1 :half false #{"c" "k"}])

;;           (->
;;            (state-after-answer st {"c" 1})
;;            (get-var "i:s:c")) => 1)

;;         (let [st (state-after-answer st "place")]
;;           (get-request-for-role st "u")
;;           => (just [:choose-place-influence anything
;;                     2 nil false #{"l"} nil]))))

;; (fact "Non-Invasion Pledge"
;;       (let [st (state-for-event "u" "noinpl"
;;                                 #(update % :vars assoc "i:s:t" 3))]
;;         (get-request-for-role st "u")
;;         => (just [:choose-remove-influence anything
;;                   "s" 2 :single-bg false #{"t"}])

;;         (let [st (state-after-answer st {"t" 2})]
;;           (get-var st "i:s:t") => 1

;;           (get-request-for-role st "u")
;;           => (just [:choose-change-defcon anything "u" 1 #{"p"} -2 2 nil]))))

;; (fact "Offensive Missiles"
;;       (let [st (state-for-event "u" "offmis")]
;;         (get-request-for-role st "u")
;;         => (just [:choose-place-influence anything
;;                   3 1 false #{"k" "t" "i"} nil]))

;;       (let [st (state-for-event "u" "offmis"
;;                                 #(update % :vars assoc "d:u:p" 3))]
;;         (get-request-for-role st "u") => nil))

;; (fact "U-2 Photographs"
;;       (let [st (state-for-event "u" "u2ph")]
;;         (get-request-for-role st "u")
;;         => (just [:choose-command-onto anything 3 #{"c" "a" "b"}])

;;         (let [st (state-after-answer st ["c" 2])]
;;           (get-var st ["i:u:c"]) => 2
;;           (get-var st ["d:u:m"]) => 1)))

;; (fact "National Liberation"
;;       (let [st (state-for-event "s" "natlib")]
;;         (get-request-for-role st "s")
;;         => (just [:choose-command-onto anything 3 #{"k" "i" "t"}])

;;         (let [st (state-after-answer st ["k" 2])]
;;           (get-var st ["i:s:k"]) => 2
;;           (get-var st ["d:s:p"]) => 1)))

;; (fact "Eyeball to Eyeball"
;;       (let [st (state-for-event "u" "eytoey")]
;;         (get-request-for-role st "u") => nil)

;;       (let [st (state-for-event "u" "eytoey"
;;                                 #(update % :vars assoc
;;                                          "d:u:m" 3
;;                                          "d:s:m" 2))]
;;         (get-request-for-role st "u")
;;         => (just [:choose-place-influence anything
;;                   3 nil true #{"c" "k"} nil])))

;; (fact "MRBMs & IRBMs"
;;       (let [st (state-for-event "s" "mrbirb"
;;                                 #(update % :vars
;;                                          assoc
;;                                          "d:s:m" 5))]
;;         (fact (get-request-for-role st "s") =>
;;               (just [:choose-change-defcon anything "s" 1 #{"m"} -2 2 nil]))
;;         (let [st (state-after-answer st {"m" 2})]
;;           (fact (get-var st "d:s:m") => 7)
;;           (fact (get-request-for-role st "s") => (just [:choose-command anything 1])))))

;; (fact "Berlin Blockade"
;;       (let [st (state-for-event "s" "berblo")]
;;         (get-var st "prestige") => 2

;;         (get-request-for-role st "u")
;;         => (just [:choose-change-defcon anything "s" 1 (three-of string?) -2 2 nil])))

;; (fact "U-2 Downed"
;;       (let [st (state-for-event "s" "u2do"
;;                                 #(update % :vars
;;                                          assoc
;;                                          "i:u:c" 3, "i:u:k" 4))]
;;         (get-request-for-role st "s")
;;         => (just [:choose-place-influence anything
;;                   2 nil false #{"t"} nil])

;;         (-> (state-after-answer st {})
;;             (get-request-for-role "s"))
;;         => (just [:choose-remove-influence [:choose-remove-influence-t "u" 1 :half false]
;;                   "u" 1 :half false #{"c" "k"}])))

;; (fact "Wave and Smile"
;;       (let [st (state-for-event "u" "waansm"
;;                                 #(update % :vars assoc
;;                                          "i:u:k" 2, "i:u:c" 3))]
;;         (get-request-for-role st "u")
;;         => (just [:choose-remove-influence anything
;;                   "u" 2 :many-bgs false #{"i" "t" "c" "k"}])

;;         (let [st (state-after-answer st {"i" 1, "t" 1})]
;;           (get-var st "i:u:i") => 0
;;           (get-var st "i:u:t") => 0
;;           (get-request-for-role st "u")
;;           => (just [:choose-place-influence anything
;;                     2 nil false (nine-of string?) :exact-amount]))

;;         (fact "1 influence to place when 1 removed"
;;               (let [st (state-after-answer st {"c" 1})]
;;                 (get-request-for-role st "u")
;;                 => (just [:choose-place-influence anything
;;                           1 nil false (nine-of string?) :exact-amount])))

;;         (fact "Don't place if nothing removed"
;;               (let [st (state-after-answer st {})]
;;                 (get-request-for-role st "u") => nil))))

;; (fact "Missile Trade"
;;       (let [st (state-for-event "s" "mistra"
;;                                 #(update % :vars assoc "i:s:v" 4, "i:s:a" 3))]
;;         (get-request-for-role st "s")
;;         => (just [:choose-remove-influence anything
;;                   "s" 3 :many-bgs true #{"b" "c" "v" "a"}])))

;; (fact "Suez-Hungary"
;;       (let [tbl [["i:u:i" 0, "c:s" 15, 1]
;;                  ["i:u:i" 4, "c:s" 15, 5]
;;                  ["i:u:i" 5, "c:s" 15, 5]
;;                  ["i:u:i" 5, "c:s" 3, 3]
;;                  ["i:u:i" 3, "c:s" 3, 3]
;;                  ["i:u:i" 3, "c:s" 2, 2]
;;                  ["i:u:i" 2, "i:s:i" 1, 3]
;;                  ["i:u:i" 2, "i:s:i" 2, 3]
;;                  ["i:u:i" 2, "i:s:i" 3, 3]
;;                  ["i:u:i" 2, "i:s:i" 4, 4]]]
;;         (doall (for [[var1 val1, var2 val2, expected] tbl]
;;                  (let [st (state-for-event "s" "suehun"
;;                                            #(update % :vars assoc var1 val1, var2 val2))]
;;                    (get-var st "i:s:i") => expected)))))

;; (fact "Maskirovka"
;;       (let [st (state-for-event "s" "maskir"
;;                                 #(update % :vars assoc
;;                                          "d:s:m" 5))]
;;         (get-request-for-role st "s") => nil)

;;       (let [st (state-for-event "s" "maskir"
;;                                 #(update % :vars assoc
;;                                          "d:s:m" 0))]
;;         (get-request-for-role st "s")
;;         => (just [:choose-place-influence anything
;;                   3 1 false #{"a" "b" "c"} nil])))

;; (fact "Bay of Pigs"
;;       (let [st (state-for-event "s" "baofpi"
;;                                 #(update % :vars assoc
;;                                          "i:u:l" 2))]
;;         (get-request-for-role st "u")
;;         => (just [:choose-mode :baofpi-remove-or-effect
;;                   ["remove" "effect"]])

;;         (-> (state-after-answer st "remove")
;;             (get-var "i:u:l"))
;;         => 0

;;         (let [st (state-after-answer st "effect")]
;;           (get-var st "ef:u") => ["baofpi"]

;;           (fact "limit lower bound on a request"
;;                 (let [st (-> st
;;                              (assoc-in [:vars "initiative-player"] "u")
;;                              (update-in [:zones "u:hand" :cards] conj "tgoa")
;;                              (state-after-action-sequence :play-strategy-cards)
;;                              (state-after-answer ["tgoa" "event"])
;;                              )]
;;                   (fact (get-request-for-role st "u")
;;                         => (just [:choose-change-defcon anything "u" 1 #{"m" "p" "w"} 0 2 nil]))))))

;;       (let [st (state-for-event "s" "baofpi"
;;                                 #(update % :vars assoc
;;                                          "i:u:l" 0))]
;;         (get-request-for-role st "u")
;;         => (just [:choose-mode anything
;;                   ["effect"]])))

;; (fact "Turn Back the Ships"
;;       (let [st (state-for-event "s" "tbts"
;;                                 #(update % :vars assoc
;;                                          "d:s:m" 5
;;                                          "d:s:p" 3
;;                                          "d:s:w" 3))]
;;         (get-request-for-role st "s")
;;         => (just [:choose-change-defcon anything
;;                   "s" 1 #{"m"} -2 0 nil]))

;;       (let [st (state-for-event "s" "tbts"
;;                                 #(update % :vars assoc
;;                                          "d:s:m" 5
;;                                          "d:s:p" 5
;;                                          "d:s:w" 3))]
;;         (get-request-for-role st "s")
;;         => (just [:choose-change-defcon anything
;;                   "s" 1 #{"m" "p"} -2 0 nil])))

;; (fact "Invasion of Cuba"
;;       (let [st (state-for-event "u" "inofcu")]
;;         (get-request-for-role st "u")
;;         => (just [:choose-change-defcon anything
;;                   "u" 1 #{"m"} 0 2 nil])

;;         (-> (state-after-answer st {"m" 2})
;;             (get-request-for-role "u"))
;;         => (just [:choose-change-defcon anything "u" 1 #{"p" "w"} -2 0 :exact-amount]))

;;       (let [st (state-for-event "u" "inofcu"
;;                                 #(update % :vars assoc
;;                                          "d:u:m" 7))]
;;         (get-request-for-role st "u") => nil)

;;       (let [st (state-for-event "u" "inofcu"
;;                                 #(update % :vars assoc
;;                                          "d:u:m" 6))]
;;         (get-request-for-role st "u")
;;         => (just [:choose-change-defcon anything
;;                   "u" 1 #{"m"} 0 2 nil])

;;         (-> (state-after-answer st {"m" 2})
;;             (get-request-for-role "u"))
;;         => (just [:choose-change-defcon anything "u" 1 #{"p" "w"}
;;                   -1 ;; Even if we answer 2, the actual change was 1 hence can
;;                      ;; change max by 1 here.
;;                   0 :exact-amount])))

;; (fact "side-selection option"
;;       (let [st (new-game ["John" "Bill"] {:side-selection "owner-u"})]
;;         (player-role st "John") => "u"))

;; (fact "When a precondition of Offensive Missiles is not met, don't ask to play for event"
;;       (let [st (-> (new-game*)
;;                    (state-after-action-sequence
;;                     [:move-card "strategy-deck" "u:hand" "offmis"]
;;                     [:set-vars
;;                      ["d:u:p" 3] ;; more escalated than 3 area
;;                      ["initiative-player" "u"]]
;;                     :play-strategy-cards)
;;                    (state-after-answer ["offmis" nil]))]
;;         (get-request-for-role st "u")
;;         => (just [:choose-mode anything ["command" "command+pl"]])))

;; (fact "When a precondition of Offensive Missiles is not met, don't ask opponent to play event"
;;       (let [st (-> (new-game*)
;;                    (state-after-action-sequence
;;                     [:move-card "strategy-deck" "s:hand" "offmis"]
;;                     [:set-vars
;;                      ["d:u:p" 3] ;; more escalated than 3 area
;;                      ["initiative-player" "s"]]
;;                     :play-strategy-cards)
;;                    (state-after-answer ["offmis" nil]))]
;;         (get-request-for-role st "s")
;;         => (just [:choose-mode anything ["opp-command"]])

;;         (state-after-answer st "opp-command")
;;         => (contains {:request-map (just {"s" [:choose-command [:choose-command-t 2] 2]})})
;;         )
;;       )

;; (fact "Bay of Pigs"
;;       (let [st (-> (new-game*)
;;                    (state-after-action-sequence
;;                     [:move-cards "strategy-deck" "s:hand" ["matpre" "baofpi"]]
;;                     [:move-cards "strategy-deck" "u:hand" ["utha" "noinpl"]]
;;                     [:set-vars
;;                      ["d:u:p" 2]
;;                      ["initiative-player" "s"]]
;;                     :play-strategy-cards)
;;                    (state-after-answer ["baofpi" "event"])

;;                    ;; us
;;                    (state-after-answer "effect")

;;                    (state-after-answer ["noinpl" "event"])
;;                    (state-after-answer {})

;;                    )]

;;         (get-request-for-role st "u") => [:choose-change-defcon [:choose-change-defcon-t "u" 1 #{"p"}] "u" 1 #{"p"} 0 2 nil]

;;         (let [st (-> st
;;                      (state-after-answer {})

;;                      ;; su
;;                      (state-after-answer ["matpre" "opp-command"])

;;                      ;; us
;;                      (state-after-answer true))]
;;           (get-request-for-role st "u")
;;           ;;                                                                            v
;;           => [:choose-change-defcon [:choose-change-defcon-t "u" 1 #{"p"}] "u" 1 #{"p"} 0 2 nil]
;;           ;;                                                                            ^


;;       )))
