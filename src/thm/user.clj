(ns thm.user
  "Local stuff for easier dev."
  (:use boot.user thm.game-engine.game-base  )
  (:require
   [system.repl :refer [system set-init! start stop reset]]
   [taoensso.carmine :as car]
   [taoensso.nippy :as nippy]
   [thm.tm.tm-engine]
   [thm.game-engine.all-game-modules]
   [thm.systems :refer [dev-system prod-system]]
   )
  (:use
   [clojure.repl])
  )

(defmacro wcar** [& body]
  `(car/wcar {} ~@body))

(defn has-it-good? [engine-checkpoint]
  (map? (get-in (:state engine-checkpoint) [:zones "strategy-deck"])))

(defn has-corp? [st c] (contains? (set (get-in st [:zones "s:choosing-corp" :cards])) c))

(defn make [seed] (-> (thm.tm.tm-engine/new-game ["J"] {:seed seed}) state-after-pending-actions))

(defn has-proj? [st c] (contains? (set (get-in st [:zones "s:hand" :cards])) c))

(defn findwithpred [pred]
  (first (filter #(pred (make %)) (range))))

(defn findcorp [corp]
  (findwithpred #(has-corp? % corp)))

(defn findthat [proj]
  (findwithpred #(has-proj? % proj)))

(defn findwithboth [proj1 proj2]
  (findwithpred #(and (has-proj? % proj1)
                      (has-proj? % proj2))))


(defn findwiththree [proj1 proj2 proj3]
  (findwithpred #(and (has-proj? % proj1)
                      (has-proj? % proj2)
                      (has-proj? % proj3))))

(defn find-corp-and-proj [corp proj]
  (findwithpred #(and (has-corp? % corp)
                      (has-proj? % proj))))



(comment

  (require '[thm.backend-components.ds :as ds])
  (def e (eng/new-engine ["J" "B"]))
  (def cp (eng/create-checkpoint e))
  (def st (new-game ["John" "Bill"] {:seed 1}))

  )

(set-init! #'dev-system)
