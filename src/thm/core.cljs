(ns thm.core
  (:require [reagent.core :as r]
            [re-frame.core :as re-frame :refer (dispatch)]
            [thm.subs] [thm.handlers] ; needed here to kick-start re-frame
            [thm.views.views :as views]
            [thm.views.marker :as marker]
            [thm-accountant.core :as accountant]
            [taoensso.timbre :as timbre  :refer-macros (tracef debugf infof warnf errorf debug)]
            [clojure.string :as str]
            [goog.dom :as dom]
            [goog.events :as events]
            )
  (:require-macros
   [cljs.core.async.macros :as asyncm :refer (go go-loop)]))

;; (timbre/set-level! :trace) ; For debugging

(defn handle-key-press [e]
  (dispatch [:key-press e]))

(defn handle-resize [e]
  (dispatch [:resize]))

(defn ^:export run []
  (r/render [views/app]
            (js/document.getElementById "container")))

(defonce ^:dynamic *first-time* true)

(defn nav-handler [path]
  (let [page-desc
        (if (= path "/")
          ["/"]
          (rest (str/split path #"/")))]
    ;; TODO change page title
    (dispatch [:navigate page-desc])))

(defn init! []
  (if *first-time*
    (do
      (accountant/configure-navigation!
       {:nav-handler nav-handler
        :path-exists? (fn [path]
                        ;; XXX white-list paths?
                        true)
        }
       )
      (re-frame/dispatch-sync [:initialize-db])
      (re-frame/dispatch-sync [:initialize-sente])
      (run)
      (accountant/dispatch-current!)
      (set! *first-time* false))
    (do (run)
        (re-frame/dispatch-sync [:reloaded]))))

(enable-console-print!)

(events/listen js/window
               goog.events.EventType.KEYPRESS
               handle-key-press)

(events/listen js/window
               goog.events.EventType.RESIZE
               handle-resize)

(events/listen js/window
               goog.events.EventType.LOAD
               init!)
