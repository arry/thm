(ns thm.test-game-base
  (:use  thm.game-engine.game-base)
  (:require

   [taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug)]
   [thm.game-engine.game-engine :refer [player-role]]))

(defaction-base a [state]
  (perform-sequence state :b :c))

(defaction-base b [state]
  (perform-sequence state [:put-request {"John" [:yn :wanna-go-first]}]
                    :h))

(defaction-base h [state]
  (do-nothing state))

(defaction-base c [state]
  (do-nothing state))

(defaction-base deeper [state]
  (perform-sequence state :a :t))

;; (fact "pending actions when performing sequence with requests"
;;       (let [st {:test "state"}
;;             new-state (:state (a st))]
;;         (fact "request map is set"
;;               (:request-map new-state) => (just {"John" identity}))
;;         (fact "pending actions are set"
;;               (:pending-actions new-state) => (just '(:h :c))))
;;       (let [st {:test "state with old pending actions"
;;                 :pending-actions [:x :y]}
;;             new-state (:state (a st))]
;;         (fact "pending actions are prepended to old ones"
;;               (:pending-actions new-state) => (just '(:h :c :x :y))))
;;       (fact "deeper sequence nesting"
;;             (let [st {:test "deeper state"
;;                       :pending-actions [:x :y]}
;;                   new-state (:state (deeper st))]
;;               (:pending-actions new-state) => (just '(:h :c :t :x :y)))))
