-- Deploy sqitch/sqitch.plan:games to pg

BEGIN;

CREATE TYPE game_time_kind AS ENUM ('live', 'async');

CREATE TABLE games (
    gid serial PRIMARY KEY,
    code varchar(10) not null,
    kind game_time_kind null default 'live',
    status varchar(12) not null,
    created_at timestamp not null default now(),

    owner_username text references users (username),
    comment varchar(100) null,
    game_settings jsonb null,
    players varchar(20)[] not null,  -- 20 corresponds to :max-username-length in src/cljc/thm/utils.cljc
    last_modified_at timestamp not null default now(),

    runtime_info jsonb null,
    outcome jsonb null
);

CREATE TRIGGER update_games_modtime
BEFORE UPDATE ON games
FOR EACH ROW EXECUTE PROCEDURE update_last_modified();

create table game_engine_checkpoints (
    gid integer unique references games(gid) on delete cascade,
    engine_checkpoint bytea
);

COMMIT;
