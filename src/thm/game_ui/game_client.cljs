(ns thm.game-ui.game-client
  "Game client: manipulate client state in accordance to received deltas."
  (:require [taoensso.timbre :refer-macros (debug warnf warn spy)]
            [re-frame.core :refer [dispatch]]
            [thm.utils :refer [multipart-name-from-descr all-cards-in-zone first-index
                               all-figures-in-space
                               choose-mode-like-request-kind?
                               map-values
                               as-set
                               show-choose-mode-modal
                               index-of
                               remove-from-vector-at-index
                               add-to-vector-at-index
                               get-your-request]]
            [thm.game-ui.card-animator :as card-animator]
            [clojure.string :as str]
            [thm.game-ui.game-ui-module :as game-ui-module]
            [thm.game-ui.all-game-ui-modules :refer [module-from-cs]]))

(defn update-zone [cs zone-descr f & [opt]]
  ;; Note: as opposed to clojure.core/update, this function doesn't receive
  ;; additional arguments for f, because it needs an optional argument for
  ;; itself.
  (let [cards (all-cards-in-zone cs zone-descr)
        new-cards (f cards)
        new-cards (if (number? new-cards)
                    new-cards
                    (vec new-cards))]
    (assoc-in cs [:zones (multipart-name-from-descr zone-descr) :cards] new-cards)))

(defn remove-cards [cs zone-descr cids]
  (update-zone cs zone-descr #(remove (set cids) %)))

(defn remove-cards-active [cs zone-descr cids]
  (update-zone cs zone-descr (fn [cards]
                               (remove #(contains? (set cids) (:cid %)) cards))))

(defn remove-retain-cards-active [cs zone-descr cids]
  (let [cards (all-cards-in-zone cs zone-descr)
        groups (group-by #(contains? (set cids) (:cid %)) cards)]
    ;; TODO reorder by cid, like in game-base/move-cards
    [(vec (get groups true))
     (update cs :zones
             update (multipart-name-from-descr zone-descr)
             ,      assoc :cards (vec (get groups false)))]))

(defn add-cards [cs zone-descr cards]
  (update-zone cs zone-descr #(concat % cards)))

(defn remove-card-count [cs zone-descr card-count & [opt]]
  (update-zone cs zone-descr #(- % card-count) opt))

(defn add-card-count [cs zone-descr card-count]
  (update-zone cs zone-descr #(+ % card-count)))

(defn build-request-helper [cs]
  (when-let [[kind text-descr & args :as req] (get-your-request cs)]
    (case kind
      ;; XXX for :choose-mode and :yn, duplication with prepare-modes.
      :choose-mode {:button-actions (mapv (fn [mode]
                                            {:answer mode, :action-id mode})
                                          (first args))}
      :yn {:button-actions [{:answer true, :action-id "yes", :for-humans "Yes"}
                            {:answer false, :action-id "no", :for-humans "No"}]}
      :choose-cards-flexible (let [[zone-name cids options] args
                                   cids (if (:all-in-zone? options)
                                          (map :cid (all-cards-in-zone cs zone-name))
                                          cids)]
                               {:set-of-cids (set cids)
                                :lower (or (:lower options) 0)
                                :higher (or (:higher options) (count cids))})
      :choose-space (let [[spaces options] args]
                      {:set-of-space-names (set spaces)
                       :button-actions (when (:can-pass? options)
                                         [{:answer "*pass*", :action-id "pass", :for-humans "Pass"}])})
      :choose-spaces (let [[spaces options] args]
                       {:set-of-space-names (set spaces)
                        :lower (or (:lower options) 0)
                        :higher (or (:higher options) (count spaces))})
      :choose-spaces-from-bags (let [[bags] args]
                                 {:bags bags

                                  :space-name->bag-index
                                  (into {}
                                        (for [bag-index (range (count bags))
                                              space (:spaces (get bags bag-index))]
                                          [space bag-index]))

                                  :set-of-space-names
                                  (into #{}
                                        (for [bag bags
                                              space (:spaces bag)]
                                          space))})

      nil)))

(defn promote-cids-to-active [cids & [opts]]
  (mapv (fn [cid]
         (cond-> {:cid cid}
           (:change-to-facedown? opts) (assoc :facedown? true)))
       cids))

(defn prepare-request-ui [cs]
  (when-let [req (get-your-request cs)]
    (debug "prepare-request-ui" req)
    (when-let [pane-id (game-ui-module/get-pane-associated-with-request (module-from-cs cs) req (:your-role cs))]
      (dispatch [:thm.game-ui.compact/switch-pane pane-id]))

    (when-let [play-area-tabs (game-ui-module/get-play-areas-associated-with-request (module-from-cs cs) req (:your-role cs))]
      (dispatch [:thm.game-ui/activate-one-of-play-area-tabs (as-set play-area-tabs)]))

    (let [[kind text-descr & args] req]
      (when (choose-mode-like-request-kind? kind)
        (show-choose-mode-modal req))))
  cs)

(defn prepare-card-movement-animation [cs from to cids]
  (let [primary-role (:your-role cs)

        from-kind (game-ui-module/get-animation-kind-for-zone-name (module-from-cs cs) from primary-role)
        to-kind (game-ui-module/get-animation-kind-for-zone-name (module-from-cs cs) to primary-role)

        from-animation-kind (case from-kind
                              (nil :v-none) :none
                              :i-non-holding-place :appearing
                              :v-non-holding-place :appearing
                              :v-row :simple
                              :v-stack :simple  ;; TODO cards that are not shown are Appearing.
                              :i-facedown-row :simple-facedown

                              (do (warn "Unrecoginzed from zone kind: " from from-kind)
                                  :none))

        to-animation-kind (case to-kind
                            (nil :v-none) :none
                            :i-non-holding-place :disappearing
                            :v-non-holding-place :disappearing
                            :v-row :simple
                            :v-stack :simple  ;; TODO not really
                            :i-facedown-row :simple-facedown

                            (do (warn "Unrecoginzed to zone kind: " to to-kind)
                                :none)
                            )]
    (cond
      (or (= to-animation-kind :none)
          (and (= from-animation-kind :appearing)
               (= to-animation-kind :disappearing)))
      cs

      :else
      (let [;; XXX This could be wrong, but works for td.
            cids (if (number? cids)
                   (mapv (fn [_] (str "*back*:" (gensym ""))) (range cids))
                   cids)

            cid->old-rect (case from-animation-kind
                            :simple (card-animator/get-original-rects-by-cids primary-role from cids (all-cards-in-zone cs from))
                            ;; XXX wrong because doesn't track the assigned
                            ;; facedown cids and doesn't touch the other cards
                            ;; in the zone; will do for the needs of td, i.e.
                            ;; at most one card at a time.
                            :simple-facedown (card-animator/get-original-rects-by-sequence primary-role from cids)
                            :appearing (card-animator/get-original-rects-by-zone primary-role from cids)
                            {})
            ]
        (cond-> cs
          true
          (assoc-in [:animation-info to]
                    (cond-> {:animate-cids cids
                             :cid->old-rect cid->old-rect}
                      (= to-animation-kind :disappearing)
                      (assoc :fade-mode :disappearing)

                      (= from-animation-kind :appearing)
                      (assoc :fade-mode :appearing)))

          true
          (assoc-in [:animation-info from]
                    {:animate-the-remainder-in-place? true
                     :cid->old-rect cid->old-rect})

          (or (= to-animation-kind :disappearing)
              (= to-animation-kind :simple-facedown))
          (assoc-in [:animation-info to :alternate-cids] cids))))))

(defn prepare-figure-movement-animation [cs from to figure-id]
  (let [primary-role (:your-role cs)

        from-play-area (game-ui-module/get-play-area-of-space (module-from-cs cs) from)
        to-play-area (game-ui-module/get-play-area-of-space (module-from-cs cs) to)

        replaced-from (when-not (= from-play-area to-play-area)
                        (game-ui-module/get-replaced-from-space-name (module-from-cs cs) to))

        figure-id->old-rect (if (= from-play-area to-play-area)
                              (card-animator/get-original-rects-by-figure-ids primary-role from          (all-figures-in-space cs from))
                              (card-animator/get-original-rects-by-space      primary-role replaced-from (all-figures-in-space cs from)))

        from-kind (game-ui-module/get-space-visibility (module-from-cs cs) (or replaced-from from) primary-role)
        to-kind (game-ui-module/get-space-visibility (module-from-cs cs) to primary-role)

        fade-mode (cond (and (= from-kind :invisible)
                             (= to-kind :visible))
                        :appearing

                        (and (= from-kind :visible)
                             (= to-kind :invisible))
                        :disappearing

                        :else nil)
        ]
    (cond-> cs
      true
      (assoc-in [:animation-info to]
                {:animate-figure-ids [figure-id]
                 :figure-id->old-rect figure-id->old-rect
                 :fade-mode fade-mode})

      (= from-kind :visible)
      (assoc-in [:animation-info from]
                {:animate-the-remainder-in-place? true
                 :figure-id->old-rect figure-id->old-rect}))))

(defn create-request-helper [cs]
  (assoc cs :request-helper (build-request-helper cs)))

(defn apply-one-delta [cs delta]
  ;; (debug "apply-one-delta" (:your-role cs) delta)
  (let [[kind & args] delta]
    (case kind
      :pause {:new-cs (assoc cs :paused? true)
              :delay :none}
      :var-set (let [[var-name value] args]
                 (game-ui-module/on-var-set (module-from-cs cs) (:your-role cs) var-name value)
                 {:new-cs (assoc-in cs [:vars var-name] value)
                  :delay (game-ui-module/get-delay-of-var (module-from-cs cs) var-name)})
      :var-unset (let [[var-name] args]
                   {:new-cs (update cs :vars dissoc var-name)
                   :delay (game-ui-module/get-delay-of-var (module-from-cs cs) var-name)})
      :vars-set (let [[mappings] args]
                  {:new-cs (update cs :vars into mappings)
                   :delay (game-ui-module/get-delay-of-vars (module-from-cs cs) (map first mappings))})
      :var-changed (let [[var-name amount new-value] args]
                     {:new-cs (-> cs
                                  (assoc-in [:vars var-name] new-value)
                                  (update-in [:recent var-name] (fnil + 0) amount))
                      :delay (game-ui-module/get-delay-of-var (module-from-cs cs) var-name)})
      :deck-shuffled {:new-cs (update cs :log conj delta)
                      :delay :none}
      :text {:new-cs (update cs :log conj delta)
             :delay :none}
      :remove-last-text {:new-cs (update cs :log pop)
                         :delay :none}
      :request-put {:new-cs (let [[request-map] args]
                              (-> cs
                                  (assoc :request-map request-map
                                         :can-cancel? false)
                                  create-request-helper
                                  prepare-request-ui))
                    :delay :none}
      :request-answered {:new-cs (let [[role] args]
                                   (cond-> cs
                                     true (update :request-map dissoc role)
                                     ;; XXX spread should be in the request-helper,
                                     ;; because we shouldn't have td-specific stuff in
                                     ;; here.
                                     (= role (:your-role cs)) (dissoc :answer :answer-sent? :spread :request-helper)))
                         :delay :none}
      ;; v is visible (contents are cids), a is visible&active (contents are
      ;; maps containing cid and other props), i is invisible (contents is
      ;; count).  Here we don't make distinction between active and visible
      ;; cards, always keeping them as maps with a mandatory :cid key.
      :cards-created-a {:new-cs (let [[zn cards] args]
                                  (-> cs
                                      (add-cards zn cards)))
                        :delay :none}
      :cards-created-v {:new-cs (let [[zn cids] args]
                                  (-> cs
                                      (add-cards zn (promote-cids-to-active cids))))
                        :delay :none}
      :cards-created-i {:new-cs (let [[zn card-count] args]
                                  (-> cs
                                      (add-card-count zn card-count)))
                        :delay :none}
      (:cards-destroyed-a, :cards-destroyed-v) {:new-cs (let [[zn cids] args]
                                                          (-> cs
                                                              (remove-cards-active zn cids)))
                                                :delay :none}
      :cards-destroyed-i {:new-cs (let [[zn card-count] args]
                                    (-> cs
                                        (remove-card-count zn card-count)))
                          :delay :none}
      (:cards-moved-vv
       :cards-moved-va
       :cards-moved-av
       :cards-moved-aa) {:new-cs (let [[from to cids] args
                                       cs (prepare-card-movement-animation cs from to cids)
                                       [cards cs] (remove-retain-cards-active cs from cids)]
                                   (add-cards cs to cards))
                         :delay :long}

      (:cards-moved-iv
       :cards-moved-ia) {:new-cs (let [[from to cids] args]
                                   (let [promoted (promote-cids-to-active cids)]
                                     (-> cs
                                         (prepare-card-movement-animation from to cids)
                                         (remove-card-count from (count cids) :no-animator)
                                         (add-cards to promoted))))
                         :delay :long}

      (:cards-moved-vi
       :cards-moved-ai) {:new-cs (let [[from to cids] args]
                                   (-> cs
                                       (prepare-card-movement-animation from to cids)
                                       (remove-cards-active from cids)
                                       (add-card-count to (count cids))))
                         :delay :long}

      :cards-moved-ii {:new-cs (let [[from to card-count] args]
                                 (-> cs
                                     (prepare-card-movement-animation from to card-count)
                                     (remove-card-count from card-count)
                                     (add-card-count to card-count)))
                       :delay :long}

      :ns-cards-moved {:new-cs (let [[from to from-status to-status cards-descr delta-opts] args]
                                 (case [from-status to-status]
                                   ([:a :a]
                                    [:a :v]
                                    [:v :a]
                                    [:v :v]) (let [[cards cs] (remove-retain-cards-active cs from cards-descr)]
                                               ;; TODO if :change-to-facedown?, change the cards
                                               (add-cards cs to cards))
                                   ([:a :i] [:v :i]) (-> cs
                                                         (remove-cards-active from cards-descr)
                                                         (add-card-count to (count cards-descr)))
                                   ([:i :a] [:i :v]) (-> cs
                                                         (remove-card-count from (count cards-descr))
                                                         (add-cards to (promote-cids-to-active cards-descr delta-opts)))
                                   [:i :i] (-> cs
                                               (remove-card-count from cards-descr)
                                               (add-card-count to cards-descr))))
                       :delay :none}

      :game-finished {:new-cs (let [[mode submode winner] args]
                                (-> cs
                                    (update :log conj delta)
                                    (assoc :outcome [mode submode winner])))
                      :delay :none}
      :card-props-set {:new-cs (let [[zn cid prop-new-pairs] args
                                     cards (all-cards-in-zone cs zn)
                                     index (first-index #(= (:cid %) cid) cards)]
                                 (if index
                                   (update-in cs [:zones zn :cards index]
                                              (fn [the-card]
                                                (apply assoc the-card
                                                       (apply concat prop-new-pairs))))
                                   (do (warn "No card by cid in zone when applying delta:" cid zn delta)
                                       cs)))
                       :delay :none}
      :card-props-unset {:new-cs (let [[zn cid props] args
                                       cards (all-cards-in-zone cs zn)
                                       index (first-index #(= (:cid %) cid) cards)]
                                   (if index
                                     (update-in cs [:zones zn :cards index]
                                                (fn [the-card]
                                                  (apply dissoc the-card props)))
                                     (do (warn "No card by cid in zone when applying delta:" cid zn delta)
                                         cs)))
                         :delay :none}
      :card-props-changed {:new-cs (let [[zn cid prop-change-new-triplets] args
                                         cards (all-cards-in-zone cs zn)
                                         index (first-index #(= (:cid %) cid) cards)]
                                     (if index
                                       (update-in cs [:zones zn :cards index]
                                                  (fn [the-card]
                                                    (apply assoc the-card
                                                           (apply concat
                                                                  (map (fn [[prop change new]]
                                                                         [prop new])
                                                                       prop-change-new-triplets)))))
                                       (do (warn "No card by cid in zone when applying delta:" cid zn delta)
                                           cs)))
                           :delay :none}
      :-can-cancel-request? {:new-cs (let [[can-cancel?] args]
                                       (assoc cs :can-cancel? can-cancel?))
                             :delay :none}
      :reveal-zone {:new-cs (let [[zone-name] args]
                              (dispatch
                               [:thm.game-ui/show-modal (keyword zone-name)])
                              cs)
                    :delay :none}
      :figure-created (let [[space-name figure-id index] args
                            figures (get-in cs [:spaces space-name] [])]
                        ;; TODO animate appearing?
                        {:new-cs (update cs :spaces
                                         assoc space-name (add-to-vector-at-index figures index figure-id))
                         :delay :none})
      :figure-destroyed (let [[space-name figure-id index] args
                              figures (get-in cs [:spaces space-name] [])]
                          ;; TODO animate disappearing?
                          {:new-cs (update cs :spaces
                                           assoc space-name (remove-from-vector-at-index figures index))
                           :delay :none})
      :figure-moved
      (let [[from-name to-name figure-id from-index to-index options] args
            from-figures (get-in cs [:spaces from-name])
            to-figures (get-in cs [:spaces to-name] [])
            from-index (if (some? from-index)
                         from-index
                         (index-of figure-id from-figures))
            to-index (if (some? to-index)
                       to-index
                       (count to-figures))

            disable-animation? (:disable-animation? options)
            cs (if disable-animation?
                 cs
                 (prepare-figure-movement-animation cs from-name to-name figure-id))]
        {:new-cs (update cs :spaces
                         (fn [spaces]
                           (-> spaces
                               (assoc from-name (remove-from-vector-at-index from-figures from-index))
                               (assoc to-name (add-to-vector-at-index to-figures to-index figure-id)))))
         :delay (if disable-animation?
                  :none
                  :long)})

      (do (warnf "Unrecognized delta: %s" delta) {:new-cs cs}))))

(defn clear-recent-status [cs]
  (assoc cs :recent {}))

(defn delay->ms [delay]
  (case delay
    :short 200
    :long 500
    (:none nil) 0
    :super-long 2000
    (do (warn "Unrecognized kind of delay, defaulting to none " delay)
        0)))

;; Animated deltas require that the elements-to-animate be visible.  Hence if a
;; pane/tab is associated with a given delta, we dispatch a request to switch
;; to it and a request to continue applying this delta.  Otherwise we just
;; apply it.
(defn apply-next-delta [cs progress]
  (let [do-apply (fn [cs delta remaining-deltas]
                   (let [new-cs (dissoc cs :animation-info)
                         {:keys [new-cs delay]} (apply-one-delta new-cs delta)
                         new-cs (assoc new-cs :pending-deltas remaining-deltas)

                         delay-ms (delay->ms delay)]
                     (if (:paused? new-cs)
                       new-cs
                       (do
                         (js/window.setTimeout #(dispatch [:thm.game-ui/apply-next-delta (:your-role cs) {}])
                                               delay-ms)
                         new-cs))))
        pending-deltas (:pending-deltas cs)]
    (cond (empty? pending-deltas)
          ,(assoc cs :pending-deltas nil, :applying-deltas? false)

          (some? (:current-delta progress))
          ,(do-apply cs (:current-delta progress) (:remaining-deltas progress))

          :else
          ,(let [[delta & remaining-deltas] pending-deltas
                 pane-id (game-ui-module/get-pane-associated-with-delta (module-from-cs cs) delta (:your-role cs))
                 tab-id (game-ui-module/get-play-area-associated-with-delta (module-from-cs cs) delta (:your-role cs))]
             (if (or pane-id tab-id)
               (do
                 (when pane-id
                   (dispatch [:thm.game-ui.compact/switch-pane pane-id]))
                 (when tab-id
                   (dispatch [:thm.game-ui/activate-play-area-tab tab-id]))

                 ;; Wait until the relayout happens so that preparation of
                 ;; animation gets correct bounding rects of elements.
                 (js/window.setTimeout #(dispatch [:thm.game-ui/apply-next-delta (:your-role cs)
                                                   {:current-delta delta, :remaining-deltas remaining-deltas}])
                                       100)
                 cs)
               (do-apply cs delta remaining-deltas))))))

(defn start-applying-deltas [cs deltas]
  (apply-next-delta (-> cs
                        (clear-recent-status)
                        (assoc :applying-deltas? true, :pending-deltas deltas))
                    {}))

(defn deltas-arrived [cs deltas]
  (cond
    (empty? deltas) cs
    (:applying-deltas? cs) (update cs :pending-deltas concat deltas)
    :else (start-applying-deltas cs deltas)))

(defn proceed-from-pause [cs]
  (apply-next-delta (assoc cs :paused? false) {}))

(defn promote-cards-in-visible-zones [cs]
  (update cs :zones
          (fn [zones]
            (map-values (fn [zone-name zone]
                          (if (and (not (:a zone))
                                   (coll? (:cards zone)))
                            (update zone :cards
                                    promote-cids-to-active)
                            zone))
                        zones))))

(defn prepare-initial-state [cs]
  (-> cs
      promote-cards-in-visible-zones
      create-request-helper
      prepare-request-ui))
