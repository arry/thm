(ns thm.backend-utils.emails
  (:require
   [clojure.string :as str]
   [thm.config :refer [config]]
   [thm.utils :refer [short-game-name]]
   [org.httpkit.client :as http]
   [taoensso.timbre :as timbre :refer (debug)]
   [hiccup.core :refer [html]]))

(defn send-email [to subject body]
  (future
    (let [url (format "https://api.mailgun.net/v3/%s/messages"
                      (:mailgun-domain config))
          options {:basic-auth ["api" (:mailgun-api-key config)]
                   :form-params {:from "Chantry <old-bot@chantry-games.com>"
                                 :to to
                                 :subject subject
                                 :html body}}
          {:keys [status error]} @(http/post url options)]
      (debug "Sending email to" (pr-str to) "with subject" (pr-str subject))
      (if (= status 200)
        [true nil]
        [false error]))))

(defn signup-confirmation-body [code]
  (let [base-url (:base-url config)
        url (str base-url "/confirm/" code)]
  (html [:html
         [:h1 "Welcome to Chantry!"]
         [:p "To finish your sign-up, please click this link:"]
         [:p [:a {:href url} url]]
         [:p "If you didn't want to sign up for "
          [:a {:href base-url} "Chantry"]
          " (a site to play board games online), sorry! We won't bug you again."]])))

(defn send-signup-confirmation-code! [email confirmation-code]
  (send-email email
              "Welcome to Chantry!"
              (signup-confirmation-body confirmation-code)))

(defn reset-password-body [code]
  (let [base-url (:base-url config)
        url (str base-url "/reset/" code)]
  (html [:html
         [:h1 "Reset your Chantry password"]
         [:p "Please click this link to reset your password:"]
         [:p [:a {:href url} url]]
         [:p "If you didn't request a password reset, sorry about that."]])))

(defn send-password-recovery! [email code]
  (send-email email
              "Reset your Chantry password"
              (reset-password-body code)))

(defn hi [user]
  [:p "Hi " (:username user) ","])

(def unsubscribe-footer
  [:p {:style "font-size: smaller"}
                      "Please don't reply to this email.  If you want to unsubscribe, please "
                      [:a {:href (str (:base-url config) "/account")} "go to your settings"]
                      " and uncheck the \"Allow emails from async games\" checkbox."])

(defn game-link [game kind]
  (let [part (case kind
               :view "/game/"
               :play "/play/")]
    [:a {:href (str (:base-url config) part (:gid game))}
     (short-game-name game)]))

(defn send-your-turn-notification! [user email game]
  (send-email email
              (str "Your turn in game " (short-game-name game))
              (html [:html
                     (hi user)
                     [:p "It's your turn in the async game " (game-link game :play) "."]
                     unsubscribe-footer])))

(defn send-player-joins-game-notification! [user email game that-username]
  (send-email email
              (str "User joins game " (short-game-name game))
              (html [:html
                     (hi user)
                     [:p "User " [:b that-username] " joins your game " (game-link game :view) "."]
                     [:p "It's now ready to start."]
                     unsubscribe-footer])))

(defn send-player-parts-game-notification! [user email game that-username]
  (send-email email
              (str "User parts game " (short-game-name game))
              (html [:html
                     (hi user)
                     [:p "User " [:b that-username] " parts your game " (game-link game :view) "."]
                     [:p "It can no longer be started."]
                     unsubscribe-footer])))
