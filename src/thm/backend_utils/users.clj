(ns thm.backend-utils.users
  (:require
   [clojure.string :as str]
   [taoensso.timbre :as timbre :refer (debug)]
   [thm.utils :refer [VALIDATION-CONSTS uuid-str get-form-value]]
   [thm.backend-components.ds :as ds]
   [crypto.password.bcrypt :as password]))

(defn get-password-hash [password]
  (password/encrypt password))

(defn check-password-against-hash [plain password-hash]
  (password/check plain password-hash))

(defn validate-user
  "Return username if the user identified by `username-or-email` and `password`
  is valid, or nil otherwise."
  [ds username-or-email password]
  (when-let [user (ds/user-by-username-or-email ds username-or-email)]
    (when (check-password-against-hash password (:password-hash user))
      (:username user))))

(defn validate-signup-form-data
  "Given signup form data, return a vector of error messages, each represented
  by a vector whose first item is a keyword identifying the error.  Returns an
  empty vector when there are no errors."
  [ds form-data pending-registration]
  (let [username (get-form-value form-data :username)
        password (or (get form-data :password) "")
        email (get-form-value form-data :email)]
    [{:username username, :password password, :email email}
     (cond-> []
       (not= email (:email pending-registration))
       (conj [:email-doesnt-match])

       (< (count username) (:min-username-length VALIDATION-CONSTS))
       (conj [:min-username (:min-username-length VALIDATION-CONSTS) (count username)])

       (> (count username) (:max-username-length VALIDATION-CONSTS))
       (conj [:max-username (:max-username-length VALIDATION-CONSTS) (count username)])

       ;;                               Latin Extended-A and -B
       ;; https://en.wikipedia.org/wiki/Latin_characters_in_Unicode
       (not (re-matches #"^[-_ 0-9A-Za-z\u0100-\u024F]+$" username))
       (conj [:invalid-username-characters])

       (and (>= (count username) 5)
            (= (str/lower-case (subs username 0 5)) "guest"))
       (conj [:username-guest])

       (or (ds/user-by-username-or-email ds username)
           (ds/user-by-username-or-email ds email))
       (conj [:user-exists username])

       (< (count password) (:min-password-length VALIDATION-CONSTS))
       (conj [:min-password (:min-password-length VALIDATION-CONSTS) (count password)])

       (> (count password) (:max-password-length VALIDATION-CONSTS))
       (conj [:max-password (:max-password-length VALIDATION-CONSTS) (count password)])

       (not (:nice form-data))
       (conj [:not-nice]))]))

(defn create-confirmation-code []
  (uuid-str))

(defn create-password-recovery-code []
  (uuid-str))
