(ns thm.td.td-ui-module
  (:require [thm.game-ui.game-ui-module :as game-ui-module]
            [taoensso.timbre :refer-macros (debug warn)]
            [thm.td.game-ui.td-ui :as td-ui]
            [thm.td.td-texts :as td-texts]
            [thm.td.td-utils :refer [state-timing]]
            [thm.utils :refer [parse-text-descr]]
            [thm.game-ui.ui-module-registry :refer [register-module]]
            ))

(def GAME-CODE "td")

(defmethod game-ui-module/do-resize GAME-CODE [_]
  (td-ui/do-resize))

(defn zone-name->alias [zone-name your-role]
  (let [decide (fn [r l1 l2]
                 (if (= your-role r) l1 l2))]
    (case zone-name
      "s:aftermath" (decide "s" :your-aftermath :other-aftermath)
      "u:aftermath" (decide "u" :your-aftermath :other-aftermath)

      "s:agenda" (decide "s" :your-agenda :other-agenda)
      "u:agenda" (decide "u" :your-agenda :other-agenda)

      "s:letter" (decide "s" :your-letter :other-letter)
      "u:letter" (decide "u" :your-letter :other-letter)

      "s:hand" (decide "s" :your-hand :other-hand)
      "u:hand" (decide "u" :your-hand :other-hand)

      zone-name)))

(def zone-alias->kind
  {"aftermath-reveal" :v-none
   "agenda-deck" :i-non-holding-place
   "strategy-deck" :i-non-holding-place
   :other-hand :i-non-holding-place
   :other-aftermath :i-non-holding-place
   :your-aftermath :v-non-holding-place
   :your-hand :v-row
   :your-letter :v-row
   :other-letter :v-row
   "in-play" :v-row
   :your-agenda :v-row
   "strategy-discard" :v-stack
   "agenda-discard" :v-non-holding-place
   :other-agenda :i-facedown-row})

(defn get-animation-kind-for-zone-name [zone-name your-role]
  (zone-alias->kind (zone-name->alias zone-name your-role)))

(defn get-pane-associated-with-request [req _your-role]
  (when-let [[kind text-descr & args] req]
    (case kind
      (:choose-card :choose-card-and-mode :choose-cards) "hand"
      (:choose-command
       :choose-command-onto
       :choose-place-influence
       :choose-remove-influence
       :choose-change-defcon) "map"
      nil)))

(def VAR-REGEX #"^(prestige|round|d:|i:|ef:)")

(defn get-pane-of-var [var-name]
  (when (re-matches VAR-REGEX var-name)
    "map")
  nil)

(defn get-delay-of-var [var-name]
  (when (re-matches VAR-REGEX var-name)
    :short)
  nil)

(defn get-delay-of-vars [var-names]
  (when (some #(get-delay-of-var %) var-names)
    :short))

(defn get-pane-of-cards-moved [from to your-role]
  (let [from-alias (zone-name->alias from your-role)
        to-alias (zone-name->alias to your-role)]
    (cond
      (or (= from-alias :your-hand)
          (= to-alias :your-hand))
      "hand"

      (or (#{"in-play"
             "strategy-discard"
             "agenda-discard"
             :your-aftermath :other-aftermath
             :your-agenda :other-agenda
             :your-letter :other-letter} to-alias))
      "map"

      :else
      nil)))

(defn get-pane-associated-with-delta [delta your-role]
  (let [[kind & args] delta]
    (case kind
      :var-set (let [[var-name value old-value] args]
                 (get-pane-of-var var-name))
      :var-unset (let [[var-name] args]
                   (get-pane-of-var var-name))
      :vars-set (let [[mappings] args]
                  (->> mappings
                       (map #(get-pane-of-var (first %1)))
                       (remove nil?)
                       first))

      :var-changed (let [[var-name amount new-value] args]
                     (get-pane-of-var var-name))

      (:cards-moved-vv
       :cards-moved-va
       :cards-moved-av
       :cards-moved-aa) (let [[from to cids] args]
                          (get-pane-of-cards-moved from to your-role))

      (:cards-moved-iv
       :cards-moved-ia) (let [[from to cids] args]
                          (get-pane-of-cards-moved from to your-role))

      (:cards-moved-vi
       :cards-moved-ai) (let [[from to cids] args]
                          (get-pane-of-cards-moved from to your-role))

      :cards-moved-ii (let [[from to card-count] args]
                        (get-pane-of-cards-moved from to your-role))

      nil)))

(deftype TdUiModule []
  game-ui-module/GameUiModule

  (get-game-code [_]
    GAME-CODE)

  (your-request-code-for-humans [_ text-descr primary-role]
    (td-ui/your-request-code-for-humans text-descr primary-role))

  (mode-for-humans [_ mode text-descr]
    (td-ui/mode-for-humans mode text-descr))

  (answer-for-humans [_ answer req]
    (td-ui/answer-for-humans answer req))

  (other-request-code-for-humans [_ player-name text-descr]
    (td-ui/other-request-code-for-humans player-name text-descr))

  (game-container [_ primary-role]
    (td-ui/game-container primary-role))

  (log-item-for-humans [_ log-item _log-helper]
    (td-ui/log-item-for-humans log-item))

  (small-role-identifier [_ role]
    (td-ui/small-role-identifier role))

  (player-plaque-info-line [_ primary-role role]
    (td-ui/player-plaque-info-line primary-role role))

  (sidebar-global-info [_ primary-role]
    (td-ui/sidebar-global-info primary-role))

  (compact-player-plaque [_ primary-role role player-name usernames]
    (td-ui/compact-player-plaque primary-role role player-name usernames))

  (compact-game-container [_ primary-role]
    (td-ui/compact-game-container primary-role))

  (your-hand [_ primary-role]
    (td-ui/your-hand primary-role))

  (has-left-hand-of-choose-mode-modal? [_ req]
    (td-ui/has-left-hand-of-choose-mode-modal? req))

  (left-hand-of-choose-mode-modal [_ req]
    (td-ui/left-hand-of-choose-mode-modal req))

  (extended-player-plaque-info [_ modal-data]
    (td-ui/extended-player-plaque-info modal-data))

  (modal-content [_ modal-data primary-role]
    (td-ui/modal-content modal-data primary-role))

  (get-text [_ text-descr]
    (let [{:keys [text-code text-args]} (parse-text-descr text-descr)
          fn (get td-texts/fns text-code)]
      (apply fn text-args)))

  (get-pane-associated-with-request [_ request your-role]
    (get-pane-associated-with-request request your-role))

  (get-pane-associated-with-delta [_ delta your-role]
    (get-pane-associated-with-delta delta your-role))

  (get-animation-kind-for-zone-name [_ zone-name your-role]
    (get-animation-kind-for-zone-name zone-name your-role))

  (get-delay-of-var [_ var-name]
    (get-delay-of-var var-name))

  (get-delay-of-vars [_ var-names]
    (get-delay-of-vars var-names))

  (state-timing-for-humans [_ state]
    ((:timing td-texts/fns)
     (state-timing state)))

  (get-play-areas-associated-with-request [_ request your-role]
    ;; Not applicable for this game.
    nil)

  (get-play-area-associated-with-delta [_ delta your-role]
    ;; Not applicable for this game.
    nil)

  (on-var-set [_ primary-role var-name value]
    ;; n/a
    )

  )

(register-module GAME-CODE (TdUiModule.))
