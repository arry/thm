(ns thm.views.game-ui
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [reagent.core :as r]
            [re-frame.core :refer [dispatch subscribe]]
            [taoensso.timbre :refer-macros (debug warn)]
            [thm.game-ui.game-ui-module :as game-ui-module]
            [baking-soda.core :as b]
            [thm.utils :refer [map-cross bull html-entity dispatch-e stop-event seq-contains?
                               denumber-id str-join-non-nil half-round-up
                               sequence-for-humans
                               on-mobile-browser?
                               get-user-agent-string
                               pluralize
                               game-includes-player?
                               has-your-request?
                               get-your-request
                               finished?
                               index-of
                               set-answer
                               send-cancel
                               forbid-widget-activation?
                               parenthetical-list
                               sort-explode-map
                               change-for-humans
                               choose-mode-like-request-kind?
                               show-choose-mode-modal
                               parse-text-descr
                               bind
                               ]]
            [thm.views.common :refer [link action-link dropdown-link dropdown-action-link
                                      connection-indicator
                                      connection-status indexed-component
                                      dispatch-resize-when-mounted
                                      tabbed-component tabbed-component-ng
                                      autoscroller chat
                                      avatar-img my-button
                                      outcome-for-humans
                                      behind-page-loader loader
                                      modal-content-wrapper
                                      not-found]]
            [thm.views.game-ui-common :refer [hide-card-preview
                                              ]]
            [thm.game-ui.all-game-ui-modules :refer [get-game-ui-module]]

            [clojure.string :as str]))

(defn icon [icon-name]
  [:img.icon {:src (str "/Entypo/" icon-name ".svg")}])

(def resize-timeout-id (atom nil))

(defn do-resize [code]
  (game-ui-module/do-resize code))

(defn handle-resize []
  (letfn [(f []
            (reset! resize-timeout-id nil)
            (dispatch [:do-resize]))]
    (when-let [id @resize-timeout-id]
      (js/window.clearTimeout id))
    (reset! resize-timeout-id (js/window.setTimeout f 100)))
  true)

(defn card-preview [module]
  (let [*preview (subscribe [:thm.game-ui/preview])]
    (fn [module]
      [:div {:class ["card-preview"
                     (:additional-class @*preview)
                     (when (and (or (:cursor-on-big-card @*preview)
                                    (:cursor-on-small-card @*preview))
                                (:id @*preview)) "on")]}
       [:img.big-card (merge {:key (:id @*preview)
                              :src (if-let [id (:id @*preview)]
                                     (game-ui-module/get-text module [:big-card-img-src id])
                                     "/images/unknown-card.png")}
                             (if (on-mobile-browser?)
                               {:on-touch-start #(hide-card-preview %)}
                               {:on-mouse-enter #(dispatch-e [:thm.game-ui/cursor-on-big-card true] %)
                                :on-mouse-leave #(dispatch-e [:thm.game-ui/cursor-on-big-card false] %)}))]])))

(defn thing-is-associated-with-id? [thing]
  (some? (:cid thing)))

(defn ready-button [primary-role req answer ui-layout]
  (when req
    (let [classic? (= ui-layout "classic")]
      [my-button {:hidden (and (not classic?)
                               (choose-mode-like-request-kind? (first req)))
                  :disabled @(subscribe [:thm.game-ui/should-disable-ready-button? primary-role])
                  :class (if classic?
                           "ready btn-primary"
                           "ready btn-primary square-button wider")
                  :on-click #(dispatch [:thm.game-ui/send-answer primary-role])}
       (if classic?
         [:span "\u25B6" [:span.tmi " Ready"]]
         (icon "controller-play"))])))

(defn reset-button [primary-role kind req answer ui-layout]
  (let [classic? (= ui-layout "classic")]
    ;; XXX td-specific and tm-specific behavior; they should be separated somehow.
    (when (#{:choose-place-influence
             :choose-remove-influence
             :choose-change-defcon
             :choose-thing
             :choose-things} kind)
      (when-not (and (= kind :choose-thing) (nil? answer))
        [my-button {:disabled (and (empty? answer)
                                   (not (nil? answer)))
                    :class (if classic?
                             "reset"
                             "reset square-button")
                    :on-click #(dispatch [:thm.game-ui/reset-answer primary-role])}
         (if (seq answer)
           (if classic? "Reset" (icon "cycle"))
           (if classic? "Set to 0" (icon "circle")))]))))

(defn cancel-button [primary-role req answer ui-layout]
  (let [classic? (= ui-layout "classic")]
    (when @(subscribe [:thm.game-ui/can-cancel? primary-role])
      [my-button {:class (if classic?
                           "cancel btn-danger"
                           "cancel btn-danger square-button")
                  :on-click #(send-cancel primary-role)}
       (if classic?
         [:span "\u21b6" [:span.tmi " Cancel"]]
         (icon "back"))])))

(defn prepare-modes [[kind text-descr & args :as req]]
  (if (= kind :choose-mode)
    (map (fn [mode] {:answer mode, :mode mode}) (first args))
    [{:answer true, :mode "yes", :for-humans "Yes"}
     {:answer false, :mode "no", :for-humans "No"}]))

(defn answer-in-button-actions? [answer request-helper]
  (some #(= (:answer %) answer)
        (:button-actions request-helper)))

(defn action-buttons [module primary-role text-descr request-helper answer]
  [:span
   (doall (for [x (:button-actions request-helper)]
            ^{:key (:action-id x)}
            [my-button {:class (when (= (:answer x) answer) "is-active")
                        :on-click #(set-answer primary-role (:answer x))}
             (if-let [text (:for-humans x)]
               text
               (game-ui-module/mode-for-humans module (:action-id x) text-descr))]))])

(defn your-request-controls [module primary-role req]
  (let [[kind text-descr & args] req
        answer @(subscribe [:thm.game-ui/answer-for primary-role])
        spread @(subscribe [:thm.game-ui/spread-for primary-role])
        request-helper @(subscribe [:thm.game-ui/request-helper-for primary-role])]
    [:span (game-ui-module/your-request-code-for-humans module text-descr primary-role) " "
     (when-let [[lower higher] spread]
       (doall (for [change (range lower (inc higher))]
                [my-button {:key change
                            :class "condensed"
                            :on-click #(set-answer primary-role [(first answer) change])}
                 (change-for-humans change)])))

     [action-buttons module primary-role text-descr request-helper answer]

     (when (and answer (not (answer-in-button-actions? answer request-helper)))
       [:span.answer (str " ("
                          (game-ui-module/answer-for-humans module answer req)
                          ") ")])

     (when (= kind :choose-thing)
       (when-not (and answer (thing-is-associated-with-id? (get-in request-helper [:tid->thing answer])))
         (doall (for [thing (get-in request-helper [:bucket->things :no-id])
                      :let [tid (:tid thing)]]
                  [my-button {:key tid
                              :class (when (= tid answer) "is-active")
                              :on-click #(set-answer primary-role tid)}
                   ;; TODO thing-for-humans :in-button
                   "??"]))))

     [reset-button primary-role kind req answer "classic"]

     [ready-button primary-role req answer "classic"]

     [cancel-button primary-role req answer "classic"]]))

(defn compact-your-request-controls [module primary-role req]
  (let [[kind text-descr & args] req
        answer @(subscribe [:thm.game-ui/answer-for primary-role])
        request-helper @(subscribe [:thm.game-ui/request-helper-for primary-role])]
    [:span
     (game-ui-module/your-request-code-for-humans module text-descr primary-role) " "

     (if (choose-mode-like-request-kind? kind)
       [my-button {:class "btn-info"
                   :on-click #(show-choose-mode-modal req)}
        "View choices"]
       [action-buttons module primary-role text-descr request-helper answer])

     (when (= kind :choose-thing)
       (when-not (and answer (thing-is-associated-with-id? (get-in request-helper [:tid->thing answer])))
         (doall (for [thing (get-in request-helper [:bucket->things :no-id])
                      :let [tid (:tid thing)]]
                  [my-button {:key tid
                              :class (when (= tid answer) "is-active")
                              :on-click #(set-answer primary-role tid)}
                   ;; TODO thing-for-humans :in-button
                   "??"]))))]))

;; It has this signature for generality in non-2p games where you have answered a
;; multi-request, but several other players haven't.  Not needed for 13 Days
;; which is 2p.
(defn other-request-controls [module request-map primary-role]
  (let [roles (keys request-map)
        r->pn @(subscribe [:thm.game-ui/get-map-of-role->player-name primary-role])
        role (first roles) ;; TODO when more people possible, display all of them
        player-name (r->pn role)
        [kind text-descr] (get request-map role)]
    (game-ui-module/other-request-code-for-humans module player-name text-descr)))

(defn request-controls [mode module *game primary-role]
  [:span
   (let [request-map @(subscribe [:thm.game-ui/request-map-for primary-role])
         answer-sent? @(subscribe [:thm.game-ui/answer-sent-for? primary-role])
         paused? @(subscribe [:thm.game-ui/paused-for? primary-role])
         outcome @(subscribe [:thm.game-ui/outcome])
         ui-layout @(subscribe [:thm.game-ui/layout])
         applying-deltas? @(subscribe [:thm.game-ui/applying-deltas? primary-role])]
     (cond
       (and (= mode :play)
            (:outcome @*game))
       ,[:span (outcome-for-humans (:code @*game) (:outcome @*game))
         " "
         [my-button {:on-click #(dispatch [:thm.game-ui/navigate-to-site-from-finished-game])} "To site"]]

       answer-sent? "Answer sent."

       paused? [:span "Paused "
                [my-button {:class "ready btn-primary"
                            :on-click #(dispatch [:thm.game-ui/proceed-from-pause primary-role])} "Proceed"]]

       (contains? request-map primary-role)
       (if (= ui-layout "classic")
         [your-request-controls module primary-role (get request-map primary-role)]
         [compact-your-request-controls module primary-role (get request-map primary-role)])

       (seq request-map) (other-request-controls module request-map primary-role)

       (some? outcome)
       ,[:span (outcome-for-humans (game-ui-module/get-game-code module) outcome)
         " "
         [my-button {:on-click #(dispatch [:thm.game-ui/navigate-to-site-from-finished-game])} "To site"]]

       applying-deltas?
       [:span "Working..."]

       :else "I don't know what to say."))])

(defn abandon-modal-content []
  [:div
   [b/ModalHeader {:toggle #(dispatch-e [:thm.game-ui/hide-modal] %)} "Abandon game"]
   [b/ModalBody "Abandoning a running game is not nice.  Are you sure you
   want to do it?"]
   [b/ModalFooter
    [my-button {:on-click (fn [e]
                            (dispatch-e [:thm.game-ui/hide-modal] e)
                            (dispatch [:thm.game-ui/do-abandon-game]))
                :class "btn-primary"} "Yes"]
    [my-button {:on-click #(dispatch-e [:thm.game-ui/hide-modal] %)} "No"]]])

(defn sandbox-invitation-content []
  [:div
   [b/ModalHeader {:toggle #(dispatch-e [:thm.game-ui/hide-modal] %)} "Welcome to the game sandbox!"]
   [b/ModalBody "On this page you play against yourself.  You can switch between sides to see how the game would look from their perspective, and you issue commands from both sides.  This game isn't recorded anywhere."]
   [b/ModalFooter
    [my-button {:on-click (fn [e]
                            (dispatch-e [:thm.game-ui/hide-modal] e))
                :class "btn-primary"} "Okay"]]])

(defn next-active-gid [active-gids game]
  (if-let [index (index-of (:gid game) active-gids)]
    (get active-gids (mod (inc index) (count active-gids)))
    (first active-gids)))

(defn role-link [module role your-role & [opts]]
  [action-link (dissoc opts :prefix)
   [:thm.test-ui/switch-role role]
   [(if (= role your-role) :b :span)
    (str (get opts :prefix "") (game-ui-module/get-text module [:role role]))]])

(defn testing? [mode]
  (or (= mode :test) (= mode :try)))

(defn play-menu-with-button [mode module ui-layout]
  (let [*open? (subscribe [:thm.game-ui/play-menu-open?])
        *game (subscribe [:thm.play-game/game])
        *active-gids (subscribe [:thm.play-game/active-gids])
        *test-scenario-names (subscribe [:thm.test-ui/scenario-names])
        *username (subscribe [:username])
        compact? (= ui-layout "compact")]
    (fn [mode module ui-layout]
      [:div.menu-button-container.dropdown {:class (when @*open? "show")}
       [my-button {:class (when compact?
                            "square-button btn-success")
                   :type "button"
                   :on-click #(dispatch-e [:thm.game-ui/toggle-play-menu] %)}
        (if compact?
          (icon "menu")
          (html-entity "#9776"))]
       [:div.dropdown-menu {:class (when @*open? "show")}
        (when (and (game-includes-player? @*game @*username)
                   (= mode :play))
          (list
           [dropdown-action-link {:key "personal-notes"}
            [:thm.game-ui/show-personal-notes] "Personal notes"]
           (when-not (and (= mode :play)
                          (:outcome @*game))
             [dropdown-action-link {:key "abandon"}
              [:thm.game-ui/show-modal :abandon] "Abandon this game"])
           ;; TODO
           #_[dropdown-action-link {:key "preferences"} [:thm.game-ui/show-preferences] "Preferences"]
           [:div.dropdown-divider {:key "divider"}]))
        (if (= ui-layout "compact")
          [dropdown-action-link {:key "switch-ui-layout"} [:thm.game-ui/switch-ui-layout "classic"]
           "Switch to classic layout"]
          [dropdown-action-link {:key "switch-ui-layout"} [:thm.game-ui/switch-ui-layout "compact"]
           "Switch to compact layout"])
        (when (and (= ui-layout "compact")
                   (testing? mode))
          (let [your-role @(subscribe [:thm.game-ui/your-role])]
            (list
             (indexed-component @(subscribe [:thm.game-ui/roles-of-client-states])
                                (fn [index role]
                                  (if (nil? role)
                                    (warn "roles-of-client-states contains a nil key")
                                    (role-link module role your-role {:key role
                                                                    :class "dropdown-item"
                                                                    :prefix "POV: "}))))
             [:div.dropdown-divider {:key "divider"}])))
        (when (and (= ui-layout "compact")
                   (= mode :test))
          (list
           (indexed-component @*test-scenario-names
                              (fn [i name]
                                [action-link {:class "dropdown-item"
                                              :key i, :value i} [:thm.test-ui/switch-scenario i] "S:" name]))
           [:div.dropdown-divider {:key "divider"}]))
        (when (and (= ui-layout "compact")
                   (= mode :try))
          (list
           [dropdown-action-link {:key "new-game"} [:thm.test-ui/new-game] "New game"]
           [:div.dropdown-divider {:key "divider"}]))
        [dropdown-link "/lobby" "To lobby"]
        (when @*username
          [dropdown-link "/home" "To home"])
        (when (and (= mode :play) (> (count @*active-gids) 1))
          [dropdown-link
           (str "/play/" (next-active-gid @*active-gids @*game))
           "To next active game " [:span.badge (str (dec (count @*active-gids)))]])]
       [:div.backdrop {:class (when @*open? "on")
                       :on-click #(dispatch-e [:thm.game-ui/toggle-play-menu] %)}]])))

(defn top-bar-class [*game mode outcome your-request paused?]
  ["top-bar"
   (cond
     (or (some? outcome)
         (and (= mode :play)
              (:outcome @*game))) "is-finished"
     (or (some? your-request)
         paused?) "is-active"
     :else "")])

(defn top-bar [mode module primary-role]
  (let [ui-layout @(subscribe [:thm.game-ui/layout])
        *game (subscribe [:thm.play-game/game])
        outcome @(subscribe [:thm.game-ui/outcome])
        your-request @(subscribe [:thm.game-ui/your-request-for primary-role])
        paused? @(subscribe [:thm.game-ui/paused-for? primary-role])]
    [:div {:class (top-bar-class *game mode outcome your-request paused?)}
     [:div.message [request-controls mode module *game primary-role]]
     (when (= ui-layout "classic")
       [play-menu-with-button mode module "classic"])]))

(defn ui-main [mode module primary-role]
  (dispatch-resize-when-mounted
      (fn [mode module primary-role]
        [:div.main
         [top-bar mode module primary-role]
         [game-ui-module/game-container module primary-role]])))

(defn log [module primary-role]
  (let [log-helper @(subscribe [:thm.game-ui/log-helper primary-role])
        item-fn (fn [log-item]
                  [:li [game-ui-module/log-item-for-humans module log-item log-helper]])]
    (fn [module primary-role]
      [:div.text.log
       [autoscroller :ul (subscribe [:thm.game-ui/log-of-client-state-for primary-role]) item-fn]])))

(defn chat-n-log [module primary-role]
  (let [*active-tab (subscribe [:thm.game-ui/chat-n-log-active-tab])]
    [tabbed-component-ng :horizontal "chat-n-log"
     *active-tab :thm.game-ui/activate-chat-n-log-tab nil
     [["chat" "Chat" [chat]]
      ["log" "Log" [log module primary-role]]]]))

(defn player-plaque [module primary-role role player-name usernames]
  [:div.player-plaque {:class role}
   [avatar-img player-name]
   [:div.line [game-ui-module/small-role-identifier module role]
    " "
    [:b player-name]
    " "
    (connection-indicator (seq-contains? usernames player-name))]
   [game-ui-module/player-plaque-info-line module primary-role role]])

(defn ui-sidebar [module primary-role]
  (let [pn->r @(subscribe [:thm.game-ui/get-ordered-map-of-player-name->role primary-role])
        usernames @(subscribe [:usernames])]
    [:div.sidebar
     (doall
      (for [[player-name role] pn->r]
        ^{:key role}
        [player-plaque module primary-role role player-name usernames]))
     (let [observer-usernames (remove (set (keys pn->r)) usernames)
           n (count observer-usernames)
           limit 5]
       (if (pos? n)
         [:div.observers.text {:title (str/join "\n"
                                                (map #(str "- " %) observer-usernames))}
          (pluralize n "observer")

          " ("
          (str/join ", " (take limit observer-usernames))
          (when (> n limit) (html-entity "hellip"))
          ")"]
         [:div.text (html-entity "nbsp")]))
     [:div.clearfix]
     [:div.timing.text @(subscribe [:thm.game-ui/state-timing-for-humans primary-role module])]
     [game-ui-module/sidebar-global-info module primary-role]
     [chat-n-log module primary-role]]))

(defn switcher-link [*sub event value text]
  [action-link
   [event value]
   [(if (= (or @*sub false) value) :b :span)
    text]])

(defn test-controls [mode module]
  [:div.test-controls
   "States: "
   (let [*one-page? (subscribe [:thm.test-ui/display-all-client-states-on-one-page])
         event :thm.test-ui/set-display-all-client-states-on-one-page]
     (list
      ^{:key true}
      [switcher-link *one-page? event false "Tabbed"]
      ^{:key "/"}
      [:span " / "]
      ^{:key false}
      [switcher-link *one-page? event true "Single-page"]))

   [:hr]

   "POV: "
   (indexed-component @(subscribe [:thm.game-ui/roles-of-client-states])
                      (fn [index role]
                        [:span {:key index}
                         (when (> index 0)
                           "/")
                         [role-link module role @(subscribe [:thm.game-ui/your-role])]]))
   [:hr]
   (case mode
     :test
     (doall (list
             ^{:key "select"}
             [:select.scenarios {:on-change #(when (not= (-> % .-target .-value) "nothing")
                                               (dispatch-e
                                                [:thm.test-ui/switch-scenario
                                                 (-> % .-target .-value js/parseInt)] %))}
              [:option {:value "nothing"} "scenarios"]
              (indexed-component @(subscribe [:thm.test-ui/scenario-names])
                                 (fn [i name]
                                   [:option {:key i, :value i} name]))]

             ^{:key "redo"}
             [action-link [:thm.test-ui/redo-current-scenario] "Redo current"]))


     :try
     [action-link [:thm.test-ui/new-game] "New game"]

     [:div "unknown mode: " mode])
   [:hr]
   [link (case mode
           :test "/home"
           "/") "Return to site"]])

(defn pane-switcher-button [id options contents]
  [my-button {:class (str (if (:active? options)
                            "active "
                            "")
                          "square-button")
              :on-click #(dispatch-e [:thm.game-ui.compact/switch-pane id] %)}
   contents
   (when (:attention-needed? options)
     [:span.addition.attention-needed])
   (when (:new-chat-message-alert? options)
     [:span.addition.new-chat-message-alert])])

(defn pane [id active-pane child]
  [:div {:class ["pane"
                 (when (= id active-pane)
                       "active")]}
   child])

(defn compact-ui [mode module *game primary-role]
  (dispatch-resize-when-mounted
      (fn [mode module *game primary-role]
        (let [active-pane @(subscribe [:thm.game-ui.compact/active-pane])
              usernames @(subscribe [:usernames])
              request-map @(subscribe [:thm.game-ui/request-map-for primary-role])
              new-chat-message-alert? @(subscribe [:thm.game-ui.compact/new-chat-message-alert?])
              pn->r @(subscribe [:thm.game-ui/get-ordered-map-of-player-name->role primary-role])
              your-request @(subscribe [:thm.game-ui/your-request-for primary-role])
              attention-pane (game-ui-module/get-pane-associated-with-request module your-request primary-role)
              psb (fn [id contents]
                    (let [options {:active? (= id (or active-pane "map"))
                                   :attention-needed? (= id attention-pane)
                                   :new-chat-message-alert? (and (= id "chat")
                                                                 new-chat-message-alert?)}]
                      [pane-switcher-button id options contents]))
              your-request @(subscribe [:thm.game-ui/your-request-for primary-role])
              answer @(subscribe [:thm.game-ui/answer-for primary-role])]
          [:div
           [:div.first-button-row
            [play-menu-with-button mode module "compact"]
            (psb "map" (icon "image"))
            (psb "hand" (icon "colours"))
            (psb "info" (icon "info"))
            (psb "chat" (icon "chat"))]
           [:div.second-button-row
            [:div.compact-avatars
             (doall
              (for [[player-name role] pn->r
                    :let [role (pn->r player-name)]]
                ^{:key role}
                [game-ui-module/compact-player-plaque module primary-role role player-name usernames]))]
            (when your-request
              (let [[kind text-descr & args] your-request]
                [:div.request-buttons
                 [cancel-button primary-role your-request answer "compact"]
                 [reset-button primary-role kind your-request answer "compact"]
                 [ready-button primary-role your-request answer "compact"]]))]
           [:div.main-container
            [top-bar mode module primary-role]
            [:div.game-container
             [pane "map" active-pane
              [game-ui-module/compact-game-container module primary-role]]
             [pane "hand" active-pane
              [game-ui-module/your-hand module primary-role]]
             [pane "info" active-pane
              [:div.info-container
               [:div.general-container
                [:div.timing.text @(subscribe [:thm.game-ui/state-timing-for-humans primary-role module])]
                [game-ui-module/sidebar-global-info module primary-role]
                [:hr]]
               [:div.log-container
                [log primary-role]]]]
             [pane "chat" active-pane
              [chat]]]]]))))

(defn look-at-card-modal-content [modal-data module]
  (let [cid (:cid modal-data)]
    (modal-content-wrapper (game-ui-module/get-text module [:card-title-by-id cid])
                           [:div.row
                            [:div.col-6
                             [:img.big-card {:src (game-ui-module/get-text module [:big-card-img-src cid])}]]
                            [:div.list-group.col-6
                             (indexed-component (remove nil? (:actions modal-data))
                                                (fn [index action]
                                                  [:a.list-group-item {:key (:short-name action)
                                                                       :href (str "#" (:short-name action))
                                                                       :on-click (fn [e]
                                                                                   (dispatch [:thm.game-ui/hide-modal])
                                                                                   (apply (:on-chosen action) [])
                                                                                   (stop-event e))}
                                                   (:for-humans action)]))
                             [:a.list-group-item {:href "#dismiss"
                                                  :on-click #(dispatch-e [:thm.game-ui/hide-modal] %)}
                              "Dismiss"]]])))

(defn choose-mode-modal-content [modal-data module primary-role]
  (let [req (:request modal-data)
        [kind text-descr & args] req
        modes (prepare-modes req)
        answer @(subscribe [:thm.game-ui/answer-for primary-role])]
    (modal-content-wrapper
     (game-ui-module/your-request-code-for-humans module text-descr primary-role)
     [:div.row
      (when (game-ui-module/has-left-hand-of-choose-mode-modal? module req)
        [:div.col
         [game-ui-module/left-hand-of-choose-mode-modal module req]])
      [:div.col
       [:div.list-group
        (indexed-component modes
                           (fn [index x]
                             [:a.list-group-item {:key (:mode x)
                                                  :href (str "#" (:mode x))
                                                  :class (when (= (:answer x) answer) "is-active")
                                                  :on-click (fn [e]
                                                              (set-answer primary-role (:answer x))
                                                              (stop-event e))}
                              (if-let [text (:for-humans x)]
                                text
                                (game-ui-module/mode-for-humans module (:mode x) text-descr))]))]
       [:div.list-group
        (let [should-disable? @(subscribe [:thm.game-ui/should-disable-ready-button? primary-role])]
          [:a.list-group-item.ready {:href "#ready"
                                     :class (when should-disable? "disabled")
                                     :on-click #(do
                                                  (when-not should-disable?
                                                    (dispatch [:thm.game-ui/hide-modal])
                                                    (dispatch [:thm.game-ui/send-answer primary-role]))
                                                  (stop-event %))}
           (icon "controller-play") " Ready"])
        [:a.list-group-item {:href "#dismiss"
                             :on-click (fn [e]
                                         (dispatch-e [:thm.game-ui/hide-modal] e)
                                         ;; Continuation of hack: 2-stage card play in td.
                                         ;; If we don't like any option, then return to the card selection.
                                         (when (and (= (game-ui-module/get-game-code module) "td")
                                                    (let [{:keys [text-code text-args]} (parse-text-descr text-descr)]
                                                      (= text-code :choose-mode-of-play)))
                                           (send-cancel primary-role)))}
         "Dismiss"]]]])))

(defn extended-player-plaque-modal-content [modal-data module]
  (let [{:keys [role player-name connected?]} modal-data]
    (modal-content-wrapper
     player-name
     [:div.row
      [:div.col-6
       [:div.avatar [avatar-img player-name]]
       [:div (if connected?
               [:span.connected {:title "Online"} "\u2713 Online"]
               [:span.disconnected {:title "Offline"} "\u2717 Offline"])]
       [game-ui-module/extended-player-plaque-info module modal-data]]
      [:div.col-6
       [:div.list-group
        [:a.list-group-item {:href "#dismiss"
                             :on-click (fn [e]
                                         (dispatch-e [:thm.game-ui/hide-modal] e))}
         "Dismiss"]]]])))

(def PERSONAL-NOTES-MAX-LENGTH 2048)

(defn personal-notes-modal-content [modal-data]
  (let [max-length PERSONAL-NOTES-MAX-LENGTH
        data (r/atom {:content (:content modal-data)})
        get-remaining-length (fn []
                               (- max-length (count (:content @data))))]
    (fn [modal-data]
      (modal-content-wrapper
       "Personal notes"
       [:div.row
        [:div.col
         [:form
          [:textarea {:default-value (:content @data)
                      :on-change (bind data :content)}]]]]
       [:div.row
        [:div.float-left
         [:div.alert.smaller.mb-0.mr-2 {:class (if (neg? (get-remaining-length))
                                       "alert-danger"
                                       "alert-info")}
          "Remaining symbols: " (get-remaining-length)]]
        [my-button {:disabled (neg? (get-remaining-length))
                    :on-click (fn [e]
                                (dispatch [:thm.game-ui/save-personal-notes (:content @data)])
                                (dispatch-e [:thm.game-ui/hide-modal] e))
                    :class "btn-primary mr-2"} "Save"]]))))

(defn game-ui-modal [*modal-data module primary-role]
  [b/Modal {:is-open (some? @*modal-data)
            :toggle #(dispatch-e [:thm.game-ui/hide-modal] %)
            :class-name ["modal-lg" (game-ui-module/get-game-code module)]}
   (when-let [data @*modal-data]
     (case (:kind data)
       :abandon [abandon-modal-content]
       :sandbox-invitation [sandbox-invitation-content]
       :look-at-card [look-at-card-modal-content data module]
       :choose-mode [choose-mode-modal-content data module primary-role]
       :extended-player-plaque [extended-player-plaque-modal-content data module]
       :personal-notes [personal-notes-modal-content data]
       [game-ui-module/modal-content module data primary-role]))])

(defn ui [mode code]
  (let [*game (subscribe [:thm.play-game/game])
        *page-loading? (subscribe [:page-loading?])
        *modal-data (subscribe [:thm.game-ui/modal-data])
        *ui-layout (subscribe [:thm.game-ui/layout])]
    (fn [mode code]
      (let [module (get-game-ui-module code)]
        (cond
          (testing? mode)
          [behind-page-loader
           (cond
             (= @*ui-layout "compact")
             (let [your-role @(subscribe [:thm.game-ui/your-role])]
               [:div.ui.compact {:class code}
                [compact-ui mode module *game your-role]
                [game-ui-modal *modal-data module your-role]])

             @(subscribe [:thm.test-ui/display-all-client-states-on-one-page])
             [:div {:style {:width "100%" :height "200%"}}
              [test-controls mode module]
              [card-preview module]
              [game-ui-modal *modal-data module @(subscribe [:thm.game-ui/your-role])]

              (doall (for [role @(subscribe [:thm.game-ui/roles-of-client-states])]
                       ^{:key role}
                       [:div {:style {:position "relative"
                                      :width "100%" :height "50%"}}
                        [:div.ui {:class code}
                         [ui-main mode module role]
                         [ui-sidebar module role]]]))]

             :else
             [:div
              [test-controls mode module]
              [card-preview module]
              [tabbed-component (subscribe [:thm.game-ui/your-role]) :thm.test-ui/switch-role
               (map (fn [role]
                      [role
                       role
                       [:div.ui {:class code}
                        [ui-main mode module role]
                        [ui-sidebar module role]]])
                    @(subscribe [:thm.game-ui/roles-of-client-states]))]
              [game-ui-modal *modal-data module @(subscribe [:thm.game-ui/your-role])]])]

          (some? @*game)
          (let [your-role @(subscribe [:thm.game-ui/your-role])]
            [behind-page-loader
             (if (= @*ui-layout "compact")
               [:div.ui.compact {:class code}
                [compact-ui mode module *game your-role]
                [game-ui-modal *modal-data module your-role]]
               [:div.ui {:class code}
                (when (testing? mode)
                  [test-controls mode module])
                [card-preview module]
                [ui-main mode module your-role]
                [ui-sidebar module your-role]
                [game-ui-modal *modal-data module your-role]])])

          @*page-loading?
          [loader]

          :else
          [not-found])))))
