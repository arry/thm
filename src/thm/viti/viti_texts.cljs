(ns thm.viti.viti-texts
  (:require [thm.viti.viti-data :as viti-data]
            [thm.utils :refer [denumber-id shorten-title-for-humans
                               unrecognized]]
            [clojure.string :as str]
            [taoensso.timbre :refer-macros (debug warn)]))

(defn symbol-of-role [r]
  ;; TODO
  r)

;; TODO other colors
(def role-for-humans {"r" "Red"
                      "y" "Yellow"})

(defn big-card-img-src [cid]
  (let [short (denumber-id cid)
        design (viti-data/cid->design short)]
    (str "/viti-images/" (:type design) "/" short ".jpg")))

(defn small-card-img-src [cid]
  (if (nil? cid)
    (do (warn "Nil card id!")
        "/images/unknown-card.png")
    (let [short (denumber-id cid)
          design (viti-data/cid->design short)]
      (str "/viti-images/" (:type design) "/" short ".jpg"))))

(defn card-title-by-id [cid]
  ;; TODO
  cid)

(defn short-card-title [cid]
  ;; TODO
  cid)

(defn season-for-humans [season]
  (case season
    "setup" "Setup"
    "spring" "Spring"
    "summer" "Summer"
    "fall" "Fall"
    "winter" "Winter"
    (unrecognized "season" season)))

(def fns
  {:outcome-submode
   (fn [submode translation-kind]
     (case submode
       "most-vp" "most victory points"
       "most-lira" "most lira (first tiebreak)"
       "best-wine" "highest total wine value (second tiebreak)"
       "best-grapes" "highest total grapes value (third tiebreak)"
       "shared-win" "shared win"
       (unrecognized "outcome submode" submode)))

   :role role-for-humans

   :timing
   (fn [timing-obj]
     (let [{:keys []} timing-obj]
       (str "Year " (:year timing-obj) " - " (season-for-humans (:season timing-obj)))))

   :symbol-of-role symbol-of-role

   :big-card-img-src big-card-img-src
   :small-card-img-src small-card-img-src

   :card-title-by-id card-title-by-id

   })
