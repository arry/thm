(ns thm.tm.tm-engine
  "Engine for Terraforming Mars - solo play only."
  (:use [thm.tm.tm-data])
  (:require [thm.game-engine.game-base :refer [defaction-coded def-answer-validator
                                   has-action? perform-sequence
                                   perform-action set-vars text do-nothing
                                   all-playing-roles++ lens-creation-options
                                   find-active-card
                                   register-game-definition]]
            [thm.game-engine.game-engine :as game-engine]
            [taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug)]
            [clojure.set]
            [thm.utils :refer [map-values map-cross find-first remove-first shuffle-with-random
                               to-integer
                               enumerate-things
                               not-empty?
                               pick-with-random
                               all-cards-in-zone
                               n-cards-in-zone
                               one-dim-map two-dim-map first-value denumber-id
                               half-round-up
                               all-cards-in-hand player-name-by-role prepare-request
                               multipart-name-from-descr all-cards-in-zone
                               finished? get-request-for-role
                               get-var prepare-zones
                               only-answer only-role&answer
                               in-spread?
                               make-rnd-from-options
                               pairs->map
                               ]]
  ))

(def GAME-CODE "tm")

(defn new-game [player-names & [opts]]
  (assert (= (count player-names) 1) "Only solitaire is supported for the time being")
  (let [player-name (first player-names)
        rnd (make-rnd-from-options opts)
        player-name->role {player-name solo-role}
        zones (prepare-zones zones-data all-roles)]
    {:code GAME-CODE
     :ordered-roles all-roles
     :player-name->role player-name->role
     :role->player-name (clojure.set/map-invert player-name->role)
     :vars (into (sorted-map)
                 (merge {"temperature" (get TEMPERATURE-TRACK :starting)
                         "oxygen" (get OXYGEN-TRACK :starting)
                         "remaining-oceans" (get REMAINING-OCEANS :starting)
                         "generation" 1
                         "s:tr" 14
                         "s:w:s" 2
                         "s:w:t" 3
                         "s:cgc" 8}
                        (two-dim-map "s:" ["a" "p"] basic-resources 0)))
     :zones (-> zones
                (assoc-in ["deck" :cards] (vec (shuffle-with-random all-project-cids rnd)))
                (assoc-in ["corp-deck" :cards] (vec (shuffle-with-random all-corp-cids rnd))))
     :pending-actions [:do-setup]
     :rnd rnd}))

;; === Helpers

(defn sum [s]
  (reduce + s))

(defn get-effect [state role effect]
  (get-var state ["-e" role effect]))

(defn has-effect? [state role effect]
  (some? (get-effect state role effect)))

(defn get-money [state role]
  (get-var state [role "a:m"]))

(defn get-resource [state role resource]
  (get-var state [role "a" resource] 0))

(defn get-available-money [state role]
  (+ (get-money state role)
     (if (has-effect? state role "helion")
       (get-resource state role "h")
       0)))


;; === Game actions

(defmacro defaction
  [n & forms]
  `(defaction-coded ~GAME-CODE ~n ~@forms))

(defaction :do-setup [state]
  (perform-sequence state
                    [:draw-cards "corp-deck" "s:choosing-corp" 2]
                    [:draw-cards "deck" "s:hand" 10]
                    [:place-solo-setup-tiles]
                    [:choose-corp-to-play solo-role]
                    [:choose-starting-projects solo-role]
                    [:do-generation]))

(defn all-surface-areas [state]
  (range 1 (inc N-SURFACE-AREAS)))

(defn all-in-play-areas [state]
  (range 1 (+ (inc N-SURFACE-AREAS) 2)))

(defn get-tile-kind [state area]
  (get-var state ["t" area "kind"]))

(defn get-area-inherent-bonus [state area]
  (-> map-features
      (get area)
      (get :bonus "")))

(defn has-tile-at? [state area]
  (some? (get-tile-kind state area)))

(defn get-tile-owner [state area]
  (get-var state ["t" area "owner"]))

(defn get-tile-neighbors [state area]
  (get map-adjacency area))

(defn allowed-to-place-tile-at? [state role area]
  (and (not (has-tile-at? state area))
       ;; Land Claim can create an owner without a tile
       (let [owner (get-tile-owner state area)]
         (or (nil? owner) (= owner role)))))

(defn is-land-area? [state area]
  (= "l" (:type (get map-features area))))

(defn is-water-area? [state area]
  (= "w" (:type (get map-features area))))

(defn find-area-for-solo-setup-city [state n backwards?]
  (let [zero-based-n (if (zero? n) 0
                         (dec n))
        initial-areas (cond->> (all-surface-areas state)
                         backwards? (reverse))
        areas (->> initial-areas
                    (filter #(is-land-area? state %))
                    (remove #(has-tile-at? state %)))
       index (mod zero-based-n (count areas))]
    (nth areas index)))

(defn find-area-for-solo-setup-greenery [state city-area n]
  (let [zero-based-n (if (zero? n) 0
                         (dec n))
        ;; Note: We've set up each value of map-adjacency in correct order.
        initial-areas (get-tile-neighbors state city-area)
        areas (->> initial-areas
                    (filter #(is-land-area? state %))
                    (remove #(has-tile-at? state %)))]
    (when (not-empty? areas)
      (nth areas (mod zero-based-n (count areas))))))

(defaction :place-solo-setup-city [state card-cost backwards?]
  (let [area (find-area-for-solo-setup-city state card-cost backwards?)]
    (perform-sequence state
                      [:place-ownerless-tile area "city"]
                      [:set-var "-solo-city-areas" (conj (get-var state "-solo-city-areas" []) area)])))

(defaction :place-solo-setup-greenery [state city-area card-cost]
  (let [area (find-area-for-solo-setup-greenery state city-area card-cost)]
    (if (some? area)
      (perform-action state [:place-ownerless-tile area "greenery"])
      (do-nothing state))))

(defaction :place-solo-setup-greeneries [state costs]
  (let [city-areas (get-var state "-solo-city-areas")]
    (perform-sequence state
                      [:place-solo-setup-greenery (nth city-areas 0) (nth costs 2)]
                      [:place-solo-setup-greenery (nth city-areas 1) (nth costs 3)])))

(defaction :place-solo-setup-tiles [state]
  (let [cids (take 4 (all-cards-in-zone state "deck"))
        costs (map #(:cost (card-design-by-id %)) cids)]
    (perform-sequence state
                      [:place-solo-setup-city (nth costs 0) false]
                      [:place-solo-setup-city (nth costs 1) true]
                      [:place-solo-setup-greeneries costs]
                      [:unset-var "-solo-city-areas"]
                      [:move-cards "deck" "discard" cids])))

(defaction :place-owner-marker [state area owner]
  (perform-sequence state
                    [:set-var ["t" area "owner"] owner]))

(defaction :place-tile-with-owner [state area kind owner]
  (perform-sequence state
                    [:set-var ["t" area "kind"] kind]
                    [:set-var ["t" area "owner"] owner]))

(defaction :place-ownerless-tile [state area kind]
  (perform-sequence state
                    [:set-var ["t" area "kind"] kind]))

(defaction :choose-corp-to-play [state role]
  (let [zone-name (multipart-name-from-descr [role "choosing-corp"])]
    (perform-action state
                    [:easy-put-request role [:choose-card :choose-corp
                                             {zone-name (all-cards-in-zone state zone-name)}]
                     [:on-corp-chosen role]])))

(defaction :on-corp-chosen [state role]
  (let [cid (only-answer state)
        card (card-design-by-id cid)]
    (perform-sequence state
                      [:move-card [role "choosing-corp"] [role "table-corp"] cid]
                      [:move-all-cards-in-zone [role "choosing-corp"] "corp-deck"]
                      [:set-var [role "a:m"] (:money card)]
                      [:apply-starting-corp-conditions role cid])))

(defaction :set-effect [state role effect & [value]]
  (perform-sequence state [:set-var ["-e" role effect] (or value 1)]))

(defaction :unset-effect [state role effect]
  (perform-sequence state [:unset-var ["-e" role effect]]))

(defaction :apply-starting-corp-conditions [state role corp]
  (let [design (card-design-by-id corp)]
    (perform-sequence state
                      (when-let [imm (:first-action design)]
                        [:set-effect role "first-action" imm])
                      (case corp
                        ("teract" "invent" "credic") [:do-nothing]
                        "unmi" [:set-effect role "unmi"]
                        "tharep" [:change-var [role "p:m"] 2] ;; for 2 starting cities; XXX solo only
                        "thorga" [:action-sequence
                                  [:change-var [role "p:e"] 1]
                                  [:set-effect role "thorgate"]]
                        "phobol" [:action-sequence
                                  [:change-var [role "a:t"] 10]
                                  [:change-var [role "w:t"] 1]]
                        "satsys" [:action-sequence
                                  [:change-var [role "p:t"] 1]
                                  [:change-var [role "p:m"] 1]] ;; for this Jovian tag
                        "mingui" [:action-sequence
                                  [:change-var [role "a:s"] 5]
                                  [:change-var [role "p:s"] 1]]
                        "helion" [:action-sequence
                                  [:change-var [role "p:h"] 3]
                                  [:set-effect role "helion"]]
                        "intcin" [:change-var [role "a:s"] 20]
                        "ecolin" [:action-sequence
                                  [:change-var [role "p:p"] 2]
                                  [:change-var [role "a:p"] 3]
                                  [:change-var [role "cgc"] -1]]))))

(defaction :choose-starting-projects [state role]
  (let [zn (multipart-name-from-descr [role "hand"])]
    (perform-sequence state
                      [:easy-put-request role [:choose-cards-flexible :choose-starting-projects
                                               zn nil
                                               {:all-in-zone? true
                                                :higher (let [n (n-cards-in-zone state zn)
                                                              money (get-money state role)]
                                                          (min n
                                                               (int (/ money 3))))
                                                :lower 0}]
                       [:on-starting-projects-chosen role]])))

(defaction :on-starting-projects-chosen [state role]
  (let [cids (only-answer state)
        the-set (set cids)
        cards-in-hand (all-cards-in-hand state role)
        non-chosen (remove #(contains? the-set %) cards-in-hand)]
    (perform-sequence state
                      [:move-cards [role "hand"] "discard" non-chosen]
                      [:change-var [role "a:m"] (* -3 (count cids))])))


(defaction :do-generation [state]
  (perform-sequence state
                    (when-not (= (get-var state "generation") 1)
                      [:do-research-phase])
                    [:do-actions]
                    [:do-production]
                    [:cleanup-per-generation-stuff]
                    [:increase-generation]))

(defaction :do-research-phase [state]
  (let [role solo-role]
    (perform-sequence state
                      [:tm-draw-cards-storing role 4 ["-drawn-cards" role]]
                      [:ask-research-projects-to-keep role]
                      [:unset-var ["-drawn-cards" role]])))

(defaction :tm-draw-cards [state role amount & [options]]
  (let [from-deck "deck"
        to-zone (or (:to-zone options [role "hand"]))
        deck-n (n-cards-in-zone state from-deck)
        shuffle-needed? (>= amount deck-n) ;; reshuffle as soon as the deck becomes empty
        initial-amount (min amount deck-n)]
    (perform-sequence state
                      [:draw-cards from-deck to-zone initial-amount options]
                      (when shuffle-needed?
                        [:shuffle-deck from-deck])
                      [:draw-cards from-deck to-zone (- amount initial-amount) options])))

(defaction :tm-draw-cards-storing [state role amount var-to-store]
  (perform-sequence state
                    [:set-var var-to-store []]
                    [:tm-draw-cards role amount {:storing-in var-to-store}]))

(defaction :ask-research-projects-to-keep [state role]
  (let [zn (multipart-name-from-descr [role "hand"])
        cids (get-var state ["-drawn-cards" role])
        money (get-available-money state role)
        higher (min (count cids) (int (/ money 3)))]
    (perform-sequence state
                      [:easy-put-request role [:choose-cards-flexible :choose-research-projects
                                               zn cids
                                               {:higher higher
                                                :lower 0}]
                       [:on-research-projects-chosen role]])))

(defaction :on-research-projects-chosen [state role]
  (let [chosen-cids (only-answer state)
        the-set (set chosen-cids)
        cids (get-var state ["-drawn-cards" role])
        non-chosen (remove #(contains? the-set %) cids)]
    (perform-sequence state
                      [:pay-me role (* 3 (count chosen-cids))]
                      [:move-cards [role "hand"] "discard" non-chosen])))

(defn reached-goal? [state goal]
  (case goal
    :temperature (= (get-var state "temperature") (:goal TEMPERATURE-TRACK))
    :oceans (= (get-var state "remaining-oceans") (:goal REMAINING-OCEANS))
    :oxygen (= (get-var state "oxygen") (:goal OXYGEN-TRACK))
    (assert false (str "Unrecognized terraforming goal: %s" goal))))

(defn terraformed? [state]
  (every? #(reached-goal? state %) [:temperature :oceans :oxygen]))

(defn tile-kind-is-city-like? [tile-kind]
  (contains? #{"city" "capital"} tile-kind))

(defn area-contains-tile-by-kind? [state area tile-kind]
  (if (= tile-kind :city-like)
    (tile-kind-is-city-like? (get-tile-kind state area))
    (= (get-tile-kind state area) tile-kind)))

(defn area-contains-your-greenery? [state area role]
  (and (area-contains-tile-by-kind? state area "greenery")
       (= role (get-tile-owner state area))))

(defn area-contains-your-city? [state area role]
  (and (area-contains-tile-by-kind? state area :city-like)
       (= role (get-tile-owner state area))))

(defn area-contains-your-mine? [state area role]
  (and (area-contains-tile-by-kind? state area "mine")
       (= role (get-tile-owner state area))))

(defn has-city-at-area? [state area]
  (contains? #{"city" "capital"} (get-tile-kind state area)))

(defn has-ocean-at-area? [state area]
  (= (get-tile-kind state area) "ocean"))

(defn count-of-adjacent-tile-kind [state area tile-kind]
  (count (filter #(area-contains-tile-by-kind? state % tile-kind)
                 (get-tile-neighbors state area))))

(defn has-adjacent-city? [state area]
  (some #(has-city-at-area? state %)
        (get-tile-neighbors state area)))

(defn near-two-cities? [state area]
  (>= (count-of-adjacent-tile-kind state area :city-like) 2))

(defn has-adjacent-greenery? [state area]
  (some #(= (get-tile-kind state %) "greenery")
        (get-tile-neighbors state area)))

(defn has-adjacent-tile? [state area]
  (some #(has-tile-at? state %)
        (get-tile-neighbors state area)))

(defn bonus-has-metal? [bonus]
  (re-find #"s|t" bonus))

(defn area-contains-metal? [state area]
  (bonus-has-metal? (get-area-inherent-bonus state area)))

(defn bonus-is-metal? [bonus-str]
  (or (= bonus-str "s")
      (= bonus-str "t")))

(defn has-adjacent-your-tile? [state area role]
  (some #(and (has-tile-at? state %)
              (= (get-tile-owner state %) role))
        (get-tile-neighbors state area)))

(defn collect-potential-land-areas [state role]
  (->> (all-surface-areas state)
       (filter #(and (is-land-area? state %)
                     (allowed-to-place-tile-at? state role %)))))



(defn collect-tile-areas [state role placement]
  (case placement
    (:ocean-normal :water-spot :water-spot-ignoring-restrictions)
    (filter #(and (is-water-area? state %)
                  (allowed-to-place-tile-at? state role %))
            (all-surface-areas state))

    (:land-spot :non-reserved)
    (->> (all-surface-areas state)
         (filter #(is-land-area? state %))
         (filter #(allowed-to-place-tile-at? state role %)))

    :greenery-normal
    (let [all (all-surface-areas state)
          with-my-tile (filter #(and (has-tile-at? state %)
                                     (= role (get-tile-owner state %)))
                               all)
          potential-tiles (if (not-empty? with-my-tile)
                            (set (mapcat #(get-tile-neighbors state %) with-my-tile))
                            all)
          actual (filter #(and (allowed-to-place-tile-at? state role %)
                               (is-land-area? state %))
                         potential-tiles)]
      actual)

    (:city-normal :like-city)
    (->> (collect-potential-land-areas state role)
         (remove #(has-adjacent-city? state %)))

    :not-adjacent-to-any
    (->> (collect-potential-land-areas state role)
         (remove #(has-adjacent-tile? state %)))

    :noctis
    [29] ;; XXX hard-code

    :ganymede
    [62]

    :phobos
    [63]

    :spot-with-metal
    (->> (collect-potential-land-areas state role)
         (filter #(area-contains-metal? state %)))

    :spot-with-metal-near-your-tile
    (->> (collect-potential-land-areas state role)
         (filter #(and (area-contains-metal? state %)
                       (has-adjacent-your-tile? state % role))))

    :near-city
    (->> (collect-potential-land-areas state role)
         (filter #(has-adjacent-city? state %)))

    :near-2-cities
    (->> (collect-potential-land-areas state role)
         (filter #(near-two-cities? state %)))

    :near-greenery
    (->> (collect-potential-land-areas state role)
         (filter #(has-adjacent-greenery? state %)))

    :lavflo ;; XXX hard-code; could be filter all tiles that have :other="lava-flows"
    [7 12 19 27]

    (assert false (format "Unrecognized placement: %s" placement))))

(def paid-cardless-actions [{:type :power-plant, :cost 11, :-thorgate-friendly? true,
                             :-standard? true
                             :-imm [:inc-prod "e" 1]}
                            {:type :asteroid, :cost 14,
                             :-standard? true
                             :-imm [:raise-temperature 1]}
                            {:type :aquifer, :cost 18,
                             :-standard? true
                             :-imm [:place-ocean]}
                            {:type :greenery, :cost 23
                             :-standard? true
                             :-imm [:place-greenery]}
                            {:type :city, :cost 25
                             :-standard? true
                             :-imm [:multi [:place-city] [:inc-prod "m" 1]]}
                            {:type :convert-greenery,
                             :-imm [:multi [:spend-plants-needed-to-convert-greenery]
                                    [:place-greenery]]}
                            {:type :convert-heat
                             :-imm [:multi [:spend 8 "h"] [:raise-temperature 1]]}])

(defn n-cards-in-hand [state role]
  (n-cards-in-zone state [role "hand"]))

(defn all-active-cards-of-player [state role]
  (mapcat #(all-cards-in-zone state %)
          [[role "table-corp"] [role "table-green"] [role "table-blue"]]))

(defn all-played-cards-of-player [state role]
  (mapcat #(all-cards-in-zone state %)
          [[role "table-corp"] [role "table-green"] [role "table-blue"] [role "table-red"]]))

(defn zone->cards-of-table-cards-of-player [state role]
  (let [zones (map multipart-name-from-descr [[role "table-corp"] [role "table-green"] [role "table-blue"]])]
    (zipmap zones
            (map #(all-cards-in-zone state %)
                 zones))))

(defn zone->cards-of-effectful-cards-of-player [state role]
  (let [zones (map multipart-name-from-descr [[role "table-corp"] [role "table-blue"]])]
    (zipmap zones
            (map #(all-cards-in-zone state %)
                 zones))))

(defn cid-of-active-card [card]
  (if (string? card)
    card
    (:cid card)))

(defn design-of-active-card [card]
  (let [cid (cid-of-active-card card)
        design (card-design-by-id cid)]
    design))

(defn discount-of-card [state card]
  (let [design (design-of-active-card card)]
    (:b-discount design)))

(defn all-effectful-cards-of-player [state role]
  (mapcat #(all-cards-in-zone state %)
          [[role "table-corp"] [role "table-blue"]]))

(defn get-all-discounts-of-player [state role]
  (cond-> []
    true (into (->> (all-effectful-cards-of-player state role)
                    (map #(discount-of-card state %))
                    (remove nil?)))

    (has-effect? state role "indwor") (conj [:any-card 8])))

(defn count-of-tag-in-tags-str [tag tags]
  (count (re-seq (re-pattern tag) tags)))

(defn count-of-this-tag [card tag]
  (let [design (design-of-active-card card)]
    (count-of-tag-in-tags-str tag (:tags design))))

(defn count-of-any-of-these-tags [card sought-tags-str]
  (let [design (design-of-active-card card)]
    (count (re-seq (re-pattern (clojure.string/join "|" sought-tags-str))
                   (:tags design)))))

(defn get-count-of-this-tag-on-active-cards [state role tag]
  (->> (all-active-cards-of-player state role)
       (map #(count-of-this-tag % tag))
       (sum)))

(defn discount-amount [discount]
  (second discount))

(defn discount-kind [discount]
  (first discount))

(defn discount-applies? [discount card-tags]
  (case (discount-kind discount)
    :any-card true
    :energy (pos? (count-of-tag-in-tags-str "n" card-tags))
    :earth (pos? (count-of-tag-in-tags-str "e" card-tags))
    :space (pos? (count-of-tag-in-tags-str "t" card-tags))
    (do (errorf "Unrecognized discount kind of discount: %s" discount)
        false)))

(defn player-steel-worth [state role]
  (get-var state [role "w:s"] 2))

(defn player-titanium-worth [state role]
  (get-var state [role "w:t"] 3))

(defn can-pay-me? [state role cost & [options]]
  (let [money (or (:money options)
                  (get-money state role))
        total (+ money
                 (if (:with-steel? options)
                   (* (player-steel-worth state role)
                      (get-resource state role "s"))
                   0)
                 (if (:with-titanium? options)
                   (* (player-titanium-worth state role)
                      (get-resource state role "t"))
                   0)
                 (if (has-effect? state role "helion")
                   (get-resource state role "h")
                   0))]
    (>= total cost)))

(defn tags-str-has-tag? [tags tag]
  (re-find (re-pattern tag) tags))

(defn card-by-cid-has-tag? [cid tag]
  (let [design (card-design-by-id cid)]
    (tags-str-has-tag? (:tags design) tag)))

(defn card-type-is-permanent? [card-type]
  (contains? #{"g" "b"} card-type))

(defn card-type-is-event? [card-type]
  (= card-type "r"))

(defn n-resources-on-card [card]
  (get card "res" 0))

(declare check-resolution-of-immediate)

(defn extract-production-box [state role imm]
  (let [imm (or imm [:empty])
        [tag & args] imm]
    (case tag
      :empty nil

      :choose (let [choices (->> args
                                 (map (fn [choice]
                                        {:choice-tag (:choice-tag choice)
                                         :imm (extract-production-box state role (:imm choice))}))
                                 (remove #(nil? (:imm %))))]
                (cond
                  (empty? choices) nil

                  (= (count choices) 1) (:imm (first choices))

                  :else
                  (into [tag] choices)))

      :multi (let [children (->> args
                                 (map #(extract-production-box state role %))
                                 (remove nil?))]
               (cond
                 (empty? children) nil

                 (= (count children) 1) (first children)

                 :else (into [tag] children)))

      :may nil ;; relying on the fact that it's only on a blue action

      (:inc-prod
       :inc-prod-formula
       :convert-prod-any-amount  ;; not strictly needed, because it's not on a building
       :dec-prod
       :dec-any-prod) imm

      :place-mine-inc-prod (let [single-char-strings (->> (all-surface-areas state)
                                                          (filter #(area-contains-your-mine? state % role))
                                                          (mapcat #(clojure.string/split (get-area-inherent-bonus state %) #""))
                                                          set
                                                          (filterv bonus-is-metal?))
                                 choice-tag-of-inc-prod (fn [res]
                                                          (case res
                                                            "s" :inc-prod-s-1
                                                            "t" :inc-prod-t-1
                                                            (assert false
                                                                    (format "Wrong resource to increase production: %s" res))))]
                             (case (count single-char-strings)
                               0 (do
                                   ;; can be if in testing we put the card into play but not the corresponding mine
                                   (warnf "No mines found for player in the state: %s %s" role state)
                                   nil)
                               1 [:inc-prod (str (first single-char-strings)) 1]
                               (into [:choose]
                                     (map (fn [res]
                                            {:choice-tag (choice-tag-of-inc-prod res),
                                             :imm [:inc-prod res 1]})
                                          (sort single-char-strings)))))

      nil)))

(defn find-robwor-targets [state role]
  (let [zone->cards (zone->cards-of-table-cards-of-player state role)
        matching-zone-cid-imm-triplets (for [[zone cards] zone->cards
                                             card cards
                                             :let [cid (cid-of-active-card card)
                                                   design (card-design-by-id cid)]
                                             :when (tags-str-has-tag? (:tags design) "b")
                                             :let [production-box-imm (extract-production-box state role (:imm design))]
                                             :when (and (some? production-box-imm)
                                                        (:ok? (check-resolution-of-immediate state role production-box-imm)))]
                                         [zone cid production-box-imm])]
    matching-zone-cid-imm-triplets))

(defn immediate-fulfills-predicate? [imm pred]
  (when (some? imm)
    (let [[tag & args] imm]
      (case tag
        :choose (some #(immediate-fulfills-predicate? (:imm %) pred) args)

        (:may :multi) (some #(immediate-fulfills-predicate? % pred) args)

        (pred imm)))))

(defn immediate-capable-of-adding-resource? [imm resource-descriptor]
  (immediate-fulfills-predicate? imm
                                 (fn [[tag & args :as imm]]
                                   (and (= tag :add-this)
                                        (or (= resource-descriptor :any)
                                            (let [[resource amount] args]
                                              (= resource resource-descriptor)))))))

(defn immediate-contains-production-box? [imm]
  (immediate-fulfills-predicate? imm
                                 (fn [[tag & args :as imm]]
                                   (contains? #{:inc-prod
                                                :inc-prod-formula
                                                :dec-prod
                                                :dec-any-prod
                                                :convert-prod-any-amount ;; not strictly needed, because it's not on a building
                                                :place-mine-inc-prod}
                                              tag))))

(defn card-by-id-collects-resource? [cid resource-descriptor]
  (let [design (card-design-by-id cid)
        imm-on-action (:b-action design)
        triggered (:b-triggered design)]
    (or (and (some? imm-on-action)
             (immediate-capable-of-adding-resource? imm-on-action resource-descriptor))
        (and (some? triggered)
             (immediate-capable-of-adding-resource? (:a triggered) resource-descriptor)))))

(defn card-collects-resource? [card resource]
  (card-by-id-collects-resource? (:cid card) resource))

(defn check-resolution-of-immediate [state role imm & [context]]
  (let [imm (or imm [:empty])
        [tag & args] imm]
    (case tag
      :empty {:ok? true}

      :choose
      (let [child-resolutions (map #(check-resolution-of-immediate state role (:imm %) context) args)]
        {:ok? (some :ok? child-resolutions)
         :useless? (every? :useless? child-resolutions)})

      (:may :multi)
      (let [child-resolutions (map #(check-resolution-of-immediate state role % context) args)]
        {:ok? (every? :ok? child-resolutions)
         :useless? (every? :useless? child-resolutions)})

      :add-another
      (let [[resource amount] args
            zone-card-pairs (for [[zone cards] (zone->cards-of-effectful-cards-of-player state role)
                                  card cards
                                  :when (card-collects-resource? card resource)]
                              [zone card])]
        {:ok? true,
         :useless? (empty? zone-card-pairs)})

      :add-resource-where-has-resource
      (let [[resource amount] args
            zone-card-pairs (for [[zone cards] (zone->cards-of-effectful-cards-of-player state role)
                                  card cards
                                  :when (pos? (n-resources-on-card card))]
                              [zone card])]
        {:ok? true,
         :useless? (empty? zone-card-pairs)})

      :convert-prod-any-amount
      (let [[falling rising] args
            current (get-var state [role "p" falling])]
        {:ok? true, :useless? (zero? current)})

      :look-buy-or-discard
      ;; MULTIPLAYER might make sense strategically e.g. to dig to a reshuffle
      {:ok? true, :useless? (not (can-pay-me? state role 3))}

      :spend-any-gain-same
      (let [[falling rising] args
            current (get-resource state role falling)]
        {:ok? true, :useless? (zero? current)})

      (:add-this
       :draw-cards
       :gain
       :gain-formula
       :inc-prod
       :inc-prod-formula
       :look-take
       :next-card-this-gen-8m-less
       :next-card-this-gen-tweak-global-requirements
       :raise-tr
       :raise-tr-formula
       :remove-any-from-adj-owner
       :special-sefoli
       :steal-from-any)
      {:ok? true}

      :add-that (let [[amount] args
                      action (:action context)
                      that-cid (:cid action)]
                  {:ok? (card-by-id-collects-resource? that-cid :any)})

      (:dec-any-prod :remove-another :remove-any) {:ok? true} ;; MULTIPLAYER check other players

      :dec-prod (let [[resource amount] args
                      actual (get-var state [role "p" resource] 0)]
                  {:ok? (if (= resource "m")
                          (>= (- actual amount) -5)
                          (>= actual amount))})

      :discard-card {:ok? (pos? (n-cards-in-hand state role))}

      :duplicate-production-of-your-building
      {:ok? (not-empty? (find-robwor-targets state role))}

      :place (let [[tile-kind & [placement]] args]
               {:ok? (not-empty? (collect-tile-areas state role (or placement :land-spot)))})

      :place-mine-inc-prod (let [[placement] args]
                             {:ok? (not-empty? (collect-tile-areas state role placement))})

      :place-city (let [[& [placement]] args]
                    {:ok? (not-empty? (collect-tile-areas state role (or placement :city-normal)))})

      :place-named-city {:ok? true}

      :place-greenery (let [[& [placement]] args]
                        {:ok? (not-empty? (collect-tile-areas state role (or placement :greenery-normal)))})

      :place-marker {:ok? true} ;; strictly, if there are free land spots, though who cares

      :place-ocean (let [[& [placement]] args]
                     {:ok? (if (:strict-globals? context)
                             (and (not (reached-goal? state :oceans))
                                  (not-empty? (collect-tile-areas state role (or placement :ocean-normal))))
                             true)})

      :place-oceans {:ok? true} ;; not checking strict-globals because only on cards

      :raise-oxygen {:ok? true} ;; not checking strict-globals because no standard project does this

      :raise-temperature {:ok? (if (:strict-globals? context)
                                 (not (reached-goal? state :temperature))
                                 true)}

      :remove-this (let [[resource amount] args
                         [_zone card _index] (find-active-card state (:cid context))]
                     {:ok? (>= (n-resources-on-card card) amount)})

      :spend (let [[amount resource] args]
               {:ok? (if (= resource "m")
                       (can-pay-me? state role amount)
                       (>= (get-resource state role resource) amount))})

      :spend-plants-needed-to-convert-greenery {:ok? (>= (get-resource state role "p")
                                                         (get-var state [role "cgc"]))}

      :spend-with-steel (let [[amount _resource] args]
                          {:ok? (can-pay-me? state role amount {:with-steel? true})})

      :spend-with-titanium (let [[amount _resource] args]
                             {:ok? (can-pay-me? state role amount {:with-titanium? true})})

      :unmi {:ok? (and (has-effect? state role "raised-tr")
                       (can-pay-me? state role 3))}

      )))

(defn prepare-cardless-action [state role cardless]
  (cond-> cardless
    true
    (assoc :-base-cost (or (:cost cardless) 0))

    (and (:-thorgate-friendly? cardless)
         (has-effect? state role "thorgate"))
    (assoc :cost (- (:cost cardless) 3))

    ))

(defn collect-player-actions [state role]
  (let [money (get-money state role)]
    (letfn [(cpm? [cost & [options]]
              (can-pay-me? state role cost (assoc options :money money)))

            (assign-tids [actions]
              (let [numerizer (atom {})]
                (mapv (fn [action]
                        (let [type-str (name (:type action))
                              many? (:-many? action)]
                          (if many?
                            (let [n (get @numerizer type-str 1)]
                              (swap! numerizer assoc type-str (inc n))
                              (-> action
                                  (dissoc :-many?)
                                  (assoc :tid (str type-str n))))
                            (assoc action :tid type-str))))
                      actions)))

            (has-tag? [tags tag]
              (tags-str-has-tag? tags tag))

            (parse-requirements [s]
              (let [[_ tag max? n] (re-find #"(.)(x)?([+-]?[0-9]+)?" s)]
                {:tag tag
                 :max? max?
                 :n (to-integer n)}))

            (has-production? [resource]
              (pos? (get-var state [role "p" resource])))

            (has-n-of-tag? [tag n]
              (>= (get-count-of-this-tag-on-active-cards state role tag) n))

            (has-cities-in-play? [n]
              (>= (count (map (fn [area]
                                (tile-kind-is-city-like? (get-tile-kind state area)))
                              (all-in-play-areas state)))
                  n))

            (has-your-greenery-tile? []
              (some #(and (= "greenery" (get-tile-kind state %))
                          (= role (get-tile-owner state %)))
                    (all-surface-areas state)))

            (scale-temperature [in-str]
              (+ 15 (/ in-str 2)))

            (meets-global-requirement? [which n max?]
              (let [[actual required] (case which
                                        :temperature [(get-var state "temperature") (scale-temperature n)]
                                        :oceans [(- 9 (get-var state "remaining-oceans")) n]
                                        :oxygen [(get-var state "oxygen") n])
                    tweak-blue (sum (for [card (all-effectful-cards-of-player state role)
                                          :let [design (design-of-active-card card)]
                                          :when (= (:b-special design) :tweak-global-requirements)]
                                      2))

                    tweak-effect (if (has-effect? state role "spedes")
                                   2
                                   0)
                    tweak (+ tweak-blue tweak-effect)]
                (if max?
                  (<= actual (+ required tweak))
                  (>= actual (- required tweak)))))

            (cost-after-discounts [cost tags]
              (let [all (get-all-discounts-of-player state role)
                    applicable (filter #(discount-applies? % tags) all)
                    total-discount (sum (map discount-amount applicable))]
                (max 0 (- cost total-discount))))

            (prepare-card-play-descriptor [card]
              (let [{:keys [req type tags cost imm]} card]
                {:cost (cost-after-discounts cost tags)
                 :cost-options (cond-> nil
                                 (has-tag? tags "t") (assoc :with-titanium? true)
                                 (has-tag? tags "b") (assoc :with-steel? true))
                 :meets-requirements? (if (nil? req)
                                        true
                                        (let [{:keys [tag max? n]} (parse-requirements req)
                                              n (or n 1)]
                                          (case tag
                                            "A" (every? #(has-n-of-tag? % 1) ["a" "m" "p"])
                                            "S" (has-production? "s")
                                            "T" (has-production? "t")
                                            "c" (has-cities-in-play? n)
                                            "g" (has-your-greenery-tile?)
                                            "j" (has-n-of-tag? "j" n)
                                            "n" (has-n-of-tag? "n" n)
                                            "o" (meets-global-requirement? :oxygen n max?)
                                            "s" (has-n-of-tag? "s" n)
                                            "t" (meets-global-requirement? :temperature n max?)
                                            "w" (meets-global-requirement? :oceans n max?)
                                            (assert false
                                                    (format "Unrecognized requirement: %s" req)))))
                 :-base-cost cost
                 :type type
                 :imm imm}))

            ]
      (->>
       (cond-> []
         true
         (conj {:type :pass})

         (pos? (n-cards-in-hand state role))
         (conj {:type :sell-patents})

         true
         (into (->> paid-cardless-actions
                      (map #(prepare-cardless-action state role %))

                      (filter (fn [action]
                                (and (if-let [cost (:cost action)]
                                       (cpm? cost)
                                       true)
                                     (:ok? (check-resolution-of-immediate state role (:-imm action) {:strict-globals? true})))))))

         true
         (into (for [cid (all-cards-in-zone state [role "hand"])
                       :let [card (card-design-by-id cid)
                             card-play-descriptor (prepare-card-play-descriptor card)
                             cost (:cost card-play-descriptor)
                             base-cost (:-base-cost card-play-descriptor)
                             imm (:imm card-play-descriptor)]
                       :when (:meets-requirements? card-play-descriptor)
                       :when (cpm? cost (:cost-options card-play-descriptor))
                       :when (:ok? (check-resolution-of-immediate state role imm))]
                 {:-many? true
                  :type :play-card, :cid cid,
                  :cost cost,
                  :-base-cost base-cost
                  :-card-play-descriptor card-play-descriptor}))

         true
         (into (for [[zone-name cards] (zone->cards-of-effectful-cards-of-player state role)
                     card cards
                     :let [_ (assert (associative? card)
                                     (format "An effectful card must be active: %s" card))]
                     :when (zero? (get card "used" 0))
                     :let [cid (:cid card)
                           design (card-design-by-id cid)
                           imm (:b-action design)]
                     :when (and (some? imm)
                                (:ok? (check-resolution-of-immediate state role imm {:cid cid})))]
                 {:-many? true
                  :type :activate-blue,
                  :cid cid, :card card
                  :zone-name zone-name
                  :-imm imm}))

         ;; MULTIPLAYER: claim milestone, sponsor award
         )

       assign-tids))))

(defaction :do-actions [state]
  ;; MULTIPLAYER: track whose turn it is
  (let [role solo-role]
    (if-let [imm (get-effect state role "first-action")]
      (perform-sequence state
                        [:unset-effect role "first-action"]
                        [:prepare-for-triggers]
                        [:resolve-immediate role imm]
                        [:resolve-action-triggers role {:type :first-action}]
                        [:do-actions])
      (let [actions (collect-player-actions state role)]
        (perform-sequence state
                          [:easy-put-request role [:choose-thing :choose-player-action
                                                   actions
                                                   {:multilevel? true}]
                           :on-player-action-chosen])))))

(defaction :on-player-action-chosen [state]
  (let [[r answer] (only-role&answer state)]
    (if (= (:type answer) :pass)
      (perform-action state [:handle-pass r])
      (perform-sequence state
                        [:handle-player-action r answer]
                        [:do-actions]))))

(defaction :handle-pass [state role]
  ;; MULTIPLAYER: set passed, end if everyone passed
  (do-nothing state))

(defaction :handle-player-action [state role action]
  (case (:type action)
    :sell-patents (perform-sequence state
                                    [:sell-patents role])

    (:power-plant :asteroid :aquifer :greenery :city)
    (perform-sequence state
                      [:prepare-for-triggers]
                      [:pay-me role (:cost action)]
                      [:resolve-immediate role (:-imm action)]
                      [:resolve-action-triggers role action])

    (:convert-greenery :convert-heat)
    (perform-sequence state
                      [:resolve-immediate role (:-imm action)])

    :play-card (let [card-play-descriptor (:-card-play-descriptor action)
                     cid (:cid action)]
                 (perform-sequence state
                                   [:prepare-for-triggers]
                                   [:pay-me role (:cost card-play-descriptor) (:cost-options card-play-descriptor)]
                                   [:move-card-into-play role cid]
                                   [:resolve-immediate role (:imm card-play-descriptor) {:cid cid}]
                                   [:dispose-if-red role cid]
                                   [:resolve-action-triggers role action]
                                   [:cleanup-card-play-this-generation-effects role cid]))

    :activate-blue
    (perform-sequence state
                      [:prepare-for-triggers]
                      [:set-card-props (:cid action) "used" 1]
                      [:resolve-immediate role (:-imm action) {:cid (:cid action), :card (:card action)}]
                      [:resolve-action-triggers role action])

    ;; MULTIPLAYER: milestones and awards
    (assert false
            (format "Unrecognized type of player action: %s" action))))

(defaction :sell-patents [state role]
  (let [zn (multipart-name-from-descr [role "hand"])]
    (perform-sequence state
                      [:easy-put-request role [:choose-cards-flexible :choose-patents-to-sell
                                               zn nil
                                               {:all-in-zone? true
                                                :higher (n-cards-in-zone state zn)
                                                :lower 1}]
                       [:on-sell-projects-chosen role]])))

(defaction :on-sell-projects-chosen [state role]
  (let [cids (only-answer state)]
    (perform-sequence state
                      [:text :sells-projects role (count cids)]
                      [:move-cards [role "hand"] "discard" cids]
                      [:change-var [role "a:m"] (* 1 (count cids))])))

(defaction :move-card-into-play [state role cid]
  (let [design (card-design-by-id cid)
        dest (get {"b" [role "table-blue"]
                   "r" "revealed"
                   "g" [role "table-green"]} (:type design))]
    (perform-sequence state
                      [:move-card [role "hand"] dest cid]

                      (when (= (:b-special design) :steel-and-titanium-worth-1-more)
                        [:action-sequence
                         [:change-var [role "w:s"] 1]
                         [:change-var [role "w:t"] 1]]))))

(defaction :dispose-if-red [state role cid]
  (let [design (card-design-by-id cid)]
    (if (= (:type design) "r")
      (perform-sequence state [:move-card "revealed" [role "table-red"] cid])
      (do-nothing state))))

(defn evaluate-formula [state role formula & [context]]
  (let [[a b of-what] formula]
    (if (= of-what :special-barrier-at-3-plant-tags)
      (if (>= (get-count-of-this-tag-on-active-cards state role "p") 3)
        b
        a)
      (let [[how-many for-how-many] [a b]
            n (case of-what
                :adjacent-city (let [area (:area context)]
                                 (count-of-adjacent-tile-kind state area :city-like))

                :adjacent-ocean (let [area (:area context)]
                                  (count-of-adjacent-tile-kind state area "ocean"))


                :this-resources (let [card (:card context)]
                                  (n-resources-on-card card))

                :each-space-tag-of-opponents 0 ;; MULTIPLAYER actually calculate

                :city-in-play (->> (all-in-play-areas state)
                                   (filter #(has-city-at-area? state %))
                                   count)

                :city-on-mars (->> (all-surface-areas state)
                                   (filter #(has-city-at-area? state %))
                                   count)

                :each-event-ever-played (sum
                                         (map #(n-cards-in-zone state [% "table-red"]) all-roles))

                :your-building-tag (get-count-of-this-tag-on-active-cards state role "b")
                :your-earth-tag    (get-count-of-this-tag-on-active-cards state role "e")
                :your-jovian-tag   (get-count-of-this-tag-on-active-cards state role "j")
                :your-microbe-tag  (get-count-of-this-tag-on-active-cards state role "m")
                :your-plant-tag    (get-count-of-this-tag-on-active-cards state role "p")
                :your-power-tag    (get-count-of-this-tag-on-active-cards state role "n")
                :your-space-tag    (get-count-of-this-tag-on-active-cards state role "t")

                (assert false
                        (format "Unrecognized of-what of formula: %s" formula)))]
        (* how-many (int (/ n for-how-many)))))))

(defn all-playing-roles [state]
  all-roles)

(defaction :change-card-resource [state cid amount]
  (perform-sequence state [:change-card-props cid "res" amount]))

(defaction :resolve-immediate [state role imm & [context]]
  (if (nil? imm)
    (do-nothing state)
    (let [[tag & args] imm]
      (case tag
        ;; Complex actions (that have child actions)

        :choose (let [useful-list (for [choice args
                                        :let [imm (:imm choice)
                                              resolution (check-resolution-of-immediate state role imm context)]
                                        :when (and (:ok? resolution)
                                                   (not (:useless? resolution)))]
                                    {:mode (-> choice :choice-tag name)
                                     :imm imm})

                      useful-list (if (empty? useful-list)
                                    [{:mode :play-without-effect
                                      :imm [:empty]}]
                                    useful-list)

                      modes (mapv :mode useful-list)
                      the-map (zipmap modes
                                      (mapv :imm useful-list))]
                  (perform-sequence state
                                    [:easy-put-request role [:choose-mode :choose-effect-to-resolve
                                                             modes]
                                     [:on-choose-imm-chosen the-map context]]))

        :may (perform-sequence state
                               [:easy-put-request role [:choose-mode [:want-resolve-effect? (denumber-id (:cid context))]
                                                        ["resolve-effect" "ignore-effect"]]
                                [:on-may-answered role imm context]])

        ;; XXX really it's more complex, becaues the rules allow you to choose the order
        :multi (apply perform-sequence state
                      (map (fn [child-imm] [:resolve-immediate role child-imm context])
                           args))


        ;; Simple actions

        :add-another
        (let [[resource amount] args
              zone-card-pairs (for [[zone cards] (zone->cards-of-effectful-cards-of-player state role)
                                    card cards
                                    :when (card-collects-resource? card resource)]
                                [zone card])]
          (if (seq zone-card-pairs)
            (perform-sequence state
                              [:easy-put-request role [:choose-card [:choose-card-to-add-resources resource amount]
                                                       (pairs->map zone-card-pairs :cid)]
                               [:on-add-another-chosen resource amount]])

            (perform-sequence state
                              [:text :cannot-add-another resource amount])))

        :add-resource-where-has-resource
        (let [[amount] args
              zone-card-pairs (for [[zone cards] (zone->cards-of-effectful-cards-of-player state role)
                                    card cards
                                    :when (pos? (n-resources-on-card card))]
                                [zone card])]
          (if (seq zone-card-pairs)
            (perform-sequence state
                              [:easy-put-request role [:choose-card [:choose-card-for-ceo-attention amount]
                                                       (pairs->map zone-card-pairs :cid)]
                               [:on-ceo-chosen amount]])
            (perform-sequence state
                              [:text :cannot-add-ceo amount])))

        :add-this (let [[resource amount] args
                        cid (:cid context)]
                    (perform-sequence state
                                      [:change-card-resource cid amount]))

        :add-that (let [[amount] args
                        action (:action context)
                        that-cid (:cid action)]
                    (perform-sequence state
                                      [:change-card-resource that-cid amount]))


        :convert-prod-any-amount
        (let [[falling rising] args
              current (get-var state [role "p" falling])]
          (perform-sequence state [:easy-put-request role [:choose-number [:choose-amount-of-production-to-convert falling rising]
                                                           0 current]
                                   [:on-convert-prod-any-amount-chosen falling rising]]))

        (:dec-any-prod
         :remove-any
         :remove-any-from-adj-owner)
        (do-nothing state) ;; MULTIPLAYER collect potential players and ask which to remove

        :dec-prod (let [[resource amount] args]
                    (perform-sequence state
                                      [:change-var [role "p" resource] (- amount)]))

        :discard-card
        (let [zone (multipart-name-from-descr [role "hand"])]
          (perform-sequence state
                            [:easy-put-request role [:choose-card :choose-card-to-discard
                                                     {zone (all-cards-in-zone state zone)}]
                             [:on-discard-card-chosen]]))

        :draw-cards (let [[amount] args]
                      (perform-sequence state
                                        [:tm-draw-cards role amount]))

        :duplicate-production-of-your-building
        (let [zone-cid-imm-triplets (find-robwor-targets state role)
              _ (assert (not-empty? zone-cid-imm-triplets)
                        "Robotic workforce targets must not be empty")

              cid->imm (zipmap (map #(nth % 1) zone-cid-imm-triplets)
                               (map #(nth % 2) zone-cid-imm-triplets))

              groups (group-by first zone-cid-imm-triplets)

              zone->cids (map-values (fn [k v]
                                       (mapv second v))
                                     groups)]

          (perform-sequence state
                            [:easy-put-request role [:choose-card :choose-card-to-duplicate-with-robwor
                                                     zone->cids]
                             [:on-duplicate-with-robwor-chosen cid->imm]]))

        :gain (let [[amount resource] args]
                (perform-sequence state
                                  [:change-var [role "a" resource] amount]))

        :gain-formula (let [[resource formula] args
                            amount (evaluate-formula state role formula)]
                        (perform-sequence state
                                          [:change-var [role "a" resource] amount]))

        :inc-prod (let [[resource amount] args]
                    (perform-sequence state
                                      [:change-var [role "p" resource] amount]))


        :inc-prod-formula (let [[resource formula] args
                                amount (evaluate-formula state role formula)]
                            (perform-sequence state
                                              [:change-var [role "p" resource] amount]))

        :look-buy-or-discard
        (perform-sequence state
                          [:tm-draw-cards-storing role 1 "-drawn"]
                          [:ask-buy-or-discard role]
                          [:unset-var "-drawn"])

        :look-take
        (let [[n-look n-take] args]
          (perform-sequence state
                            [:tm-draw-cards-storing role n-look "-drawn"]
                            [:ask-look-take role n-take]
                            [:unset-var "-drawn"]))


        :next-card-this-gen-8m-less (perform-sequence state
                                                      [:set-effect role "indwor"])

        :next-card-this-gen-tweak-global-requirements (perform-sequence state
                                                                        [:set-effect role "spedes"])

        :place
        (let [[tile-kind & [placement]] args]
          (perform-sequence state
                            [:ask-place-tile role tile-kind (or placement :land-spot)]))

        :place-city
        (let [[& [placement]] args]
          (perform-sequence state
                            [:ask-place-tile role "city" (or placement :city-normal)]))

        :place-named-city
        (let [[placement] args
              areas (collect-tile-areas state role placement)
              _ (assert (= (count areas) 1)
                        (format "Each named city only has one area: %s" placement areas))]
          (perform-sequence state
                            [:do-place-tile role (first areas) "city" {}]))

        :place-greenery
        (let [[& [placement]] args]
          (perform-sequence state
                            [:ask-place-tile role "greenery" (or placement :greenery-normal)]
                            [:raise-oxygen role 1]))

        :place-marker
        (let [[placement] args
              areas (collect-tile-areas state role placement)]
          (perform-sequence state [:easy-put-request role [:choose-number-from-list :choose-area-to-place-marker
                                                           (vec areas)]
                                   [:on-area-to-place-marker-chosen]]))

        :place-mine-inc-prod
        (let [[placement] args]
          (perform-sequence state
                            [:ask-place-tile role "mine" placement {:inc-that-prod? true}]))


        :place-ocean
        (let [[& [placement]] args]
          (perform-sequence state [:try-place-ocean role (or placement :ocean-normal)]))

        :place-oceans
        (let [[n] args
              remaining-oceans (get-var state "remaining-oceans")
              m (min n remaining-oceans)]
          (cond
            (<= n remaining-oceans)
            (apply perform-sequence state
                   (repeat n [:ask-place-ocean role :ocean-normal]))

            (zero? remaining-oceans)
            (perform-sequence state
                              [:text :no-more-oceans])

            :else
            (apply perform-sequence state
                   [:text :place-m-oceans-instead-of-n m n]
                   (repeat m [:ask-place-ocean role :ocean-normal]))))

        :remove-this (let [[resource amount] args
                           cid (:cid context)]
                       (perform-sequence state
                                         [:change-card-resource cid (- amount)]))

        :raise-temperature (let [[amount] args]
                             (perform-sequence state [:raise-temperature role amount]))

        :raise-oxygen (let [[amount] args]
                        (perform-sequence state [:raise-oxygen role amount]))

        :raise-tr (let [[amount] args]
                    (perform-sequence state [:raise-tr role amount]))

        :raise-tr-formula (let [[formula] args
                                amount (evaluate-formula state role formula)]
                            (perform-sequence state
                                              [:raise-tr role amount]))

        :special-sefoli
        (perform-sequence state
                          [:tm-draw-cards role 1 {:to-zone "revealed"}]
                          [:check-revealed-for-signs-of-life role]
                          [:move-all-cards-in-zone "revealed" "discard"])


        :spend (let [[amount resource] args]
                 (if (= resource "m")
                   (perform-action state [:pay-me role amount])
                   (perform-action state [:change-var [role "a" resource] (- amount)])))

        :spend-any-gain-same
        (let [[falling rising] args
              current (get-resource state role falling)]
          (perform-sequence state [:easy-put-request role [:choose-number [:choose-amount-of-resource-to-convert falling rising]
                                                           0 current]
                                   [:on-spend-any-gain-some-chosen falling rising]]))

        :spend-plants-needed-to-convert-greenery
        (let [required (get-var state [role "cgc"])]
          (perform-sequence state
                            [:change-var [role "a:p"] (- required)]))

        :spend-with-steel (let [[amount _resource] args]
                               (perform-action state [:pay-me role amount {:with-steel? true}]))

        :spend-with-titanium (let [[amount _resource] args]
                               (perform-action state [:pay-me role amount {:with-titanium? true}]))

        :steal-from-any (let [[resource amount] args]
                          ;; MULTIPLAYER choose of whom to steal, possibly not a full amount
                          (perform-action state [:change-var [role "a" resource] amount]))

        :unmi (perform-sequence state
                                [:pay-me role 3]
                                [:raise-tr role 1])

        (do
          (warnf "Unrecognized tag of immediate effect: %s" imm)
          (do-nothing state))
        ))))

(defaction :try-place-ocean [state role placement]
  (let [remaining-oceans (get-var state "remaining-oceans")]
    (if (pos? remaining-oceans)
      (perform-sequence state
                        [:ask-place-ocean role placement])
      (perform-sequence state
                        [:text :no-more-oceans]))))

(defaction :ask-place-ocean [state role placement]
  (perform-sequence state
                    [:ask-place-tile role "ocean" placement]
                    [:raise-tr role 1]
                    [:change-var "remaining-oceans" -1]))

(defaction :ask-place-tile [state role tile-kind placement & [options]]
  (let [areas (collect-tile-areas state role placement)]
    (assert (seq areas)
            (format "Must have at least one area to place the tile %s %s %s" role placement tile-kind))
    (perform-sequence state [:easy-put-request role [:choose-number-from-list [:choose-area-to-place-tile tile-kind]
                                                     (vec areas)]
                             [:on-area-to-place-tile-chosen tile-kind options]])))

(defaction :ask-buy-or-discard [state role]
  (let [cids (get-var state "-drawn")
        _ (assert (= (count cids) 1)
                  (format "In :look-buy-or-discard, must have exactly 1 drawn card: %s" cids))
        cid (first cids)]
    (if (can-pay-me? state role 3)
      (perform-sequence state [:easy-put-request role [:choose-mode [:buy-or-discard (denumber-id cid)]
                                                       ["buy" "discard"]]
                               [:on-buy-or-discard-chosen cid]])
      (perform-sequence state
                        [:text :cannot-buy]
                        [:discard-card role cid]))))

(defaction :on-buy-or-discard-chosen [state cid]
  (let [[role mode] (only-role&answer state)]
    (if (= mode "buy")
      (perform-sequence state
                        [:pay-me role 3])
      (perform-sequence state
                        [:discard-card role cid]))))

(defaction :on-duplicate-with-robwor-chosen [state cid->imm]
  (let [[role cid] (only-role&answer state)
        imm (get cid->imm cid)]
    (assert (some? imm)
            (format "Must have a production box imm for card %s: %s" cid cid->imm))
    (perform-sequence state
                      [:resolve-immediate role imm])))


(defaction :ask-look-take [state role n-take]
  (let [cids (get-var state "-drawn")
        zn (multipart-name-from-descr [role "hand"])]
    (perform-sequence state [:easy-put-request role [:choose-cards-flexible :choose-cards-to-take
                                                     zn cids
                                                     {:lower n-take, :higher n-take}]
                             [:on-look-take-chosen cids]])))

(defaction :on-look-take-chosen [state drawn-cids]
  (let [[role cids] (only-role&answer state)
        the-set (set cids)
        non-chosen (remove #(contains? the-set %) drawn-cids)]
    (perform-sequence state
                      [:move-cards [role "hand"] "discard" non-chosen])))

(defaction :on-add-another-chosen [state resource amount]
  (let [cid (only-answer state)]
    (perform-sequence state
                      [:change-card-props cid "res" amount])))

(defn calculate-placement-bonuses [state area]
  (let [inherent-bonus (get-area-inherent-bonus state area)
        money (* 2 (count (filter #(has-ocean-at-area? state %)
                                  (get-tile-neighbors state area))))]
    {:inherent-bonus inherent-bonus, :money money}))

(defaction :apply-placement-bonuses [state role bonuses]
  (let [{:keys [inherent-bonus money]} bonuses]
    (perform-sequence state
                      [:change-var [role "a:m"] money]
                      (case inherent-bonus
                        nil :do-nothing
                        "" :do-nothing
                        "c" [:tm-draw-cards role 1]
                        "cc" [:tm-draw-cards role 2]
                        "p" [:change-var [role "a:p"] 1]
                        "pp" [:change-var [role "a:p"] 2]
                        "pt"  [:action-sequence
                              [:change-var [role "a:p" ] 1]
                              [:change-var [role "a:t" ] 1]]
                        "s" [:change-var [role "a:s"] 1]
                        "ss" [:change-var [role "a:s"] 2]
                        "t" [:change-var [role "a:t"] 1]
                        "tt" [:change-var [role "a:t"] 2]
                        (assert false
                                (format "Unrecognized bonus: %s" inherent-bonus))))))

(defaction :inc-that-prod [state role bonuses]
  (let [inherent-bonus (:inherent-bonus bonuses)]
    (cond (re-find #"s" inherent-bonus) (perform-sequence state [:change-var [role "p:s"] 1])
          (re-find #"t" inherent-bonus) (perform-sequence state [:change-var [role "p:t"] 1])
          :else (assert false
                        "Area must contain either steel or titanium bonus"))))

(defaction :on-area-to-place-tile-chosen [state tile-kind options]
  (let [[role area] (only-role&answer state)]
    (perform-sequence state [:do-place-tile role area tile-kind options])))

(defaction :do-place-tile [state role area tile-kind options]
  (let [bonuses (calculate-placement-bonuses state area)
        potential-triggers (get-var state "-potential-triggers")]
    (perform-sequence state
                      (if (= tile-kind "ocean")
                        [:place-ownerless-tile area tile-kind]
                        [:place-tile-with-owner area tile-kind role])
                      [:set-var "-potential-triggers"
                       (conj potential-triggers {:kind :placed-tile
                                                 :tile-kind tile-kind
                                                 :area area
                                                 :inherent-bonus (:inherent-bonus bonuses)})]
                      [:apply-placement-bonuses role bonuses]
                      (when (:inc-that-prod? options)
                        [:inc-that-prod role bonuses]))))

(defaction :on-area-to-place-marker-chosen [state]
  (let [[role area] (only-role&answer state)]
    (perform-sequence state
                      [:place-owner-marker area role])))

(defaction :on-choose-imm-chosen [state the-map context]
  (let [[role mode] (only-role&answer state)
        imm (get the-map mode)]
    (perform-sequence state
                      [:resolve-immediate role imm context])))

(defaction :on-may-answered [state role parent-imm context]
  (let [answer (only-answer state)]
    (if (= answer "resolve-effect")
      (perform-sequence state
                        [:resolve-immediate role (second parent-imm) context])
      (do-nothing state))))

(defaction :on-ceo-chosen [state amount]
  (let [cid (only-answer state)]
    (perform-sequence state
                      [:change-card-props cid "res" amount])))

(defaction :on-convert-prod-any-amount-chosen [state falling rising]
  (let [[role amount] (only-role&answer state)]
    (perform-sequence state
                      [:change-var [role "p" falling] (- amount)]
                      [:change-var [role "p" rising] (+ amount)])))

(defaction :discard-card [state role cid]
  (perform-sequence state
                    [:move-card [role "hand"] "discard" cid]))


(defaction :on-discard-card-chosen [state]
  (let [[role cid] (only-role&answer state)]
    (perform-sequence state
                      [:discard-card role cid])))

(defaction :on-spend-any-gain-some-chosen [state falling rising]
  (let [[role amount] (only-role&answer state)]
    (perform-sequence state
                      [:change-var [role "a" falling] (- amount)]
                      [:change-var [role "a" rising] (+ amount)])))

(defaction :prepare-for-triggers [state]
  (perform-sequence state
                    [:set-var "-potential-triggers" []]))

(defn n-times-trigger-matches [state trigger role that-role action]
  (let [potential-triggers (get-var state "-potential-triggers")

        count-in-potential (fn [pred]
             (count (filter pred potential-triggers)))

        count-boolean (fn [bool]
                        (if bool 1 0))

        is-you? (= role that-role)]
    (case trigger
      :anyone-places-city
      (count-in-potential (fn [that]
            (and (= (:kind that) :placed-tile)
                 (tile-kind-is-city-like? (:tile-kind that)))))

      :anyone-places-city-on-mars
      (count-in-potential (fn [that]
                            (and (= (:kind that) :placed-tile)
                                 (tile-kind-is-city-like? (:tile-kind that))
                                 (<= (:area that) N-SURFACE-AREAS))))

      :anyone-places-ocean
      (count-in-potential (fn [that]
            (and (= (:kind that) :placed-tile)
                 (= (:tile-kind that) "ocean"))))

      :anyone-puts-jovian-tag-in-play
      (count-boolean
          (and (= (:type action) :play-card)
               (let [design (card-design-by-id (:cid action))]
                 (and (card-type-is-permanent? (:type design))
                      (tags-str-has-tag? (:tags design) "j")))))

      :you-get-metal-placement-bonus
      (count-in-potential (fn [that]
            (and is-you?
                 (= (:kind that) :placed-tile)
                 (bonus-has-metal? (:inherent-bonus that)))))

      :you-pay-20+
      (count-boolean
       (and is-you?
            (and (or (:-standard? action)
                     (= (:type action) :play-card))
                 (>= (:-base-cost action) 20))))

      :you-pay-for-standard
      (count-boolean
       (and is-you? (:-standard? action)))

      :you-place-city
      (count-in-potential (fn [that]
                            (and is-you?
                                 (= (:kind that) :placed-tile)
                                 (tile-kind-is-city-like? (:tile-kind that)))))

      :you-place-greenery
      (count-in-potential (fn [that]
                            (and is-you?
                                 (= (:kind that) :placed-tile)
                                 (= (:tile-kind that) "greenery"))))

      :you-play-amp
      (if (and is-you? (= (:type action) :play-card))
        (count-of-any-of-these-tags (:cid action) "amp")
        0)

      :you-play-ap
      (if (and is-you? (= (:type action) :play-card))
        (count-of-any-of-these-tags (:cid action) "ap")
        0)

      :you-play-event
      (count-boolean
       (and is-you?
            (= (:type action) :play-card)
            (let [design (card-design-by-id (:cid action))]
              (card-type-is-event? (:type design)))))

      :you-play-science
      (if (and is-you? (= (:type action) :play-card))
        (count-of-this-tag (:cid action) "s")
        0)

      :you-play-space-event
      (count-boolean
       (and is-you?
            (= (:type action) :play-card)
            (let [design (card-design-by-id (:cid action))]
              (and (card-type-is-event? (:type design))
                   (tags-str-has-tag? (:tags design) "t")))))

      (assert false
              (format "Unrecognized trigger: %s" trigger)))))

(defaction :resolve-action-triggers [state role action]
  (let [expanded-triggered-actions (for [that-role (all-playing-roles state)
                                         card (all-effectful-cards-of-player state that-role)
                                         :let [cid (cid-of-active-card card)
                                               design (card-design-by-id cid)
                                               b-triggered (:b-triggered design)

                                               triggered-seq (cond (nil? b-triggered) []
                                                                   (sequential? b-triggered) b-triggered
                                                                   :else [b-triggered])]
                                         triggered triggered-seq
                                         :let [trigger (:t triggered)
                                               imm (:a triggered)
                                               n (n-times-trigger-matches state trigger role that-role action)]]
                                     {:that-role that-role, :imm imm, :cid cid, :n n})]
    (apply perform-sequence state
           [:unset-var "-potential-triggers"]
           (mapcat (fn [{:keys [that-role imm cid card n]}]
                     (repeat n [:resolve-immediate that-role imm {:cid cid, :action action}]))
                expanded-triggered-actions))))

(defaction :increase-generation [state]
  (perform-sequence state
                    [:change-var "generation" 1]
                    [:check-solo-game-end]))

(def OXYGEN-TEMPERATURE-BONUS 8)

(defaction :raise-oxygen [state role amount]
  (let [current (get-var state "oxygen")
        goal (:goal OXYGEN-TRACK)
        actual (min amount (- goal current))

        reached-temperature? (and (< current OXYGEN-TEMPERATURE-BONUS)
                                  (>= (+ current actual) OXYGEN-TEMPERATURE-BONUS))]
    (perform-sequence state
                      [:change-var "oxygen" actual]
                      [:raise-tr role actual]
                      (when reached-temperature?
                        [:raise-temperature role 1]))))

(defaction :raise-tr [state role amount]
  (perform-sequence state
                    [:change-var [role "tr"] amount]
                    (when (has-effect? state role "unmi")
                      [:set-effect role "raised-tr"])))

(defaction :raise-temperature [state role amount]
  (let [current (get-var state "temperature")
        goal (:goal TEMPERATURE-TRACK)
        bonuses [{:step 3, :gets :hp}
                 {:step 5, :gets :hp}
                 {:step 15, :gets :ocean}]

        actual (min amount (- goal current))

        bonus-reachings (for [{:keys [step gets]} bonuses
                              :when (and (< current step)
                                         (>= (+ current actual) step))]
                          gets)]
    (apply perform-sequence state
           [:change-var "temperature" actual]
           [:raise-tr role actual]
           (map (fn [gets]
                  (case gets
                    :hp [:change-var [role "p:h"] 1]
                    :ocean [:try-place-ocean role :ocean-normal]
                    (assert false
                            (format "Unrecognized bonus gets: %s" gets))))
                bonus-reachings))))

(defaction :check-revealed-for-signs-of-life [state role]
  (let [cards (all-cards-in-zone state "revealed")
        _ (assert (= (count cards) 1)
                  (format "Must have exactly one revealed card: %s" (vec cards)))
        cid (first cards)]
    (if (pos? (count-of-this-tag cid "m"))
      (perform-sequence state
                        [:text :life-found cid]
                        [:change-card-resource "sefoli" 1])

      (perform-sequence state
                        [:text :no-life-found cid]))))


(defaction :check-solo-game-end [state]
  (let [generation (get-var state "generation")]
    (if (> generation 14)
      (perform-sequence state
                        :final-convert-greeneries
                        :finish-calculate-score)
      (perform-action state [:do-generation]))))

(defaction :final-convert-greeneries [state]
  (let [role solo-role ;; MULTIPLAYER go round
        actions (->> paid-cardless-actions
                     (filter #(= (:type %) :convert-greenery))
                     (map #(prepare-cardless-action state role %))
                     (filter #(:ok? (check-resolution-of-immediate state role (:-imm %)))))]
    (if (empty? actions)
      (do-nothing state)
      (perform-sequence state
                        [:resolve-immediate role (:-imm (first actions)) {:action (first actions)}]
                        [:final-convert-greeneries]))))

(defn count-adjacent-greeneries [state area]
  (count-of-adjacent-tile-kind state area "greenery"))

(defn calculate-score-breakdown [state]
  (let [role solo-role]
    {"tr" (get-var state [role "tr"])
     "greeneries" (count (filter #(area-contains-your-greenery? state % role)
                                (all-surface-areas state)))
     "cities" (sum (->> (all-surface-areas state)
                       (filter #(area-contains-your-city? state % role))
                       (map #(count-adjacent-greeneries state %))))

     ;; MULTIPLAYER awards and milestones

     "vp-from-formula-cards" (sum (for [card (all-played-cards-of-player state role)
                                        :let [design (design-of-active-card card)]
                                        :when (some? (:vp-formula design))
                                        :let [amount (evaluate-formula state role (:vp-formula design)
                                                                       {:card card})]]
                                    amount))

     "vp-from-straight-cards" (sum (for [card (all-played-cards-of-player state role)
                                         :let [design (design-of-active-card card)]
                                         :when (some? (:vp design))]
                                     (:vp design)))

     }))

(defn score-from-breakdown [score-breakdown]
  (sum (vals score-breakdown)))

(defaction :finish-calculate-score [state]
  (let [score-breakdown (calculate-score-breakdown state)
        total (score-from-breakdown score-breakdown)
        [mode winner other] (if (terraformed? state)
                              ["win" solo-role {:score total
                                                :would-be-score 0
                                                :score-breakdown score-breakdown}]
                              ["loss" nil {:score 0
                                           :would-be-score total
                                           :score-breakdown score-breakdown}])]
    (perform-sequence state
                      [:finish-game mode other winner])))

(defaction :do-production [state]
  (let [role solo-role
        n-energy (get-var state [role "a:e"])]
    (perform-sequence state
                      [:change-var [role "a:e"] (- n-energy)]
                      [:change-var [role "a:h"] (+ n-energy)]
                      [:change-var [role "a:m"] (+ (get-var state [role "tr"])
                                                   (get-var state [role "p:m"]))]
                      (into [:action-sequence]
                            (map (fn [resource]
                                   [:change-var [role "a" resource]
                                    (get-var state [role "p" resource])])
                                 ["s" "t" "p" "e" "h"])))))

(defaction :cleanup-card-play-this-generation-effects [state role & [cid]]
  (let [actions (cond-> []
                  (and (has-effect? state role "indwor")
                       (not (= cid "indwor")))
                  (conj [:unset-effect role "indwor"])

                  (and (has-effect? state role "spedes")
                       (not (= cid "spedes")))
                  (conj [:unset-effect role "spedes"]))]
    (if (seq actions)
      (apply perform-sequence state actions)
      (do-nothing state))))

(defaction :cleanup-per-generation-stuff [state]
  (let [role solo-role ;; MULTIPLAYER of all players
        actions (for [role (all-playing-roles state)
                      card (all-effectful-cards-of-player state role)
                      :when (pos? (get card "used" 1))]
                  [:unset-card-props (:cid card) "used"])]
    (apply perform-sequence state
           [:cleanup-card-play-this-generation-effects role]
           (when (and (has-effect? state role "unmi")
                      (has-effect? state role "raised-tr"))
             [:unset-effect role "raised-tr"])
           actions)))

(defaction :pay-me [state role cost & [cost-options]]
  (if (= cost 0)
    (do-nothing state)
    (let [alternatives (cond-> []
                         true
                         (conj {:resource "m"
                                :worth 1
                                :min 0
                                :max (get-resource state role "m")})

                         (and (:with-steel? cost-options)
                              (pos? (get-resource state role "s")))
                         (conj {:resource "s"
                                :worth (player-steel-worth state role)
                                :min 0
                                :max (get-resource state role "s")})

                         (and (:with-titanium? cost-options)
                              (pos? (get-resource state role "t")))
                         (conj {:resource "t"
                                :worth (player-titanium-worth state role)
                                :min 0
                                :max (get-resource state role "t")})

                         (and (has-effect? state role "helion")
                              (pos? (get-resource state role "h")))
                         (conj {:resource "h"
                                :worth 1
                                :min 0
                                :max (get-resource state role "h")}))]
      (if (= (count alternatives) 1)
        (perform-action state [:change-var [role "a:m"] (- cost)])
        (perform-sequence state
                          [:easy-put-request role [:choose-payment-alternatives :choose-payment-alternatives
                                                   cost alternatives]
                           [:on-payment-alternatives-chosen alternatives]])))))

(defaction :on-payment-alternatives-chosen [state alternatives]
  (let [[role answer] (only-role&answer state)]
    ;; answer is a list of amounts of each alternative, e.g. [0 2] to pay 0 money and 2 steel.
    (apply perform-sequence state
           (map-indexed (fn [index amount]
                          [:change-var [role "a" (:resource (nth alternatives index))]
                           (- amount)])
                        answer))))

(def-answer-validator GAME-CODE :choose-payment-alternatives
  [state role [kind text-descr & args :as req] answer]
  (let [[cost alternatives] args]
    (and (sequential? answer)
         (= (count answer) (count alternatives))
         (every? integer? answer)
         (every? identity (map-indexed (fn [index amount]
                                         (let [alternative (nth alternatives index)]
                                           (<= (:min alternative) amount (:max alternative))))
                                       answer))
         (>= (sum (map-indexed (fn [index amount]
                                 (* amount (:worth (nth alternatives index))))
                               answer))
             cost))))

;; === Hooks to external world

(defn state-timing [state]
  {:generation (get-var state "generation")
   ;; :phase (get-var state "phase") ;; for the future
   ;; :active-player (get-var state "active-player") ;; for the future
   })

(def game-definition {:new-game new-game
                      :state-timing state-timing
                      :has-spaces? false})

(register-game-definition GAME-CODE game-definition)

(defn new-engine [& args]
  (apply game-engine/new-engine GAME-CODE args))

(defn new-engine-from-checkpoint [checkpoint mode]
  (game-engine/new-engine-from-checkpoint GAME-CODE checkpoint mode))

(defmethod all-playing-roles++ GAME-CODE [state]
  (all-playing-roles state))

(defmethod lens-creation-options GAME-CODE [state]
  {:disable-card-numbers? true})
