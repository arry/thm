(ns thm.views.views
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [reagent.core :as r]
            [baking-soda.core :as b]
            [re-frame.core :refer [register-handler dispatch subscribe]]
            [thm.views.marker :as marker]
            [demo.anim]
            [thm.views.game-ui :as game-ui]
            [taoensso.timbre :refer-macros (debug warn)]
            [ajax.core :refer [POST]]
            [thm.utils :refer [bull html-entity dispatch-e format-timestamp
                               sequence-for-humans
                               seq-contains? VALIDATION-CONSTS
                               pluralize
                               short-game-name
                               value-from-event
                               bind
                               get-game-code-from-page-arg
                               ]]
            [thm.views.common :refer [link outlink connection-status chat
                                      indexed-component
                                      tabbed-component avatar-img your-avatar-img my-button
                                      past-outcome-for-humans
                                      loader loader-when-loading
                                      behind-page-loader behind-loader
                                      not-found
                                      game-logo-src
                                      ]]
            [thm.td.td-data :refer [td-options]]
            [thm.texts :as t]
            [clojure.string :as str]

            [thm.views.arbo :refer [arbo]]

            [thm.tm.tm-data :as tm-data]
            ))


(defn landing []
  (let [*username (subscribe [:username])]
    (fn []
      [:div#landing
        [:div.container
       [:div.jumbotron
         [:h1.display-4 "Chantry"]
         [:p.lead "Play board games in your browser."]
         [:hr.my-4]
         (if @*username
           [:p [:a.btn.btn-primary.btn-lg {:href "/lobby"} "Go to lobby"]]
           [:p.buttons
            [:a.btn.btn-primary.btn-lg {:href "/signup"} "Sign up for free"]
            [:a.btn.btn-light.btn-lg {:href "/login"} "Log in"]
            [:a.btn.btn-light {:href "/lobby"} "Browse as guest"]])]]
       [:div.container
        [:div.row
         [:div.col
          [:h4 "Play with other people"]
          [:p "Connect with people all over the world."]]
         [:div.col
          [:h4 "Live or async"]
          [:p "Play a full game in one sitting, or make turns now and then."]]
         [:div.col
          [:h4 "Rules-enforced"]
          [:p "Software handles all game rules, leaving the thinking to you."]]]

        [:hr.my-4]

        [:div
         [:h3 "Available games"]
         [:div.game
          [:img.logo.float-left {:src "/images/td-logo.png"}]
          [:div [:b "13 Days: Cuban Missile Crisis"]]
          [:div
           [outlink "https://www.boardgamegeek.com/boardgame/177590/13-days-cuban-missile-crisis" "BGG"] (bull)
           [outlink "http://www.jollyrogergames.com/13-days" "Publisher"]]
          [:div [link (str "/sandbox/td") "Try it out in sandbox"]]]
         [:hr.my-4]

         [:div [:i "More to come!"]]]
        ]

       ])))

(defn input-group [label type id & [opt]]
  [:div.form-group.row
   [:label.col-4.col-form-label {:for id} label]
   [:div.col
    [:input (merge {:type type :id id :name id} opt)]]])

(defn radio [key value data content]
  [:div.form-check
   [:label.form-check-label
    [:input.form-check-input {:type "radio" :name (name key)
                              :checked (= (key @data) value)
                              :on-change #(swap! data assoc key value)}]
    [:span.label-body content]]])

(defn radio-btn [key value data content]
  (let [active? (= (key @data) value)]
    [:label.btn.btn-light.form-check-label {:class (when active? "active")}
     [:input.form-check-input {:type "radio" :name (name key)
                               :checked active?
                               :on-change #(swap! data assoc key value)}]
     content]))

(defn checkbox [key data text]
  [:div.form-check
   [:label.form-check-label
    [:input.form-check-input {:type "checkbox"
                              :name (name key)
                              :checked (get @data key)
                              :on-change #(swap! data update key not)}]
    text]])

(defn login []
  (let [data (r/atom {})
        *loading? (subscribe [:loading?])
        *login-failure? (subscribe [:thm.login/login-failure?])]
    (fn []
      [:div#login.container
       [:div.col-6
        [:h1 "Log in"]
        [:form {:on-submit #(dispatch-e [:thm.login/submit-login-form @data] %)}
         (input-group "Username or email" :text :username-or-email
                      {:max-length 30
                       :on-change (bind data :username-or-email)})
         (input-group "Password" :password :password
                      {:on-change (bind data :password)})
         [:div.row
          [:div.col-4]
          [:div.col
           [:button.btn.btn-primary.btn-lg {:type "submit"
                                            :disabled @*loading?} "Log in"]]]]

        [:div.row.mt-2
         [loader-when-loading]
         (when @*login-failure?
           [:div.alert.alert-danger "Invalid username or password"])]

        [:hr]
        [:div.row "Forgot your password?" (html-entity "nbsp") [link "/forgot" "Reset password"]]
        [:div.row "No account yet?" (html-entity "nbsp") [link "/signup" "Sign up for free"]]]])))

(def account-settings-data (r/atom {}))

(defn set-initial-account-settings! [v]
  (reset! account-settings-data v))

(def MAX-AVATAR-FILE-SIZE-MB 2)

(defn on-avatar-file-change [e anti-forgery-token]
  (let [file (-> e .-target .-files (aget 0))
        size (.-size file)]
    (if (> size (* MAX-AVATAR-FILE-SIZE-MB 1024 1024))
      (dispatch [:thm.account/avatar-file-too-large])
      (let [form-data (doto
                          (js/FormData.)
                        (.append "csrf-token" anti-forgery-token)
                        (.append "avatar-file" file))]
        (dispatch [:thm.account/starting-avatar-upload])
        (POST "/upload-avatar" {:body form-data
                                :handler (fn [_]
                                           (dispatch [:thm.account/avatar-upload-finished :ok]))
                                :error-handler (fn [data]
                                                 (dispatch [:thm.account/avatar-upload-finished (:response data)]))})))))


(defn account []
  (let [data account-settings-data
        *username (subscribe [:username])
        *loading? (subscribe [:loading?])
        *anti-forgery-token (subscribe [:anti-forgery-token])
        *settings-applied? (subscribe [:thm.account/settings-applied?])
        *avatar-upload-result (subscribe [:thm.account/avatar-upload-result])]
    (fn []
      [:div#account.container
       [:div.row
        [:div.col-6.my-4
         [your-avatar-img]]]
       [:h1 "Account settings"]
       [:div.row
        [:div.col-6
         [:form
          (if @*loading?
            [loader]
            [:div.upload-avatar
             [:div.form-group.row
              [:label.col-4 {:for "avatar-file"} "Upload avatar:"]

              [:div.col
               [:input {:type "file"
                        :name "avatar-file"
                        :accept "image/png, image/jpeg"
                        :value ""
                        :on-change #(on-avatar-file-change % @*anti-forgery-token)}]]]
             [:div.smaller "(Maximum " MAX-AVATAR-FILE-SIZE-MB "MB; PNG or JPEG; will display at 64x64px.)"]
             (when-let [avatar-upload-result @*avatar-upload-result]
               (if (= avatar-upload-result :ok)
                 [:div.alert.alert-success
                  "Uploaded successfully."]
                 [:div.alert.alert-danger
                  (case avatar-upload-result
                    ":file-too-large" "The chosen file is too large."
                    ":error-processing-file" "Cannot process the file - are you sure it's an image?"
                    ":no-file" "Server error."
                    ":invalid-username" "Not allowed.")]))])]]]
       [:hr]
       [behind-page-loader
        [:div.row
         [:div.col-6
          [:form {:on-submit #(dispatch-e [:thm.account/submit-account-settings-form @data] %)}
           [:div.row.mb-2
            ;; XXX duplication of the checkbox text with email body in unsubscribe-footer in clj/thm/emails.clj
            (checkbox "allow-game-emails" data "Allow emails from async games")]
           [:button.btn.btn-primary {:type "submit"
                                     :disabled @*loading?} "Apply"]
           (when @*settings-applied?
             [:div.alert.alert-success.mt-2 "Settings saved!"])]]]]
       [:hr]
       [:button.btn.btn-light.btn-lg {:on-click #(dispatch-e
                                                    [:thm.account/submit-logout-form] %)}
        "Log out"]])))

(defn signup-error-message-for-humans [kind]
  (case kind
    :unconfirmed-registration-exists "We've already sent a confirmation email
      to this address, and we won't send more than one."
    :user-exists "User with this email already exists."
    :invalid-email "Email address must contain a @ symbol."
    (do (warn "Unrecognized signup error message kind: " kind)
        "And something else!")))

(defn signup []
  (let [data (r/atom {})
        *loading? (subscribe [:loading?])
        *error-message (subscribe [:thm.signup/signup-error-message])
        *sent? (subscribe [:thm.signup/confirmation-email-sent?])]
    (fn []
      [:div#signup.container
       [:div.col-6
        [:h1 "Sign up"]
        [:form {:on-submit #(dispatch-e [:thm.signup/submit-signup-form @data] %)}
         (input-group "Email" :email :email {:max-length (:max-email-length VALIDATION-CONSTS)
                                             :on-change (bind data :email)})
         [:button.btn.btn-primary.btn-lg {:type "submit"
                                          :disabled (or @*loading? @*sent?)} "Send confirmation code"]]
        [:div [loader-when-loading]]
        (when @*error-message
          [:div.alert.alert-danger
           (signup-error-message-for-humans @*error-message)])
        [:hr]
        (when @*sent?
          [:div.alert.alert-success
           "We've sent you a confirmation code.  Please click a link there to
           finish signing up."])
        [:div.row "Already have an account?" (html-entity "nbsp") [link "/login" "Log in"]]
        [:div.row "Forgot your password?" (html-entity "nbsp") [link "/forgot" "Reset password"]]]])))

(defn confirm-registration-error-message-for-humans [error-message]
  (let [[kind & args] error-message]
    (str (case kind
           :email-doesnt-match "Supplied email doesn't match the email for which this
      registration was created"
           :min-username (let [[min actual] args]
                           (str "Username must have at least " (pluralize min "character")
                                " but has " actual))
           :max-username (let [[max actual] args]
                           (str "Username must have at most " (pluralize max "character")
                                " but has " actual))
           :invalid-username-characters "Username must consist only of Latin letters, numbers, spaces, minuses and underscores"
           :username-guest "Username shouldn't pretend you're a guest"
           :user-exists "This username is already taken"
           :min-password (let [[min actual] args]
                           (str "Password must have at least " (pluralize min "character")
                                " but has " actual))
           :max-password (let [[max actual] args]
                           (str "Password must have at most " (pluralize max "character")
                                " but has " actual))
           :not-nice (str "We don't serve people who're not nice")
           (do (warn "Unrecognized confirm-registration error message kind: " kind)
               "And something else"))
         ".")))

(defn confirm-registration [confirmation-code]
  (let [*can-proceed? (subscribe [:thm.signup/confirmation-code-valid?])
        data (r/atom {:confirmation-code confirmation-code})
        *loading? (subscribe [:loading?])
        *error-messages (subscribe [:thm.signup/confirm-error-messages])]
    (fn [confirmation-code]
      [behind-page-loader
       [:div#confirm-registration.container
        (if @*can-proceed?
          [:div.col-6
           [:h1 "Finish sign-up"]
           [:form {:on-submit #(dispatch-e [:thm.signup/submit-confirm-registration-form @data] %)}
            (input-group "Email" :email :email {:max-length (:max-email-length VALIDATION-CONSTS)
                                                 :on-change (bind data :email)})
            (input-group "Username" :text :username {:max-length (:max-username-length VALIDATION-CONSTS)
                                                     :on-change (bind data :username)})
            (input-group "Password" :password :password {:max-length (:max-password-length VALIDATION-CONSTS)
                                                         :on-change (bind data :password)})
            [:div.row
             [:div.col-4]
             [:div.col
              [:div.form-check
               [:label {:for :nice}
                [:input {:type "checkbox" :name :nice :id :nice
                         :on-change (bind data :nice)}]
                [:span.label-body "I agree to be nice"]]]]]
            [:button.btn.btn-primary.btn-lg {:type "submit"
                                             :disabled @*loading?} "Sign up"]]
           [:div [loader-when-loading]]
           [:hr]
           (when (seq @*error-messages)
             [:div.alert.alert-danger
              [:ul
               (indexed-component @*error-messages
                                  (fn [i error-message]
                                    [:li {:key i} (confirm-registration-error-message-for-humans error-message)]))]])]
          [not-found])]])))

(defn forgot []
  (let [data (r/atom {})
        *loading? (subscribe [:loading?])
        *error-message (subscribe [:thm.reset-password/forgot-error-message])
        *sent? (subscribe [:thm.reset-password/reset-email-sent?])]
    (fn []
      [:div#forgot.container
       [:div.col-6
        [:h1 "Forgot password"]
        [:form {:on-submit #(dispatch-e [:thm.reset-password/submit-forgot-form @data] %)}
         (input-group "Username or email" :username-or-email :username-or-email
                      {:max-length (max (:max-email-length VALIDATION-CONSTS)
                                        (:max-username-length VALIDATION-CONSTS))
                       :on-change (bind data :username-or-email)})
         [:button.btn.btn-primary.btn-lg {:type "submit"
                                          :disabled (or @*loading? @*sent?)} "Send the reset code"]]
        [:div [loader-when-loading]]
        (when-let [kind @*error-message]
          [:div.alert.alert-danger
           (case kind
             :recovery-exists "We have already sent a recovery email to this
            address, and we won't send another until that one is resolved.  If
            you've lost that email and still want to recover password, please
            use Contact link in the footer."
             :no-user "We don't have a user with this username or email."
             (do (warn "Unrecognized forgot error message kind: " kind)
                 "And something else!"))])
        (when @*sent?
          [:div.alert.alert-success
           "We've sent you a reset code.  Please check your email, and click a
            link there to reset your password."])]])))

(defn reset-password [reset-code]
  (let [*can-proceed? (subscribe [:thm.reset-password/code-valid?])
        data (r/atom {:reset-code reset-code})
        *loading? (subscribe [:loading?])
        *error-messages (subscribe [:thm.reset-password/error-messages])
        *reset-successful? (subscribe [:thm.reset-password/reset-successful?])]
    (fn [reset-code]
      [behind-page-loader
       [:div#reset.container
        (if @*can-proceed?
          [:div.col-6
           [:h1 "Reset password"]
           [:form {:on-submit #(dispatch-e [:thm.reset-password/submit-reset-form @data] %)}
            (input-group "Username or email" :username-or-email :username-or-email
                         {:max-length (max (:max-email-length VALIDATION-CONSTS)
                                           (:max-username-length VALIDATION-CONSTS))
                          :on-change (bind data :username-or-email)})
            ;; TODO add explanation why username-or-email is needed again (to
            ;; make sure that people who brute-forced the code have smaller
            ;; chance of doing harm.)
            (input-group "New password" :password :password {:max-length (:max-password-length VALIDATION-CONSTS)
                                                             :on-change (bind data :password)})
            [:button.btn.btn-primary.btn-lg {:type "submit"
                                             :disabled (or @*loading? @*reset-successful?)}
             "Reset password"]]
           [:div [loader-when-loading]]
           (when (seq @*error-messages)
             [:div.alert.alert-danger
              [:ul
               (indexed-component @*error-messages
                                  (fn [i [kind & args]]
                                    [:li {:key i}
                                     (str
                                      (case kind
                                        :wrong-username-or-email "This recovery belongs to another user"
                                        :min-password (let [[min actual] args]
                                                        (str "Password must have at least " (pluralize min "character")
                                                             " but has " actual))
                                        :max-password (let [[max actual] args]
                                                        (str "Password must have at most " (pluralize max "character")
                                                             " but has " actual))
                                        (do (warn "Unrecognized reset-password error message" kind)
                                            "And something else"))
                                      ".")]))]])
           (when @*reset-successful?
             [:div.alert.alert-success
              "Your password has been reset. "
              [link "/home" "To home"]])]
          [not-found])]])))



(defn marker [marker-state]
  (let [set-coords (fn [e]
                     (let [coords {:x (.-pageX e)
                                   :y (.-pageY e)}]
                       (swap! marker-state assoc :coords coords))
                     false)
        img-src "/images/board.jpg"
        style-from-marker-state (fn []
                                  {:left (str "-" (* 4 (-> @marker-state :coords :x)) "px")
                                   :top (str "-"  (* 4 (-> @marker-state :coords :y)) "px")})]
    [:div#marker
     [:div.controls
      [link "/" "Done"]]
     [:div.content
      [:div.preview
       [:div.placer
        [:img {:src img-src
               :style (style-from-marker-state)}]]
       [:div.crosshair-topleft]
       [:div.crosshair-bottomright]
       [:div.status (str (:coords @marker-state) " " (:state @marker-state))]]
      [:div.main-map [:img {:on-mouse-over set-coords
                            :on-mouse-move set-coords
                            :on-click marker/mark-point
                            :src img-src}]]]
     [:div.footer "built by arry"]]))



(defn term []
  (let [term-state (subscribe [:thm.term/state])
        focus (fn [& [dom-node]]
                (let [node (or dom-node (:dom-node @term-state))]
                  (-> node
                      (.getElementsByTagName "input")
                      (aget 0)
                      (.focus))))
        do-scroll (fn []
                    (let [node (:dom-node @term-state)]
                      (aset node "scrollTop" (.-scrollHeight node))))
        custom-input (fn []
                       [:input {:type "text", :name "term-input", :id "term-input",
                                :on-key-up (fn [e]
                                             (when-not (or (.-altKey e) (.-ctrlKey e) (.-metaKey e))
                                               (case (.-key e)
                                                 "ArrowUp"
                                                 (dispatch [:thm.term/on-arrow-up e])

                                                 "ArrowDown"
                                                 (dispatch [:thm.term/on-arrow-down e])

                                                 nil)))
                                :size 80, :maxlength 80
                                :value (:input @term-state)
                                :on-change (fn [e]
                                             (dispatch [:thm.term/on-input-changed (value-from-event e)]))}])]
    (r/create-class
     {:component-did-mount #(do (dispatch [:thm.term/did-mount (r/dom-node %)])
                                (focus (r/dom-node %)))
      :reagent-render
      (fn []
        [:pre#term {:on-click #(focus)}
         (indexed-component (:lines @term-state)
                            (fn [i line]
                              [:div.line {:key i} line]))
         [:form {:auto-complete "off"
                 :on-submit (fn [e]
                              (.preventDefault e)
                              (dispatch [:thm.term/on-submit (:input @term-state)])
                              (do-scroll))}
          [:span (:prompt @term-state)] [custom-input]]])})))

(defn test-term [state]
  [term])

(def PROXY-KIND :corp)
(if (= PROXY-KIND :project)
  (do
    (def cw 244)
    (def ch 350))
  (do
    (def cw 350)
    (def ch 244)))

(defn draw-proxy [card-index anti-forgery-token]
  (let [upload-proxy (fn [short-name blob]
                       (let [form-data (doto
                                           (js/FormData.)
                                         (.append "csrf-token" anti-forgery-token)
                                         (.append "short-name" short-name)
                                         (.append "image_name" blob (str short-name ".png")))]
                         (POST "/upload-proxy" {:body form-data
                                                :handler (fn [_]
                                                           (js/console.log "uploaded proxy" short-name))
                                                 })))
        marg 4
        rect-marg 4
        cost-size 22
        tag-size 22
        effects-size 14
        set-font (fn [ctx px & [fam]]
                   (let [fam (or fam "Prototype")]
                     (aset ctx "font" (str px "px " fam))))

        compact-title? (fn [title]
                         (#{"Shuttles" "Search for Life" "Ecological Zone" "Herbivores" "Immigrant City"} title))

        split-text (fn [text title]
                     ;; Given a text, return a sequence of strings that, if
                     ;; drawn to the image, wouldn't overflow the width.  Not
                     ;; the best implementation, because it cuts off by char
                     ;; size, not taking into account the variable width of the
                     ;; letters in the font.
                     (if (str/blank? text)
                       []
                       (let [words (str/split text " ")
                             measure (fn [s]
                                       (reduce +
                                               (map #(if (re-matches #"[A-Z]" %)
                                                       1.2
                                                       1) s)))
                             max-chars (if (= PROXY-KIND :project)
                                         (case title
                                           "Mangrove" 23
                                           "Ecological Zone" 32
                                           "Immigrant City" 33
                                           "Capital" 31
                                           31)
                                         18)]
                         (reduce (fn [result word]
                                   (let [last-index (dec (count result))
                                         tentative (str (nth result last-index) " " word)]
                                     (if (> (measure tentative) max-chars)
                                       (conj result word)
                                       (assoc result last-index tentative))))
                                 [(first words)] (rest words)))))

        tag-letter->image-name {"s" "tag-science"
                                "c" "tag-city"
                                "b" "tag-building"
                                "e" "tag-earth"
                                "t" "tag-space"
                                "1" "tag-event"
                                "j" "tag-jovian"
                                "p" "tag-plant"
                                "a" "tag-animal"
                                "m" "tag-microbe"
                                "n" "tag-energy"}]
    (let [card (if (= PROXY-KIND :project)
                 (get tm-data/project-cards card-index)
                 (get tm-data/corp-cards card-index))
          canvas (js/document.getElementById "the-canvas")
          ctx (.getContext canvas "2d")]

      (debug "rendering card proxy " card)

      (.clearRect ctx 0 0 cw ch)

      (aset ctx "textBaseline" "alphabetic")
      (aset ctx "textAlign" "center")

      ;; border
      (aset ctx "fillStyle" "white")
      (.fillRect ctx rect-marg rect-marg
                 (- cw (* 2 rect-marg)) (- ch (* 2 rect-marg)))

      (aset ctx "lineWidth" 3)
      (set! (.-strokeStyle ctx)
            (case (:type card)
              "r" "red"
              "b" "blue"
              "green"))
      (.strokeRect ctx rect-marg rect-marg
                   (- cw (* 2 rect-marg)) (- ch (* 2 rect-marg)))

      (aset ctx "fillStyle" "black")

      ;; cost & requirements
      (set-font ctx cost-size)
      (aset ctx "textAlign" "left")
      (.fillText ctx (if (= PROXY-KIND :project)
                       (str (:cost card)
                            (when-not (nil? (:req card))
                              (str " " (:req card))))
                       (str (:money card)))
                 (+ (* 2 marg))
                 (+ (* 2 marg)    cost-size))


      ;; icons

      (when-not (= (:tags card) "-")
        (doseq [[index tag] (map-indexed
                             (fn [i v] [i v])
                             (reverse (:tags card)))]
          (.drawImage ctx (js/document.getElementById (tag-letter->image-name tag))
                      (- cw (* 2 marg) (* index (+ tag-size marg)) tag-size) (* 2 marg)
                      tag-size tag-size)))


      ;; title

      (set-font ctx 22)
      (aset ctx "textAlign" "center")

      (if (= PROXY-KIND :project)
        (.fillText ctx (:title card) (/ cw 2) (* marg 13) (- cw (* marg 4)))
        (.fillText ctx (:title card) (/ cw 2) (* marg 8) (- cw (* marg 4))))

      ;; image
      (.drawImage ctx (js/document.getElementById (thm.utils/shorten-title (:title card)))
                  (* marg 2) (* marg 15)
                  (if (= PROXY-KIND :project) 228 (* 228 .9))
                  (if (= PROXY-KIND :project) 160 (* 160 .9)))

      ;; text
      (set-font ctx effects-size "serif")
      (aset ctx "textAlign" "left")

      (let [lines (if (= PROXY-KIND :project)
                    (if (:blue card)
                      (concat (split-text (:blue card) (:title card))
                              (when-not (compact-title? (:title card)) [" "])
                              (split-text (:effect card) (:title card)))
                      (split-text (:effect card) (:title card)))
                    (concat (split-text (:first card) (:title card))
                            (split-text (:second card) (:title card))))

            start-point (if (= PROXY-KIND :project)
                          {:x (* marg 2)
                           :y (* marg 60)}
                          {:x 216
                           :y (* marg 15)})
            ]
        (loop [lines lines, index 0]
          (when (seq lines)
            (let [line (first lines)]
              (.fillText ctx line
                         (:x start-point) (+ (:y start-point) (* index (+ 2 effects-size)))
                         (- cw (* marg 4)))
              (recur (rest lines), (inc index))))))

      ;; upload it
      (.toBlob canvas (fn [blob]
                        (js/console.log "got blob" blob)
                        (upload-proxy (thm.utils/shorten-title (:title card))
                                      blob)))
      )))

(defn proxier-canvas [card-index-r anti-forgery-token]
  (r/create-class
   {:component-did-mount #(draw-proxy @card-index-r anti-forgery-token)
    :component-did-update #(draw-proxy @card-index-r anti-forgery-token)
    :reagent-render
    (fn []
      [:div
       [:canvas#the-canvas {:width cw
                            :height ch}]])}))

(defn test-proxier [state]
  (let [proxier-state (subscribe [:thm.proxier/state])
        *anti-forgery-token (subscribe [:anti-forgery-token])
        ]
    (r/create-class
     {:component-did-mount identity
      :reagent-render
      (fn [state]
        [:div.row
         [:div.col-6
          [:br]
          [:br]
          [:br]
          [:br]

          [:span (:card-index @proxier-state)]

          [:div {:style {"fontFamily" "Prototype"
                         "color" "red"}}
           "testing custom font"]

          (indexed-component ["tag-animal" "tag-building" "tag-city" "tag-earth" "tag-energy" "tag-event" "tag-jovian" "tag-microbe" "tag-plant" "tag-science" "tag-space"]
                             (fn [_index image-name]
                               [:img {:id image-name
                                      :class "hidden"
                                      :key image-name
                                      :src (str "/tm-images/icons/" image-name ".png")}]))

          (indexed-component ["acqcom" "adalic" "adatec" "advall" "adveco" "aeamas" "aicen" "algae" "angrte" "ants" "aqupum" "arcalg" "archae" "artlak" "artpho" "asmico" "astero" "astmin" "bfata" "bigast" "biocom" "birds" "blpodu" "brefil" "bricom" "buiind" "buscon" "bushes" "busnet" "capemi" "capita" "carcon" "carpro" "cartel" "cefapr" "closee" "cofreu" "comdis" "comet" "corstr" "cotrca" "credic" "cupcit" "decomp" "deidow" "desmic" "devcen" "dewehe" "domcra" "dussea" "earcat" "earoff" "ecnp" "ecolin" "ecozon" "elecat" "enesav" "enetap" "equmag" "excofu" "farmin" "fish" "floodi" "foofac" "fuefac" "fuegen" "fuspow" "gancol" "genrep" "geopow" "ghgfac" "ghprba" "giicas" "gispmi" "grass" "gredam" "greenh" "gresco" "hacker" "heathe" "heatra" "helion" "herbiv" "hirrai" "iccame" "iceast" "immcit" "immshu" "impghg" "imphyd" "impnit" "incosh" "indcen" "indmic" "indwor" "insect" "insula" "intcin" "invcon" "invent" "invgui" "invloa" "ioag" "iomiin" "ironwo" "kelfar" "lagobs" "lakmar" "lancla" "larcon" "lavflo" "lichen" "lighar" "livest" "lohetr" "lunbea" "mafido" "mafige" "mangro" "marrai" "maruni" "mascon" "medarc" "medgro" "medlab" "mefrti" "micmil" "minare" "mindep" "mine" "minexp" "mingui" "minrig" "mirres" "mohare" "moss" "natpre" "nireba" "nirias" "nitmos" "noccit" "nocfar" "nucpow" "nuczon" "olycon" "opecit" "optaer" "orepro" "perext" "perpow" "pets" "phobol" "phspha" "phycom" "planta" "posuco" "powgri" "powinf" "powpla" "predat" "prohab" "proval" "quaext" "rachfa" "radsui" "regeat" "resare" "resear" "resout" "robwor" "roig" "rovcon" "sabota" "satell" "satsys" "secfle" "sefoli" "serero" "shuttl" "smaani" "smaast" "soifac" "solett" "solpow" "sowipo" "spaele" "spamir" "spasta" "spedes" "sponso" "statec" "steelw" "strmin" "subres" "symfun" "tardig" "tecdem" "teract" "tergan" "testpo" "tharep" "thorga" "titmin" "toaco" "tolsta" "trees" "trnepr" "trores" "tunfar" "undcit" "unddet" "unmi" "urbare" "vesshi" "virenh" "virus" "wasppl" "wavpow" "wife" "windmi" "worms" "zeppel"]
                             (fn [_index image-name]
                               [:img {:id image-name
                                      :class "hidden"
                                      :key image-name
                                      :src (str "/tm-images/cats/" image-name ".jpg")}]))

          ]
         [:div.col-6
          [proxier-canvas (reaction (or (:card-index @proxier-state) 0))
           @*anti-forgery-token]]]
        )})))

(defn nav-item [href text]
  [:li.nav-item
   [link href {:class "nav-link"} text]])

(defn navbar [*ws-open?]
  (let [*username (subscribe [:username])
        *navbar-open? (subscribe [:thm.global/navbar-open?])]
    (fn []
      [b/Navbar {:color "dark"  :expand "md"
;:collapse true
                 :dark true}
       [:a.navbar-brand {:href "/"} ""]
       [:div.navbar-toggler-container
        [b/NavbarToggler {:on-click #(dispatch-e [:thm.global/toggle-navbar] %)}]]

       [:div.navbar-text.connection-status-container (connection-status *ws-open?)]

       [b/Collapse {:navbar true :is-open @*navbar-open?}
        [:ul.navbar-nav.mr-auto
         (when @*username
           (nav-item "/home" "Home"))
         (nav-item "/lobby" "Lobby")
         #_(nav-item [link "/news" "News"])]
        [:ul.navbar-nav.ml-auto
         (if-let [username @*username]
           [nav-item "/account" username]
           [nav-item "/login" "Guest"])]]])))

(defn game-slots [game]
  (let [max-players 2
        players (:players game)]
    (concat players (repeat (- max-players (count players)) :empty))))

(defn get-role-for-player-in-game [player-name game]
  (get-in game [:runtime-info :player-name->role player-name]))

(defn short-game-slots [game]
  (let [slots (game-slots game)
        make-slot (fn [slot]
                    (if (= slot :empty)
                      [:i "(empty)"]
                      (if-let [role (get-role-for-player-in-game slot game)]
                        [:span {:title (t/text (:code game) :role role)}
                         (t/text (:code game) :symbol-of-role role) " " slot]
                        [:span slot])))]
    (into [:span] (sequence-for-humans (map make-slot slots)
                                       :omit-and))))

(defn short-game-settings [game]
  (let [descriptions td-options
        options (:game-settings game)
        display-names (map (fn [descr]
                             (let [value (get options (:name descr))]
                               (when-not (or (nil? value)
                                             (= value (:default descr)))
                                 (let [value->display-name (into {} (map (fn [value-d]
                                                                           [(:value value-d) (:display-name value-d)])
                                                                         (:values descr)))]
                                   (value->display-name value)))))
                           descriptions)
        display-names (remove nil? display-names)]
    (when (seq display-names)
      (sequence-for-humans display-names))))

(defn your-turn? [game username]
  (seq-contains? (:current-players (:runtime-info game)) username))

(defn timing-text-of-game [game]
  (t/text (:code game) :timing (:timing (:runtime-info game))))

(defn game-list-item [game username & [options]]
  (let [gid (:gid game)
        with-status? (:with-status? options)]
    [:a.list-group-item.list-group-item-action {:key gid
                                                :href (str "/game/" gid)}
     [:img.logo.float-left {:src (game-logo-src game)}]
     [:div
      [:div
       [:b (short-game-name game)]
       [:span (bull) (:kind game)]
       (when with-status?
         [:span (bull) (:status game)])]
      (when (= (:status game) "running")
        [:div.smaller "(" (timing-text-of-game game) ")"])
      [:div
       (format-timestamp (:created-at game))]]
     (when (your-turn? game username)
       [:div.float-right "Your turn"])
     [:div
      (short-game-slots game)]
     [:div
      (short-game-settings game)]
     (when (:comment game)
       [:div>b (:comment game)])]))

(defn game-list [*games & [options]]
  (let [*username (subscribe [:username])]
    (fn [*games & [options]]
      (if (empty? @*games)
        [:div.alert.alert-info "No games here"]
        [:div.list-group.game-list
         (indexed-component @*games
                            (fn [i [gid game]]
                              (game-list-item game @*username options)))]))))

(defn admin? [username]
  (= username "arry"))

(defn home []
  (let [*active-game-by-id (subscribe [:thm.home/active-game-by-id])
        *past-game-by-id (subscribe [:thm.home/past-game-by-id])
        *username (subscribe [:username])]
    (fn []
      (if @*username
        [:div#home.container
         [:div.row
          [:div.sidebar.col-4
           [:div.avatar-container.my-4
            [your-avatar-img]]
           [:ul
            [:li [link "/lobby" "Lobby"]]
            #_[:li [link "/profile" "Profile"]]
            [:li [link "/account" "Account"]]]
           [:hr]
           [:ul
            [:li [link "/sandbox/td" "13 Days sandbox"]]]
           (when (admin? @*username)
             [:ul
              [:li [link "/marker" "Marker"]]
              [:li [link "/anim" "Animation demo"]]
              [:li [link "/test-term" "Test terminal"]]
              [:li [link "/test-proxier" "Test proxier"]]
              [:li [link "/test-ui/td" "Test td UI"]]
              [:li [link "/test-ui/viti" "Test viti UI"]]
              [:li [link "/arbo" "Arboretum fun times"]]])]
          [:div.col-8
           [:h2 "Active games"]
           [game-list *active-game-by-id {:with-status? true}]
           [:h2 "Past games"]
           [game-list *past-game-by-id {:with-status? true}]]]]
        [not-found]))))

(def all-options {"td" td-options
                  "viti" []})

(defn create-game-form []
  (let [*kind-and-game-settings (subscribe [:thm.lobby/game-settings-on-last-creation])
        *loading? (subscribe [:loading?])
        default-options (merge {:comment ""
                                :kind "live"
                                :code "td"}
                               (into {}
                                     (map (fn [opt]
                                            [(:name opt) (:default opt)])
                                          (get all-options "td")))
                               )
        data (r/atom nil)
        on-submit (fn [e]
                    (dispatch [:thm.lobby/submit-new-game-form @data])
                    (.preventDefault e))
        error (subscribe [:thm.lobby/create-game-error])]
    (fn []
      ;; XXX dirty hacks to beat reframe into submission.
      (when (and (nil? @data) @*kind-and-game-settings)
        (swap! data merge default-options (:td @*kind-and-game-settings)))
      [:div
       [:form.create-game-form {:auto-complete "off"}
        [:div.form-group.row
         [:div.col
          [:div.btn-group.btn-group-toggle.btn-group-lg
           [radio-btn :code "td" data
            [:span [:img.logo {:src (game-logo-src {:code "td"})}] "  13 Days"]]
           #_[radio-btn :code "viti" data
            [:span [:img.logo {:src (game-logo-src {:code "viti"})}] "  Viticulture"]]]]]

        [:div.form-group.row
         [:label.col-4.col-form-label
          "Kind: "]
         [:div.col-4
          [:div.btn-group.btn-group-toggle
           [radio-btn :kind "live" data "live"]
           [radio-btn :kind "async" data "async"]
           ]]]

        (input-group "Comment:" :text :comment {:max-length (:max-comment-length VALIDATION-CONSTS)
                                                :on-change (bind data :comment)})
        (doall (for [opt (get all-options (:code @data))]
                 (let [opt-name (:name opt)]
                   [:div.form-group.row {:key opt-name}
                    [:label.col-4 {:for opt-name} (:display-name opt)]
                    [:div.col
                     (case (:type opt)
                       :selection
                       [:select {:name opt-name,
                                 :default-value (or (get (:td @*kind-and-game-settings) opt-name)
                                                    (get default-options opt-name))
                                 :on-change (bind data opt-name)}
                        (for [val (:values opt)]
                          [:option {:key (val :value), :value (val :value)}
                           (val :choice-display-name)])])]])))
        [:p [loader-when-loading]]
        (when @error
          [:div.alert.alert-danger @error])
        [:div.form-group.row
         [:button.btn.btn-primary {:type "submit"
                                   :disabled @*loading?
                                   :on-click on-submit} "Create game"]]]])))

(defn people []
  (let [*usernames (subscribe [:usernames])]
    (fn []
      [behind-page-loader
       [:ul.people
        (indexed-component @*usernames
                           (fn [i username]
                             [:li {:key i} username]))]])))

(defn notice []
  [:div
   [:div.alert.alert-dark
    [outlink "https://chantry-games.com" "The new version is here."]]])

(defn lobby []
  (let [*open-games (subscribe [:thm.lobby/open-game-by-id])
        *running-games (subscribe [:thm.lobby/running-game-by-id])
        *active-tab (subscribe [:thm.lobby/active-tab])
        *username (subscribe [:username])]
    (fn []
      [:div#lobby.container
       [:div.row.my-4
        [:div.col-5.games-column
         [:h3 "Games"]
         [tabbed-component *active-tab :thm.lobby/activate-tab
          [["join"
            (if @*username "Join" "Open")
            [behind-page-loader
             [game-list *open-games]]]
           (when @*username
             ["create"
              "Create"
              [behind-page-loader
               [create-game-form]]])
           ["running"
            (if @*username "Observe" "Running")
            [behind-page-loader
             [game-list *running-games]]]]]]
        [:div.col-5
         [notice]
         [:h3 "Chat"]
         [:div.chat-container
          [chat]]]
        [:div.col-2
         [:h3 "People"]
         [people]]]])))

(defn game-control-btn [controls kind control text *loading?]
  (if (control controls)
    [(if (= kind :left)
       :button.btn.mx-2.my-2.btn-primary.float-left
       :button.btn.mx-2.my-2.btn-danger.float-right)
     {:disabled @*loading?
      :on-click #(dispatch [:thm.view-game/game-control control])}
     text]))

(defn view-game [gid]
  (let [*game (subscribe [:thm.view-game/game])
        *controls (subscribe [:thm.view-game/controls])
        *username (subscribe [:username])
        *loading? (subscribe [:loading?])]
    (fn [gid]
      (let [game @*game
            controls @*controls]
        (if game
          [:div#view-game.container
           [:div.row.my-4
            [:div.col-5
             [behind-page-loader
              [:div
               [:img.logo.float-left {:src (game-logo-src game)}]
               [:h3 (short-game-name game)]
               ;; TODO display created-at ?
               [:p "The game is " [:b (:status game)]
                (when (= (:status game) "running")
                  [:span.smaller " (" (timing-text-of-game game) ")"])
                "."]
               (when (your-turn? game @*username)
                 [:p "It's your turn."])
               (when (#{"finished" "abandoned"} (:status game))
                 [:p (past-outcome-for-humans (:code game) (:outcome game))])
               (when-not (str/blank? (:comment game))
                 [:div
                  [:p>b (:comment game)]])
               (when-let [opt (short-game-settings game)]
                 [:p opt "."])
               [:h4 "Players"]
               [:div.list-group
                (indexed-component (game-slots game)
                                   (fn [i slot]
                                     (if (= slot :empty)
                                       [:div.list-group-item {:key i}
                                        [:i "empty"]]
                                       (let [role (get-role-for-player-in-game slot game)]
                                         [:div.list-group-item {:key i}
                                          [avatar-img slot]
                                          [:div
                                           (when role
                                             [:span (t/text (:code game) :symbol-of-role role) " "])
                                           [:b slot] " (a nice user)"]
                                          [:div
                                           (when role
                                             [:span "as " (t/text (:code game) :role role)])]
                                          [:div.clearfix]]))))]]]]
            [:div.col-5
             [:h3 "Game controls"]
             [:div.game-controls
              (case (:status game)
                "open" [:div
                        (game-control-btn controls :left :join "Join" *loading?)
                        (game-control-btn controls :right :part "Part" *loading?)
                        (game-control-btn controls :left :start "Start" *loading?)
                        (game-control-btn controls :right :cancel "Cancel" *loading?)]
                ("running" "finished" "abandoned" "voided") [:div
                                                             [:a.btn.btn-primary.float-right {:href (str "/play/" gid)}
                                                              (if (= (:status game) "running")
                                                                "Go to running game"
                                                                "View final situation")]]
                [:div])
              [loader-when-loading]
              [:div.clearfix (html-entity "nbsp")]
              (when @*username [:a.btn.mx-2.btn-light {:href "/home"} "To home"])
              [:a.btn.mx-2.btn-light {:href "/lobby"} "To lobby"]]
             [:div.clearfix (html-entity "nbsp")]
             [:h3 "Chat"]
             [:div.chat-container
              [chat]]]
            [:div.col-2
             [:h3 "People"]
             [people]]]]
          [not-found])))))

(defn contact-n-credits []
  [:div#contact-n-credits.container
   [:h1 "Contact"]
   [:p "If you want to report a bug or suggest a new feature, please use the "
    [outlink "https://bitbucket.org/arry/thm/issues" "issue tracker on
    Bitbucket"] "."]
   [:p "Do you like the site and want to thank the author with beer money? "
    [outlink "https://www.patreon.com/arry_maker_of_chantry" "Support Chantry on Patreon"] "."]
   [:p "Want to talk about anything else?  Please write me an email: deadhh
    gmail com."]
   [:h1 "Credits"]
   [:p "Site by Alexander Rymasheusky.  "
    [outlink "https://bitbucket.org/arry/thm" "Open-source"]
    " under the " [outlink "https://opensource.org/licenses/MIT" "MIT
    license"] "."]
   [:p "Built with " [outlink "http://clojure.org" "Clojure"] " and "
    [outlink "http://getbootstrap.com" "Bootstrap"] "."]
   [:p "Robots lovingly delivered by "
    [outlink "http://robohash.org" "Robohash.org"]
    " (" [outlink "https://creativecommons.org/licenses/by/2.0/" "CC-BY"] ")."]
   [:p "The external link icon is taken from "
    [outlink "https://www.iconfinder.com/icons/298812/external_link_icon#size=128" "here"]
    " under the conditions of the MIT license."]
   [:p "Entypo pictograms by Daniel Bruce — " [outlink "http://www.entypo.com" "www.entypo.com"]]
   [:p [:b "13 Days"] " is a game by Daniel Skjold Pedersen & Asger Sams Granerud, published by "
    [outlink "http://www.jollyrogergames.com/13-days" "Jolly Roger Games"] " and "
    [outlink "http://www.ultrapro.com/product_info.php?products_id=4170" "Ultra Pro"]
    ".  Online adaptation is released with permission."]])

(defn app []
  (let [*page-desc (subscribe [:page-desc])
        *ws-open? (subscribe [:ws-open?])
        *game (subscribe [:thm.play-game/game])]
    ;; TODO allow certain pages only for logged-in or admin.
    (fn []
      (let [[page-name & page-args] @*page-desc]
        (case page-name
          "marker" [marker marker/marker-state]
          "arbo" [arbo]
          "anim" [demo.anim/main]
          "play" (if (some? @*game)
                   [game-ui/ui :play (:code @*game)]
                   [loader])
          "test-term" [test-term]
          "test-proxier" [test-proxier]
          "test-ui" [game-ui/ui :test (get-game-code-from-page-arg (first page-args))]
          "sandbox" [game-ui/ui :try (get-game-code-from-page-arg (first page-args))]
          [:div.wrap
           [:div.content
            [navbar *ws-open?]
            (case page-name
              "/" [landing]
              "login" [login]
              "account" [account]
              "signup" [signup]
              "confirm" [confirm-registration (first page-args)]
              "forgot" [forgot]
              "reset" [reset-password (first page-args)]
              "home" [home]
              "lobby" [lobby]
              "game" [view-game (first page-args)]
              "contact-n-credits" [contact-n-credits]
              [not-found])]
           [:footer
            [:div "Made by arry with lots of love"
             (bull)
             [link "/contact-n-credits" "Contact & Credits"]]]])))))
