(ns thm.td.td-engine
  "Engine for 13 days."
  (:use [thm.td.td-data])
  (:require
   [thm.game-engine.game-base :refer [defaction-coded def-answer-validator
                          has-action? perform-sequence
                          perform-action set-vars text do-nothing
                          all-playing-roles++
                          register-game-definition]]
   [thm.game-engine.game-engine :as game-engine]
   [taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug)]
   [thm.utils :refer [map-values map-cross find-first remove-first shuffle-with-random
                      pick-with-random
                      state-version-is-at-least?
                      one-dim-map two-dim-map first-value denumber-id
                      half-round-up
                      all-cards-in-hand player-name-by-role prepare-request
                      multipart-name-from-descr all-cards-in-zone
                      finished? get-request-for-role
                      has-zone?
                      get-var prepare-zones
                      only-answer only-role&answer
                      in-spread?
                      make-rnd-from-options
                      prepare-player-name->role-for-2p
                      ]]
   [thm.td.td-utils :refer [other-role
                            within-allowed-command-spread?
                            get-defcon-area
                            good-answer-for-choose-place-influence?
                            good-answer-for-choose-remove-influence?
                            good-answer-for-choose-change-defcon?
                            good-for-war?
                            has-effect? contained?
                            state-timing
                            get-war-reason
                            ]]
   ))

(def GAME-CODE "td")

(def VERSION 3)

(defn new-game [player-names & [opts]]
  (assert (= (count player-names) 2) "Must have exactly two players")
  (let [rnd (make-rnd-from-options opts)
        player-name->role (prepare-player-name->role-for-2p
                           opts player-names ["owner-u" "owner-s"] ["u" "s"] rnd)
        role->player-name (clojure.set/map-invert player-name->role)
        zones (prepare-zones zones-data both-roles)]
    {:code GAME-CODE
     :version VERSION
     :ordered-roles both-roles
     :player-name->role player-name->role
     :role->player-name role->player-name
     :vars (into (sorted-map) (merge {"prestige" 0
                                      "round" 1
                                      "phase" nil
                                      "initiative-player" nil
                                      "active-player" nil
                                      "turn" nil}
                                     (one-dim-map "c:" both-roles starting-cubes)
                                     (two-dim-map "i:" both-roles (keys battlegrounds) 0)
                                     (two-dim-map "f:" both-roles (range 3) nil)
                                     (one-dim-map "ef:" both-roles [])
                                     initial-defcon-values
                                     initial-influence-values))
     :zones (-> zones
                (assoc-in ["strategy-deck" :cards] (vec (shuffle-with-random all-strategy-cids rnd)))
                (assoc-in ["u:letter" :cards] [personal-letter-cid])
                (assoc-in ["agenda-deck" :cards] (vec all-agenda-cids)))
     :pending-actions [[:text :game-starts (role->player-name "u") (role->player-name "s")]
                       :do-round]
     :rnd rnd}))

;; === Helper functions

(defn get-influence-var-name [role battleground]
  (str "i:" role ":" battleground))

(defn get-cubes-var-name [role]
  (str "c:" role))

(defn get-defcon-track-var-name [role track]
  (str "d:" role ":" track))

(defn get-flag-name-var [role flag-index]
  (str "f:" role ":" flag-index))

(defn get-hand-zone-name [role]
  (str role ":hand"))

(defn get-agenda-zone-name [role]
  (str role ":agenda"))

(def-answer-validator GAME-CODE :choose-command
  [state role [kind text-descr & args :as req] answer]
  (let [[cubes] args]
    (and (sequential? answer)
         (= (count answer) 2)
         (contains? battlegrounds (first answer))
         (integer? (second answer))
         (within-allowed-command-spread? state role (first answer) cubes (second answer)
                                         :normal))))

(def-answer-validator GAME-CODE :choose-command-onto
  [state role [kind text-descr & args :as req] answer]
  (let [[cubes bgs] args]
    (and (sequential? answer)
         (= (count answer) 2)
         (contains? bgs (first answer))
         (>= (second answer) 0)
         (within-allowed-command-spread? state role (first answer) cubes (second answer)
                                         :onto))))

(def-answer-validator GAME-CODE :choose-place-influence
  [state role [kind text-descr & args :as req] answer]
  (and (associative? answer)
       (good-answer-for-choose-place-influence? state role args answer)))

(def-answer-validator GAME-CODE :choose-remove-influence
  [state role [kind text-descr & args :as req] answer]
  (and (associative? answer)
       (good-answer-for-choose-remove-influence? state role args answer)))

(def-answer-validator GAME-CODE :choose-change-defcon
  [state role [kind text-descr & args :as req] answer]
  (and (associative? answer)
       (good-answer-for-choose-change-defcon? state role args answer)))

(defn get-defcon-change-from-command [change]
  (cond
    (neg? change) (inc change)
    (zero? change) 0
    (pos? change) (dec change)))

(defn get-defcon-track-corresponding-to-bg [bg]
  (:track (battlegrounds bg)))

(defn with-pl? [card-play-mode]
  (or (= card-play-mode "command+pl")
      (= card-play-mode "opp-command+pl")))

(defn event-precondition-met? [state r cid]
  ;; XXX it's bad to separate card text into this function and the card text
  ;; proper, but it's the most direct way and only three cards.
  (case cid
    "offmis" (= (get-defcon-area state "u" "p") 3)
    "eytoey" (> (get-var state "d:u:m")
                (get-var state "d:s:m"))
    "maskir" (= (get-defcon-area state "s" "m") 3)
    true))

(defn cubes-for-command-action [state r cubes & [with-letter?]]
  (let [modifiers (cond-> 0
                    (has-effect? state r "tothbr") dec
                    (has-effect? state r "sops") inc
                    with-letter? inc)]
    (max 1 (+ cubes modifiers))))

(defn dominator&difference [state var-f]
  (let [u (get-var state (var-f "u"))
        s (get-var state (var-f "s"))]
    (cond
      (> s u) ["s" (- s u)]
      (> u s) ["u" (- u s)]
      :else [nil 0])))

(defn get-dominating-player [state bg]
  (first (dominator&difference state (fn [r] ["i" r bg]))))

(defn set-of-defcon-tracks []
  (set (keys defcon-tracks)))

(defn get-tracks-at-defcon-2 [state letter]
  (let [affected-roles (filter #(= 2 (get-defcon-area state % letter)) both-roles)]
    (map (fn [r] [r letter]) affected-roles)))

(defn who-has-personal-letter [state]
  (if (seq (all-cards-in-zone state "u:letter"))
    "u"
    "s"))

(defn count-dominated-neighbors [state role neighbors]
  (when role
    (count (filter (fn [bg]
                     (= (get-dominating-player state bg)
                        role))
                   neighbors))))

(defn prestige-sign [role]
  ({"u" -1, "s" 1} role))

(defn bgs-set-by-val [good-fn]
  (set (keys (filter #(good-fn (val %)) battlegrounds))))

(defn bgs-set-by-key [good-fn]
  (set (keys (filter #(good-fn (key %)) battlegrounds))))

;; === Game actions

(defmacro defaction
  [n & forms]
  `(defaction-coded ~GAME-CODE ~n ~@forms))

(defaction do-round [state]
  (perform-sequence state
                    [:text :round-starts (get-var state "round")]
                    :do-escalate-defcon-phase
                    :do-draw-agendas-phase
                    :do-strategy-cards-phase
                    :do-save-for-aftermath-phase
                    :do-wo-bonus-phase
                    :do-resolve-agendas-phase
                    :do-check-war-phase
                    :do-advance-round-phase
                    ))

(defaction report-defcon-changed [state role track->amount & [options]]
  (let [reported (remove #(zero? (val %)) track->amount)]
    (perform-action state
                    (if (= (count reported) 0)
                      (if (:dont-report-defcon-if-no options)
                        [:do-nothing]
                        [:text :no-defcon-changed role])
                      [:text :defcon-changed role (map (fn [[track amount]]
                                                         [track amount (get-var state ["d" role track])])
                                                       reported)]))))

(defaction change-defcon-tracks [state role track->amount & [options]]
  (apply perform-sequence state
         (concat
          (map (fn [[track amount]]
                 [:change-var-with-limits ["d" role track] amount 0 MAX-DEFCON
                  {:remember-change (:remember-change options)}])
               track->amount)
          (when-let [var-name (and (empty? track->amount)
                                   (:remember-change options))]
            [[:set-var var-name 0]])
          [[:report-defcon-changed role track->amount options]
           [:report-war-danger role]])))

(defaction report-war-danger [state role]
  (let [reason (get-war-reason state role)]
    (perform-sequence state
                      (when reason
                        [:text :war-danger-warning role reason]))))

(defaction escalate-defcon-track [state role track]
  (change-defcon-tracks state role {track 1}))

(defaction do-escalate-defcon-phase [state]
  (apply perform-sequence state
         [:text :phase-starts :escalate-defcon]
         (map (fn [r]
                [:change-defcon-tracks r (zipmap (keys defcon-tracks)
                                                 (repeat 1))])
              both-roles)))

(defaction report-influence-changed [state role bg amount]
  (if (zero? amount)
    (perform-sequence state [:text :no-influence-change role])
    (perform-sequence state [:text :influence-changed role bg amount
                             (get-var state ["i" "u" bg])
                             (get-var state ["i" "s" bg])])))
(defaction change-influence [state role bg amount defcon? & [options]]
  (perform-sequence state
                    [:change-var (get-influence-var-name role bg) amount]
                    [:change-var (get-cubes-var-name role) (- amount)]
                    [:report-influence-changed role bg amount]
                    (when defcon?
                      (let [track (get-defcon-track-corresponding-to-bg bg)
                            defcon-change (get-defcon-change-from-command amount)]
                        [:change-defcon-tracks role {track defcon-change} options]))))

(defaction deal-cards [state deck-name amount]
  (apply perform-sequence state (map (fn [role]
                                       [:draw-cards deck-name (get-hand-zone-name role) amount])
                                     both-roles)))

(defaction update-flags [state]
  (let [mappings (mapcat (fn [r]
                          (map-indexed (fn [flag-index card]
                                         [(get-flag-name-var r flag-index)
                                          (-> card card-design-by-id :letter)])
                                       (all-cards-in-hand state r)))
                         both-roles)]
    (apply perform-sequence state
           (into [:set-vars] mappings)
           (map (fn [r]
                  [:text :has-agenda-options r
                   (map denumber-id (all-cards-in-hand state r))])
                both-roles))))

(defaction select-agendas [state]
  (perform-sequence
   state
   [:put-request (prepare-request
                  both-roles :choose-card
                  :choose-agenda-card
                  (fn [r]
                    {(get-hand-zone-name r) (all-cards-in-hand state r)}))]
   :on-agendas-chosen))

(defaction on-agendas-chosen [state]
  (apply perform-sequence state
         (mapcat (fn [r]
                   [[:move-card (get-hand-zone-name r) (get-agenda-zone-name r)
                     (get-in state [:answers r])]
                    [:move-all-cards-in-zone (get-hand-zone-name r) "agenda-deck"]])
                 both-roles)))

(defaction do-draw-agendas-phase [state]
  (perform-sequence state
                    [:text :phase-starts :draw-agendas]
                    [:set-var "phase" "agenda"]
                    [:shuffle-deck "agenda-deck"]
                    [:deal-cards "agenda-deck" 3]
                    [:update-flags]
                    [:select-agendas]))

(defaction do-strategy-cards-phase [state]
  (perform-sequence state
                    [:text :phase-starts :strategy-cards]
                    [:set-var "phase" "strategy"]
                    [:deal-cards "strategy-deck" 5]
                    [:determine-initiative-player]
                    [:play-strategy-cards]
                    [:pause]
                    [:cleanup-after-strategy-cards]))

(defaction determine-initiative-player [state]
  (let [role (if (pos? (get-var state "prestige")) "u" "s")]
    (perform-sequence state
                      [:text :choosing-initiative-player role]
                      [:put-request {role [:choose-mode :choose-initiative-player both-roles]}]
                      :on-initiative-player-chosen)))

(defaction on-initiative-player-chosen [state]
  (let [answer (only-answer state)]
    (perform-sequence state
                      [:set-var "initiative-player" answer]
                      [:text :initiative-player answer])))

(defaction play-strategy-cards [state]
  (perform-sequence state
                    [:set-vars
                     ["active-player" (get-var state "initiative-player")]
                     ["turn" 1]]
                    [:do-turn]
                    [:advance-strategy-cards-turn]))


(defn get-modes-of-play [state r cid]
  (let [design (card-design-by-id cid)
        alignment (:alignment design)]
    (cond->> (if (or (= alignment r)
                     (= alignment "n"))
               ["command" "command+pl" "event"]
               ["opp-command" "opp-command+pl"])
      (empty? (all-cards-in-zone state [r "letter"]))
      ,(remove #(with-pl? %))

      (and (= cid "utha")
           (contained? state r))
      ,(remove #{"event"})

      (not (event-precondition-met? state r cid))
      ,(remove #{"event"}))))


(defaction do-turn [state]
  (let [r (get-var state "active-player")]
    (if (state-version-is-at-least? state CHOOSE-CARD-AND-MODE-VERSION)
      (perform-sequence state
                        [:text :turn-starts (get-var state "active-player") (get-var state "turn")]
                        [:put-request {r [:choose-card-and-mode :choose-card-to-play
                                          {(get-hand-zone-name r) (all-cards-in-hand state r)}
                                          (apply hash-map (mapcat (fn [cid]
                                                                    [cid (get-modes-of-play state r cid)]
                                                                    ) (all-cards-in-hand state r)))]}]
                        [:on-card-and-mode-chosen r])
      (perform-sequence state
                        [:text :turn-starts (get-var state "active-player") (get-var state "turn")]
                        [:put-request {r [:choose-card :choose-card-to-play
                                          {(get-hand-zone-name r) (all-cards-in-hand state r)}]}]
                        [:on-card-chosen r])
      )))

(defaction on-card-chosen [state r]
  (let [answer (only-answer state)]
    (perform-action state [:play-card r answer])))

(defaction on-card-and-mode-chosen [state r]
  (let [answer (only-answer state)
        [cid mode] answer]
    (if mode
      (perform-action state [:play-card-known-mode r cid mode])
      (perform-action state [:play-card r cid]))))

(defaction play-card [state r cid]
  (let [design (card-design-by-id cid)
        alignment (:alignment design)
        modes (get-modes-of-play state r cid)]
    (perform-sequence state
                      [:put-request {r [:choose-mode
                                        [:choose-mode-of-play (denumber-id cid)]
                                        modes]}]
                      [:on-mode-of-play-chosen cid])))

(defaction on-mode-of-play-chosen [state cid]
  (let [r (get-var state "active-player")
        mode (only-answer state)]
    (perform-sequence state
                      [:text :play-card r (denumber-id cid) mode]
                      (when (with-pl? mode)
                        [:move-card [r "letter"] "in-play" personal-letter-cid])
                      [:move-card [r "hand"] "in-play" cid]
                      [:play-card-with-mode r cid mode])))

(defaction play-card-known-mode [state r cid mode]
  (perform-sequence state
                    [:text :play-card r (denumber-id cid) mode]
                    (when (with-pl? mode)
                      [:move-card [r "letter"] "in-play" personal-letter-cid])
                    [:move-card [r "hand"] "in-play" cid]
                    [:play-card-with-mode r cid mode]))

(defaction play-card-with-mode [state r cid mode]
  (let [design (card-design-by-id cid)
        cubes (:cubes design)]
    (apply perform-sequence state
           (conj (case mode
                   "command" [[:do-command r cubes]]
                   "command+pl" [[:do-command r cubes {:with-letter? true}]]
                   "event" [[:do-event r cid]]
                   "opp-command" [[:ask-event (other-role r) cid]
                                  [:do-command r cubes]]
                   "opp-command+pl" [[:ask-event (other-role r) cid]
                                     [:do-command r cubes {:with-letter? true}]])
                 [:dispose-of-played-cards r cid mode]))))

(defaction do-command [state r cubes & [options]]
  (let [mod-cubes (cubes-for-command-action state r cubes (:with-letter? options))]
    (perform-sequence state
                      [:put-request {r [:choose-command [:choose-command-t mod-cubes] mod-cubes]}]
                      [:on-command-chosen r])))

(defaction do-command-onto [state r cubes bgs]
  (let [mod-cubes (cubes-for-command-action state r cubes)]
    (perform-sequence state
                      [:put-request {r [:choose-command-onto [:choose-command-onto-t mod-cubes] mod-cubes bgs]}]
                      [:on-command-chosen r])))

(defaction on-command-chosen [state r]
  (let [answer (only-answer state)
        [bg change] answer]
    (perform-sequence state
                      [:change-influence r bg change true {:dont-report-defcon-if-no true}])))

(defaction do-event [state r cid]
  (perform-sequence state
                    [:text :resolving-event r (denumber-id cid)]
                    (when (has-action? state cid)
                      [cid r])))

(defaction ask-event [state r cid]
  (if (event-precondition-met? state r cid)
    (perform-sequence state
                      [:put-request {r [:yn [:want-event? (denumber-id cid)] nil]}]
                      [:on-ask-event-answered cid])
    (perform-sequence state
                      [:text :precondition-not-met (denumber-id cid)])))

(defaction on-ask-event-answered [state cid]
  (let [[r answer] (only-role&answer state)]
    (if answer
      (perform-sequence state
                        [:do-event r cid])
      (text state :skip-event r))))

(defaction dispose-of-played-cards [state r cid mode]
  (perform-sequence state
                    (when (with-pl? mode)
                      [:move-card "in-play" [(other-role r) "letter"] personal-letter-cid])
                    [:move-card "in-play" "strategy-discard" cid]))

(defaction cleanup-after-strategy-cards [state]
  (perform-sequence state
                    [:set-vars
                     ["initiative-player" nil]
                     ["ef:u" []]
                     ["ef:s" []]]))

(defaction advance-strategy-cards-turn [state]
  (let [ap (get-var state "active-player")
        ip (get-var state "initiative-player")]
    (apply perform-sequence state
           (cond
             (= ap ip)
             [[:set-var "active-player" (other-role ap)]
              [:do-turn]
              [:advance-strategy-cards-turn]]
             ,
             (< (get-var state "turn") 4)
             [[:set-vars
               ["active-player" (other-role ap)]
               ["turn" (inc (get-var state "turn"))]]
              [:do-turn]
              [:advance-strategy-cards-turn]]
             ,
             :else
             [[:set-vars
               ["active-player" nil]
               ["turn" nil]]]))))

(defaction do-save-for-aftermath-phase [state]
  (apply perform-sequence state
         [:text :phase-starts :save-for-aftermath]
         (map (fn [r]
                (if (state-version-is-at-least? state SEPARATE-AFTERMATH-VERSION)
                  [:move-all-cards-in-zone (get-hand-zone-name r) [r "aftermath"]]
                  [:move-all-cards-in-zone (get-hand-zone-name r) "aftermath"]))
              both-roles)))

(defaction do-wo-bonus-phase [state]
  (perform-sequence state
                    [:text :phase-starts :wo-bonus]
                    [:set-var "phase" "wo-bonus"]
                    [:wo-bonus "v" :tv-wo-bonus]
                    [:wo-bonus "u" :un-wo-bonus]
                    [:wo-bonus "l" :alliances-wo-bonus]))

(defaction wo-bonus [state bg action-code]
  (let [r (get-dominating-player state bg)]
    (if r
      (perform-action state [action-code r])
      (perform-action state [:text :no-wo-bonus bg]))))

(defaction tv-wo-bonus [state r]
  (perform-sequence state
                    [:text :tv-wo-bonus r]
                    [:choose-change-defcon r 1]))

(defaction un-wo-bonus [state r]
  (perform-sequence state
                    [:text :un-wo-bonus r]
                    [:move-all-cards-in-zone [(other-role r) "letter"] [r "letter"]]))

(defaction alliances-wo-bonus [state r]
  (perform-sequence state
                    [:text :alliances-wo-bonus r]
                    [:draw-cards "strategy-deck" (get-hand-zone-name r) 1]
                    [:aftermath-or-discard-card r]))

(defaction aftermath-or-discard-card [state r]
  (let [cid (first (all-cards-in-zone state (get-hand-zone-name r)))]
    (perform-sequence state
                      [:put-request {r [:choose-mode
                                        [:aftermath-or-discard (denumber-id cid)]
                                        ["aftermath" "discard"]]}]
                      [:on-aftermath-or-discard-chosen cid])))

(defaction on-aftermath-or-discard-chosen [state cid]
  (let [[r answer] (only-role&answer state)
        [dest text] (if (= answer "aftermath")
                      [(if (state-version-is-at-least? state SEPARATE-AFTERMATH-VERSION)
                         [r "aftermath"] "aftermath")
                       [:text :adds-to-aftermath r]]
                      ["strategy-discard"
                       [:text :discards r (denumber-id cid)]])]
    (perform-sequence state
                      [:move-all-cards-in-zone (get-hand-zone-name r) dest]
                      text)))

(defaction do-resolve-agendas-phase [state]
  (apply perform-sequence state
         [:text :phase-starts :resolve-agendas]
         [:set-var "phase" "resolve-agendas"]
         (concat
          [[:report-revealed-agendas]]
          (map (fn [r]
                 [:move-all-cards-in-zone (get-agenda-zone-name r) "in-play"])
               both-roles)
          [[:escalate-defcon-by-agendas]
           [:award-prestige-by-agendas]
           [:move-all-cards-in-zone "in-play" "agenda-discard"]])))

(defaction report-revealed-agendas [state]
  (apply perform-sequence state
         (map (fn [r]
                [:text :player-reveals-agenda r
                 (-> (all-cards-in-zone state (get-agenda-zone-name r))
                     first
                     denumber-id)])
              both-roles)))

(defaction escalate-defcon-by-agendas [state]
  (let [affected-tracks (mapcat (fn [cid]
                                  (let [{:keys [defcon letter]} (card-design-by-id cid)]
                                    (when defcon
                                      (get-tracks-at-defcon-2 state letter))))
                                (all-cards-in-zone state "in-play"))
        actions (map (fn [[r letter]]
                       [:change-defcon-tracks r {letter 1}])
                     affected-tracks)]
  (apply perform-sequence state actions)))

(defaction report-prestige-changed [state amount]
  (if (= amount 0)
    (do-nothing state)
    (perform-action state [:text :prestige-changed amount (get-var state "prestige")])))

(defaction change-prestige [state amount]
  (perform-sequence state
                    [:change-var-with-limits "prestige" amount -5 5]
                    [:report-prestige-changed amount]))

(defaction award-prestige-by-agendas [state]
  (let [resolutions (map
                     (fn [cid]
                       (let [{:keys [letter type bonus neighbors]} (card-design-by-id cid)
                             [winner base-award] (case type
                                                   "bg" (dominator&difference state (fn [r]
                                                                                      ["i" r letter]))
                                                   "track" (dominator&difference state (fn [r]
                                                                                         ["d" r letter]))
                                                   "card" [(who-has-personal-letter state) 0])
                             total-bonus (if (= bonus :x)
                                           (count-dominated-neighbors state winner neighbors)
                                           bonus)]
                         ;; TODO include role of player who played this agenda
                         (if winner
                           {:cid (denumber-id cid)
                            :winner winner
                            :base-award base-award
                            :total-bonus total-bonus
                            :total (* (+ base-award total-bonus)
                                      (prestige-sign winner))}
                           {:cid (denumber-id cid)
                            :total 0})))
                     (all-cards-in-zone state "in-play"))
        total-prestige-change (reduce + (map :total resolutions))
        actions (into (mapv (fn [resolution]
                             [:text :resolve-agenda resolution])
                            resolutions)
                      [[:change-prestige total-prestige-change]
                       [:pause]])]
    (apply perform-sequence state actions)))

(defaction do-check-war-phase [state]
  (let [u (good-for-war? state "u")
        s (good-for-war? state "s")
        action (cond
                 (and u s) [:finish-game "tie" "both-war"]
                 u         [:finish-game "win" "war" "s"]
                 s         [:finish-game "win" "war" "u"]
                 :else     [:text :no-war])]
    (perform-sequence state
                      [:text :phase-starts :check-war]
                      action)))


(defaction do-advance-round-phase [state]
  (let [cur (get-var state "round")]
    (if (< cur 3)
      (perform-sequence state
                        [:change-var "round" 1]
                        [:do-round])
      (perform-sequence state
                        [:set-var "round" "a"]
                        [:do-aftermath-phase]
                        [:determine-winner]))))

(defaction do-aftermath-phase [state]
  (perform-sequence state
                    [:text :aftermath-starts]
                    [:set-var "phase" "aftermath"]
                    (if (state-version-is-at-least? state SEPARATE-AFTERMATH-VERSION)
                      (into [:action-sequence]
                            (map (fn [r]
                                   [:move-all-cards-in-zone [r "aftermath"] "aftermath-reveal"])
                                 both-roles))
                      [:move-all-cards-in-zone "aftermath" "aftermath-reveal"])
                    [:reveal-zone "aftermath-reveal"]
                    [:count-aftermath]))

(defaction count-aftermath [state]
  (let [cards (map card-design-by-id (all-cards-in-zone state "aftermath-reveal"))
        by-alignment (group-by :alignment cards)
        s (reduce + (map :cubes (by-alignment "s")))
        u (reduce + (map :cubes (by-alignment "u")))
        dominator (cond
                    (> s u) "s"
                    (> u s) "u"
                    :else nil)]
    (perform-sequence state
                      [:text :count-aftermath {:dominator dominator
                                               :s-cubes s
                                               :u-cubes u}]
                      (when dominator
                        [:change-prestige (* 2 (prestige-sign dominator))]))))

(defaction determine-winner [state]
  (let [prestige (get-var state "prestige")
        [submode winner] (cond (neg? prestige) ["more-prestige" "u"]
                               (pos? prestige) ["more-prestige" "s"]
                               :else ["has-letter" (who-has-personal-letter state)])]
    (perform-action state [:finish-game "win" submode winner])))

(defaction add-effect [state role effect]
  (let [old (get-var state ["ef" role])
        new (conj old effect)]
    (perform-sequence state
                      [:set-var ["ef" role] new]
                      [:text :effect-started role effect])))

(defaction choose-place-influence [state r max max-per defcon? bgs & [options]]
  (let [exact-amount (if (:exact-amount? options)
                       :exact-amount
                       nil)]
    (perform-sequence state
                      [:put-request {r [:choose-place-influence
                                        [:choose-place-influence-t max max-per defcon?
                                         exact-amount]
                                        max max-per defcon? bgs exact-amount]}]
                      [:on-choose-place-answered r defcon?])))

(defaction on-choose-place-answered [state r defcon?]
  (let [bg->amount (only-answer state)]
    (if (seq bg->amount)
      (apply perform-sequence state (map (fn [[bg amount]]
                                           [:change-influence r bg amount defcon? {:dont-report-defcon-if-no true}])
                                         bg->amount))
      (perform-action state [:text :no-cubes-placed r]))))

(defaction choose-remove-influence [state r whose max policy defcon? bgs & [options]]
  (perform-sequence state
                    [:put-request {r [:choose-remove-influence
                                      [:choose-remove-influence-t whose max policy defcon?]
                                      whose max policy defcon?
                                      (set (filter #(pos? (get-var state ["i" whose %])) bgs))]}]
                    [:on-choose-remove-answered r whose policy defcon? options]))

(defaction on-choose-remove-answered [state r whose policy defcon? & [options]]
  (let [bg->amount (only-answer state)]
    (apply perform-sequence state
           (concat
            (if (seq bg->amount)
              (map (fn [[bg amount]]
                     (let [change (case policy
                                    :half (- (half-round-up (get-var state ["i" whose bg])))
                                    (- amount))]
                       ;; TODO report role and whose separately, so that text can be rendered as
                       ;; "USA removes 3 USSR's cubes..."
                       [:change-influence whose bg change defcon? {:dont-report-defcon-if-no true}]))
                   bg->amount)
              [[:text :no-cubes-removed r]])
            (when-let [var-name (:remember-count options)]
              [[:set-var var-name (reduce + (vals bg->amount))]])))))

(defaction choose-change-defcon [state r bound & [options]]
  (let [{:keys [whose max-tracks allowed-tracks]
         :or {whose r, max-tracks 1, allowed-tracks (set-of-defcon-tracks)}} options
        lower (if (or (contained? state r whose)
                      (= (:mode options) :escalate-only))
                0
                (- bound))
        higher (if (= (:mode options) :deflate-only)
                 0
                 bound)]
    (perform-sequence state
                      [:put-request {r [:choose-change-defcon
                                        [:choose-change-defcon-t whose max-tracks allowed-tracks]
                                        whose max-tracks allowed-tracks lower higher
                                        (if (:exact-amount? options) :exact-amount nil)]}]
                      [:on-choose-change-defcon-chosen whose options])))

(defaction on-choose-change-defcon-chosen [state whose options]
  (let [track->amount (only-answer state)]
    (change-defcon-tracks state whose track->amount {:remember-change (:remember-change options)})))

(defaction discard-card [state role cid]
  (perform-sequence state
                    [:text :discards role (denumber-id cid)]
                    [:move-card [role "hand"] "strategy-discard" cid]))

(defaction choose-mode [state role text-descr mode-action-pairs]
  (perform-sequence state
                    [:put-request {role [:choose-mode
                                         text-descr
                                         (mapv first mode-action-pairs)]}]
                    [:on-mode-chosen mode-action-pairs]))

(defaction on-mode-chosen [state mode-action-pairs]
  (let [[r answer] (only-role&answer state)
        action (some #(and (= answer (first %))
                           (second %)) mode-action-pairs)]
    (perform-action state action)))

;; === Event game actions

(defaction tothbr [state role]
  (perform-action state [:add-effect (other-role role) "tothbr"]))

(defaction sops [state role]
  (perform-action state [:add-effect role "sops"]))

(defaction sttn [state role]
  (perform-action state [:choose-place-influence
                         role 3 2 false
                         (bgs-set-by-val #(= "w" (:track %)))]))

(defaction fidcas [state role]
  (perform-action state [:choose-place-influence
                         role 3 nil false
                         #{"c" "k"}]))

(defaction cloall [state role]
  (perform-action state [:choose-place-influence
                         role 2 nil false
                         (bgs-set-by-val #(= "p" (:track %)))]))

(defaction nucsub [state role]
  (perform-action state [:choose-place-influence
                         role 2 nil false
                         (bgs-set-by-val #(= "m" (:track %)))]))

(defaction scramb [state role]
  (perform-action state [:choose-place-influence
                         role 3 1 false
                         (bgs-set-by-val identity)]))

(defaction excomm [state role]
  (perform-action state [:choose-place-influence
                         role 4 2 true
                         (bgs-set-by-key (fn [bg]
                                           (zero? (get-var state ["i" role bg]))))]))

(defaction leofmu [state role]
  (perform-action state [:choose-place-influence
                         role 4 2 false
                         #{"b" "i" "t"}]))

(defaction quaran [state role]
  (perform-action state [:choose-place-influence
                         role 2 nil false
                         #{"a"}]))

(defaction miob [state role]
  (perform-action state [:choose-place-influence
                         role 4 2 true
                         (bgs-set-by-key (fn [bg]
                                           (not (zero? (get-var state ["i" role bg])))))]))

(defaction strbal [state role]
  (perform-action state [:choose-place-influence
                         role 3 nil true
                         #{"a"}]))

(defaction defmis [state role]
  (perform-action state [:choose-place-influence
                         role 2 nil true
                         #{"u" "v"}]))

(defaction tgoa [state role]
  (perform-sequence state
                    [:choose-change-defcon role 2]
                    [:do-command role 1]))

(defaction intrep [state role]
  (let [opp (other-role role)
        cid (pick-with-random (all-cards-in-zone state [opp "hand"]) (:rnd state))]
    (perform-sequence state
                      [:move-card [opp "hand"] [role "hand"] cid]
                      [:text :intrep-got (denumber-id cid)]
                      [:put-request {role [:choose-mode
                                           [:intrep-play-or-discard (denumber-id cid)]
                                           ["play" "discard"]]}]
                      [:intrep-on-play-or-discard-chosen cid]
                      [:draw-cards "strategy-deck" [opp "hand"] 1])))

(defaction intrep-on-play-or-discard-chosen [state cid]
  (let [[role answer] (only-role&answer state)]
    (perform-action state
                    (if (= answer "play")
                      [:play-card role cid]
                      [:discard-card role cid]))))

(defaction summee [state role]
  (perform-sequence state
                    [:put-request {role [:choose-cards
                                         [:summee-choose-cards-to-discard]
                                         (multipart-name-from-descr [role "hand"])
                                         (all-cards-in-zone state [role "hand"])]}]
                    [:summee-on-cards-chosen]))

(defaction summee-on-cards-chosen [state]
  (let [[role cids] (only-role&answer state)]
    (if (empty? cids)
      (perform-action state [:text :summee-no-cards-discarded role])
      (perform-sequence state
                        [:move-cards [role "hand"] "strategy-discard" cids]
                        [:text :discards-many role (map denumber-id cids)]
                        [:draw-cards "strategy-deck" [role "hand"] (count cids)]))))

(defaction afasa [state role]
  (perform-sequence state
                    [:do-command role 3]
                    [:do-command (other-role role) 1]))

(defaction fiffif [state role]
  (let [[dom-role _] (dominator&difference state #(identity ["i" % "v"]))]
    (perform-action state
                    (if dom-role
                      [:choose-change-defcon dom-role 1 {:max-tracks 2}]
                      [:text :fiffif-no-dominator]))))

(defaction contai [state role]
  (perform-action state [:add-effect (other-role role) "contai"]))

(defaction utha [state role]
  (if (contained? state role)
    (do-nothing state)
    (change-defcon-tracks state role (zipmap (keys defcon-tracks)
                                             (repeat -1)))))

(defaction matpre [state role]
  (perform-sequence state
                    [:choose-change-defcon role 2 {:allowed-tracks #{"p"}}]
                    [:do-command role 1]))

(defaction pubpro [state role]
  (perform-sequence state
                    [:choose-remove-influence role "u" 5 :single-bg false (set (keys battlegrounds))]))

(defaction opemon [state role]
  (perform-sequence state
                    [:change-prestige -1]
                    [:choose-change-defcon "s" 1 {:whose "u"}]))

(defaction modes-with-prereqs
  "Given triplets of [prereq, mode, action], return a sequence of pairs of [mode,
  action] for which prereq is true."
  [& triplets]
  (map (fn [[_ mode action]]
         [mode action])
       (filter first triplets)))

(defaction airstr [state role]
  (perform-sequence state
                    [:choose-mode role :airstr-remove-or-place
                     (modes-with-prereqs
                      [(some pos? (map #(get-var state ["i" "s" %]) ["c" "k"]))
                       "remove" :airstr-remove]
                      [true "place" :airstr-place])]))

(defaction airstr-remove [state]
  (perform-sequence state
                    [:choose-remove-influence "u" "s" 1 :half false #{"c" "k"}]))

(defaction airstr-place [state]
  (perform-sequence state
                    [:choose-place-influence "u" 2 nil false #{"l"}]))


(defaction noinpl [state role]
  (perform-sequence state
                    [:choose-remove-influence role "s" 2 :single-bg false #{"t"}]
                    [:choose-change-defcon role 2 {:allowed-tracks #{"p"}}]))

(defaction offmis [state role]
  (perform-sequence state
                    (if (event-precondition-met? state role "offmis")
                      [:choose-place-influence
                       role 3 1 false
                       (bgs-set-by-val #(= "p" (:track %)))]
                      [:text :offmis-defcon-not-3])))

(defaction u2ph [state role]
  (perform-sequence state
                    [:do-command-onto role 3 (bgs-set-by-val #(= "m" (:track %)))]))

(defaction natlib [state role]
  (perform-sequence state
                    [:do-command-onto role 3 (bgs-set-by-val #(= "p" (:track %)))]))

(defaction eytoey [state role]
  (perform-sequence state
                    (if (event-precondition-met? state role "eytoey")
                      [:choose-place-influence role 3 nil true #{"c" "k"}]
                      [:text :eytoey-defcon-not-dominated])))

(defaction mrbirb [state role]
  (perform-sequence state
                    [:choose-change-defcon role 2 {:allowed-tracks #{"m"}}]
                    [:do-command role 1]))

(defaction berblo [state role]
  (perform-sequence state
                    [:change-prestige 2]
                    [:choose-change-defcon "u" 2 {:whose "s"}]))

(defaction u2do [state role]
  (perform-sequence state
                    [:choose-place-influence role 2 nil false #{"t"}]
                    [:choose-remove-influence "s" "u" 1 :half false #{"c" "k"}]))

(defaction waansm [state role]
  (perform-sequence state
                    [:choose-remove-influence "u" "u" 2 :many-bgs false
                     (bgs-set-by-val identity)
                     {:remember-count "-waansm-count"}]
                    [:waansm-cont]))

(defaction waansm-cont [state]
  (let [cnt (get-var state "-waansm-count" 0)]
    (perform-sequence state
                      (when (pos? cnt)
                        [:choose-place-influence "u" cnt
                         nil false (bgs-set-by-val identity)
                         {:exact-amount? true}])
                      [:unset-var "-waansm-count"])))

(defaction mistra [state role]
  (perform-sequence state
                    [:choose-remove-influence "s" "s" 3 :many-bgs true
                     (bgs-set-by-val identity)]))

(defaction suehun [state role]
  (let [c (get-var state "c:s")
        u (get-var state "i:u:i")
        s (get-var state "i:s:i")
        target (min MAX-CUBES-IN-BG (inc u))]
    (if (>= s target)
      (do-nothing state)
      (perform-action state
                      [:change-influence "s" "i" (min c (- target s)) false]))))

(defaction maskir [state role]
  (perform-sequence state
                    (if (event-precondition-met? state role "maskir")
                      [:choose-place-influence
                       role 3 1 false
                       (bgs-set-by-val #(= "m" (:track %)))]
                      [:text :maskir-defcon-not-3])))

(defaction baofpi [state role]
  (perform-sequence state
                    [:choose-mode (other-role role) :baofpi-remove-or-effect
                     (modes-with-prereqs
                      [(>= (get-var state "i:u:l") 2)
                       "remove" :baofpi-remove]
                      [true "effect" :baofpi-effect])]))

(defaction baofpi-remove [state]
  (perform-action state
                  [:change-influence "u" "l" -2 false]))

(defaction baofpi-effect [state]
  (perform-action state [:add-effect "u" "baofpi"]))

(defaction tbts [state role]
  (let [pairs (map (fn [t]
                     [t (get-var state ["d" "s" t])])
                   (keys defcon-tracks))
        max-value (apply max (map second pairs))
        allowed-tracks (set (map first
                                 (filter #(= max-value (second %))
                                         pairs)))]
    (perform-action state
                    [:choose-change-defcon role 2 {:mode :deflate-only
                                                   :allowed-tracks allowed-tracks}])))

(defaction inofcu [state role]
  (if (= (get-var state "d:u:m") MAX-DEFCON)
    (perform-action state
                    [:text :inofcu-max-defcon])
    (perform-sequence state
                      [:choose-change-defcon role 2 {:mode :escalate-only
                                                     :allowed-tracks #{"m"}
                                                     :remember-change "-inofcu-change"}]
                      :inofcu-cont)))

(defaction inofcu-cont [state]
  (perform-sequence state
                    [:choose-change-defcon "u" (get-var state "-inofcu-change")
                     {:mode :deflate-only
                      :allowed-tracks #{"p" "w"}
                      :exact-amount? true}]
                    [:unset-var "-inofcu-change"]))

;; === Hooks to external world

(def game-definition {:new-game new-game
                      :state-timing state-timing
                      :has-spaces? false})

(register-game-definition GAME-CODE game-definition)

(defn new-engine [& args]
  (apply game-engine/new-engine GAME-CODE args))

(defn new-engine-from-checkpoint [checkpoint mode]
  (game-engine/new-engine-from-checkpoint GAME-CODE checkpoint mode))

(defmethod all-playing-roles++ GAME-CODE [state]
  both-roles)
