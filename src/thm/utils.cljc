(ns thm.utils
  #?(:clj (:require
           [taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug warn)]
           [clojure.string :as str]
           [clojure.set]
           [clojure.pprint]
           )

     :cljs (:require [goog.string :as gstring]
                     [re-frame.core :refer [dispatch subscribe]]
                     [taoensso.timbre :refer-macros (debug errorf warnf warn)]
                     [clojure.string :as str]
                     [clojure.set]
                     [goog.userAgent :as user-agent]
                     ))

  #?(:clj (:import java.io.File))
  )

(defn not-empty? [coll]
  (seq coll))

(defn map-values [f m]
  "Returns a map with same keys as m, but values are results of calling f with
  a key and its value."
  (into {} (map (fn [[k v]] [k (f k v)]) m)))

(defn map-cross
  "Returns a sequence that contains the result of calling f on each possible
  pair of the values of the two collections."
  [f c1 c2]
  (mapcat (fn [v1]
            (map (fn [v2] (f v1 v2)) c2))
          c1))

(defn remove-first
  "Returns a lazy sequence of items in coll.  The first item for which (pred
  item) is false will not be in the result, and all others will be."
  [pred coll]
  (lazy-seq
   (when-let [s (seq coll)]
     (let [f (first s) r (rest s)]
       (if (pred f)
         r
         (cons f (remove-first pred r)))))))

(defn find-first [pred coll]
  (first (filter pred coll)))

(defn first-index [pred coll]
  (get (first (filter #(pred (second %))
                      (map-indexed (fn [i x] [i x]) coll)))
       0))

(defn seq-contains? [s v]
  (some #{v} s))

(defn one-dim-map
  "Returns a map which has as keys strings, which are concatenation of the
  prefix and each k of ks, and as values the initial-value."
  [prefix ks initial-value]
  (into {} (map (fn [k] [(str prefix k) initial-value]) ks)))

(defn two-dim-map
  "Returns a map which has values of the initial-value, and keys are formed by
  joining every possible pair of first-keys and second-keys with a colon,
  prefixed by prefix."
  [prefix first-keys second-keys initial-value]
  (into {} (map-cross (fn [k1 k2]
                        [(str prefix k1 ":" k2) initial-value])
                      first-keys second-keys)))

(defn denumber-id [cid]
  (if (nil? cid)
    (do (warnf "nil cid in denumbering: %s" cid)
        nil)
    (str/replace cid #":\d+$" "")))

(defn first-value
  "Return the first value of the associative collection.  (Might be useful when
  you know that there is only one k-v pair, or for sorted maps.)"
  [m]
  (second (first m)))

(defn k-map
  "Return a map whose keys come from the sequential coll, and whose values are
  the results of applying f to the key."
  [f coll]
  (into {} (map (fn [k]
                  [k (f k)]) coll)))

(defn str-join-non-nil
  "Like clojure.string/join, but nil values in coll are ignored."
  ([coll]
   (str-join-non-nil "" coll))
  ([separator coll]
   (str/join separator (remove nil? coll))))

(defn first-key [m]
  (key (first m)))

(defn game-comparator [a b]
  (let [f #?(:clj     bigdec
             :cljs    js/Number.parseInt
             :default identity)]
    (compare (f b) (f a))))

(defn username-comparator [a b]
  (compare (str/lower-case (or a ""))
           (str/lower-case (or b ""))))

;; From http://stackoverflow.com/questions/14488150/how-to-write-a-dissoc-in-command-for-clojure
(defn dissoc-in
  "Dissociates an entry from a nested associative structure returning a new
  nested structure. keys is a sequence of keys. Any empty maps that result
  will not be present in the new structure."
  [m [k & ks :as keys]]
  (if ks
    (if-let [nextmap (get m k)]
      (let [newmap (dissoc-in nextmap ks)]
        (if (seq newmap)
          (assoc m k newmap)
          (dissoc m k)))
      m)
    (dissoc m k)))

(defn sequence-for-humans
  "Return a sequence which has commas or the word \"and\" between elements, as
  appropriate to make it seem like the sequence is read by a human.

  For example, (f [:foo :bar :baz]) => [:foo \", \" :bar \", and\" :baz].

  If opt is equal to :always-comma, then a comma inserted before \"and\" even
  if there are only two elements.

  If opt is equal to :omit-and, then the sequence is joined by commas, without
  the \"and\".
  "
  [coll & [opt]]
  (case (count coll)
    (0 1) coll
    2 (concat (butlast coll) (case opt
                               :always-comma [", and " (last coll)]
                               :omit-and [", " (last coll)]
                               [" and " (last coll)]))
    (concat (interleave (butlast coll) (repeat ", "))
            [(when-not (= opt :omit-and) "and ") (last coll)])))

(def VALIDATION-CONSTS {:min-username-length 3
                        :max-username-length 20
                        :min-password-length 3 ;; XXX should be 8
                        :max-password-length 1024
                        :max-email-length 100
                        :max-comment-length 50})

(defn game-includes-player? [game player-name]
  (seq-contains? (:players game) player-name))

(defn get-and-dissoc [m k & ks]
  "For one key k, return a sequence of two values: a value at the map m's given
  key k, and the map without that key.  For n keys ks, return a sequence of n+1
  values: first are values corresponding to m's values for given keys, and last
  is the map without those keys."
  (concat
   [(get m k)]
   (map #(get m %) ks)
   [(apply dissoc m k ks)]))

(defn multipart-name-from-descr
  "Several things (zones, vars, spaces) have multipart names, which are allowed
  as strings with the colon separating the parts, or as vectors of parts,
  e.g. \"s:hand\" or [role \"hand\"].  This function normalizes the input to
  the string version."
  [multipart-descr]
  (if (string? multipart-descr)
    multipart-descr
    (str/join ":" multipart-descr)))

(defn split-multipart [multipart-descr]
  (if (vector? multipart-descr)
    multipart-descr
    (str/split multipart-descr #":")))

(defn last-part-of-multipart [multipart-descr]
  (last (split-multipart multipart-descr)))

(defn state-version-is-at-least? [state n]
  (>= (get state :version 0) n))

(defn has-zone? [state zone-descr]
  (contains? (:zones state) (multipart-name-from-descr zone-descr)))

(defn get-zone [state zone-descr]
  (get-in state [:zones (multipart-name-from-descr zone-descr)]))

(defn all-cards-in-zone [state zone-descr]
  (get-in state [:zones (multipart-name-from-descr zone-descr) :cards]))

(defn all-figures-in-space [state space-descr]
  (get-in state [:spaces (multipart-name-from-descr space-descr)]))

(defn get-zone-props [cs zone-descr]
  (get-zone cs zone-descr))

(defn n-cards-in-zone [cs zone-descr]
  (let [z (all-cards-in-zone cs zone-descr)]
    (cond
      (coll? z) (count z)
      (number? z) z ;; this helps for clients in which hidden zones are represented by a number
      (nil? z) 0
      :else (do (errorf "Cannot get size of zone: %s, %s" zone-descr z)
                0))))

(defn has-active-request? [state]
  (seq (:request-map state)))

(defn all-cards-in-hand [state role]
  (all-cards-in-zone state [role "hand"]))

(defn players [state]
  (keys (:player-name->role state)))

(defn player-name-by-role [state role]
  (get (clojure.set/map-invert (:player-name->role state)) role))

(defn prepare-request [roles kind text-descr & args]
  (letfn [(actualize [r arg]
            (if (fn? arg)
              (arg r)
              arg))]
    (into {} (map (fn [r]
                    [r (into [kind text-descr] (map #(actualize r %) args))])
                  roles))))

(defn shorten-title [title & [options]]
  (let [shorten (fn [parts len]
                  (apply str (map #(subs % 0 (min len (count %))) parts)))
        title (if (:preserve-case? options)
                title
                (.toLowerCase title))
        parts (-> title
                  (clojure.string/replace "-" " ")
                  (clojure.string/replace #"[()&!']" "")
                  (clojure.string/split #"\s+"))]
    (case (count parts)
      1 (shorten parts 6)
      2 (shorten parts 3)
      3 (shorten parts 2)
      (shorten parts 1))))

(defn short-game-name [game]
  (str (:code game) "-" (:gid game)))

(defn has-your-request? [cs]
  (and (not (:answer-sent? cs))
       (contains? (:request-map cs) (:your-role cs))))

(defn get-your-request [cs]
  (get (:request-map cs) (:your-role cs)))

(defn finished? [state]
  (some? (:outcome state)))

(defn get-request-for-role [state role]
  (get-in state [:request-map role]))

(defn get-var [cs var-descr & [default]]
  (let [var-name (multipart-name-from-descr var-descr)]
    (or (get-in cs [:vars var-name]) default)))

(defn get-all-figures-in-space [cs space-descr]
  (get-in cs [:spaces (multipart-name-from-descr space-descr)] []))

(defn index-of-matching [pred coll]
  (first (keep-indexed #(if (pred %2) %1) coll)))

(defn index-of [e coll]
  (index-of-matching #(= e %) coll))

(defn remove-from-vector-at-index [v i]
  (vec (concat
        (subvec v 0 i)
        (subvec v (inc i)))))

(defn add-to-vector-at-index [v i value]
  (if (= i (count v))
    (conj v value)
    (vec (concat
          (subvec v 0 i)
          [value]
          (subvec v i)))))

(defn get-role->player-name [state]
  (or (:role->player-name state)
      (clojure.set/map-invert (:player-name->role state))))

(defn get-ordered-roles [state]
  (or (:ordered-roles state)
      (vec (sort (vals (:player-name->role state))))))

(defn parenthetical-list [& contents]
  (let [c (remove nil? contents)]
    (if (empty? c)
      ""
      (str " (" (str/join ", " c) ")"))))

(defn sort-explode-map
  "Return a sequence of map keys, each appearing the same number of times as
  its value, sorted.  E.g.,

  (sort-explode-map {:k 2, :a 3}) => '(:a :a :a :k :k)."
  [m]
  (sort (mapcat #(repeat (second %) (first %)) m)))

(defn in-spread? [value lower higher]
  (<= lower value higher))

(defn parse-text-descr [text-descr]
  (if (keyword? text-descr)
    {:text-code text-descr}
    (let [[text-code & args] text-descr]
      {:text-code text-code, :text-args args})))

(defn shorten-title-for-humans [title]
  (shorten-title title {:preserve-case? true}))

(defn vector-without-element-at [v index]
  (vec (concat (subvec v 0 index)
               (subvec v (inc index)))))

(defn map-indexed-human
  "Like map-indexed, but counts elements from 1."
  [f coll]
  (map-indexed (fn [i e]
                 (f (inc i) e)) coll))

(defn enumerate-things [type things]
  (map-indexed-human (fn [i thing]
                       (assoc thing
                              :tid (str (name type) i)
                              :type type))
                     things))

(defn backwards-apply [receiver f]
  (f receiver))

(defn sum [coll]
  (reduce + coll))

(defn get-game-code-from-page-arg [page-arg]
  (first (clojure.string/split page-arg #"#")))

(defn range-inclusive [lower higher]
  (range lower (inc higher)))

(defn fit-to-range [value a b]
  "Return value, but if it's outside the inclusive range defined by a..b,
  return the range's end."
  (max a (min b value)))

(defn shift-so-it-starts-with [a-vector el]
  (let [index (first-index #(= % el) a-vector)]
    (if (some? index)
      (vec (concat (subvec a-vector index)
                   (subvec a-vector 0 index)))
      a-vector)))

(defn good-answer-for-choose-spaces-from-bags? [bags answer]
  (and (sequential? answer)
       (= (count answer) (count bags))
       (every? (fn [index]
                 (let [bag (get bags index)
                       bag-answer (get answer index)]
                   (and (= (count bag-answer)
                           (:amount bag))
                        (every? #(contains? (set (:spaces bag))
                                            %)
                                bag-answer))))
               (range (count bags)))))

(defn unrecognized [warning value]
  (do (warn "Unrecognized " warning ": " value)
      (str "?" value "?")))

(do
  #?@(:clj
      [
       (defn shuffle-with-random
         "Return a random permutation of coll using the supplied
  java.util.Random instance."
         [coll random]
         (let [al (java.util.ArrayList. coll)]
           (java.util.Collections/shuffle al random)
           (clojure.lang.RT/vector (.toArray al))))

       (defn pick-with-random
         "Return a random value from sequential coll, using the supplied
  java.util.Random instance."
         [coll random]
         (let [index (.nextInt random (count coll))]
           (nth coll index)))

       (defn half-round-up [v]
         (Math/round (/ v 2.0)))

       ;; XXX works only on Unixes.
       (defn my-pid []
         (-> (File. "/proc/self") .getCanonicalFile .getName))

       (defn now-timestamp [] (.getTime (java.util.Date.)))

       (defn uuid-str []
         (str (java.util.UUID/randomUUID)))

       (defn get-form-value [form-data k]
         (str/trim (or (get form-data k) "")))

       (defn to-integer [s]
         (if (string? s)
           (Integer/parseInt s)
           s))

       (defn prepare-zones [zones-data roles]
         (let [z (atom (sorted-map))]
           (doseq [props zones-data
                   :let [n (:name props)
                         new-props (dissoc props :name :per-player?)]]
             (if (:per-player? props)
               (doseq [r roles]
                 (swap! z assoc (str r ":" n) (assoc new-props :role r, :cards [])))
               (do
                 (swap! z assoc n (assoc new-props :cards [])))))
           @z))

       (defn make-rnd [seed]
         (if seed
           (java.util.Random. seed)
           (java.util.Random.)))

       (defn make-rnd-from-options [opts]
         (if-let [seed (:seed opts)]
           (java.util.Random. seed)
           (java.util.Random.)))

       (defn only-role&answer [state]
         (first (:answers state)))

       (defn only-answer [state]
         (first-value (:answers state)))

       (defn bull []
         "●")

       (defn pprint-str [obj]
         (with-out-str (clojure.pprint/pprint obj)))

       (defn pairs->map
         "Given a sequence of pairs [k v], return a map in which under the `k`s
  reside the vectors of `v`s (transformed by `transform-f`) in the order of
  appearance in `pairs`."
         [pairs transform-f]
         (reduce (fn [result [zone card]]
                   (if (contains? result zone)
                     (update result zone conj (transform-f card))
                     (assoc result zone [(transform-f card)])))
                 {}
                 pairs))

       (defn prepare-player-name->role-for-2p [opts player-names side-selection-values role-values rnd]
         (let [index (.indexOf side-selection-values (:side-selection opts))]
           (if (= index -1)
             (zipmap player-names (shuffle-with-random role-values rnd))
             {(first player-names) (get role-values index)
              (second player-names) (get role-values (- 1 index))})))

       ]))

(defn as-set
  "When the argument is a collection, return set of that collection, otherwise
  return a set of a single element which is the arg."
  [coll-or-el]
  (if (coll? coll-or-el)
    (set coll-or-el)
    #{coll-or-el}))

(do
  #?@(:cljs
      [
       (defn html-entity [name]
         (gstring/unescapeEntities (str "&" name ";")))

       (defn bull []
         (str " " (html-entity "bull") " "))

       (defn format-timestamp
         "Return a string representation of timestamp of the form YYYY-MM-DD
          HH:MM:SS."
         [timestamp]
         (let [d (js/Date. 0)
               f (fn [s]
                   (if (= (count (str s)) 2)
                     (str s)
                     (str "0" s)))]
           (.setUTCMilliseconds d timestamp)
           (str (when (not= (.toDateString d) (.toDateString (js/Date.)))
                  (str (.getFullYear d) "-" (f (inc (.getMonth d))) "-" (f (.getDate d))
                       " "))
                (f (.getHours d)) ":" (f (.getMinutes d)))))

       (defn stop-event [event]
         (.stopPropagation event)
         (.preventDefault event))

       (defn dispatch-e
         "Dispatch the 'what', and prevent default action on the 'event'."
         [what event]
         (stop-event event)
         (dispatch what))

       (defn half-round-up [v]
         (js/Math.round (/ v 2.0)))

       (defn get-user-agent-string []
         (user-agent/getUserAgentString))

       (defn on-mobile-browser? []
         (or (user-agent/isMobile_)
             (re-matches #".*Mobile.*" (get-user-agent-string))))

       (defn pluralize [n word]
         (if (and (= (rem n 10) 1)
                  (not= (rem n 100) 11))
           (str n " " word)
           (str n " " word "s")))

       (defn set-answer [primary-role answer]
         (debug "set-answer" primary-role answer)
         (dispatch [:thm.game-ui/set-answer primary-role answer]))

       (defn send-answer [primary-role answer]
         (set-answer primary-role answer)
         (dispatch [:thm.game-ui/send-answer primary-role]))

       (defn send-cancel [primary-role]
         (send-answer primary-role "*cancel*"))

       (defn forbid-widget-activation? [cs]
         (or (:answer-sent? cs)
             (:outcome cs)))

       (defn svg-arrow [from to stroke opts]
         (let [big (or (:arrow-big-size opts) 30)
               small (/ big 3)

               x1 (:x from), y1 (:y from)
               x2 (:x to),   y2 (:y to)

               orientation (or (:orientation opts) :auto)
               orientation (if (= orientation :auto)
                             (cond (> x2 x1) :right
                                   (< y2 y1) :up
                                   (< x2 x1) :left
                                   :else     :down)
                             orientation)

               [dx1 dy1 dx2 dy2] (case orientation
                                   :right [(- big)   (+ small) (- big)   (- small)]
                                   :up    [(+ small) (+ big)   (- small) (+ big)]
                                   :left  [(+ big)   (- small) (+ big)   (+ small)]
                                   :down  [(+ small) (- big)   (- small) (- big)]
                                   )]
           [:path {:d (str "M"     x1  " "    y1
                           "L"     x2  " "    y2
                           "l"    dx1  " "    dy1
                           "l" (- dx1) " " (- dy1)
                           "l"    dx2  " "    dy2
                           "l" (- dx2) " " (- dy2))
                   :stroke-linecap "round"
                   :style {:stroke-width (or (:stroke-width opts) 8)
                           :stroke stroke}}]))

       (defn change-for-humans [v]
         (if (pos? v)
           (str "+" v)
           (str v)))


       (defn size-style [size]
         {:width (str (:w size) "px")
          :height (str (:h size) "px")})

       (defn choose-mode-like-request-kind? [kind]
         (contains? #{:choose-mode :yn} kind))

       (defn show-choose-mode-modal [req]
         (dispatch [:thm.game-ui/show-choose-mode-modal req]))

       (defn to-integer [s]
         (if (string? s)
           (js/Number.parseInt s)
           s))

       (defn value-from-event [e]
         (-> e
             (aget "nativeEvent")
             (aget "target")
             (aget "value")))

       (defn bind [data key]
         (fn [e]
           (swap! data assoc key (value-from-event e))))

       (defn now-timestamp [] (js/Date.now))

       (defn stub [& args]
         (str/join (bull) args))

       ]))
