-- Deploy sqitch/sqitch.plan:personal_notes to pg

BEGIN;

create table personal_notes (
   username text references users (username) on delete cascade,
   gid integer unique references games(gid) on delete cascade,
   content varchar(2048)
);

alter table personal_notes add constraint personal_notes_username_gid unique (username, gid);

COMMIT;
