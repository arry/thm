(ns thm.config
  (:require
   [taoensso.carmine.ring :refer (carmine-store)]
   [cprop.core :refer (load-config)]))

(def cookie-name "ring-session")

(def cookie-key-prefix "session")

(defn make-config []
  (let [m (if (= (System/getenv "THM_MODE") "prod")
            (load-config :file "config.prod.edn")
            (load-config :file "config/config.edn"))]
    (merge m
           {:cookies {:path "/"}
            :store (carmine-store (:redis m)
                                  {:key-prefix cookie-key-prefix
                                   :expiration-secs (:secs m)})})))

(def config (make-config))

(defn reload-config! []
  (alter-var-root #'config (constantly (make-config))))
