(ns thm.backend-components.backend-listener
  (:require
   [com.stuartsierra.component :as component]
   [taoensso.timbre :as timbre :refer (tracef debugf infof warnf errorf debug)]
   [system.repl :refer [system]]
   [thm.backend-components.carmine :refer [subscribe unsubscribe]]
   [thm.utils :refer (my-pid)]))

;; A component that subscribes to worker's messages and sends them to Sente
;; clients.

(defn subscriber [c [tag chan rest]]
  (when (= tag "message")
    (let [[uid msg] rest
          send-fn (-> system :sente :chsk-send!)]
      ;; (debug "Backend listener sends: " uid msg)
      (send-fn uid msg))))

(defrecord BackendListener [sub-prefix]
  component/Lifecycle
  (start [component]
    (let [pid (my-pid)
          listener (subscribe (:carmine system) (str sub-prefix ":worker->" pid)
                              #(subscriber component %))]
      (assoc component :pid pid :listener listener)))
  (stop [component]
    (when (:listener component)
      (unsubscribe (:listener component)))
    (dissoc component :pid :listener)))

(defn new-backend-listener [sub-prefix]
  (map->BackendListener {:sub-prefix sub-prefix}))
