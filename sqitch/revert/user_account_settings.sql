-- Revert sqitch/sqitch.plan:user_account_settings from pg

BEGIN;

alter table users drop column account_settings;

COMMIT;
