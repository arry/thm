-- Verify sqitch/sqitch.plan:personal_notes on pg

BEGIN;

select (username, gid, content)
from personal_notes
where false;

ROLLBACK;
